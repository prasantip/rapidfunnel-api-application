-- MySQL dump 10.13  Distrib 5.6.19, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: rapidfunnel
-- ------------------------------------------------------
-- Server version 5.6.19-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `GetGroupAncestry`(GivenID INT) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv VARCHAR(1024);
    DECLARE cm CHAR(1);
    DECLARE ch INT;

    SET rv = '';
    SET cm = '';
    SET ch = GivenID;
    
    WHILE ch > 0 DO
        SELECT IFNULL(parentGroupId,-1) INTO ch FROM
        (SELECT parentGroupId FROM accountGroup WHERE id = ch) A;
        
        IF ch > 0 THEN
            SET rv = CONCAT(rv,cm,ch);
            SET cm = ',';
        END IF;
        
    END WHILE;
    
    RETURN rv;
    
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `GetGroupFamily`(`GivenID` INT) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv,q,queue,queue_children VARCHAR(1024);
    DECLARE queue_length,front_id,pos INT;

    SET rv = '';
    SET queue = GivenID;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        SET front_id = FORMAT(queue,0);
        
        IF queue_length = 1 THEN
            SET queue = '';
        ELSE
            SET pos = LOCATE(',',queue) + 1;
            SET q = SUBSTR(queue,pos);
            SET queue = q;
        END IF;
        
        SET queue_length = queue_length - 1;

        SELECT IFNULL(qc,'') INTO queue_children
        FROM (SELECT GROUP_CONCAT(id) qc
        FROM accountGroup WHERE parentGroupId = front_id) A;

        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_children;
            ELSE
                SET rv = CONCAT(rv,',',queue_children);
            END IF;
            
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;

    RETURN rv;

END$$

DELIMITER ;


--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `suite` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateId` INT( 11 ) UNSIGNED NOT NULL,
  `countryId` INT( 11 ) UNSIGNED NOT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL COMMENT 'foreign key of id of account status',
  `accountLevel` tinyint(3) unsigned DEFAULT NULL COMMENT 'foreign key of accountLabel',
  `ignoreAccountPayment` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1-Yes, 0-No',
  `passThrough` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - no pass through, 1- pass through',
  `trial` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0-off, 1-on',
  `trialPeriod` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'in days',
  `testAccount` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 - for test accounts',
  `trialUserEnabled` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1-Yes, 0-No',
  `userTrialPeriod` int(4) NOT NULL DEFAULT '0' COMMENT 'in days',
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned NOT NULL,
  `dateModified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  `wistiaProjectId` varchar(100) DEFAULT NULL COMMENT 'publicId of project in Wistia',
  `userToken` text DEFAULT NULL COMMENT 'UserToken to add contacts using posting controller',
  `basePrice` DECIMAL( 9, 2 ) NULL DEFAULT  '0' COMMENT  'Account Base Price',
  `basePricePlanId` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT  'Account Base Price plan id',
  `subscriptionPlanId` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT  'user subscription plan id at stripe',
  `payThroughAuthNet` TINYINT NOT NULL DEFAULT  '0' COMMENT  'Payment done through autherize.net',
  `enableMyWeeklyStats` BOOLEAN DEFAULT TRUE COMMENT  '1-Yes, 0-No',
  PRIMARY KEY (`id`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`),
  KEY `accountLevel` (`accountLevel`),
  KEY `status` (`status`),
  KEY `stateId` (`stateId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountAwardType`
--

DROP TABLE IF EXISTS `accountAwardType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountAwardType` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountBranding`
--

DROP TABLE IF EXISTS `accountBranding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountBranding` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL COMMENT 'foreign key from account table',
  `dashboardLogo` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `mobileLogo` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `primaryColor` VARCHAR( 20 ) NULL DEFAULT '#77B800',
  `primaryColorOffset` VARCHAR( 20 ) NULL DEFAULT '#FFFFFF',
  `secondaryColor` VARCHAR( 20 ) NULL DEFAULT '#220090',
  `secondaryColorOffset` VARCHAR( 20 ) NULL DEFAULT '#FFFFFF',
  `tertiaryColor` VARCHAR( 20 ) NULL DEFAULT '#E75300',
  `tertiaryColorOffset` VARCHAR( 20 ) NULL DEFAULT '#FFFFFF',
  `isShowColorToMobile` BOOLEAN NOT NULL DEFAULT FALSE,
  `domainName` varchar(200) DEFAULT NULL,
  `supportLink` VARCHAR( 200 ) NULL DEFAULT NULL,
  `reBrand` varchar(200) DEFAULT NULL,
  `reBrandAdditionalNotificationEmail` varchar(100) DEFAULT NULL,
  `reBrandRepIdToolTip` text,
  `reBrandAdditionalNotificationEmailToolTip` text,
  `reBrandRepId2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `reBrandRepId2ToolTip` text CHARACTER SET utf8,
  `upgradeInformationForFreeUsers` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountId` (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountBroadcastEmails`
--

DROP TABLE IF EXISTS `accountBroadcastEmails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountBroadcastEmails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `sentTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'It is also the creation time',
  `sentBy` int(11) NOT NULL COMMENT 'It is also created by',
  `fromEmail` varchar(255) NOT NULL COMMENT 'Email from where this email sent',
  `subject` varchar(255) NOT NULL,
  `htmlContent` text NOT NULL,
  `textContent` text NOT NULL,
  `isDraft` tinyint(4) DEFAULT '0',
  `recipientType` enum('1','2','3') NOT NULL COMMENT '1-for Users, 2-for Contacts, 3-for Incentive',
  `recipients` text NOT NULL,
  `recipientCount` INT( 11 ) NULL,
  `incentiveId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountCampaign`
--

DROP TABLE IF EXISTS `accountCampaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCampaign` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `showInApp` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1-Show, 0-Hide',
  `sendFromEmail` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountCampaignContact`
--

DROP TABLE IF EXISTS `accountCampaignContact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCampaignContact` (
  `accountCampaignId` int(11) unsigned NOT NULL,
  `accountContactId` int(11) unsigned NOT NULL,
  `channel` enum('web','mobile') NOT NULL DEFAULT 'web',
  `createdBy` int(11) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`accountCampaignId`,`accountContactId`),
  KEY `accountContactId` (`accountContactId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountCampaignContactEmail`
--

DROP TABLE IF EXISTS `accountCampaignContactEmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCampaignContactEmail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL COMMENT 'Foreign key for accountUser',
  `campaignId` int(11) unsigned NOT NULL COMMENT 'Foreign key for accountCampaign',
  `contactId` int(11) unsigned NOT NULL COMMENT 'Foreign key for accountContact',
  `emailId` int(11) unsigned NOT NULL COMMENT 'Foreign key for accountCampaignEmail',
  `sendOnDate` date NOT NULL,
  `sendStatus` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0-not send, 1-sent',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=696 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountCampaignEmail`
--

DROP TABLE IF EXISTS `accountCampaignEmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCampaignEmail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `campaignId` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `textContent` text NOT NULL,
  `type` TINYINT( 1 ) NOT NULL DEFAULT '0' COMMENT '0=campaign email, 1= campaign optIn',
  `days` smallint(6) NOT NULL,
  `status` enum('published', 'draft', 'pending review') NOT NULL DEFAULT 'draft',
  `isDeleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1-deleted',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountCampaignGroup`
--

DROP TABLE IF EXISTS `accountCampaignGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCampaignGroup` (
  `accountCampaignId` int(11) unsigned NOT NULL,
  `accountGroupId` int(11) unsigned NOT NULL,
  UNIQUE KEY `campaignGroup` (`accountCampaignId`,`accountGroupId`),
  KEY `accountGroupId` (`accountGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `accountCardProfile`
--

DROP TABLE IF EXISTS `accountCardProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCardProfile` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `firstName` varchar(255) NOT NULL COMMENT 'name on card',
  `middleName` varchar(255) DEFAULT NULL COMMENT 'name on card',
  `lastName` varchar(255) DEFAULT NULL COMMENT 'name on card',
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL COMMENT 'billing address',
  `suite` varchar(255) NOT NULL COMMENT 'billing address',
  `city` varchar(255) NOT NULL COMMENT 'billing address',
  `stateId` INT( 11 ) NULL COMMENT  'billing address',
  `countryId` INT( 11 ) NULL DEFAULT  '0',
  `zip` varchar(20) NOT NULL COMMENT 'billing address',
  `maskedCcNo` varchar(30) NOT NULL,
  `ccType` varchar(100) NOT NULL,
  `ccExpMonth` tinyint(2) unsigned NOT NULL,
  `ccExpYear` smallint(4) NOT NULL,
  `maskedCvv` varchar(5) NOT NULL,
  `authorizeProfileToken` varchar(20) NOT NULL,
  `authorizePaymentProfileToken` varchar(20) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  `firstCycleStartDate` date DEFAULT NULL,
  `lastPaymentDoneOn` date DEFAULT NULL,
  `nextPaymentOn` date DEFAULT NULL,
  `failureCount` tinyint(2) unsigned DEFAULT '0',
  `lastAmountPaid` FLOAT NULL DEFAULT NULL ,
  `lastTransactionId` INT( 11 ) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountId` (`accountId`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountCollateral`
--

DROP TABLE IF EXISTS `accountCollateral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCollateral` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL COMMENT 'Foreign key to account user tabel',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `path` text CHARACTER SET utf8 NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  `isDeleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 => Deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountContact`
--

DROP TABLE IF EXISTS `accountContact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountContact` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `accountId` int(11) unsigned NOT NULL COMMENT 'Foreign key from account table',
 `firstName` varchar(100) CHARACTER SET utf8 NOT NULL,
 `lastName` varchar(100) CHARACTER SET utf8 NOT NULL,
 `email` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
 `homeEmail` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
 `workEmail` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
 `otherEmail` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
 `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
 `mobile` text CHARACTER SET utf8,
 `home` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
 `work` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
 `other` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
 `address1` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
 `address2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
 `city` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
 `stateId` INT( 11 ) UNSIGNED NULL DEFAULT NULL,
 `countryId` INT( 11 ) UNSIGNED NULL DEFAULT NULL,
 `zip` varchar(20) DEFAULT NULL,
 `company` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
 `title` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
 `interest` tinyint(1) DEFAULT NULL COMMENT '1 => low, 2=> medium, 3=> high',
 `note` text CHARACTER SET utf8,
 `noteCount` MEDIUMINT( 4 ) NOT NULL DEFAULT  '0' COMMENT  'total number of notes for the contact',
 `contactNotes` text,
 `noteType` tinyint(1) NOT NULL DEFAULT '0',
 `channel` enum('Mobile','Web Application','Landing Page') DEFAULT 'Web Application',
 `optInSend` tinyint(1) NOT NULL DEFAULT '0',
 `optIn` tinyint(1) NOT NULL DEFAULT '0',
 `optInDate` timestamp NULL DEFAULT NULL,
 `optInResend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = Not Sent, 1 = Sent',
 `optOut` tinyint(1) DEFAULT '0',
 `optOutDate` timestamp NULL DEFAULT NULL,
 `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=New(NotSent), 1=New , 2=Active, 3=Bounced, 4=Unsubscribed, 6=Pre Release, 7=Released',
 `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `createdBy` int(11) unsigned NOT NULL,
 `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
 `modifiedBy` int(11) unsigned NOT NULL,
 `customContact` tinyint(1) DEFAULT '0',
 `noteTimeStamps` text,
 `optInSendDate` TIMESTAMP NULL,
 `campaignAssignTime` TIMESTAMP NULL DEFAULT NULL,
 `campaignId` INT( 11 ) NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `accountId` (`accountId`),
 KEY `channel` (`channel`),
 KEY `advanceSearch` (`firstName`,`lastName`,`email`,`stateId`,`createdBy`,`phone`,`optInDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountCustomVideoResource`
--

DROP TABLE IF EXISTS `accountCustomVideoResource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountCustomVideoResource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `accountResourceId` int(11) unsigned NOT NULL,
  `leadGenerationForm` tinyint(1) NOT NULL DEFAULT '1',
  `embeddedVideo` text,
  `embedOptions` tinyint(1) DEFAULT '0',
  `actionButtonLabel1` text,
  `actionButtonLink1` text,
  `actionButtonLabel2` text,
  `actionButtonLink2` text,
  `actionButtonLabel3` text,
  `actionButtonLink3` text,
  `textHead` text,
  `textTop` text,
  `textBottom` text,
  `mediaName` varchar(200) DEFAULT NULL,
  `mediaDuration` varchar(100) DEFAULT NULL,
  `mediaHash` varchar(200) DEFAULT NULL,
  `autoPlay` tinyint(1) DEFAULT '0',
  `playerControl` tinyint(1) DEFAULT '0',
  `playlistLoop` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `accountId` (`accountId`),
  KEY `accountResourceId` (`accountResourceId`,`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountEmailResource`
--

DROP TABLE IF EXISTS `accountEmailResource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountEmailResource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaignEmailId` int(11) NOT NULL DEFAULT '0',
  `campaignId` int(11) DEFAULT NULL,
  `systemEmailId` int(11) NOT NULL DEFAULT '0',
  `resourceId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignEmailId` (`campaignEmailId`),
  KEY `systemEmailId` (`systemEmailId`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountFreeUserCampaigns`
--

DROP TABLE IF EXISTS `accountFreeUserCampaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountFreeUserCampaigns` (
  `accountId` int(11) NOT NULL,
  `accountCampaignId` int(11) NOT NULL,
  UNIQUE KEY `accountCampaignId` (`accountCampaignId`,`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountFreeUserResources`
--

DROP TABLE IF EXISTS `accountFreeUserResources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountFreeUserResources` (
  `accountId` int(11) NOT NULL,
  `accountResourceId` int(11) NOT NULL,
  UNIQUE KEY `accountResourceId` (`accountResourceId`,`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountGroup`
--

DROP TABLE IF EXISTS `accountGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountGroup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `groupCode` varchar(10) DEFAULT NULL,
  `accountId` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(128) NOT NULL,
  `parentGroupId` int(11) unsigned DEFAULT NULL,
  `rank` int(11) unsigned DEFAULT NULL COMMENT 'setup according to lowest rank last, hence 1 is last',
  PRIMARY KEY (`id`),
  KEY `accountId` (`accountId`),
  KEY `parentGroupId` (`parentGroupId`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1 COMMENT='group of access to account user';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountGroupAlias`
--

DROP TABLE IF EXISTS `accountGroupAlias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountGroupAlias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountGroupId` int(11) unsigned NOT NULL,
  `aliasGroupCode` varchar(16) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accountGroupId` (`accountGroupId`),
  KEY `aliasGroupCode` (`aliasGroupCode`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountIncentiveProgram`
--

DROP TABLE IF EXISTS `accountIncentiveProgram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountIncentiveProgram` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `accountAwardTypeId` tinyint(3) unsigned NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date DEFAULT NULL,
  `leadsGenerated` int(11) unsigned DEFAULT NULL COMMENT 'Number of users opts in during this period',
  `award` varchar(255) DEFAULT NULL,
  `goal` int(11) unsigned DEFAULT NULL,
  `topPercentage` tinyint(4) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `emailContent` text,
  `from` varchar(100) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `accountAwardTypeId` (`accountAwardTypeId`),
  KEY `accountId` (`accountId`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountLandingPage`
--

DROP TABLE IF EXISTS `accountLandingPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountLandingPage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `accountUserId` int(11) unsigned NOT NULL,
  `leadGenerationForm` tinyint(1) NOT NULL DEFAULT '1',
  `personalPhoto` varchar(100) DEFAULT NULL,
  `embeddedVideo` text,
  `actionButtonLabel1` text,
  `actionButtonLink1` text,
  `actionButtonLabel2` text,
  `actionButtonLink2` text,
  `actionButtonLabel3` text,
  `actionButtonLink3` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountUserId` (`accountUserId`,`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountLevel`
--

DROP TABLE IF EXISTS `accountLevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountLevel` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `pricePerMonth` float NOT NULL DEFAULT '0',
  `pricePerUser` float NOT NULL DEFAULT '0',
  `callForPricing` tinyint(1) NOT NULL DEFAULT '0',
  `maxUsers` int(11) NOT NULL DEFAULT '0',
  `maxContacts` int(11) NOT NULL DEFAULT '0',
  `maxCampaigns` int(11) NOT NULL DEFAULT '0',
  `maxCampaignEmails` int(11) NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned NOT NULL,
  `dateModified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountMailLog`
--

DROP TABLE IF EXISTS `accountMailLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountMailLog` (
  `messageId` varchar(250) NOT NULL,
  `accountId` int(11) NOT NULL,
  `recepientId` int(11) NOT NULL,
  `recipient` varchar(100) NOT NULL,
  `recepientType` tinyint(4) DEFAULT NULL COMMENT '1 = User , 2 = Contact',
  `Subject` varchar(300) NOT NULL,
  `sendDate` datetime NOT NULL,
  `month` TINYINT( 2 ) NULL DEFAULT '0',
  `status` enum('1','2','3','4','5') DEFAULT NULL COMMENT '1=Sent, 2=Opened, 3=Clicked, 4=Bounced, 5=Unsubscribed',
  `eventDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Status Date',
  `mailType` varchar(50) DEFAULT NULL,
  `mailId` int(11) DEFAULT NULL,
  `broadcastType` tinyint(4) DEFAULT NULL COMMENT '1 = Users, 2 = Contacts, 3 = Incentive',
  PRIMARY KEY (`messageId`,`accountId`,`recepientId`),
  KEY `Subject` (`Subject`),
  KEY `recipient` (`recipient`),
  KEY `recepientId` (`recepientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountPrivilege`
--

DROP TABLE IF EXISTS `accountPrivilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountPrivilege` (
  `accountGroupId` int(11) unsigned NOT NULL DEFAULT '0',
  `accountResourceId` int(11) unsigned NOT NULL DEFAULT '0',
  `access` text,
  `assert` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`accountGroupId`,`accountResourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountRefund`
--

DROP TABLE IF EXISTS `accountRefund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountRefund` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `refundTransactionId` varchar(20) DEFAULT NULL,
  `origTransactionId` varchar(20) NOT NULL,
  `authorizeProfileToken` varchar(20) DEFAULT NULL,
  `authorizePaymentProfileToken` varchar(20) DEFAULT NULL,
  `refundAmount` float NOT NULL,
  `method` varchar(100) DEFAULT NULL,
  `approved` tinytext,
  `declined` tinyint(1) DEFAULT NULL,
  `error` tinyint(1) DEFAULT NULL,
  `held` tinyint(1) DEFAULT NULL,
  `transactionType` varchar(100) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountResource`
--

DROP TABLE IF EXISTS `accountResource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountResource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(100) DEFAULT NULL,
  `parent` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountResourceCategory`
--

DROP TABLE IF EXISTS `accountResourceCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountResourceCategory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountResourceGroup`
--

DROP TABLE IF EXISTS `accountResourceGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountResourceGroup` (
  `accountResourcesId` int(11) unsigned NOT NULL,
  `accountGroupId` int(11) unsigned NOT NULL,
  UNIQUE KEY `accountResourcesId` (`accountResourcesId`,`accountGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountResourceType`
--

DROP TABLE IF EXISTS `accountResourceType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountResourceType` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `typeImage` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountResources`
--

DROP TABLE IF EXISTS `accountResources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountResources` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `accountResourceTypeId` tinyint(3) unsigned NOT NULL,
  `accountResourceCategoryId` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `content` text,
  `link` varchar(1000) DEFAULT NULL,
  `fileSavedName` varchar(255) DEFAULT NULL,
  `fileOriginalName` varchar(255) DEFAULT NULL,
  `showInApp` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '1-Show, 0-Hide',
  `status` ENUM(  '0',  '1' ) NOT NULL DEFAULT  '1' COMMENT  'To save active status',
  `isPublished` ENUM(  '0',  '1' ) NOT NULL DEFAULT  '0' COMMENT  'used for publish status page of custom pages',
  `created` datetime NOT NULL,
  `createdBy` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accountResourceTypeId` (`accountResourceTypeId`),
  KEY `accountId` (`accountId`),
  KEY `accountResourceCategoryId` (`accountResourceCategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountRoles`
--

DROP TABLE IF EXISTS `accountRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountRoles` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountRolesCustom`
--

DROP TABLE IF EXISTS `accountRolesCustom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountRolesCustom` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parentId` int(11) unsigned NOT NULL DEFAULT '0',
  `accountId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountRolesResources`
--

DROP TABLE IF EXISTS `accountRolesResources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountRolesResources` (
  `accountRoleId` tinyint(3) unsigned NOT NULL,
  `accountResourceId` int(11) unsigned NOT NULL,
  `customRoleId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`accountRoleId`,`accountResourceId`),
  KEY `accountResourceId` (`accountResourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountStatus`
--

DROP TABLE IF EXISTS `accountStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountStatus` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountSystemEmailType`
--

DROP TABLE IF EXISTS `accountSystemEmailType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountSystemEmailType` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountSystemEmailUserEmail`
--

DROP TABLE IF EXISTS `accountSystemEmailUserEmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountSystemEmailUserEmail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `systemEmailId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `sendOnDate` date NOT NULL,
  `sendStatus` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=not sent, 1=sent',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sendOnDateStatus` (`sendOnDate`,`sendStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountSystemEmails`
--

DROP TABLE IF EXISTS `accountSystemEmails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountSystemEmails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `type` tinyint(3) NOT NULL COMMENT '1=OptIn, 2=Welcome,3=Link Tracking Notification,4=Additional notification email,5=New Use Email',
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `htmlBody` text,
  `textBody` text,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0 = Inactive, 1 = Active, 2 = Default',
  `days` smallint(6) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountTransaction`
--

DROP TABLE IF EXISTS `accountTransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountTransaction` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `userId` int(11) unsigned DEFAULT NULL,
  `invoiceNumber` varchar(30) NOT NULL,
  `amount` float DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `authorizeProfileToken` varchar(20) NOT NULL,
  `authorizePaymentProfileToken` varchar(20) NOT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `declined` tinyint(1) DEFAULT NULL,
  `error` tinyint(1) DEFAULT NULL,
  `refund` tinyint(1) DEFAULT '0' COMMENT '0 = no, 1 = yes',
  `held` tinyint(1) DEFAULT NULL,
  `refundApproved` tinyint(1) DEFAULT '0',
  `refundDeclined` tinyint(1) DEFAULT '0',
  `refundError` tinyint(1) DEFAULT '0',
  `refundHeld` tinyint(1) DEFAULT '0',
  `transactionId` varchar(20) DEFAULT NULL,
  `transactionType` varchar(100) DEFAULT NULL,
  `refundTransactionId` varchar(20) DEFAULT NULL,
  `refundTransactionType` varchar(100) DEFAULT NULL,
  `customerId` varchar(20) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` tinyint(4) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `accountNumber` varchar(30) DEFAULT NULL,
  `cardType` varchar(100) DEFAULT NULL,
  `paymentDoneBy` TINYINT( 1 ) NOT NULL COMMENT '1-cron, 2-user',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `refundDate` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=385 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountUser`
--

DROP TABLE IF EXISTS `accountUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountUser` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `new` tinyint(1) unsigned DEFAULT '1',
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `phoneNumber` varchar(20) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(10) unsigned DEFAULT NULL COMMENT 'user id if anyone is adding him',
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(10) unsigned DEFAULT NULL COMMENT 'user id if anyone is modifying him',
  `password` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
  `accessToken` text DEFAULT NULL,
  `accessTokenExpiresOn` DATETIME NULL DEFAULT NULL,
  `salt` text NOT NULL,
  `forceChange` tinyint(4) NOT NULL DEFAULT '1',
  `passwordResetSendTime` timestamp NULL DEFAULT NULL,
  `profileImage` varchar(45) DEFAULT NULL COMMENT 'image name with extention',
  `address` varchar(255) DEFAULT NULL,
  `suite` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateId` INT( 11 ) UNSIGNED NULL,
  `countryId` INT( 11 ) UNSIGNED NULL,
  `zip` varchar(20) DEFAULT NULL,
  `roleId` tinyint(11) unsigned NOT NULL,
  `customRoleId` int(11) unsigned NOT NULL DEFAULT '0',
  `status` enum('0','1','3') NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive, 3-suspend',
  `autoSuspendStatus` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  '0-normal, 1-warned, 2-disabled',
  `isTrial` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1- in Trial, 0-not in trial',
  `trialExpiresOn` datetime DEFAULT NULL,
  `isCancelPremiumService` tinyint(1) DEFAULT '0',
  `isDelete` tinyint(1) NOT NULL DEFAULT '0',
  `isBillingContact` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if 1 billing info send to this user.',
  `registrationCode` varchar(255) DEFAULT NULL COMMENT 'This will be used for new user reigstration ',
  `invitationDate` date DEFAULT NULL,
  `lastLogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `linkTrackingNotify` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1-Yes, 0-No',
  `additionalEmailNotification` varchar(255) DEFAULT NULL,
  `repId` varchar(50) DEFAULT NULL,
  `repId2` varchar(200) DEFAULT NULL,
  `facebookUrl` VARCHAR( 255 ) NULL,
  `twitterUrl` VARCHAR( 255 ) NULL,
  `instagramUrl` VARCHAR( 255 ) NULL,
  `ignoreUserPayment` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1-Yes, 0-No',
  `contactRecordsPerPage` int(11) DEFAULT NULL,
  `resourceRecordsPerPage` int(11) DEFAULT NULL,
  `userRecordsPerPage` int(11) DEFAULT NULL,
  `groupRecordsPerPage` int(11) DEFAULT NULL,
  `campaignRecordsPerPage` int(11) DEFAULT NULL,
  `broadcastRecordsPerPage` int(11) DEFAULT NULL,
  `currProgramRecordsPerPage` int(11) DEFAULT NULL,
  `pastProgramRecordsPerPage` int(11) DEFAULT NULL,
  `systemEmailsRecordsPerPage` int(11) DEFAULT NULL,
  `newUserEmailsRecordsPerPage` int(11) DEFAULT NULL,
  `contactDefSort` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `webViewToken` TEXT CHARACTER SET utf8 COMMENT 'temporary token send to mobile app to get user id and access token',
  `resetPassHash` VARCHAR(100) NULL DEFAULT ''COMMENT 'random number',
  `exportContacts` TINYINT( 1 ) NOT NULL DEFAULT  '0',
  `sqsMessageId` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `payThroughAuthNet` TINYINT NOT NULL DEFAULT  '0' COMMENT  'Payment done through autherize.net',
  `rewardNotifications` BOOLEAN DEFAULT FALSE COMMENT '1-Yes, 0-No',
  `emailsFromAdministrators` BOOLEAN DEFAULT FALSE COMMENT '1-Yes, 0-No',
  `myWeeklyStats` BOOLEAN DEFAULT FALSE COMMENT '1-Yes, 0-No',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`),
  KEY `accountId` (`accountId`),
  KEY `stateId` (`stateId`),
  KEY `advanceSearch` (`firstName`,`lastName`,`created`,`lastLogin`,`phoneNumber`,`roleId`,`status`),
  KEY `sqsMessageId` (`sqsMessageId`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountUserAppCampaign`
--

DROP TABLE IF EXISTS `accountUserAppCampaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountUserAppCampaign` (
  `accountUserId` int(11) NOT NULL,
  `accountCampaignId` int(11) NOT NULL,
  UNIQUE KEY `accountUserId` (`accountUserId`,`accountCampaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountUserAppResource`
--

DROP TABLE IF EXISTS `accountUserAppResource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountUserAppResource` (
  `accountUserId` int(11) NOT NULL,
  `accountResourceId` int(11) NOT NULL,
  UNIQUE KEY `accountUserId` (`accountUserId`,`accountResourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountUserCardProfile`
--

DROP TABLE IF EXISTS `accountUserCardProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountUserCardProfile` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `userId` int(11) unsigned NOT NULL,
  `firstName` varchar(255) NOT NULL COMMENT 'name on card',
  `middleName` varchar(255) DEFAULT NULL COMMENT 'name on card',
  `lastName` varchar(255) DEFAULT NULL COMMENT 'name on card',
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL COMMENT 'billing address',
  `suite` varchar(255) NOT NULL COMMENT 'billing address',
  `city` varchar(255) NOT NULL COMMENT 'billing address',
  `stateId` INT( 11 ) NULL COMMENT  'billing address',
  `countryId` INT( 11 ) NULL DEFAULT  '0',
  `zip` varchar(20) NOT NULL COMMENT 'billing address',
  `maskedCcNo` varchar(30) NOT NULL,
  `ccType` varchar(100) NOT NULL,
  `ccExpMonth` tinyint(2) unsigned NOT NULL,
  `ccExpYear` smallint(4) NOT NULL,
  `maskedCvv` varchar(5) NOT NULL,
  `authorizeProfileToken` varchar(20) NOT NULL,
  `authorizePaymentProfileToken` varchar(20) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  `firstCycleStartDate` date DEFAULT NULL,
  `lastPaymentDoneOn` date DEFAULT NULL,
  `nextPaymentOn` date DEFAULT NULL,
  `failureCount` tinyint(2) unsigned DEFAULT '0',
  `lastAmountPaid` FLOAT NULL DEFAULT NULL,
  `lastTransactionId` INT( 11 ) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId` (`userId`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountUserGroup`
--

DROP TABLE IF EXISTS `accountUserGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountUserGroup` (
  `accountUserId` int(11) unsigned NOT NULL,
  `accountGroupId` int(11) unsigned NOT NULL,
  UNIQUE KEY `accountUserId` (`accountUserId`,`accountGroupId`),
  KEY `accountGroupId` (`accountGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountUserIncentive`
--

DROP TABLE IF EXISTS `accountUserIncentive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountUserIncentive` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountIncentiveProgramId` int(11) unsigned NOT NULL,
  `userId` int(11) unsigned NOT NULL,
  `leadsGenerated` int(11) unsigned DEFAULT NULL COMMENT 'Number of users opts in during this period',
  `awardsEarned` smallint(5) unsigned DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emailStatus` enum('0','1') DEFAULT '0' COMMENT 'O = ''Mail not sent'', 1=''Mail sent'' ',
  PRIMARY KEY (`accountIncentiveProgramId`,`userId`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=497 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mailLog`
--

DROP TABLE IF EXISTS `mailLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailLog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `messageId` text CHARACTER SET utf8,
  `type` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `event` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `contactId` int(11) unsigned DEFAULT NULL,
  `campaignId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sentBy` int(11) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` text,
  `sendToRoles` varchar(255) NOT NULL COMMENT 'comma seperated role ids',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privilege` (
  `roleId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `resourceId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `access` text,
  `assert` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`roleId`,`resourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `parent` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `InheritPermissionsFrom` varchar(150) DEFAULT NULL,
  `rank` tinyint(3) unsigned DEFAULT NULL COMMENT 'setup according to lowest rank last, hence 1 is last',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbreviation` char(2) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `support`
--

DROP TABLE IF EXISTS `support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `support` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL COMMENT 'Foreign key for the accountUser table',
  `accountId` int(11) unsigned NOT NULL COMMENT 'Foreign key for account table',
  `firstName` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `lastName` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `feedbackSubject` varchar(100) NOT NULL,
  `feedbackDetails` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-> Open, 2->Closed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `new` tinyint(1) unsigned DEFAULT '1',
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(10) unsigned DEFAULT NULL COMMENT 'user id if anyone is adding him',
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(10) unsigned DEFAULT NULL COMMENT 'user id if anyone is modifying him',
  `password` text NOT NULL,
  `salt` text NOT NULL,
  `forceChange` tinyint(4) NOT NULL DEFAULT '1',
  `roleId` tinyint(3) unsigned NOT NULL DEFAULT '3',
  `profileImage` varchar(45) DEFAULT NULL COMMENT 'image name with extention',
  `address` varchar(255) DEFAULT NULL,
  `suite` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateId` INT( 11 ) NOT NULL,
  `countryId` INT( 11 ) NOT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `isSuspended` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'account can be suspended due to payment issue',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `createdBy` (`createdBy`),
  KEY `modifiedBy` (`modifiedBy`),
  KEY `stateId` (`stateId`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountIncentiveGroup`
--

DROP TABLE IF EXISTS `accountIncentiveGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountIncentiveGroup` (
 `accountIncentiveProgramId` int(11) unsigned NOT NULL,
  `accountGroupId` int(11) unsigned NOT NULL,
  UNIQUE KEY `accountIncentiveProgramId` (`accountIncentiveProgramId`,`accountGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountIncentiveCampaign`
--

DROP TABLE IF EXISTS `accountIncentiveCampaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountIncentiveCampaign` (
  `accountIncentiveProgramId` int(11) unsigned NOT NULL,
  `accountCampaignId` int(11) unsigned NOT NULL,
  UNIQUE KEY `accountIncentiveProgramId` (`accountIncentiveProgramId`,`accountCampaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- 8 Dec Prasanti
ALTER TABLE `accountMailLog` CHANGE `recepientId` `recipientId` INT( 11 ) NOT NULL ;
ALTER TABLE `accountMailLog` CHANGE `recepientType` `recipientType` TINYINT( 4 ) NOT NULL DEFAULT '0' COMMENT '1 = User , 2 = Contact';

-- 27 Jan Tan
DROP TABLE IF EXISTS timeDimension;
CREATE TABLE timeDimension (
        id                      INTEGER PRIMARY KEY,  -- year*10000+month*100+day
        dbDate                 DATE NOT NULL,
        year                    INTEGER NOT NULL,
        month                   INTEGER NOT NULL, -- 1 to 12
        day                     INTEGER NOT NULL, -- 1 to 31
        quarter                 INTEGER NOT NULL, -- 1 to 4
        week                    INTEGER NOT NULL, -- 1 to 52/53
        dayName                VARCHAR(9) NOT NULL, -- 'Monday', 'Tuesday'...
        monthName              VARCHAR(9) NOT NULL, -- 'January', 'February'...
        holidayFlag            CHAR(1) DEFAULT 'f' CHECK (holidayFlag in ('t', 'f')),
        weekendFlag            CHAR(1) DEFAULT 'f' CHECK (weekdayFlag in ('t', 'f')),
        event                   VARCHAR(50),
        UNIQUE tdYmdIdx (year,month,day),
        UNIQUE tdDbdateIdx (dbDate)

) Engine=MyISAM;

DROP PROCEDURE IF EXISTS fillDateDimension;
DELIMITER //
CREATE PROCEDURE fillDateDimension(IN startdate DATE,IN stopdate DATE)
BEGIN
    DECLARE currentdate DATE;
    SET currentdate = startdate;
    WHILE currentdate < stopdate DO
        INSERT INTO timeDimension VALUES (
                        YEAR(currentdate)*10000+MONTH(currentdate)*100 + DAY(currentdate),
                        currentdate,
                        YEAR(currentdate),
                        MONTH(currentdate),
                        DAY(currentdate),
                        QUARTER(currentdate),
                        WEEKOFYEAR(currentdate),
                        DATE_FORMAT(currentdate,'%W'),
                        DATE_FORMAT(currentdate,'%M'),
                        'f',
                        CASE DAYOFWEEK(currentdate) WHEN 1 THEN 't' WHEN 7 then 't' ELSE 'f' END,
                        NULL);
        SET currentdate = ADDDATE(currentdate,INTERVAL 1 DAY);
    END WHILE;
END
//
DELIMITER ;

TRUNCATE TABLE timeDimension;

CALL fillDateDimension('2015-01-01','2030-01-01');
OPTIMIZE TABLE timeDimension;

ALTER TABLE `accountMailLog` ADD `eventOccurredTime` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `broadcastType` ;

-- 24th Feb Tan
CREATE TABLE `accountContactNotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountContactId` int(11) NOT NULL,
  `contactNote` text,
  `noteTimeStamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `guestNote` tinyint(1) DEFAULT '0' COMMENT '0 note added in contact page, 1 note added from custom resource page',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- 8 March 2016
CREATE TABLE IF NOT EXISTS `session` (
  `id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `lifetime` smallint(6) NOT NULL,
  `modified` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  -- 11 May Raj
CREATE TABLE IF NOT EXISTS `accountUserLogs` (
  `accountId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `userRoleId` enum('1','2','3') NOT NULL COMMENT 'This is for which user type the data has been calculated.',
  `date` date NOT NULL,
  `totalContacts` int(11) NOT NULL,
  `totalActiveContacts` int(11) NOT NULL,
  `contactsThisMonth` int(11) NOT NULL,
  `activeContactsThisMonth` int(11) NOT NULL,
  `totalEmailsSent` int(11) NOT NULL,
  `totalEmailsSentThisMonth` int(11) NOT NULL,
  `monthlyContacts` varchar(1000) NOT NULL,
  `monthlyEmailsLast11Months` VARCHAR( 1000 ) NOT NULL COMMENT  'Monthly emails of last 11 months',
  `monthlyEmails` varchar(1000) NOT NULL,
  `monthlyClickedLinks` varchar(1000) NOT NULL,
  `contacts30Days` varchar(3000) NOT NULL,
  `totalUsers` int(11) NOT NULL,
  `totalPaidUsers` int(11) NOT NULL,
  `users45Days` varchar(5000) NOT NULL,
  `contactsAssignedToCampaign` int(11) NOT NULL,
  `totalOptedInContacts` int(11) NOT NULL,
  UNIQUE KEY `userId` (`userId`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 26th April Sanjeev
CREATE TABLE `accountBroadcastTempMail` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `accountId` int(11) NOT NULL,
 `broadcastMailId` int(11) NOT NULL,
 `isDraft` TINYINT( 1 ) NULL DEFAULT '0',
 `recipientType` enum('1','2','3') NOT NULL COMMENT '''1-for Users, 2-for Contacts, 3-for Incentive',
 `recipients` text NOT NULL,
 `sendStatus` tinyint(1) NOT NULL DEFAULT '0',
 `inProgress` tinyint(1) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`),
 KEY `broadcastMailId` (`broadcastMailId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- 19 Oct Prasanti
CREATE TABLE IF NOT EXISTS `accountLinkTracking` (
  `userId` int(11) NOT NULL,
  `contactId` int(11) NOT NULL,
  `campaignId` int(11) NOT NULL,
  `link` varchar(1000) NOT NULL,
  `clickedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Table structure for table `accountCustomPageResource`
CREATE TABLE IF NOT EXISTS `accountCustomPageResource` (
  `accountId` int(11) unsigned NOT NULL,
  `accountResourceId` int(11) unsigned NOT NULL,
  `mediaHash` varchar(20) NOT NULL,
  UNIQUE KEY `accountResourceId` (`accountResourceId`,`mediaHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 27 July 2016 Sanjeev
CREATE TABLE IF NOT EXISTS `accountCampaignTempEmail` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `accountId` int(11) NOT NULL,
 `campaignId` int(11) NOT NULL,
 `campaignEmailId` int(11) NOT NULL,
 `days` int(5) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `campaignEmailId` (`campaignEmailId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- 04th Aug 2016 Raj
--
-- Table structure for table `accountCampaignContactCount`
--
CREATE TABLE IF NOT EXISTS `accountCampaignContactCount` (
  `accountId` int(10) unsigned NOT NULL,
  `accountUserId` int(10) unsigned NOT NULL,
  `accountCampaignId` int(10) unsigned NOT NULL,
  `contactCount` int(10) unsigned NOT NULL,
  UNIQUE KEY `accountUserCampaignId` (`accountId`,`accountUserId`,`accountCampaignId`),
  KEY `accountId` (`accountId`),
  KEY `accountCampaignId` (`accountCampaignId`),
  KEY `accountUserId` (`accountUserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 25th July 2016 Raj
--
-- Table structure for table `accountUserWarnedSuspend`
--
CREATE TABLE IF NOT EXISTS `accountUserWarnedSuspend` (
  `userId` int(11) unsigned NOT NULL,
  `spamComplaintStatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-normal, 1-warned, 2-disabled',
  `noOfSpamComplaints` smallint(4) NOT NULL DEFAULT '0' COMMENT 'total number of spam complaints in last warning or deactivation ',
  `deactivationSpamComplaints` smallint(4) NOT NULL DEFAULT '0' COMMENT 'It is the number of spam complaints in past deactivation',
  `spamWarnDate` date DEFAULT NULL,
  `spamDeactivateDate` date DEFAULT NULL,
  `lowOptinStatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-normal, 1-warned, 2-disabled',
  `lowOptinWarnDate` date DEFAULT NULL,
  `lowOptinDeactivateDate` date DEFAULT NULL,
  UNIQUE KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 29th August Tan
DROP TABLE IF EXISTS accountContactAnalytics;

CREATE TABLE `accountContactAnalytics` (
  `id`                INT(11)      NOT NULL AUTO_INCREMENT,
  `accountContactId`  INT(11)      NOT NULL,
  `accountResourceId` INT(11)      NOT NULL,
  `accountUserId`     INT(11)      NOT NULL,
  `percentWatched`    FLOAT        NOT NULL,
  `visitorKey`        VARCHAR(100) NOT NULL,
  `mediaHash`         VARCHAR(100) NOT NULL,
  `duration`          FLOAT NOT NULL,
  `date`          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_duplicate` (`accountContactId`, `accountResourceId`, `accountUserId`, `visitorKey`, `mediaHash`)
)
  ENGINE = InnoDB
  CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- 30th Sep 2016 Tan
CREATE TABLE `accountUserLegalshield` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `accountUserId` int(10) unsigned NOT NULL,
  `associateNumber` int(10) unsigned NOT NULL COMMENT 'unique id from Legalshield',
  `associateName` varchar(255) DEFAULT NULL COMMENT 'first name plus last name from Legalshield',
  `associateEmail` varchar(255) DEFAULT NULL COMMENT 'email from Legalshield',
  `advantageStatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'can_have_crm from Legalshield',
  `accessToken` blob NOT NULL COMMENT 'access token from Legalshield',
  `refreshToken` text NOT NULL COMMENT 'Refresh token used to fetch new access token',
  `tokenExpireTime` int(11) NOT NULL COMMENT 'token expire time',
  `isTokenExpired` tinyint(1) NOT NULL COMMENT 'token expired or not',
  `connectedDate` datetime DEFAULT '0000-00-00 00:00:00' COMMENT 'Legalshield connection date',
  `updatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountUserId` (`accountUserId`),
  KEY `lsAcUsr` (`accountUserId`),
  KEY `lsAssNum` (`associateNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;


-- Raj 20 Sept
-- Table structure for table `accountTransactions`
--

CREATE TABLE IF NOT EXISTS `accountTransactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `userId` int(11) unsigned DEFAULT NULL,
  `eventId` varchar(50) DEFAULT NULL COMMENT 'Stripe event Id',
  `eventType` varchar(100) NOT NULL COMMENT 'event happened on Stripe',
  `stripeCustomerId` varchar(50) NOT NULL,
  `subscriptionId` varchar(50) DEFAULT NULL COMMENT 'Subscription id in stripe',
  `subscriptionPlanId` varchar(50) DEFAULT NULL,
  `invoiceNumber` varchar(50) DEFAULT NULL,
  `paymentFor` varchar(20) DEFAULT NULL COMMENT 'for user, base price or entire company',
  `last4CardNumber` varchar(4) NOT NULL,
  `amount` decimal(9,2) DEFAULT '0.00',
  `chargeId` varchar(50) DEFAULT NULL,
  `paymentStatus` tinyint(1) NOT NULL DEFAULT '0',
  `refundId` varchar(50) DEFAULT NULL COMMENT 'refund id from stripe, if refund request',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eventId` (`eventId`),
  KEY `chargeId` (`chargeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- Raj 20 Sept
-- Table structure for table `accountUserBillingInfo`
--

CREATE TABLE IF NOT EXISTS `accountUserBillingInfo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned NOT NULL,
  `userId` int(11) unsigned NOT NULL,
  `isForCompany` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 if the billing details is of company else of user',
  `stripeCustomerId` varchar(50) NOT NULL,
  `lastChargeId` varchar(50) DEFAULT NULL COMMENT 'Stores last charge id',
  `name` varchar(100) NOT NULL COMMENT 'name on card',
  `email` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL COMMENT 'billing address',
  `city` varchar(100) NOT NULL COMMENT 'billing address',
  `stateId` INT( 11 ) NULL,
  `countryId` INT( 11 ) NULL,
  `zip` varchar(20) NOT NULL COMMENT 'billing address',
  `expMonth` varchar(2) NOT NULL,
  `expYear` varchar(4) DEFAULT NULL,
  `last4` varchar(4) DEFAULT NULL COMMENT 'Last 4 digits of the card',
  `brand` varchar(20) DEFAULT NULL COMMENT 'brand of card like Visa',
  `funding` varchar(20) DEFAULT NULL COMMENT 'funding type like credit, debit',
  `isSubscriptionActive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 if subscription active on stripe',
  `subscriptionId` varchar(50) DEFAULT NULL COMMENT 'Subscription id in stripe',
  `subscriptionPlanId` varchar(50) DEFAULT NULL COMMENT 'Subscription plan id of stripe',
  `subscriptionAmount` decimal(6,2) NOT NULL DEFAULT '0.00' COMMENT 'Amount in dollars',
  `subscriptionCreated` timestamp NULL DEFAULT NULL COMMENT 'Subscription starting date',
  `subPaymentStartDate` date NOT NULL COMMENT 'It is the subscription payment start date',
  `lastPaymentDoneOn` date NOT NULL COMMENT 'Last Payment Date',
  `lastPaidAmount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `nextPaymentOn` date DEFAULT NULL COMMENT 'Next payment date',
  `failureCount` tinyint(1) DEFAULT '0',
  `inActiveOrIgnoreBilling` tinyint(1) DEFAULT '0' COMMENT 'It is one if user billing ignored or user deactivated',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(11) unsigned DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stripeToken` (`stripeCustomerId`),
  UNIQUE KEY `userId` (`userId`,`accountId`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=247 ;


--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `countryId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `countryId` (`countryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4121 ;

-- 21 Nov 2016 Neeraj
CREATE TABLE IF NOT EXISTS `userResourcesSentLog` (
  `userId` int(10) unsigned NOT NULL,
  `accountId` INT(11) NOT NULL,
  `type` TINYINT( 1 ) NOT NULL DEFAULT '0' COMMENT '0=resource page, 1=campaign email',
  `resourceId` int(10) unsigned NOT NULL,
  `resourceSentDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
-- 21 Nov 2016 Neeraj
CREATE TABLE IF NOT EXISTS `userResourcesViewedLog` (
  `userId` int(10) unsigned NOT NULL,
  `resourceId` int(10) unsigned NOT NULL,
  `resourceViewedDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

--
-- Table structure for table `accountPreference`
--
CREATE TABLE IF NOT EXISTS `accountPreference` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `accountId` int(11) NOT NULL,
 `enableTwitterUrl` tinyint(1) DEFAULT '0' COMMENT '0-off, 1-on',
 `enableFacebookUrl` tinyint(1) DEFAULT '0' COMMENT '0-off, 1-on',
 `enableInstagramUrl` tinyint(1) DEFAULT '0' COMMENT '0-off, 1-on',
 `enableAdditionalNotificationEmail` tinyint(1) DEFAULT '0' COMMENT '0-off, 1-on',
 `enableRepId1` tinyint(1) DEFAULT '0' COMMENT '0-off, 1-on',
 `enableRepId2` enum('0','1') CHARACTER SET utf8 DEFAULT '0' COMMENT '0-off, 1-on',
 `enableTwitterUrlRequired` tinyint(4) DEFAULT '0' COMMENT '0=Not Required, 1=Required',
 `enableFacebookUrlRequired` tinyint(4) DEFAULT '0' COMMENT '0=Not Required, 1=Required',
 `enableInstagramUrlRequired` tinyint(1) DEFAULT '0' COMMENT '0=Not Required, 1=Required',
 `enableAdditionalNotificationEmailRequired` tinyint(4) DEFAULT '0' COMMENT '0=Not Required, 1=Required',
 `enableRepId1Required` tinyint(4) DEFAULT '0' COMMENT '0=Not Required, 1=Required',
 `enableRepId2Required` tinyint(4) DEFAULT '0' COMMENT '0=Not Required, 1=Required',
 `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `createdBy` int(11) NOT NULL,
 `dateModified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
 `modifiedBy` int(11) DEFAULT NULL,
 PRIMARY KEY (`accountId`),
 UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Table structure for table `accountCommissionUser`
--
CREATE TABLE IF NOT EXISTS `accountCommissionUser` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `accountId` int(11) NOT NULL,
 `firstName` varchar(100) CHARACTER SET utf8 NOT NULL,
 `lastName` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
 `email` varchar(250) CHARACTER SET utf8 NOT NULL,
 `phoneNumber` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
 `commissionTypeId` int(11) NOT NULL,
 `percentage` float unsigned NOT NULL,
 `commissionPercentageId` INT( 11 ) NULL DEFAULT NULL,
 `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `createdBy` int(11) NOT NULL,
 `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
 `modifiedBy` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `accountId` (`accountId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Table structure for table `accountCommissionType`
--
CREATE TABLE IF NOT EXISTS `accountCommissionType` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Table structure for table `accountCommissionPercentage`
--
CREATE TABLE `accountCommissionPercentage` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `accountId` int(11) NOT NULL,
 `percentage250` float unsigned DEFAULT NULL,
 `percentage251` float unsigned DEFAULT NULL,
 `percentage501` float unsigned DEFAULT NULL,
 `percentage5001` float unsigned DEFAULT NULL,
 `percentage7000` float unsigned DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER ;;
CREATE FUNCTION FIRST_DAY_OF_WEEK(day DATE)
  RETURNS DATE DETERMINISTIC
  BEGIN
    RETURN SUBDATE(day, WEEKDAY(day));
  END;;
DELIMITER ;

CREATE TABLE accountLogs (
  id                INT        NOT NULL AUTO_INCREMENT,
  accountId         INT        NOT NULL,
  rfAdmin           TINYINT(1) NOT NULL DEFAULT 0
  COMMENT 'RF Admin user or not',
  createdVsUpgraded TEXT       NOT NULL
  COMMENT 'Contains json string of statistics',
  created DATETIME NOT NULL COMMENT 'DATE Of Monday of current week',
  updated DATETIME NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY createdVsUpgraded (accountId, rfAdmin)
);