/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@email.org',1,'Sanjeev',NULL,'2015-10-24 20:00:49',NULL,'2015-10-24 20:00:49',NULL,'fda1fab11db2e2fb903b8b1ff1eedeb2','Xt1hNUypUCdQG2UUUd3RNA',1,1,NULL,NULL,NULL,NULL,0,0,NULL,'0','active');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` (`id`, `name`, `companyName`, `address`, `suite`, `city`, `stateId`, `zip`, `url`, `status`, `accountLevel`, `ignoreAccountPayment`, `passThrough`, `trial`, `trialPeriod`, `testAccount`, `dateCreated`, `createdBy`, `dateModified`, `modifiedBy`, `wistiaProjectId`) VALUES
  (1, 'Mindfire', NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 5, '0', 1, '0', 0, '0', '2015-10-24 14:30:49', 0, '0000-00-00 00:00:00', NULL, NULL),
  (2, 'Free Test', NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 1, '0', 1, '0', 0, '0', '2015-10-24 14:30:49', 0, '0000-00-00 00:00:00', NULL, NULL),
  (3, 'Contact Test',NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 6, '0', 1, '0', 0, '0', '2015-10-24 20:00:49', 0, '0000-00-00 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountTransaction`
--
LOCK TABLES `accountTransaction` WRITE;
/*!40000 ALTER TABLE `accountTransaction` DISABLE KEYS */;
INSERT INTO `accountTransaction` (`id`, `accountId`, `userId`, `invoiceNumber`, `amount`, `method`, `authorizeProfileToken`, `authorizePaymentProfileToken`, `approved`, `declined`, `error`, `refund`, `held`, `refundApproved`, `refundDeclined`, `refundError`, `refundHeld`, `transactionId`, `transactionType`, `refundTransactionId`, `refundTransactionType`, `customerId`, `firstName`, `lastName`, `company`, `address`, `city`, `state`, `zip`, `email`, `accountNumber`, `cardType`, `created`, `refundDate`, `modified`) VALUES (NULL, '1', '65', '001', '1', NULL, '', '', '1', NULL, NULL, '1', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CURRENT_TIMESTAMP, NULL, NULL);
/*!40000 ALTER TABLE `accountTransaction` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `accountAwardType`
--

LOCK TABLES `accountAwardType` WRITE;
/*!40000 ALTER TABLE `accountAwardType` DISABLE KEYS */;
INSERT INTO `accountAwardType` VALUES (1,'Award for the most leads'),(2,'Award for achieving goal'),(3,'Award per lead'),(4,'Award after X leads');
/*!40000 ALTER TABLE `accountAwardType` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `accountBranding`
--

LOCK TABLES `accountBranding` WRITE;
/*!40000 ALTER TABLE `accountBranding` DISABLE KEYS */;
INSERT INTO `accountBranding` (id, accountId, dashboardLogo, mobileLogo, isShowColorToMobile, domainName, reBrand,
reBrandAdditionalNotificationEmail, reBrandRepIdToolTip, reBrandAdditionalNotificationEmailToolTip, reBrandRepId2,
reBrandRepId2ToolTip, upgradeInformationForFreeUsers)
VALUES (1,1,'a.jpg',NULL,0,NULL,NULL,NULL,NULL,NULL, NULL, '', '');
/*!40000 ALTER TABLE `accountBranding` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `accountBroadcastEmails`
--

LOCK TABLES `accountBroadcastEmails` WRITE;
/*!40000 ALTER TABLE `accountBroadcastEmails` DISABLE KEYS */;
INSERT INTO `accountBroadcastEmails` VALUES (1,1,'2015-10-24 20:00:49',65,'','Hello','Hii','Hii',0,'1','', NULL,NULL);
/*!40000 ALTER TABLE `accountBroadcastEmails` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `accountCampaign`
--

LOCK TABLES `accountCampaign` WRITE;
/*!40000 ALTER TABLE `accountCampaign` DISABLE KEYS */;
INSERT INTO `accountCampaign` VALUES (1,1,'Hello','HI','0',NULL,'1','2015-10-24 20:00:49',65,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `accountCampaign` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `accountCampaignContact`
--

LOCK TABLES `accountCampaignContact` WRITE;
/*!40000 ALTER TABLE `accountCampaignContact` DISABLE KEYS */;
INSERT INTO `accountCampaignContact` VALUES (1,1,'web',0,'2015-10-24 20:00:49');
/*!40000 ALTER TABLE `accountCampaignContact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountCampaignContactCount`
--

INSERT  INTO accountCampaignContactCount SELECT
  accountId, accountContact.createdBy, accountCampaignId, COUNT(accountContactId) AS totalContacts
FROM accountCampaignContact
  INNER JOIN accountContact ON accountCampaignContact.accountContactId = accountContact.id
GROUP BY accountCampaignContact.accountCampaignId, accountContact.createdBy, accountContact.accountId;


--
-- Dumping data for table `accountCampaignEmail`
--

LOCK TABLES `accountCampaignEmail` WRITE;
/*!40000 ALTER TABLE `accountCampaignEmail` DISABLE KEYS */;
INSERT INTO `accountCampaignEmail` VALUES (1,1,'Hello','Html Bode','Text Body',0,1,'published','0','2015-10-24 20:00:49',65,NULL,65);
/*!40000 ALTER TABLE `accountCampaignEmail` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `accountCampaignGroup`
--

LOCK TABLES `accountCampaignGroup` WRITE;
/*!40000 ALTER TABLE `accountCampaignGroup` DISABLE KEYS */;
INSERT INTO `accountCampaignGroup` VALUES (1,1);
/*!40000 ALTER TABLE `accountCampaignGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountCardProfile`
--

LOCK TABLES `accountCardProfile` WRITE;
/*!40000 ALTER TABLE `accountCardProfile` DISABLE KEYS */;
INSERT INTO `accountCardProfile` VALUES (1,1,'Sanjeev',NULL,NULL,'','','','','','0','','','',0,0,'','',NULL,'2015-10-24 20:00:49',NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `accountCardProfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountCollateral`
--

LOCK TABLES `accountCollateral` WRITE;
/*!40000 ALTER TABLE `accountCollateral` DISABLE KEYS */;
INSERT INTO `accountCollateral` VALUES (1,1,'Hello','','','2015-10-24 20:00:49',0,NULL,NULL,'0');
/*!40000 ALTER TABLE `accountCollateral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountContact`
--

LOCK TABLES `accountContact` WRITE;
/*!40000 ALTER TABLE `accountContact` DISABLE KEYS */;

-- INSERT INTO `accountContact` VALUES (1,1,'Sanjeev','','sanjeev.kumar@mindfiresolutions.com','555-555-555, 666-666-666',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Web Application',0,0,NULL,0,0,NULL,0,'2015-10-24 20:00:49',65,'0000-00-00 00:00:00',0);

-- INSERT INTO `accountContact` VALUES (2,3,'Temp','Contact 1','temp.contact1@email.org','555-555-555',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Web Application',0,0,NULL,0,0,NULL,0,NOW(),65,'0000-00-00 00:00:00',0);

-- INSERT INTO `accountContact` VALUES (3,3,'Temp','Contact 2','temp.contact2@email.org','555-555-555',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Web Application',0,0,NULL,0,0,NULL,0,NOW(),65,'0000-00-00 00:00:00',0);

-- INSERT INTO `accountContact` VALUES (4,3,'Temp','Contact 3','temp.contact3@email.org','555-555-555',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Web Application',0,0,NULL,0,0,NULL,0,NOW(),65,'0000-00-00 00:00:00',0);

INSERT INTO `accountContact` (`id`, `accountId`, `firstName`, `lastName`, `email`, `homeEmail`, `workEmail`, `otherEmail`, `phone`, `mobile`, `home`, `work`, `other`, `address1`, `address2`, `city`, `stateId`, `countryId`, `zip`, `company`, `title`, `interest`, `note`, `contactNotes`, `noteType`, `channel`, `optInSend`, `optIn`, `optInDate`, `optInResend`, `optOut`, `optOutDate`, `status`, `created`, `createdBy`, `modified`, `modifiedBy`, `customContact`, `noteTimeStamps`) VALUES (1, 1, 'Sanjeev', '', 'sanjeev.kumar@mindfiresolutions.com', NULL, NULL, NULL, '555-555-555, 666-666-666', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Web Application', 0, 0, NULL, 0, 0, NULL, 0, '2015-10-24 20:00:49', 65, '0000-00-00 00:00:00', 0, 0, NULL);

INSERT INTO `accountContact` (`id`, `accountId`, `firstName`, `lastName`, `email`, `homeEmail`, `workEmail`, `otherEmail`, `phone`, `mobile`, `home`, `work`, `other`, `address1`, `address2`, `city`, `stateId`, `countryId`, `zip`, `company`, `title`, `interest`, `note`, `contactNotes`, `noteType`, `channel`, `optInSend`, `optIn`, `optInDate`, `optInResend`, `optOut`, `optOutDate`, `status`, `created`, `createdBy`, `modified`, `modifiedBy`, `customContact`, `noteTimeStamps`) VALUES (2, 3, 'raj', 'Contact 1', 'raj@mindfiresolutions.com', NULL, NULL, NULL, '555-555-555', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Web Application', 0, 0, NULL, 0, 0, NULL, 0, '2015-10-24 20:00:49', 77, '0000-00-00 00:00:00', 0, 0, NULL);

INSERT INTO `accountContact`(`id`, `accountId`, `firstName`,  `email`, `createdBy`) VALUES ('3', '2', 'Neeraj', 'neeraj.das@mindfiresolutions.com','70');

/*!40000 ALTER TABLE `accountContact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountGroup`
--

LOCK TABLES `accountGroup` WRITE;
/*!40000 ALTER TABLE `accountGroup` DISABLE KEYS */;
INSERT INTO `accountGroup` VALUES (1,'A1B2C3',1,'123','12',NULL,NULL),(2,'H1H2',1,'Hello 2','HII',1,2);
/*!40000 ALTER TABLE `accountGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountLandingPage`
--

LOCK TABLES `accountLandingPage` WRITE;
/*!40000 ALTER TABLE `accountLandingPage` DISABLE KEYS */;
INSERT INTO `accountLandingPage` VALUES (1,1,65,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `accountLandingPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountLevel`
--

LOCK TABLES `accountLevel` WRITE;
/*!40000 ALTER TABLE `accountLevel` DISABLE KEYS */;
INSERT INTO `accountLevel` VALUES (1,'Free','Starter Account',0,0,0,3,500,0,0,'1','2014-08-08 08:52:20',0,'2014-11-07 10:05:59',NULL),(2,'Business','Business',59,10,0,100,0,0,0,'1','2014-08-08 08:52:20',0,'2014-11-05 05:18:11',NULL),(3,'Business Pro','Business Pro',249,10,0,250,0,0,0,'1','2014-08-08 08:52:20',0,'2014-11-05 05:17:35',NULL),(4,'Enterprise','Enterprise',1499,10,0,500,0,0,0,'1','2014-08-08 08:52:20',0,'2014-11-05 05:17:35',NULL),(5,'Enterprise Pro','INACTIVE',0,10,1,0,0,0,0,'1','2014-08-08 08:52:20',0,'2015-07-10 03:23:06',NULL),(6,'Test','Temp testing level',0,0,0,3,3,0,0,'1','2014-08-08 08:52:20',0,'2014-11-07 10:05:59',NULL);
/*!40000 ALTER TABLE `accountLevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountResource`
--

LOCK TABLES `accountResource` WRITE;
/*!40000 ALTER TABLE `accountResource` DISABLE KEYS */;
INSERT INTO `accountResource` VALUES (1,'account^billing^info','Billing Information',NULL),(2,'account^branding^index','Branding',NULL),(3,'account^campaign^get-campaigns','Get Campaigns',NULL),(4,'account^campaign^edit','Create / Update Campaigns',NULL),(5,'account^campaign^ajax-delete-campaigns','Ajax Delete Campaigns',NULL),(6,'account^campaign-email^edit','Campaign Email',NULL),(7,'account^contact^index','Contact',NULL),(8,'account^contact^get-contacts','Get Contacts',NULL),(9,'account^contact^edit','Create / Update Contacts',NULL),(10,'account^contact^ajax-delete-conatct','Ajax Delete Contacts',NULL),(11,'account^custom-role^get-roles','View Custom Roles',NULL),(12,'account^custom-role^edit','Create / Update Custom Roles',NULL),(13,'account^custom-role^configure-custom-role','Configure Custom Roles',NULL),(14,'account^group^get-groups','Get Groups',NULL),(15,'account^group^edit','Create / Update Groups',NULL),(16,'account^group^ajax-delete-group','Ajax Delete Group',NULL),(17,'account^group^associate-group-users','Associate Group Users',NULL),(18,'account^group^ajax-get-users-for-group','Ajax Get Users for Group',NULL),(19,'account^group^ajax-associate-group-user','Ajax Associate User Group',NULL),(20,'account^index^export-csv','Export CSV',NULL),(21,'account^invitation^index','Invitation',NULL),(22,'account^invitation^get-invitable-users','Get Invitable Users',NULL),(23,'account^invitation^set-invitation-day','Set Invitation Day',NULL),(24,'account^login^index','Login',NULL),(25,'account^login^logout','Logout',NULL),(26,'account^profile^index','Edit / Save Profile',NULL),(27,'account^profile^change-password','Change Password',NULL),(28,'account^resource^get-resource','View Resources',NULL),(29,'account^resource^edit','Create / Edit Resource',NULL),(30,'account^resource^ajax-delete-resource','Ajax delete Resource',NULL),(31,'account^users^get-users','Get Users',NULL),(32,'account^users^edit','Create / Edit Users',NULL),(33,'account^users^bulk-upload','Bulk Upload',NULL),(34,'account^users^ajax-delete-user','Ajax Delete User',NULL),(35,'account^billing^pay-now',NULL,NULL),(37,'account^users^index','Listing of users',NULL),(38,'account^campaign^index','show campaign list',NULL),(39,'account^resource^index','resource listing ',NULL),(40,'account^resource^get-resources','ajax call to get resources',NULL),(41,'account^index^index','dashboard page',NULL),(42,'account^profile^edit-landing','to edit personalized landing page',NULL),(43,'account^group^index','Show group listing',NULL),(44,'account^user-billing^info','user billing info',NULL),(45,'account^user-billing^pay-now','user billing pay now',NULL),(48,'account^award-progress^index','Show Current Award',NULL),(49,'account^award^index','Show Past Award',NULL),(50,'account^user-payment^index','Payment History',NULL);
/*!40000 ALTER TABLE `accountResource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountResourceCategory`
--

LOCK TABLES `accountResourceCategory` WRITE;
/*!40000 ALTER TABLE `accountResourceCategory` DISABLE KEYS */;
INSERT INTO `accountResourceCategory` VALUES (1,1,'Resource Category','category','2015-10-24 20:00:49',0,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `accountResourceCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountResourceGroup`
--

LOCK TABLES `accountResourceGroup` WRITE;
/*!40000 ALTER TABLE `accountResourceGroup` DISABLE KEYS */;
INSERT INTO `accountResourceGroup` VALUES (1,1);
/*!40000 ALTER TABLE `accountResourceGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountResourceType`
--

LOCK TABLES `accountResourceType` WRITE;
/*!40000 ALTER TABLE `accountResourceType` DISABLE KEYS */;
INSERT INTO `accountResourceType` VALUES (1,'Audio','account-resource-type-audio.png'),(2,'Video','account-resource-type-video.png'),(3,'PDF','account-resource-type-pdf.png'),(4,'Excel Document','account-resource-type-excel-document.jpg'),(5,'Word Document','account-resource-type-word-document.png'),(6,'Powerpoint','account-resource-type-power-point.png'),(7,'Image','account-resource-type-image.jpg'),(8,'Link','account-resource-type-link.jpg'),(9,'Custom Video Page','account-resource-type-link.jpg'),(10,'Plain Text','account-resource-type-plain-text.png');
/*!40000 ALTER TABLE `accountResourceType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountResources`
--

LOCK TABLES `accountResources` WRITE;
/*!40000 ALTER TABLE `accountResources` DISABLE KEYS */;
INSERT INTO `accountResources` VALUES (1,1,8,1,'Resource','des','Hello','http://www.google.com',NULL,NULL,'1','1',0,'2015-10-02 00:00:00',65,'2015-10-24 20:00:49',NULL);
/*!40000 ALTER TABLE `accountResources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountRoles`
--

LOCK TABLES `accountRoles` WRITE;
/*!40000 ALTER TABLE `accountRoles` DISABLE KEYS */;
INSERT INTO `accountRoles` VALUES (1,'admin','Administrator'),(2,'manager','Manager'),(3,'user','User');
/*!40000 ALTER TABLE `accountRoles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountRolesCustom`
--

LOCK TABLES `accountRolesCustom` WRITE;
/*!40000 ALTER TABLE `accountRolesCustom` DISABLE KEYS */;
INSERT INTO `accountRolesCustom` VALUES (3,'resource 1',0,1);
/*!40000 ALTER TABLE `accountRolesCustom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountStatus`
--

LOCK TABLES `accountStatus` WRITE;
/*!40000 ALTER TABLE `accountStatus` DISABLE KEYS */;
INSERT INTO `accountStatus` VALUES (1,'Active','This account is active'),(2,'Inactive','This account is inactive'),(3,'Suspended','This account is active but suspended for billing reasons');
/*!40000 ALTER TABLE `accountStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountSystemEmailType`
--

LOCK TABLES `accountSystemEmailType` WRITE;
/*!40000 ALTER TABLE `accountSystemEmailType` DISABLE KEYS */;
INSERT INTO `accountSystemEmailType` VALUES (1,'OptIn'),(2,'Welcome'),(3,'Link Tracking Notification'),(4,'Additional Notification Email'),(5,'New User First Email'),(6,'New User Second Email'),(7,'New User Third Email'),(8,'New User Fourth Email'),(9,'New User Fifth Email');
/*!40000 ALTER TABLE `accountSystemEmailType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountSystemEmails`
--

LOCK TABLES `accountSystemEmails` WRITE;
/*!40000 ALTER TABLE `accountSystemEmails` DISABLE KEYS */;
INSERT INTO `accountSystemEmails` VALUES
(1, 0, 5, NULL, 'Congratulations [firstName]', 'Welcome [firstName],<br><br>Congratulations! Your new account with RapidFunnel, is ready to go!<br><br>This system has been designed with you in mind, making it easier than ever to build relationships and generate leads.<br><br>Your login is:&nbsp; [email]<br><br>Your password is: [password]<br><br>If you have not already done so, you can download the RapidFunnel app from the App Store (iOS) or Play Store (Android) and sign in. This allows you to add prospects, assign them to an appropriate campaign and have them receive a series of emails related to their area of interest.<br><br>Your account also gives you access to a library of resources that will be invaluable to you.<br><br>To sign in to your account via the web, click here:<br><a target="_blank" href="http://my.rapidfunnel.com">http://my.rapidfunnel.com</a><br>Most of your account settings are manageable through the web portal.<br><br>Congratulations!<br><br>Sincerely,<br>The RapidFunnel Team', 'Welcome [firstName],\r\n\r\nCongratulations! Your new account with RapidFunnel, is ready to go!\r\n\r\nThis system has been designed with you in mind, making it easier than ever to build relationships and generate leads.\r\n\r\nYour login is:  [email]\r\n\r\nIf you have not already done so, you can download the RapidFunnel app from the App Store (iOS) or Play Store (Android) and sign in. This allows you to add prospects, assign them to an appropriate campaign and have them receive a series of emails related to their area of interest.\r\n\r\nYour account also gives you access to a library of resources that will be invaluable to you.\r\n\r\nTo sign in to your account via the web, click here:\r\nhttp://my.rapidfunnel.com\r\nMost of your account settings are manageable through the web portal.\r\n\r\nCongratulations!\r\n\r\nSincerely,\r\nThe RapidFunnel Team', '2', 1, '2015-07-25 11:23:12', 0, '0000-00-00 00:00:00', NULL),
(2, 0, 3, 'This is sent to the Additional Notification email when a contact clicks a link', 'Link Tracking Notification', 'Dear [firstName],<br><br>[contactEmai] just clicked on<br><br>[contactLinkClicked] ,<br>you may want to follow up.<br>', 'Dear [firstName],\r\n\r\n[contactEmai] just clicked on\r\n\r\n[contactLinkClicked] ,\r\nyou may want to follow up.', '2', NULL, '2015-07-25 11:24:06', 0, '2015-10-29 06:33:02', NULL),
(3, 0, 4, 'This is sent to the user when a contact clicks a link', 'Link Tracking Notification', 'Dear [firstName],<br><br>[contactEmai] just clicked on<br><br>[contactLinkClicked] ,<br>you may want to follow up.<br>', 'Dear [firstName],\r\n\r\n[contactEmai] just clicked on\r\n\r\n[contactLinkClicked] ,\r\nyou may want to follow up.', '2', NULL, '2015-07-25 11:24:06', 0, '2015-10-29 06:33:02', NULL),
(4, 0, 1, 'This is the email that is sent to a new contact', 'RapidFunnel Contact', 'Hello [firstName], <br/> <br/>"I''m looking forward to sending you the information we discussed. Just wanted to make sure that I have your email address correct. Just click the link below to confirm, and I will send you the information right over.<br/><br/>\n[activationLink]<br/><br/>\nThanks,<br/>\n[senderFirstName] [senderLastName]', 'Hello [firstName],\n\nI''m looking forward to sending you the information we discussed. Just wanted to make sure that I have your email address correct. Just click the link below to confirm, and I will send you the information right over.\n\n[activationLink]\n\nThanks,\n[senderFirstName] [senderLastName]', '2', NULL, '2015-02-19 07:30:00', 0, '2015-10-29 06:33:02', 0),
(5, 0, 2, 'This is sent when an admin creates a new user', 'Welcome!', 'Hello [firstName], <br> <br>I''m looking forward to sending you the information we discussed. Just wanted to make sure that I have your email address correct. Just click the link below to confirm, and I will send you the information right over.<br><br> [activationLink]<br><br> Thanks,<br> [senderFirstName] [senderLastName]', 'Hello [firstName] [lastName],\n\nWelcome to RapidFunnel. We have created an account for you. Please click on the link below and sign in using your email address and password provided. Once you are logged into your account, you can update your password and begin using your RapidFunnel account.\n\n[loginLink]\n\nTemporary Password: [password]\n\n Once you''ve updated your password, you can begin entering new contacts directly through the web based interface at the link above. Or you can use RapidFunnel while you are on the go by downloading the RapidFunnel App from either Google Play or the Apple Store. If you have any questions, please contact your application administrator.\n\nSincerely,\nThe RapidFunnel Team', '2', NULL, '2015-02-19 07:30:00', 0, '2015-10-29 06:33:02', 0);
/*!40000 ALTER TABLE `accountSystemEmails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountUser`
--

LOCK TABLES `accountUser` WRITE;
/*!40000 ALTER TABLE `accountUser` DISABLE KEYS */;
INSERT INTO `accountUser` (id, accountId, email, new, firstName, lastName, title, phoneNumber, created, createdBy, modified, modifiedBy, password,
accessToken, salt, forceChange, passwordResetSendTime, profileImage, address, suite, city, stateId, countryId, zip, roleId,
customRoleId, status, isCancelPremiumService, isDelete, isBillingContact, registrationCode, invitationDate, lastLogin,
linkTrackingNotify, additionalEmailNotification, repId, ignoreUserPayment, contactRecordsPerPage, resourceRecordsPerPage,
userRecordsPerPage, groupRecordsPerPage, campaignRecordsPerPage, broadcastRecordsPerPage, systemEmailsRecordsPerPage, currProgramRecordsPerPage,
pastProgramRecordsPerPage)
VALUES  (65,1,'sanjeev.kumar@mindfiresolutions.com',1,'Sanjeev', 'Kumar',NULL,NULL,'2015-10-24 20:00:49',NULL,'2015-10-24 20:00:49',NULL,'f2f1cb32d652f90d5186a37a58b7b99b',
NULL,'BBB4OB3TCiYy1KOIEvoLvg',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,
0,'1',0,0,0,NULL,NULL,'2015-10-24 20:00:49',
'1',NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(66,1,'invited-user@email.org',1,'John', 'User',NULL,NULL,'2015-10-24 20:00:49',NULL,'2015-10-24 20:00:49',NULL,'',NULL,'BBB4OB3TCiYy1KOIEvoLvg',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'1',0,0,0,'invited-code',NULL,'2015-10-24 20:00:49','1',NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
-- three free user in order to test the limits
(67,2,'free-user-1@email.org',1,'Free', 'User 1',NULL,NULL,'2015-10-24 20:00:49',NULL,'2015-10-24 20:00:49',NULL,'f2f1cb32d652f90d5186a37a58b7b99b',
NULL,'BBB4OB3TCiYy1KOIEvoLvg',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,
0,'1',0,0,1,NULL,NULL,'2015-10-24 20:00:49',
'1',NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(68,2,'free-user-2@email.org',1,'Free', 'User 2',NULL,NULL,'2015-10-24 20:00:49',NULL,'2015-10-24 20:00:49',NULL,'f2f1cb32d652f90d5186a37a58b7b99b',NULL,'BBB4OB3TCiYy1KOIEvoLvg',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,0,'1',0,0,1,NULL,NULL,'2015-10-24 20:00:49','1',NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(69,2,'free-user-3@email.org',1,'Free', 'User 3',NULL,NULL,'2015-10-24 20:00:49',NULL,'2015-10-24 20:00:49',NULL,'f2f1cb32d652f90d5186a37a58b7b99b',NULL,'BBB4OB3TCiYy1KOIEvoLvg',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'1',0,0,1,NULL,NULL,'2015-10-24 20:00:49','1',NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
-- testing user for checking retrieval of deleted users
(70,1,'deleted-user@email.org',1,'John', 'User',NULL,NULL,'2015-10-24 20:00:49',65,'2015-10-24 20:00:49',1,'',NULL,'BBB4OB3TCiYy1KOIEvoLvg',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,'1',0,1,0,'invited-code',NULL,'2015-10-24 20:00:49','1',NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `accountUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountUserCardProfile`
--

LOCK TABLES `accountUserCardProfile` WRITE;
INSERT INTO `accountUserCardProfile` VALUES ('1', '1', '65', 'sanjeev', NULL, NULL, 'sanjeev.kumar@mindfiresolutions.com', 'Mindfire solutions', '', 'bhubaneswar', '57', '0', '580031', '***********0005', 'AX', '7', '2025', '****', '28974247', '26320019', '2015-11-09 00:00:00', NULL, '0000-00-00 00:00:00', NULL, '2015-11-08', '2015-11-08', '2020-12-30', '0', 100, 100);
UNLOCK TABLES;

-- INSERT INTO `accountUserCardProfile` (`id`, `accountId`, `userId`, `firstName`, `middleName`, `lastName`, `email`, `address`, `suite`, `city`, `state`, `country`, `zip`, `maskedCcNo`, `ccType`, `ccExpMonth`, `ccExpYear`, `maskedCvv`, `authorizeProfileToken`, `authorizePaymentProfileToken`, `created`, `createdBy`, `modified`, `modifiedBy`, `firstCycleStartDate`, `lastPaymentDoneOn`, `nextPaymentOn`, `failureCount`) VALUES
-- (48, 1, 65, 'Test', NULL, NULL, 'user@email.org', '', '', '', '', '0', '', '', '', 1, 2050, '', '', NULL, '2015-11-07 23:40:30', NULL, '2015-11-07 23:41:50', NULL, NULL, NULL, '2020-12-30', 0);

--
-- Dumping data for table `accountUserGroup`
--

LOCK TABLES `accountUserGroup` WRITE;
/*!40000 ALTER TABLE `accountUserGroup` DISABLE KEYS */;
INSERT INTO `accountUserGroup` VALUES (1,1);
INSERT INTO `accountUserGroup` VALUES (65,1);
INSERT INTO `accountUserGroup` VALUES (67,2);
INSERT INTO `accountUserGroup` VALUES (68,3);
/*!40000 ALTER TABLE `accountUserGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `privilege`
--

LOCK TABLES `privilege` WRITE;
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;
INSERT INTO `privilege` VALUES (1,1,NULL,'No'),(2,1,NULL,'No');
/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
INSERT INTO `resource` VALUES (1,'admin',NULL),(2,'account',NULL);
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin','support-admin',1),(2,'support-admin',NULL,2);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Alabama','AL','USA'),(2,'Alaska','AK','USA'),(3,'Arizona','AZ','USA'),(4,'Arkansas','AR','USA'),(5,'California','CA','USA'),(6,'Colorado','CO','USA'),(7,'Connecticut','CT','USA'),(8,'Delaware','DE','USA'),(9,'Florida','FL','USA'),(10,'Georgia','GA','USA'),(11,'Hawaii','HI','USA'),(12,'Idaho','ID','USA'),(13,'Illinois','IL','USA'),(14,'Indiana','IN','USA'),(15,'Iowa','IA','USA'),(16,'Kansas','KS','USA'),(17,'Kentucky','KY','USA'),(18,'Louisiana','LA','USA'),(19,'Maine','ME','USA'),(20,'Maryland','MD','USA'),(21,'Massachusetts','MA','USA'),(22,'Michigan','MI','USA'),(23,'Minnesota','MN','USA'),(24,'Mississippi','MS','USA'),(25,'Missouri','MO','USA'),(26,'Montana','MT','USA'),(27,'Nebraska','NE','USA'),(28,'Nevada','NV','USA'),(29,'New Hampshire','NH','USA'),(30,'New Jersey','NJ','USA'),(31,'New Mexico','NM','USA'),(32,'New York','NY','USA'),(33,'North Carolina','NC','USA'),(34,'North Dakota','ND','USA'),(35,'Ohio','OH','USA'),(36,'Oklahoma','OK','USA'),(37,'Oregon','OR','USA'),(38,'Pennsylvania','PA','USA'),(39,'Rhode Island','RI','USA'),(40,'South Carolina','SC','USA'),(41,'South Dakota','SD','USA'),(42,'Tennessee','TN','USA'),(43,'Texas','TX','USA'),(44,'Utah','UT','USA'),(45,'Vermont','VT','USA'),(46,'Virginia','VA','USA'),(47,'Washington','WA','USA'),(48,'West Virginia','WV','USA'),(49,'Wisconsin','WI','USA'),(50,'Wyoming','WY','USA'),(51,'Washington DC','DC','USA'),(52,'Alberta ','AB','Canada'),(53,'British Columbia ','BC','Canada'),(54,'Manitoba ','MB','Canada'),(55,'New Brunswick ','NB','Canada'),(56,'Newfoundland and Labrador ','NL','Canada'),(57,'Nova Scotia ','NS','Canada'),(58,'Ontario ','ON','Canada'),(59,'Prince Edward Island ','PE','Canada'),(60,'Quebec ','QC','Canada'),(61,'Saskatchewan ','SK','Canada');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountIncentiveProgram`
--

LOCK TABLES `accountIncentiveProgram` WRITE;
/*!40000 ALTER TABLE `accountIncentiveProgram` DISABLE KEYS */;
INSERT INTO `accountIncentiveProgram` (`id`, `accountId`, `name`, `accountAwardTypeId`, `startDate`, `endDate`, `leadsGenerated`, `award`, `goal`, `topPercentage`, `subject`, `emailContent`, `from`, `created`, `modified`) VALUES
(1, 1, 'Incentive 1', 1, '2015-09-30', '0000-00-00', NULL, 'FJSSHSHSNK', NULL, 1, '', ' ', '', '2015-09-30 08:49:32', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `accountIncentiveProgram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountContactNotes`
--

LOCK TABLES `accountContactNotes` WRITE;
/*!40000 ALTER TABLE `accountContactNotes` DISABLE KEYS */;
INSERT INTO `accountContactNotes` (
`id` ,
`accountContactId` ,
`contactNote` ,
`noteTimeStamp` ,
`guestNote`
)
VALUES (
NULL , '1', 'This is a test note!', '2016-02-23 05:32:24', '0'
), (
NULL , '2', NULL , NULL , '0'
);

/*!40000 ALTER TABLE `accountContactNotes` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `accountMailLog` WRITE;
/*!40000 ALTER TABLE `accountMailLog` DISABLE KEYS */;
INSERT INTO `accountMailLog` (`messageId`, `accountId`, `recipientId`, `recipient`, `recipientType`, `Subject`, `sendDate`, `status`, `mailType`, `mailId`, `broadcastType`, `eventOccurredTime`) VALUES ('20160125231418.544.23715@broadcast.rapidfunnel.com', '1', '65', 'sanjeev.kumar@mindfiresolutions.com', '1', 'Hi', '2016-02-29 00:00:00', '1', 'broadcastEmail', '1', '1', '2016-02-29 00:00:00');
INSERT INTO `accountMailLog` (`messageId`, `accountId`, `recipientId`, `recipient`, `recipientType`, `Subject`, `sendDate`, `status`, `mailType`, `mailId`, `broadcastType`, `eventOccurredTime`) VALUES ('20160125231418.544.23715@broadcast.rapidfunnel.com', '1', '1', 'sanjeev.kumar@mindfiresolutions.com', '2', 'Hi', '2016-02-29 00:00:00', '1', 'broadcastEmail', '1', '1', '2016-02-29 00:00:00');
/*!40000 ALTER TABLE `accountMailLog` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `accountUserLegalshield` WRITE;
/*!40000 ALTER TABLE `accountUserLegalshield` DISABLE KEYS */;
INSERT INTO `accountUserLegalshield` VALUES ('3','65','113445134','ADAM KRAFT','krafty_one@hotmail.com','0','O:38:\"League\\OAuth2\\Client\\Token\\AccessToken\":4:{s:14:\"\0*\0accessToken\";s:730:\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2NjAzODBkY2IxZDM0ODkxYTE3Y2JlYWFkYjY1OTljNDllOWQ1ZDJiM2RlNjQyMTRhN2M2ZmEyNzE0ODdmYzU3IiwiYXVkIjoiaHR0cHM6Ly9hcGkuc3RhZ2luZy5sZWdhbHNoaWVsZC5jb20vYXV0aC90b2tlbiIsImlhdCI6MTQ3NTE2MzA2MywibmJmIjoxNDc1MTYzMDYzLCJleHAiOjE0OTA3MTUwNjMsImp0aSI6ImE0YzVmOTdmLTMwZWItNDg1Ny04ZjZiLTlhYzNiMDk4YTc5ZCIsImlzcyI6Imh0dHBzOi8vYXBpLnN0YWdpbmcubGVnYWxzaGllbGQuY29tIiwiYWNjb3VudF9pZCI6MTEzNDQ1MTM0LCJhY2NvdW50X3R5cGUiOiJBc3NvY2lhdGUiLCJjbGllbnRfdGFnIjoiOGMwMGU5YmZmZTQyNDg0ODllNzFhZjU4NmZhOTc2MTk5OTljZWFlNDE0ZDM0M2I2OTA1NTNkYTVkZjcyMmNlYWYwMzc5NWVlOGJiZTQ5ODE5MzYwODg2NDcyYzkzZmU4IiwiZGlzcG9zaXRpb24iOiJiZWFyZXIifQ.zpvAfPrq_SZQDPiIisnm1M8NfhTWPlBzT_MR4Zzh7kooyyWf-5glFOY3L6k9RjN8ouwii14bHV-9BPD55V_B9g\";s:10:\"\0*\0expires\";i:1490715063;s:15:\"\0*\0refreshToken\";s:739:\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2NjAzODBkY2IxZDM0ODkxYTE3Y2JlYWFkYjY1OTljNDllOWQ1ZDJiM2RlNjQyMTRhN2M2ZmEyNzE0ODdmYzU3IiwiYXVkIjoiaHR0cHM6Ly9hcGkuc3RhZ2luZy5sZWdhbHNoaWVsZC5jb20vYXV0aC90b2tlbiIsImlhdCI6MTQ3NTE2MzA2MywibmJmIjoxNDc1MTYzMDYzLCJleHAiOjE0OTE5MjQ2NjMsImp0aSI6IjQ0NGU1MGI2LTMyYjUtNDdmOS1hMTQ5LWQ0YzRlNWM0YmQ1MSIsImlzcyI6Imh0dHBzOi8vYXBpLnN0YWdpbmcubGVnYWxzaGllbGQuY29tIiwiYWNjb3VudF9pZCI6MTEzNDQ1MTM0LCJhY2NvdW50X3R5cGUiOiJBc3NvY2lhdGUiLCJjbGllbnRfdGFnIjoiOGMwMGU5YmZmZTQyNDg0ODllNzFhZjU4NmZhOTc2MTk5OTljZWFlNDE0ZDM0M2I2OTA1NTNkYTVkZjcyMmNlYWYwMzc5NWVlOGJiZTQ5ODE5MzYwODg2NDcyYzkzZmU4IiwiZGlzcG9zaXRpb24iOiJyZWZyZXNoX3Rva2VuIn0.SsCTzjeE7KrJe9gBczwwUX1bZOgFmPnGji9z7-8_2bT0CIO5CmFuRSk7Z3lAIi4qEYUFTK5SXrPifIuh3wvUHg\";s:18:\"\0*\0resourceOwnerId\";N;}','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2NjAzODBkY2IxZDM0ODkxYTE3Y2JlYWFkYjY1OTljNDllOWQ1ZDJiM2RlNjQyMTRhN2M2ZmEyNzE0ODdmYzU3IiwiYXVkIjoiaHR0cHM6Ly9hcGkuc3RhZ2luZy5sZWdhbHNoaWVsZC5jb20vYXV0aC90b2tlbiIsImlhdCI6MTQ3NTE2MzA2MywibmJmIjoxNDc1MTYzMDYzLCJleHAiOjE0OTE5MjQ2NjMsImp0aSI6IjQ0NGU1MGI2LTMyYjUtNDdmOS1hMTQ5LWQ0YzRlNWM0YmQ1MSIsImlzcyI6Imh0dHBzOi8vYXBpLnN0YWdpbmcubGVnYWxzaGllbGQuY29tIiwiYWNjb3VudF9pZCI6MTEzNDQ1MTM0LCJhY2NvdW50X3R5cGUiOiJBc3NvY2lhdGUiLCJjbGllbnRfdGFnIjoiOGMwMGU5YmZmZTQyNDg0ODllNzFhZjU4NmZhOTc2MTk5OTljZWFlNDE0ZDM0M2I2OTA1NTNkYTVkZjcyMmNlYWYwMzc5NWVlOGJiZTQ5ODE5MzYwODg2NDcyYzkzZmU4IiwiZGlzcG9zaXRpb24iOiJyZWZyZXNoX3Rva2VuIn0.SsCTzjeE7KrJe9gBczwwUX1bZOgFmPnGji9z7-8_2bT0CIO5CmFuRSk7Z3lAIi4qEYUFTK5SXrPifIuh3wvUHg','1490715063','0','2016-09-28 03:41:47','2016-09-29 09:31:03');
/*!40000 ALTER TABLE `accountUserLegalshield` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `accountUserLogs` WRITE;
/*!40000 ALTER TABLE `accountUserLogs` DISABLE KEYS */;
INSERT INTO `accountUserLogs`(`accountId`, `userId`, `userRoleId`, `totalContacts`, `totalActiveContacts`, `contactsThisMonth`, `activeContactsThisMonth`, `totalEmailsSent`, `totalEmailsSentThisMonth`, `monthlyContacts`, `monthlyEmails`, `monthlyClickedLinks`, `contacts30Days`, `totalUsers`, `totalPaidUsers`, `users45Days`, `contactsAssignedToCampaign`) VALUES (1,65,1,100,80,30,20,200,80,40,50,90,100,290,200,100,21);
/*!40000 ALTER TABLE `accountUserLogs` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `accountResource` WRITE;
/*!40000 ALTER TABLE `accountResource` DISABLE KEYS */;
INSERT INTO `accountResource` (`id`, `name`, `description`, `parent`) VALUES (NULL, 'account^broadcast-emails^index', 'Broadcast Emails', NULL);
INSERT INTO `accountResource` (`id`, `name`, `description`, `parent`) VALUES (NULL, 'account^user-payment^index', 'Payment History', NULL);
/*!40000 ALTER TABLE `accountResource` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `accountCampaignTempEmail` WRITE;
/*!40000 ALTER TABLE `accountCampaignTempEmail` DISABLE KEYS */;
INSERT INTO `accountCampaignTempEmail` (`id`, `accountId`, `campaignId`, `campaignEmailId`, `days`) VALUES ('1', '1', '1', '1', '1');
/*!40000 ALTER TABLE `accountCampaignTempEmail` ENABLE KEYS */;
UNLOCK TABLES;

UPDATE accountContact
SET noteCount = (SELECT count(acn.id) FROM accountContactNotes acn
WHERE acn.accountContactId = accountContact.id GROUP BY acn.accountContactId);

LOCK TABLES `accountUserWarnedSuspend` WRITE;
INSERT INTO `accountUserWarnedSuspend` (`userId`, `spamComplaintStatus`, `noOfSpamComplaints`, `deactivationSpamComplaints`, `spamWarnDate`, `spamDeactivateDate`, `lowOptinStatus`, `lowOptinWarnDate`, `lowOptinDeactivateDate`) VALUES ('65', '1', '2', '1', '2016-09-01', '2016-09-02', '0', '2016-09-01', '2016-09-02');
INSERT INTO `accountUserWarnedSuspend` (`userId`, `spamComplaintStatus`, `noOfSpamComplaints`, `deactivationSpamComplaints`, `spamWarnDate`, `spamDeactivateDate`, `lowOptinStatus`, `lowOptinWarnDate`, `lowOptinDeactivateDate`) VALUES ('66', '2', '2', '1', '2016-09-01', '2016-09-02', '2', '2016-09-01', '2016-09-02');
INSERT INTO `accountUserWarnedSuspend` (`userId`, `spamComplaintStatus`, `noOfSpamComplaints`, `deactivationSpamComplaints`, `spamWarnDate`, `spamDeactivateDate`, `lowOptinStatus`, `lowOptinWarnDate`, `lowOptinDeactivateDate`) VALUES ('67', '2', '2', '1', '2016-09-01', '2016-09-02', '1', '2016-09-01', '2016-09-02');
INSERT INTO `accountUserWarnedSuspend` (`userId`, `spamComplaintStatus`, `noOfSpamComplaints`, `deactivationSpamComplaints`, `spamWarnDate`, `spamDeactivateDate`, `lowOptinStatus`, `lowOptinWarnDate`, `lowOptinDeactivateDate`) VALUES ('68', '2', '1', '1', '2016-09-01', '2016-09-02', '2', '2016-09-01', '2016-09-02');
/*!40000 ALTER TABLE `accountUserWarnedSuspend` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `accountUserBillingInfo` WRITE;
/*!40000 ALTER TABLE `accountUserBillingInfo` DISABLE KEYS */;
INSERT INTO `accountUserBillingInfo` (`id`, `accountId`, `userId`, `isForCompany`, `stripeCustomerId`, `lastChargeId`, `name`, `email`, `address`, `city`, `stateId`, `countryId`, `zip`, `expMonth`, `expYear`, `last4`, `brand`, `funding`, `isSubscriptionActive`, `subscriptionId`, `subscriptionPlanId`, `subscriptionAmount`, `subscriptionCreated`, `subPaymentStartDate`, `lastPaymentDoneOn`, `lastPaidAmount`, `nextPaymentOn`, `failureCount`, `inActiveOrIgnoreBilling`, `created`, `createdBy`, `modified`, `modifiedBy`) VALUES
(1, 1, 425, 1, 'cus_9GSPPkZVAhbt0d', NULL, 'Mindfire Solutions', 'raj10@mailinator.com', 'Sukadev vihar', 'New Delhi', '', 'India', '110025', '12', '2018', '4242', 'Visa', 'credit', 0, NULL, NULL, 0.00, '2016-09-26 18:30:00', '0000-00-00', '0000-00-00', 0.00, NULL, 0, 0, '2016-09-26 09:33:54', NULL, '2016-09-27 08:01:18', NULL);
/*!40000 ALTER TABLE `accountUserBillingInfo` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `accountTransactions` WRITE;
/*!40000 ALTER TABLE `accountTransactions` DISABLE KEYS */;
INSERT INTO `accountTransactions` (`id`, `accountId`, `userId`, `eventId`, `eventType`, `stripeCustomerId`, `subscriptionId`, `subscriptionPlanId`, `invoiceNumber`, `paymentFor`, `last4CardNumber`, `amount`, `chargeId`, `paymentStatus`, `refundId`, `created`, `modified`) VALUES
(1, 6, 0, 'evt_dsfsdfsdfsdsdfsdf', 'sdfsdf', 'sdfsdfsdf', 'sub_sdkkjfnsdkfjsdf', 'sdfsdfsdfdsf', 'sdfsf', 'user', '4445', 10.00, 'ch_dfsdfsddfsdfsdfsdf', 4, NULL, '2016-10-21 09:50:20', '2016-10-26 14:00:32'),(2, 6, 0, 'evt_dsfsdsfsdfsdsdfsdf', 'sdsfsdf', 'sdsfsdfsdf', 'sub_sdkkjfnssdkfjsdf', 'sdfssdfsdfdsf', 'sdfssf', 'user', '4445', 10.00, 'ch_344dsdfsdfj', 4, NULL, '2016-10-21 09:51:00', '2016-10-26 14:00:47'),(3, 1, 65, 'evt_dsfsdsfsdfsdsdfsdfr', 'sdsfsdfssdf', 'sdsfsdfsdf', 'sub_sdkkjfnssdkfjsdfdsf', 'sdfssdfsdfdsf', 'sdfssf', 'user', '4445', 10.00, 'ch_344dsdfsdfjdfs', 5, NULL, '2016-10-21 09:51:00', '2016-10-26 14:00:37');
/*!40000 ALTER TABLE `accountTransactions` ENABLE KEYS */;
UNLOCK TABLES;


-- Update account user table for authorize payment
UPDATE accountUser au
JOIN accountUserCardProfile aucp ON aucp.userId = au.id
SET au.payThroughAuthNet = 1
WHERE au.status = 1 OR au.status = '1';

-- Update account table for authorize payment
UPDATE account a
JOIN accountCardProfile acp ON acp.accountId = a.id
SET a.payThroughAuthNet = 1
WHERE a.status = 1 OR a.status = '1';


--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'AS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua And Barbuda'),
(10, 'AR', 'Argentina');

INSERT INTO `states` (`id`, `name`, `countryId`) VALUES
(1, 'Andaman and Nicobar Islands', 101),
(2, 'Andhra Pradesh', 101),
(3, 'Arunachal Pradesh', 101),
(4, 'Assam', 101),
(5, 'Bihar', 101),
(6, 'Chandigarh', 101),
(7, 'Chhattisgarh', 101),
(8, 'Dadra and Nagar Haveli', 101),
(9, 'Daman and Diu', 101),
(10, 'Delhi', 101),
(11, 'Goa', 101),
(12, 'Gujarat', 101),
(13, 'Haryana', 101),
(14, 'Himachal Pradesh', 101),
(15, 'Jammu and Kashmir', 101);

UPDATE accountUserCardProfile ac
 JOIN state os ON os.id = ac.stateId
 JOIN states s ON os.name = s.name
SET ac.stateId = s.id,
ac.countryId = s.countryId;

UPDATE accountCardProfile ac
 JOIN state os ON os.id = ac.stateId
 JOIN states s ON os.name = s.name
SET ac.stateId = s.id,
ac.countryId = s.countryId;

LOCK TABLES `accountCommissionType` WRITE;
/*!40000 ALTER TABLE `accountCommissionType` DISABLE KEYS */;
INSERT INTO `accountCommissionType` (`id`, `name`) VALUES (1, 'Field Leader'), (2, 'Corporate'), (3, 'Affiliate partner'), (4, 'RF Account Manager'), (5, 'RF Salesperson');
/*!40000 ALTER TABLE `accountCommissionType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `accountPreference`
--

LOCK TABLES `accountPreference` WRITE;
/*!40000 ALTER TABLE `accountPreference` DISABLE KEYS */;
INSERT INTO `accountPreference` (`id`, `accountId`, `enableTwitterUrl`, `enableFacebookUrl`, `enableInstagramUrl`, `enableAdditionalNotificationEmail`, `enableRepId1`, `enableRepId2`, `enableTwitterUrlRequired`, `enableFacebookUrlRequired`, `enableInstagramUrlRequired`, `enableAdditionalNotificationEmailRequired`, `enableRepId1Required`, `enableRepId2Required`, `dateCreated`, `createdBy`, `dateModified`, `modifiedBy`) VALUES ('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2017-01-10 00:00:00', '1', '2017-01-10 00:00:00', NULL);
/*!40000 ALTER TABLE `accountPreference` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `accountFreeUserCampaigns`
--

LOCK TABLES `accountFreeUserCampaigns` WRITE;
/*!40000 ALTER TABLE `accountFreeUserCampaigns` DISABLE KEYS */;
INSERT INTO `accountFreeUserCampaigns` (`accountId`, `accountCampaignId`) VALUES('1', '1');
/*!40000 ALTER TABLE `accountFreeUserCampaigns` ENABLE KEYS */;
UNLOCK TABLES;
