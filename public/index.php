<?php

/**
 *  Index
 *
 * PHP version 5.4 or greater
 *
 * @category  Public
 * @package   Public
 * @copyright 2014 Digital Assets, Inc.
 * @license   Not licensed for external use
 */
//define if not already
defined('APP_PUBLIC_PATH') || define('APP_PUBLIC_PATH',
                realpath(dirname(__FILE__)));
defined('APP_PATH') || define('APP_PATH',
                realpath(dirname(__FILE__) . '/../app'));
defined('APP_LIB') || define('APP_LIB', realpath(dirname(__FILE__) . '/../lib'));

//set in the apache vhost config
defined('APP_ENV') || define('APP_ENV', getenv('ENVIRONMENT'));

// set APP_INI_PATH according to environment
defined('COMMON_CONFIG_INI_PATH') || define('COMMON_CONFIG_INI_PATH',
                APP_LIB . '/RapidFunnel/Config/commonConfig.ini');

switch (APP_ENV) {
    case 'Production' :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
                        APP_LIB . '/RapidFunnel/Config/app.ini');
        break;
    case 'Staging' :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
                        APP_LIB . '/RapidFunnel/Config/app_staging.ini');
        break;
    case 'QA' :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
                        APP_LIB . '/RapidFunnel/Config/app_qa.ini');
        break;
    case 'Development' :
    default :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
                        APP_LIB . '/RapidFunnel/Config/app_development.ini');
}


define('WEEK_TIMESTAMP', date("Y-m-d", strtotime('monday this week')));

//Bootstrap the application
require_once('Zend/Application.php');

//load zend_app with config file and run
$application = new Zend_Application(APP_ENV,
        array(
    'config' => array(
        COMMON_CONFIG_INI_PATH,
        APP_INI_PATH
    )
        )
);

$application->bootstrap()->run();
