<?php
$baseUrl = 'https://api.rapidfunnel.com';
$controller = 'account-contact-new';
?>
<!--<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
</head>-->
<div>
    <h2>Login</h2>
    <form id="login" method="post" action="<?php echo $baseUrl; ?>/api/login" >
        <input type="text" placeholder='username' name="username" value="" />
        <input type="password" placeholder='password' name="password" value="" />
        <input type="submit" value="submit"/>
    </form>
</div>
<hr />
<div>
    <h2>Login Using WebView Token</h2>
    <form id="login" method="post" action="<?php echo $baseUrl; ?>/api/login" >
        <input type="text" placeholder='token' name="webViewToken" value="" />
        <input type="submit" value="submit"/>
    </form>
</div>
<hr />
<div>
    <h2>Log Out</h2>
    <form id="login" method="post" action="<?php echo $baseUrl; ?>/api/login/logout-users" >
        <input type="text" placeholder='user Id' name="userId" value="" />
        <input type="text" placeholder='Auth Token' name="accessToken" value="" />
        <input type="submit" value="submit"/>
    </form>
</div>
<hr />
<div>
    <h2>Login to Web APP</h2>
    <form id="login" method="post" action="<?php echo $baseUrl; ?>/api-upgrade" >
        <input type="text" placeholder='user Id' name="id" value="" />
        <input type="text" placeholder='Auth Token' name="key" value="" />
        <input type="submit" value="submit"/>
    </form>
</div>
<hr />
<div>
    <h2>Forgot Password</h2>
    <form id="retrivePassword" method="post" action="<?php echo $baseUrl; ?>/api/login/forgot-password" >
        <input type="text" placeholder='email' name="email" value="" />
        <input type="submit" value="forgot password"/>
    </form>
</div>
<hr />
<div>
    <h2>Change Password</h2>
    <form id="changePassword" method="post" action="<?php echo $baseUrl; ?>/api/login/change-password" >
        <input type="password" placeholder='password' name="password" value="" />
        <input type="password" placeholder='confirm password' name="confirmPassword" value="" />
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="change"/>
    </form>
</div>
<hr />
<div>
    <h2>Get contactResourceCampaignRewardCount</h2>
    <form id="changePassword" method="post" action="<?php echo $baseUrl; ?>/api/login/contact-resource-campaign-reward-count" >
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="Submit"/>
    </form>
</div>
<hr />
<div>
    <h2>Add contact</h2>
    <form id="addContact" method="post"
          action="<?php echo $baseUrl; ?>/api/<?php echo $controller; ?>/add-contact">
        <input type="text" placeholder='first name' name="firstName"
               value=""/>
        <input type="text" placeholder='last name' name="lastName"
               value=""/><br>
        <input type="text" placeholder='primary email' name="email"
               value=""/>
        <input type="text" placeholder='home email' name="homeEmail"
               value=""/><br>
        <input type="text" placeholder='work email' name="workEmail"
               value=""/>
        <input type="text" placeholder='other email' name="otherEmail"
               value=""/><br>
        <input type="text" placeholder='mobile' name="phone" value=""/>
        <input type="text" placeholder='home phone' name="home" value=""/><br>
        <input type="text" placeholder='work phone' name="work" value=""/>
        <input type="text" placeholder='other phone' name="other" value=""/><br>
        <textarea placeholder='address1' name="address1"
                  value=""></textarea>
        <textarea placeholder='address2' name="address2"
                  value=""></textarea><br>
        <input type="text" placeholder='city' name="city" value=""/>
        <input type="text" placeholder='state' name="state" value=""/><br>
        <input type="text" placeholder='country' name="country" value=""/>
        <input type="text" placeholder='zip' name="zip" value=""/><br>
        <input type="text" placeholder='company' name="company" value=""/>
        <input type="text" placeholder='title' name="title" value=""/><br>
        <select name="interest">
            <option value="1">low</option>
            <option value="2">medium</option>
            <option value="3">high</option>
        </select><br>
        <?php
        if ('account-contact-new' == $controller) {
            for ($i = 1; $i < 3; $i++) { ?>
                <input type="text" placeholder='note' name="contactNotes[]"
                       value=""/>
                <input type="text" class="noteTimeStamp"
                       placeholder='note timestamp' name="noteTimeStamps[]"
                       value=""/>
                <br>
            <?php }
        } else { ?>
            <textarea placeholder='contactNotes' name="note"
                      value="" style="height: 50px;"></textarea>
            <input type="text" class="noteTimeStamp"
                   placeholder='note timestamp' name="noteTimeStamps[]"
                   value="" style="height: 50px; float: left"/>
        <?php } ?>
        <br>

        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken"
               value=""/><br>
        <input type="submit" value="Add Contact"/>
    </form>
</div>
<hr />
<div>
    <h2>Get Contacts</h2>
    <form id="getContact" method="post" action="<?php echo $baseUrl; ?>/api/<?php echo $controller; ?>/get-contact" >
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="Get Contacts"/>
    </form>
</div>
<hr />
<div>
    <h2>Search Contacts</h2>
    <form id="searchContact" method="post" action="<?php echo $baseUrl; ?>/api/<?php echo $controller; ?>/search-contact" >
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="text" placeholder='key to search' name="contactSearchKey" value="" />
        <input type="submit" value="Search Contacts"/>
    </form>
</div>
<hr />
<div>
    <h2>Get Contact Details</h2>
    <form id="getContactDetails" method="post" action="<?php echo $baseUrl; ?>/api/<?php echo $controller; ?>/get-contact-details" >
        <input type="text" placeholder='contact id' name="contactId" value="" />
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="Get Details"/>
    </form>
</div>
<hr />
<div>
    <h2>Update contact</h2>
    <form id="updateContact" method="post"
          action="<?php echo $baseUrl; ?>/api/<?php echo $controller; ?>/update-contact">
        <input type="text" placeholder='first name' name="firstName" value=""/>
        <input type="text" placeholder='last name' name="lastName"
               value=""/><br>
        <input type="text" placeholder='primary email' name="email" value=""/>
        <input type="text" placeholder='home email' name="homeemail"
               value=""/><br>
        <input type="text" placeholder='work email' name="workemail" value=""/>
        <input type="text" placeholder='other email' name="otheremail"
               value=""/><br>
        <input type="text" placeholder='mobile' name="phone" value=""/>
        <input type="text" placeholder='home phone' name="home" value=""/><br>
        <input type="text" placeholder='work phone' name="work" value=""/>
        <input type="text" placeholder='other phone' name="other" value=""/><br>
        <textarea placeholder='address1' name="address1" value=""></textarea>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <textarea placeholder='address2' name="address2"
                  value=""></textarea><br>
        <input type="text" placeholder='city' name="city" value=""/>
        <input type="text" placeholder='state' name="state" value=""/><br>
        <input type="text" placeholder='country' name="country" value=""/>
        <input type="text" placeholder='zip' name="zip" value=""/><br>
        <input type="text" placeholder='company' name="company" value=""/>
        <input type="text" placeholder='title' name="title" value=""/><br>
        <select name="interest">
            <option value="1">low</option>
            <option value="2">medium</option>
            <option value="3">high</option>
        </select><br>
        <textarea placeholder='note' name="note" value=""></textarea>
        <input type="text" class="noteTimeStamp"
               placeholder='note timestamp' name="noteTimeStamps[]"
               value="" style="height: 50px; float: left"/><br>
        <input type="text" placeholder='user id' name="userId" value=""/>
        <input type="text" placeholder='contact id' name="contactId"
               value=""/><br>
        <input type="text" placeholder='access token' name="accessToken"
               value=""/>
        <input type="submit" value="update"/>
    </form>
</div>
<hr />
<div>
    <h2>Delete contact</h2>
    <form id="deleteContact" method="post" action="<?php echo $baseUrl; ?>/api/<?php echo $controller; ?>/delete-contact" >
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="text" placeholder='contact id' name="contactId" value="" />
        <input type="submit" value="delete"/>
    </form>
</div>
<hr />
<div>
    <h2>Get Contact Notes</h2>
    <form id="getContactNotes" method="post"
          action="<?php echo $baseUrl; ?>/api/account-contact-new/get-contact-notes">
        <input type="text" placeholder='user id' name="userId" value=""/>
        <input type="text" placeholder='access token' name="accessToken"
               value=""/>
        <input type="submit" value="Get Contacts Notes"/>
    </form>
</div>
<hr/>

<div>
    <h2>Get Contact Note Details</h2>
    <form id="getContactNoteDetails" method="post"
          action="<?php echo $baseUrl; ?>/api/account-contact-new/get-contact-note-details">
        <input type="text" placeholder='user id' name="userId" value=""/>
        <input type="text" placeholder='contact id' name="contactId" value=""/>
        <input type="text" placeholder='access token' name="accessToken"
               value=""/>
        <input type="submit" value="Get Contact Note Details"/>
    </form>
</div>
<hr/>
<div>
    <h2>Add contact Notes</h2>
    <form id="updateContactNotes" method="post"
          action="<?php echo $baseUrl; ?>/api/account-contact-new/add-contact-notes">
        <input type="text" placeholder='user id' name="userId" value=""/><br>
        <input type="text" placeholder='contact id' name="contactId"
               value=""/><br>
        <input type="text" placeholder='access token' name="accessToken"
               value=""/>
        <br>
        <?php for ($i = 1; $i < 3; $i++) { ?>
            <input type="text" placeholder='note' name="contactNotes[]"
                   value=""/>
            <input type="text" class="noteTimeStamp"
                   placeholder='note timestamp' name="noteTimeStamps[]"
                   value=""/>
            <br>
        <?php } ?>
        <input type="submit" value="addNotes"/>
    </form>
</div>
<hr/>

<div>
    <h2>Update contact Notes</h2>
    <form id="updateContactNotes" method="post"
          action="<?php echo $baseUrl; ?>/api/account-contact-new/update-contact-notes">
        <input type="text" placeholder='user id' name="userId" value=""/><br>
        <input type="text" placeholder='contact id' name="contactId"
               value=""/><br>
        <input type="text" placeholder='access token' name="accessToken"
               value=""/>
        <br>
        <?php for ($i = 1; $i < 3; $i++) { ?>
            <input type="text" placeholder='note Id' name="noteIds[]" value=""/>
            <input type="text" placeholder='note' name="contactNotes[]"
                   value=""/>
            <input type="text" class="noteTimeStamp"
                   placeholder='note timestamp' name="noteTimeStamps[]"
                   value=""/>
            <br>
        <?php } ?>
        <input type="submit" value="updateNotes"/>
    </form>
</div>
<hr/>

<div>
    <h2>Delete contact Notes</h2>
    <form id="deleteContactNotes" method="post"
          action="<?php echo $baseUrl; ?>/api/account-contact-new/delete-contact-notes">
        <input type="text" placeholder='user id' name="userId" value=""/><br>
        <input type="text" placeholder='contact id' name="contactId"
               value=""/><br>
        <input type="text" placeholder='access token' name="accessToken"
               value=""/>
        <br>
        <?php for ($i = 1; $i < 3; $i++) { ?>
            <input type="text" placeholder='note Id' name="noteIds[]" value=""/>
            <br>
        <?php } ?>
        <input type="submit" value="deleteNotes"/>
    </form>
</div>
<hr/>

<div>
    <h2>Get Resources</h2>
    <form id="getResource" method="post" action="<?php echo $baseUrl; ?>/api/account-resource/get-resource" >
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="text" placeholder='Contact Id' name="contactId" value="" />
        <input type="submit" value="Get"/>
    </form>
</div>
<hr />
<div>
    <h2>Get Campaigns</h2>
    <form id="getCampaigns" method="post" action="<?php echo $baseUrl; ?>/api/account-campaign/get-campaigns" >
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="Get"/>
    </form>
</div>
<hr />
<div>
    <h2>Get Campaign Details</h2>
    <form id="getCampaignDetails" method="post" action="<?php echo $baseUrl; ?>/api/account-campaign/get-campaign-details" >
        <input type="text" placeholder='campaign id' name="campaignId" value="" />
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="Get Details"/>
    </form>
</div>
<hr />
<div>
    <h2>Assign Contact To Campaign</h2>
    <form id="assignContactToCampaign" method="post" action="<?php echo $baseUrl; ?>/api/account-campaign/assign-contact-to-campaign" >
        <input type="text" placeholder='campaign id' name="campaignId" value="" />
        <input type="text" placeholder='contact id' name="contactId" value="" />
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="assign"/>
    </form>
</div>
<hr />
<div>
    <h2>Validate User Registration</h2>
    <form id="validateUserRegistration" method="post" action="<?php echo $baseUrl; ?>/api/login/validate-user-registration" >
        <input type="text" placeholder='email' name="email" value="" />
        <input type="text" placeholder='registration code' name="registrationCode" value="" />
        <input type="submit" value="validate"/>
    </form>
</div>
<hr />
<div>
    <h2>get new password</h2>
    <form id="getNewPassword" method="post" action="<?php echo $baseUrl; ?>/api/login/get-new-password" >
        <input type="text" placeholder='email' name="email" value="" />
        <input type="text" placeholder='registration code' name="registrationCode" value="" />
        <input type="text" placeholder='password' name="password" value="" />
        <input type="text" placeholder='confirm password' name="confirmPassword" value="" />
        <input type="submit" value="new password"/>
    </form>
</div>
<hr />
<div>
    <h2>Get Branding Details</h2>
    <form id="getBrandingDetails" method="post" action="<?php echo $baseUrl; ?>/api/account-branding/get-details" >
        <input type="text" placeholder='user id' name="userId" value="" />
        <input type="text" placeholder='access token' name="accessToken" value="" />
        <input type="submit" value="Get"/>
    </form>
</div>
<div>
    <h2>Get Progress Award Details</h2>
    <form method="post" action="<?php echo $baseUrl; ?>/api/account-award/get-current-award-detail" >
        <input type="text" placeholder='user id' name="userId" value="65" />
        <input type="text" placeholder='access token' name="accessToken" value=""/>
        <input type="text" placeholder='incentive id' name="incentiveId" value="" />
        <input type="submit" value="Get"/>
    </form>
</div>
<div>
    <h2>Get Past Award Details</h2>
    <form method="post" action="<?php echo $baseUrl; ?>/api/account-award/get-past-award-detail" >
        <input type="text" placeholder='user id' name="userId" value="65" />
        <input type="text" placeholder='access token' name="accessToken" value=""/>
        <input type="text" placeholder='incentive id' name="incentiveId" value="" />
        <input type="submit" value="Get"/>
    </form>
</div>
<div>
    <h2>Get Incentive Details</h2>
    <form method="post" action="<?php echo $baseUrl; ?>/api/account-award/get-incentives" >
        <input type="text" placeholder='user id' name="userId" value="65" />
        <input type="text" placeholder='access token' name="accessToken" value=""/>
        <input type="text" placeholder='Running Status' name="runningStatus" value="" />
        <input type="submit" value="Get"/>
    </form>
</div>
<div>
    <h2>Get leads in date range</h2>
    <form method="post" action="<?php echo $baseUrl; ?>/api/account-award/get-generated-leads-by-date-range" >
        <input type="text" placeholder='user id' name="userId" value="65" />
        <input type="text" placeholder='access token' name="accessToken" value=""/>
        <input type="text" placeholder='start date' name="startDate" value=""/>
        <input type="text" placeholder='end date' name="endDate" value="" />
        <input type="submit" value="Get"/>
    </form>
</div>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
    $(function() {

        function todayDate() {
            "use strict";
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            var hours = today.getHours();
            var minutes = today.getMinutes();
            var seconds = today.getSeconds();

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            return yyyy + '-' + mm + '-' + dd + ' ' + hours + ':' + minutes + ':' + seconds;
        }

        $(".noteTimeStamp").each(function() {
            $(this).val(todayDate);
        });
    });
</script>
