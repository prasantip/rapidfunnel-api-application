<?php
/**
 * RapidFunnel_Model_AccountUserGroupTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountUserGroupTest
 * RapidFunnel/Model/AccountUserGroupTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountUserGroupTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;

    /**
     * RapidFunnel_Model_AccountUserGroupTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountUserGroup();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUserGroup.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountUserGroup::__construct()
     *
     * @return void
     */
    public function testAccountUserGroupModel()
    {
        $this->_model = new RapidFunnel_Model_AccountUserGroup();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountUserGroup',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountUserGroup::getGroupByUserId()
     *
     * Tests if the function getGroupByUserId() returns a record
     * from accountUserGroup associated with the userId
     *
     * @return void
     */
    public function testGetGroupByUserId()
    {
        $results = $this->_model->getGroupByUserId(ADMIN_ID);
        $this->assertNotCount(0, $results);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserGroup::getGroupByUserId()
     *
     * Tests if the function getGroupByUserId() returns nothing
     * if incorrect userId is given
     *
     * @return void
     */
    public function testGetGroupByUserIdWithIncorrectInput()
    {
        $results = $this->_model->getGroupByUserId(ADMIN_WRONG_ID);
        $this->assertEmpty($results);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserGroup::getGroupByUserId()
     *
     * Tests for exception in the function getGroupByUserId()
     *
     * @return void
     */
    public function testGetGroupByUserIdWithException()
    {
        $results = $this->_model->getGroupByUserId(null);
        $this->assertFalse($results);
    }

}
