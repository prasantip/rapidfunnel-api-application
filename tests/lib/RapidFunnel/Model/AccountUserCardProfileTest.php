<?php
/**
 * RapidFunnel_Model_AccountUserCardProfileTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountUserCardProfileTest
 * RapidFunnel/Model/AccountUserCardProfileTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

class RapidFunnel_Model_AccountUserCardProfileTest
    extends PHPUNit_Framework_TestCase
{
    private $_model;

    /**
     * RapidFunnel_Model_AccountUserCardProfileTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountUserCardProfile();
    }

    /**
     * Tests RapidFunnel_Model_AccountUserCardProfileTest::__Contractor()
     *
     * Tests the Contractor() of AccountUserCardProfile Model
     *
     * @return void
     */
    public function testAccountUserCardProfileContractor()
    {
        $this->_model = new RapidFunnel_Model_AccountUserCardProfile();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountUserCardProfile',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountUserCardProfileTest::testGetDetailByUserId()
     *
     * Tests SELECT operation of the getDetailByUserId() method
     *
     * @return void
     */
    public function testGetDetailByUserId()
    {
        $returnResult = $this->_model->getDetailByUserId(TEMP_USER_ID2);

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnResult);
        $this->assertArrayHasKey('id', $returnResult);
        $this->assertArrayHasKey('accountId', $returnResult);
        $this->assertArrayHasKey('userId', $returnResult);
        $this->assertEquals(TEMP_USER_ID2, $returnResult['userId']);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserCardProfileTest::testGetDetailByUserIdWithException()
     *
     * This test case generates an exception in getDetailByUserId
     *
     * @return void
     */
    public function testGetDetailByUserIdWithException()
    {
        $returnResult = $this->_model->getDetailByUserId(null);

        $this->assertFalse($returnResult);
    }

}