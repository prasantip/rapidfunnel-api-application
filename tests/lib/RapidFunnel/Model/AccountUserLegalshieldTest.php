<?php

/**
 * Unit test class for RapidFunnel_Model_AccountUserLegalshield
 *
 * @category Test
 * @package  RapidFunnnel_Model
 */
use League\OAuth2\Client\Token\AccessToken;
class RapidFunnel_Model_AccountUserLegalshieldTest extends
    RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    private $_model;

    /**
     * Constructor method for class. Model wise required data initialized here
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountUserLegalshield();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUserLegalshield.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);

        return $compositeDs;
    }

    public function testAccountUserLegalshield()
    {
        $model = new RapidFunnel_Model_AccountUserLegalshield();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountUserLegalshield',
            $model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountUserLegalshield::getLegalshieldInfo
     *
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetLegalshieldInfoWithException()
    {
        $this->_model->getLegalshieldInfo(null);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserLegalshield::getLegalshieldInfo
     *
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetLegalshieldInfoWithInvalidUserId()
    {
        $userId = "invalid format";
        $this->_model->getLegalshieldInfo($userId);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserLegalshield::getLegalshieldInfo
     *
     *
     * @return void
     */
    public function testGetLegalshieldInfoWithValidUserId()
    {
    //Testing for valid user id for which legalshield info exists
        $results = $this->_model->getLegalshieldInfo(ACCOUNT_USER_ID);

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertNotEmpty($results);

        if (is_array($results)) {
            // check array has necessary number of elements
            $this->assertCount(5, $results);

            // check that associateName key exists and it contains entities
            $this->assertArrayHasKey('associateName', $results);
            $this->assertEquals('SANJEEV KUMAR', $results['associateName']);

            // check that associateEmail key exists and it contains entities
            $this->assertArrayHasKey('associateEmail', $results);
            $this->assertEquals(
                'sanjeev.kumar@mindfiresolutions.com',
                $results['associateEmail']
            );

            // check that advantageStatus key exists and it contains entities
            $this->assertArrayHasKey('advantageStatus', $results);
            $this->assertEquals(0, $results['advantageStatus']);

            // check that associateNumber key exists and it contains entities
            $this->assertArrayHasKey('associateNumber', $results);
            $this->assertEquals(ASSOCIATE_NUMBER, $results['associateNumber']);
        }
    }

    /**
     * Tests RapidFunnel_Model_AccountUserLegalshield::getLegalshieldInfo
     *
     *
     * @return void
     */
    public function testGetLegalshieldInfoWithWrongUserId()
    {
        $userId = 111;

        // Test with unknown value(wrong value)
        $returnResult = $this->_model->getLegalshieldInfo($userId);

        $this->assertFalse($returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserLegalshield::getAdvantageStatus()
     *
     * @return void
     */
    public function testGetAdvantageStatus()
    {
        $result = $this->_model->getAdvantageStatus(
            serialize(
                new AccessToken(array('access_token' => 'TEST_ACCESS_TOKEN'))
            )
        );
        $this->assertEquals($result, -1);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserLegalshield::getAdvantageStatus()
     *
     * @expectedException Exception
     *
     * @return void
     */
    public function testGetAdvantageStatusWithNullAccessToken()
    {
        $this->_model->getAdvantageStatus(null);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserLegalshield::getAdvantageStatus()
     *
     * @expectedException Exception
     *
     * @return void
     */
    public function testGetAdvantageStatusWithInvalidAccessToken()
    {
        $this->_model->getAdvantageStatus(123);
    }

}