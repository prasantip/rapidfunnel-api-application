<?php
/**
 * RapidFunnel_Model_AbstractTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2016 Rapidfunnel Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/
 */

class RapidFunnel_Model_AbstractTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */

    public function getDataSet()
    {

        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);

        return $compositeDs;
    }


    /**
     * Tests RapidFunnel_Model_Abstract::load()
     *
     * @return void
     */
    public function testLoad()
    {
        $userModel = new RapidFunnel_Model_AccountUser();
        $key = array('id');

        $result = $userModel->load(ADMIN_ID, $key);
        $this->assertTrue($result);
    }

    /**
     * Tests RapidFunnel_Model_Abstract::load()
     *
     * @return void
     * @expectedException Exception
     */
    public function testLoadWithExceptionThree()
    {
        $userModel = new RapidFunnel_Model_AccountUser();
        $key = array('id');
        $userModel->load(ADMIN_ID, $key, $key);
    }

    /**
     * Tests RapidFunnel_Model_Abstract::load()
     *
     * @return void
     * @expectedException Exception
     */
    public function testLoadWithExceptionTwo()
    {
        $userModel = new RapidFunnel_Model_AccountUser();
        $userModel->load(ADMIN_ID, KEY);
    }

    /**
     * Tests RapidFunnel_Model_Abstract::load()
     *
     * @return void
     * @expectedException Exception
     */
    public function testLoadWithExceptionOne()
    {
        $userModel = new RapidFunnel_Model_AccountUser();
        $userModel->load(null);
    }

}
