<?php
/**
 * RapidFunnel_Model_AccountTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountTest
 * RapidFunnel/Model/AccountTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;
    public $returnResultForTestGetGroupsByAccountId
        = array('1'=>'ABC', '2'=>'GHI');

    /**
     * RapidFunnel_Model_AccountTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_Account();
    }


    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/Account.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_Account::__construct()
     *
     * @return void
     */
    public function testAccountModel()
    {
        $this->_model = new RapidFunnel_Model_Account();
        $this->assertInstanceOf('RapidFunnel_Model_Account', $this->_model);
    }

    /**
     * Tests RapidFunnel_Model_Account::getAccountDetail()
     *
     * @return void
     */
    public function testGetAccountDetail()
    {
        $result = $this->_model->getAccountDetail(ACCOUNT_ID);

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);

        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('accountLevel', $result);
        $this->assertArrayHasKey('ignoreAccountPayment', $result);
        $this->assertArrayHasKey('passThrough', $result);
        $this->assertArrayHasKey('trial', $result);
        $this->assertArrayHasKey('trialPeriod', $result);
        $this->assertArrayHasKey('trialUserEnabled', $result);
        $this->assertArrayHasKey('userTrialPeriod', $result);
        $this->assertArrayHasKey('userToken', $result);
        $this->assertArrayHasKey('basePrice', $result);
        $this->assertArrayHasKey('basePricePlanId', $result);
        $this->assertArrayHasKey('subscriptionPlanId', $result);
        $this->assertArrayHasKey('payThroughAuthNet', $result);

        $this->assertNotNull($result['id']);
        $this->assertNotNull($result['name']);
        $this->assertNotNull($result['status']);
        $this->assertNotNull($result['accountLevel']);
        $this->assertNotNull($result['ignoreAccountPayment']);
        $this->assertNotNull($result['passThrough']);
        $this->assertNotNull($result['trial']);
        $this->assertNotNull($result['trialPeriod']);
        $this->assertNotNull($result['trialUserEnabled']);
        $this->assertNotNull($result['userTrialPeriod']);
        $this->assertNotNull($result['userToken']);
        $this->assertNotNull($result['basePrice']);
        $this->assertNotNull($result['payThroughAuthNet']);
    }

    /**
     * Tests RapidFunnel_Model_Account::getAccountDetailt()
     *
     * Test to get account details with wrong accountId
     *
     * @return void
     */
    public function testGetAccountDetailWithWrongAccountId()
    {
        $result = $this->_model->getAccountDetail(WRONG_ACCOUNT_ID);
        $this->assertFalse($result);
    }

    /**
     * Tests RapidFunnel_Model_Account::getAccountDetailt()
     *
     * This test case generates an Exception in getAccountDetail()
     *
     * @return void
     *
     * @expectedException Exception
     */
    public function testGetAccountDetailWithException()
    {
        $result = $this->_model->getAccountDetail(null, null);
        $this->assertFalse($result);
    }

    /**
     * Tests RapidFunnel_Model_Account::getAccountDetail()
     *
     * This test case generates an Exception in getAccountDetail()
     * if a large number is passed as id
     *
     * @return void
     */
    public function testGetAccountDetailWithLargeNumber()
    {
        $result = $this->_model->getAccountDetail(LARGE_NUMBER, null);
        $this->assertFalse($result);
    }

}
