<?php
/**
 * RapidFunnel_Model_AccountCampaignTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountCampaignTest
 * RapidFunnel/Model/AccountCampaignTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountCampaignTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;
    private $_groupIds = array(ADMIN_GROUP_ID);
    private $_userGroupIds = array(USER_GROUP_ID);
    private $_alterAccountId = 2;
    public  $getParams = array(
                'sEcho'=>'1',
                'sSearch'=>'Hello',
                'bSearchable_0'=>true,
                'iDisplayStart'=>0,
                'iSortingCols'=>1,
                'iDisplayLength'=>1,
                'iSortCol_0'=>0,
                'bSortable_0'=>true,
                'sSortDir_0'=>"asc"
    );
    private $_testSearch = Array(
        'module' => 'account',
        'controller' => 'campaign',
        'action' => 'get-campaigns',
        'sEcho' => 3,
        'iColumns' => 6,
        'sColumns' => '',
        'iDisplayStart' => 0,
        'iDisplayLength' => 10,
        'mDataProp_0' => 0,
        'mDataProp_1' => 1,
        'mDataProp_2' => 2,
        'mDataProp_3' => 3,
        'mDataProp_4' => 4,
        'mDataProp_5' => 5,
        'sSearch' => 'He',
        'bRegex' => false,
        'sSearch_0' => '',
        'bRegex_0' => false,
        'bSearchable_0' => true,
        'sSearch_1' => '',
        'bRegex_1' => false,
        'bSearchable_1' => true,
        'sSearch_2]' => '',
        'bRegex_2' => false,
        'bSearchable_2' => true,
        'sSearch_3' => '',
        'bRegex_3' => false,
        'bSearchable_3' => true,
        'sSearch_4' => '',
        'bRegex_4' => false,
        'bSearchable_4' => true,
        'sSearch_5' => '',
        'bRegex_5' => false,
        'bSearchable_5' => false,
        'iSortCol_0' => 0,
        'sSortDir_0' => 'asc',
        'iSortingCols' => 1,
        'bSortable_0' => true,
        'bSortable_1' => true,
        'bSortable_2' => true,
        'bSortable_3' => true,
        'bSortable_4' => true,
        'bSortable_5' => false,
        '_' => 1405422947853,
    );
    private $_accountId = 1;
    private $_groupId = 1;
    private $_adminRoleId = 2;
    private $_userRoleId = 3;
    private $_campaignId = 1;
    private $_newCampaignId = 2;
    private $_newAccountId = 1;
    private $_wrongCampaignId = 0;
    private $_wrongAccountId = 0;

    /**
     * RapidFunnel_Model_AccountCampaignTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountCampaign();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountCampaign.xml'
        );
        $ds4 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds4);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaign::__construct()
     *
     * @return void
     */
    public function testAccountCampaignModel()
    {
        $this->_model = new RapidFunnel_Model_AccountCampaign();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountCampaign',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaign::
     * campaignAccessPermissionForFreeUser() method
     *
     * Functionality to test SELECT operation
     *
     * @return void
     */
    public function testCampaignAccessPermissionForFreeUser()
    {
        $campaignId = 1;

        $returnResult = $this->_model->campaignAccessPermissionForFreeUser(
            $campaignId,
            ACCOUNT_ID
        );

        $this->assertGreaterThan(0, $returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaign::
     * campaignAccessPermissionForFreeUser() method
     *
     * Functionality to test SELECT operation with wrong inputs
     *
     * @return void
     */
    public function testCampaignAccessPermissionForFreeUserWithWrongInputs()
    {
        $campaignId = 1;

        $returnResult = $this->_model->campaignAccessPermissionForFreeUser(
            $campaignId,
            WRONG_ACCOUNT_ID
        );

        $this->assertEquals(0, $returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaign::
     * campaignAccessPermissionForFreeUser() method
     *
     * This method generates an exception in
     * campaignAccessPermissionForFreeUser()
     *
     * @return void
     */
    public function testCampaignAccessPermissionForFreeUserWithException()
    {
        $returnResult = $this->_model->campaignAccessPermissionForFreeUser(
            null,
            null
        );

        $this->assertFalse($returnResult);
    }

    /**
     * RapidFunnel_Model_AccountCampaign::getCampaignDetails
     *
     * @note: Test get Campaign details by campaignId
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetCampaignDetailsWithInValidValue()
    {
        $this->_model->getCampaignDetails(null, CAMPAIGN_ID);
    }


    /**
     * RapidFunnel_Model_AccountCampaign::getCampaignDetails
     *
     * @note: Test get Campaign details by userId
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetCampaignDetailsWithInvalidCampaign()
    {
        $this->_model->getCampaignDetails(TEMP_USER_ID, null);
    }


    /**
     * RapidFunnel_Model_AccountCampaign::getCampaignDetails
     *
     * @note: Test get Campaign details
     *
     * @return void
     */
    public function testGetCampaignDetailsWithRightInputs()
    {
        $getDetail = $this->_model->getCampaignDetails(
            TEMP_USER_ID, CAMPAIGN_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $getDetail);
        if (is_arraY($getDetail)) {
            $this->assertArrayHasKey('id', $getDetail);
        }
    }


    /**
     * RapidFunnel_Model_AccountCampaign::getCampaignDetails
     *
     * @note: Test get Campaign details with wrong inputs
     *
     * @return void
     */
    public function testGetCampaignDetailsWithWrongInputs()
    {
        $get = $this->_model->getCampaignDetails(
            TEMP_USER_ID, WRONG_CAMPAIGN_ID
        );
        $this->assertEquals(null, $get);
    }

    /**
     * RapidFunnel_Model_AccountCampaign::getCampaignDetails
     *
     * @return void
     */
    public function testGetCampaignDetailsWithException()
    {
        $get = $this->_model->getCampaignDetails(
            LARGE_NUMBER, WRONG_CAMPAIGN_ID
        );
        $this->assertFalse($get);
    }

    /**
     * Functionality to test remove Campaign
     */
    public function testRemoveCampaign()
    {
        $return = $this->_model->removeCampaign(null);
        $this->assertFalse($return);
    }

    /**
     * Functionality to test remove Campaign
     */
    public function testRemoveCampaignContact()
    {
        $returnResult = $this->_model->removeCampaign(CONTACT_ID);

        $this->assertTrue($returnResult);
    }

     /** Tests RapidFunnel_Model_AccountCampaign::
     * getAllCampaignsForApp() method
     *
     * @expectedException Exception
     */
    public function testGetAllCampaignsForAppWithWrongUserID()
    {
        $this->_model->getAllCampaignsForApp(null, null, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaign::
     * getAllCampaignsForApp() method
     *
     * @expectedException Exception
     */
    public function testGetAllCampaignsForAppWithWrongAccountId()
    {
        $this->_model->getAllCampaignsForApp(USER_ID, null, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaign::
     * getAllCampaignsForApp() method
     *
     * @expectedException Exception
     */
    public function testGetAllCampaignsForAppWithWrongUserDetails()
    {
        $this->_model->getAllCampaignsForApp(USER_ID, ACCOUNT_ID, null);
    }

}