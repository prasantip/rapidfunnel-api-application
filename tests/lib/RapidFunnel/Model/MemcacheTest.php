<?php
/**
 * RapidFunnel_Model_MemcacheTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_MemcacheTest
 * RapidFunnel/Model/MemcacheTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */


class RapidFunnel_Model_MemcacheTest extends PHPUNit_Framework_TestCase
{

    private $_model;

    /**
     * RapidFunnel_Model_UserTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_Memcache();
    }

    /**
     * Tests RapidFunnel_Model_Memcache::__construct()
     *
     * @return void
     */
    public function testMemcache()
    {
        $this->_model = new RapidFunnel_Model_Memcache();
        $this->assertInstanceOf('RapidFunnel_Model_Memcache', $this->_model);
    }

}