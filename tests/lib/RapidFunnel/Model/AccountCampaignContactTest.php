<?php
/**
 * RapidFunnel_Model_AccountCampaignContactTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountCampaignContactTest
 * RapidFunnel/Model/AccountCampaignContactTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountCampaignContactTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;

    /**
     * RapidFunnel_Model_AccountCampaignContactTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountCampaignContact();
    }

    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountContact.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds3 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountCampaignEmail.xml'
        );
        $ds4 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountCampaign.xml'
        );


        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);
        $compositeDs->addDataSet($ds3);
        $compositeDs->addDataSet($ds4);

        return $compositeDs;
    }


    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::__construct()
     *
     * @return void
     */
    public function testAccountCampaignContactModel()
    {
        $this->_model = new RapidFunnel_Model_AccountCampaignContact();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountCampaignContact',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::getCampaignByContact()
     *
     * @return void
     */
    public function testGetCampaignByContact()
    {
        $campaigns = $this->_model->getCampaignByContact(CONTACT_ID);
        $this->assertNotCount(0, $campaigns);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::getCampaignByContact()
     *
     * @note: Tests getCampaignByContact() with wrong inputs
     *
     * @return void
     */
    public function testGetCampaignByContactWithWrongValues()
    {
        $campaigns = $this->_model->getCampaignByContact(CONTACT_WRONG_ID);
        $this->assertFalse($campaigns);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::getCampaignByContact()
     *
     * @note: Test case generates an Exception in getCampaignByContact()
     *
     * @return void
     */
    public function testGetCampaignByContactWithException()
    {
        $campaigns = $this->_model->getCampaignByContact(null);
        $this->assertFalse($campaigns);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::updateCampaignContact()
     *
     * Tests for Exception in the function updateCampaignContact()
     *
     * @return void
     */
    public function testSendOptInEmail()
    {
        $optInArr = array(
            'contactId'=> '1', 'accountCampaignId' => '1',
            'senderFirstName'=>'sachin', 'senderLastName'=>'sehwag',
            'accountId' => '1', 'accountName' => 'Mindfire',
            'firstName' => 'sanjeev', 'lastName' => '','company' => '',
            'email' => 'sanjeev.kumar@mindfiresolutions.com',
            'senderEmail' =>'sanjeev.kumar1@mindfiresolutions.com',
            'contactPhone' =>'555-555-555, 666-666-666',
            'phone' =>'', 'repId' =>'REP123', 'repId2' =>'REP2123',
            'senderId' => '65','title' => 'New OptIn',
            'senderFacebookUrl' => 'facebook.com', 'senderTwitterUrl' => 'twitter',
            'senderInstagramUrl' => 'instagram'
        );
        $result = $this->_model->sendOptInEmail($optInArr);

        $this->assertNotEmpty($result['to']);
        $this->assertNotEmpty($result['subject']);
        $this->assertNotEmpty($result['html']);
        $this->assertNotEmpty($result['text']);
        $this->assertNotEmpty($result['from']);
        $this->assertNotEmpty($result['v:custom-data']);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * isCampaignAssignToContact()
     *
     * @return void
     */
    public function testIsCampaignAssignToContact()
    {
        $returnResult = $this->_model->isCampaignAssignToContact(CONTACT_ID);
        $this->assertEquals(CAMPAIGN_ID, $returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * isCampaignAssignToContact()
     *
     * @return void
     */
    public function testIsCampaignAssignToContactWithWrongInput()
    {
        $returnResult = $this->_model->isCampaignAssignToContact(LARGE_NUMBER);
        $this->assertFalse($returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * isCampaignAssignToContact()
     *
     * @return void
     * @expectedException Exception
     */
    public function testIsCampaignAssignToContactWithException()
    {
        $this->_model->isCampaignAssignToContact(null);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * createOptInEmail()
     *
     * @return void
     */
    public function testCreateOptInEmail()
    {
        $accountContact = new RapidFunnel_Model_AccountContact();
        $accountContact->load(CONTACT_ID);

        $returnResult = $this->_model->createOptInEmail($accountContact);
        $this->assertNotEmpty($returnResult['firstName']);
        $this->assertNotEmpty($returnResult['contactId']);
        $this->assertNotEmpty($returnResult['accountId']);
        $this->assertNotEmpty($returnResult['email']);
        $this->assertNotEmpty($returnResult['contactPhone']);
        $this->assertNotEmpty($returnResult['senderFirstName']);
        $this->assertNotEmpty($returnResult['repId']);
        $this->assertNotEmpty($returnResult['repId2']);
        $this->assertNotEmpty($returnResult['senderLastName']);
        $this->assertNotEmpty($returnResult['senderEmail']);
        $this->assertNotEmpty($returnResult['senderId']);
        $this->assertNotEmpty($returnResult['accountCampaignId']);
        $this->assertNotEmpty($returnResult['accountName']);
        $this->assertArrayHasKey('company', $returnResult);
        $this->assertArrayHasKey('phone', $returnResult);
        $this->assertArrayHasKey('accountCompanyName', $returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * assignContactToCampaign()
     *
     * @return void
     */
    public function testAssignContactToCampaignOfOldToNew()
    {
        $contactVars = array(
            'email'     => CONTACT_EMAIL, 'accountId' => ACCOUNT_ID,
            'status'    => CONTACT_STATUS
        );
        $contactCampaignVars = array('campaign'      => ALT_CAMPAIGN_EMAIL_ID,
                                     'oldCampaignId' => CAMPAIGN_ID);
        $returnResult = $this->_model->assignContactToCampaign(
            CONTACT_ID, $contactVars, $contactCampaignVars
        );
        $this->assertEquals('true', $returnResult['message']);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * assignContactToCampaign()
     *
     * @return void
     */
    public function testAssignContactToCampaignOfNonToNew()
    {
        $contactVars = array(
            'email'     => ALT_CONTACT_EMAIL,
            'accountId' => FREE_ACCOUNT_ID, 'status' => CONTACT_STATUS
        );
        $contactCampaignVars = array('campaign'      => ALT_CAMPAIGN_EMAIL_ID,
                                     'oldCampaignId' => 0,
                                     'changedBy' => ACCOUNT_USER_ID);
        $returnResult = $this->_model->assignContactToCampaign(
            3, $contactVars, $contactCampaignVars
        );
        $this->assertEquals('true', $returnResult['message']);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * assignContactToCampaign()
     *
     * @return void
     */
    public function testAssignContactToCampaignOfOldToNon()
    {
        $contactVars = array('email' => RECIPIENT, 'accountId' => ACCOUNT_ID);
        $contactCampaignVars = array('campaign'      => 0,
                                     'oldCampaignId' => CAMPAIGN_ID);
        $returnResult = $this->_model->assignContactToCampaign(
            2, $contactVars, $contactCampaignVars
        );
        $this->assertEquals('true', $returnResult['message']);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * saveCampaignChanges()
     *
     * @return void
     */
    public function testSaveCampaignChangesWithWrongCampaignAssign()
    {
        $contactVars = array('email' => RECIPIENT, 'accountId' => ACCOUNT_ID);
        $accountContactModel = new RapidFunnel_Model_AccountContact();
        $returnResult = $this->_model->saveCampaignChanges(
            2, 0, $accountContactModel, $contactVars, true, 1
        );
        $this->assertEquals('error', $returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContact::
     * saveCampaignChanges()
     *
     * @return void
     */
    public function testSaveCampaignChanges()
    {
        $contactVars = array('email' => RECIPIENT, 'accountId' => ACCOUNT_ID);
        $accountContactModel = new RapidFunnel_Model_AccountContact();
        $returnResult = $this->_model->saveCampaignChanges(
            2, 0, $accountContactModel, $contactVars, true, 3
        );
        $this->assertEquals('true', $returnResult);
    }

}