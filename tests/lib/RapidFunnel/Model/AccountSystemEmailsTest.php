<?php
/**
 * RapidFunnel_Model_AccountSystemEmailsTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountSystemEmailsTest
 * RapidFunnel/Model/AccountSystemEmailsTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountSystemEmailsTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;

    /**
     * RapidFunnel_Model_AccountSystemEmailsTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountSystemEmails();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createArrayDataSet(
            [
                'accountCampaignEmail' => [
                    [
                        'id'          => '1', 'campaignId' => '1',
                        'title'   => 'Hello',
                        'content' => '<p>Your Info</p><p><%firstName%> <%lastName%></p><p><%email%></p><p><%company%></p><p><%title%></p><p>Its a link:&nbsp; <a oncontextmenu="return false" target="_blank" href="http://qa1.rapidfunnel.com/custom-video-resource/70/[user-id]">click here </a><br></p><p>Sender Info</p><p><%senderFirstName%> <%senderLastName%></p><p><%senderEmail%></p><p><%senderPhone%></p>',
                        'textContent' => 'Text Body', 'days' => '1',
                        'status'      => 'published', 'isDeleted' => '0',
                        'created'     => '2015-10-24 20:00:49',
                        'createdBy'   => '65', 'modified' => null,
                        'modifiedBy'  => '65'
                    ],
                    [
                        'id'          => '2', 'campaignId' => '2',
                        'title'   => 'Hello',
                        'content' => '<p>Your Info</p><p><%firstName%> <%lastName%></p><p><%email%></p><p><%company%></p><p><%title%></p><p>Its a link:&nbsp; <a oncontextmenu="return false" target="_blank" href="http://qa1.rapidfunnel.com/custom-video-resource/70/[rid]">click here </a><br></p><p><a oncontextmenu="return false" target="_blank" href="http://qa1.rapidfunnel.com/custom-video-resource/81/[user-id]"></p></p><p>Sender Info</p><p><%senderFirstName%> <%senderLastName%></p><p><%senderEmail%></p><p><%senderPhone%></p>',
                        'textContent' => 'Text Body', 'days' => '1',
                        'status'      => 'published', 'isDeleted' => '0',
                        'created'     => '2015-10-24 20:00:49',
                        'createdBy'   => '65', 'modified' => null,
                        'modifiedBy'  => '65'
                    ]
                ],
            ]
        );
        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountSystemEmails::__construct()
     *
     * @return void
     */
    public function testAccountSystemEmailsModel()
    {
        $this->_model = new RapidFunnel_Model_AccountSystemEmails();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountSystemEmails',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountSystemEmails::getSystemEmails()
     *
     * @return void
     */
    public function testGetSystemEmailss()
    {
        //$this->assertTrue(false);
        /*
         * @note This is commented due to error in implementation
         * @todo Resolve issue with this method
         *
            $testSearch = Array(
                'module' => 'account',
                'controller' => 'system-emails',
                'action' => 'get-system-emails',
                'sEcho' => 3,
                'iColumns' => 4,
                'sColumns' => '',
                'iDisplayStart' => 0,
                'iDisplayLength' => 10,
                'mDataProp_0' => 0,
                'mDataProp_1' => 1,
                'mDataProp_2' => 2,
                'mDataProp_3' => 3,
                'sSearch' => 'd',
                'bRegex' => false,
                'sSearch_0' => '',
                'bRegex_0' => false,
                'bSearchable_0' => true,
                'sSearch_1' => '',
                'bRegex_1' => false,
                'bSearchable_1' => true,
                'sSearch_2]' => '',
                'bRegex_2' => false,
                'bSearchable_2' => true,
                'sSearch_3' => '',
                'bRegex_3' => false,
                'bSearchable_3' => true,
                'iSortCol_0' => 0,
                'sSortDir_0' => 'asc',
                'iSortingCols' => 1,
                'bSortable_0' => true,
                'bSortable_1' => true,
                'bSortable_2' => true,
                'bSortable_3' => true,
                '_' => 1428042653274,
                );

            // by passing wrong accountId
            $results = $this->_model
                ->getSystemEmails($testSearch, SYSTEM_EMAIL_ID);
            $this->assertCount(4, $results);

            // by passing correct accountId
            $results = $this->_model
                ->getSystemEmails($testSearch, WRONG_SYSTEM_EMAIL_ID);
            $this->assertCount(4, $results);
        */
    }

    /**
     * Tests RapidFunnel_Model_AccountSystemEmails::getEmailDetails()
     *
     * @return void
     */
    public function testGetEmailDetails()
    {
        // if there is wrong DB operation to go Catch block
        $getDetail = $this->_model->getEmailDetails(null, null);
        $this->assertEquals(false, $getDetail);

        // if we providing correct systemEmailId
        $getDetail = $this->_model
            ->getEmailDetails(ACCOUNT_ID, SYSTEM_EMAIL_TYPE_ID);
        $this->assertArrayHasKey('id', $getDetail);

        // if we providing correct systemEmailId
        $getDetail = $this->_model
            ->getEmailDetails(ACCOUNT_ID, SYSTEM_EMAIL_TYPE_ID);
        $this->assertArrayHasKey('id', $getDetail);
    }

}