<?php
/**
 * RapidFunnel_Model_AccountUserTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountUserTest
 * RapidFunnel/Model/AccountUserTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountUserTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    private $_model;

    private $_temp_user_data
        = array(
            'id'             => TEMP_USER_ID,
            'email'          => "temp_user@email.org",
            'accountId'      => ACCOUNT_ID,
            'password'       => 'f2f1cb32d652f90d5186a37a58b7b99b',
            'salt'           => 'BBB4OB3TCiYy1KOIEvoLvg',
            'roleId'         => ADMIN_ROLE_ID,
            'isDelete'       => 0,
            'trialExpiresOn' => '2020-04-02 00:00:00',
            'createdBy'      => ADMIN_ID,
            'resetPassHash'  => 12345,
            'exportContacts' => 1
        );

    private $_bulk_import_data_valid
        = array(
            array(
                'user1',
                'import',
                'user1@email.org',
                'mr',
                ACCOUNT_ID,
                'register-code',
                '2015-01-01'
            ),
            array(
                'user1',
                'import',
                'user2@email.org',
                'mr',
                ACCOUNT_ID,
                'register-code',
                '2015-01-01'
            )
        );

    private $_bulk_import_data_duplicates
        = array(
            array('', '', 'user1@email.org', '', 0, '', 0),
            array('', '', 'user1@email.org', '', 0, '', 0)
        );

    private $_bulk_import_data_invalid
        = array(
            array('', '', 'not-valid', '', 0, '', 0)
        );

    private $_bulk_import_data_existing
        = array(
            array('', '', ADMIN_EMAIL, '', 0, '', 0),
            array('', '', 'free-user-1@email.org', '', 0, '', 0),
            array('', '', 'free-user-2@email.org', '', 0, '', 0),
            array('', '', 'free-user-3@email.org', '', 0, '', 0),
        );

    /**
     * RapidFunnel_Model_AccountUserTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountUser();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountContact.xml'
        );
        $ds4 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/Account.xml'
        );
        $ds5 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountContactNotes.xml'
        );
        $ds6 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountSystemEmails.xml'
        );
        $ds8 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUserCardProfile.xml'
        );
        $ds10 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUserGroup.xml'
        );

        $ds11 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUserBillingInfo.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet(
        );
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);
        $compositeDs->addDataSet($ds4);
        $compositeDs->addDataSet($ds5);
        $compositeDs->addDataSet($ds6);
        $compositeDs->addDataSet($ds8);
        $compositeDs->addDataSet($ds10);
        $compositeDs->addDataSet($ds11);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::__construct()
     *
     * @return void
     */
    public function testAccountUserModel()
    {
        // test that the model is instantiated properly
        $model = new RapidFunnel_Model_AccountUser();
        $this->assertInstanceOf('RapidFunnel_Model_AccountUser', $model);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::loadByEmail
     *
     * @return void
     */
    public function testLoadByEmail()
    {

        $results = $this->_model->loadByEmail(null);
        $this->assertFalse($results);

        // test invalid input (non-existent entity)
        $results = $this->_model->loadByEmail(ADMIN_WRONG_EMAIL);
        $this->assertFalse($results);

        // test valid entry result
        $results = $this->_model->loadByEmail(ADMIN_EMAIL);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        if (is_array($results)) {
            $this->assertArrayHasKey('id', $results);
        }
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::isEmailAvailable()
     *
     * @return void
     */
    public function testIsEmailAvailable()
    {
         $results = $this->_model->isEmailAvailable(null);
        $this->assertFalse($results);

        // supply unused email
        $results = $this->_model->isEmailAvailable(ADMIN_WRONG_EMAIL);
        $this->assertFalse($results);

        // supply unused email/admin association
        $results = $this->_model->isEmailAvailable(ADMIN_EMAIL, ADMIN_ID);
        $this->assertFalse($results);

        // supply used email/admin association
        $results = $this->_model->isEmailAvailable(
            ADMIN_EMAIL, ADMIN_WRONG_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertArrayHasKey('id', $results);

        // supply unused email/admin association
        $results = $this->_model
            ->isEmailAvailable(ADMIN_EMAIL, ADMIN_ID, false);
        $this->assertFalse($results);

        // supply unused email/admin association
        $results = $this->_model->isEmailAvailable(ADMIN_EMAIL, ADMIN_ID, true);
        $this->assertFalse($results);

        // supply used email/admin association
        $results = $this->_model->isEmailAvailable(
            ADMIN_EMAIL, ADMIN_WRONG_ID, false
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertArrayHasKey('id', $results);

        // supply used email/admin association
        $results = $this->_model->isEmailAvailable(
            ADMIN_EMAIL, ADMIN_WRONG_ID, true
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertArrayHasKey('id', $results);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::validateRegistration()
     *
     * @note This method needs to be tested for the case where a registration
     *       is valid as well. This might be best done with a temporary object.
     *       - Antoni 16 Oct 2015
     *
     * @return void
     */
    public function testValidateRegistration()
    {
        // test result for invalid input (force exception)
        $results = $this->_model->validateRegistration(null, null);
        $this->assertFalse($results);

        // test result for invalid input
        $results = $this->_model->validateRegistration(
            'dfgd@dsfg.com', '23423452346'
        );
        $this->assertFalse($results);

        // test restult for valid input
        $results = $this->_model->validateRegistration(
            INVITED_USER_EMAIL, 'invited-code'
        );
        $this->assertTrue($results);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::updateUserStatus()
     *
     * Tests if the function updateUserStatus() updates the status
     * field in table accountUser.
     */
    public function testUpdateUserStatus()
    {
        $result = $this->_model->updateUserStatus(
            array('status' => '1'), 'id = 1', 'accountUser'
        );
        $this->assertTrue($result);

        $resultingTable = $this->getConnection()->createQueryTable(
            'accountUser',
            'SELECT status '
            . 'FROM accountUser '
            . 'WHERE id = ' . ADMIN_ID
        );

        $expectedTable = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/expectedAfterUpdatingStatus.xml'
        )
            ->getTable("accountUser");

        $this->assertTablesEqual($expectedTable, $resultingTable);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::updateUserStatus()
     *
     * Tests if the function updateUserStatus() throws Exception
     * if null status is given.
     *
     * @return void
     * @expectedException Exception
     */
    public function testUpdateUserStatusWithNullStatus()
    {
        $this->_model->updateUserStatus(null, ADMIN_ID, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::updateUserStatus()
     *
     * Tests if the function updateUserStatus() throws Exception
     * if invalid status is given.
     *
     * @return void
     * @expectedException Exception
     */
    public function testUpdateUserStatusWithInvalidStatus()
    {
        $this->_model->updateUserStatus('ABC', ADMIN_ID, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::updateUserStatus()
     *
     * Tests if the function updateUserStatus() throws Exception
     * if null userId is given.
     *
     * @return void
     * @expectedException Exception
     */
    public function testUpdateUserStatusWithNullUserId()
    {
        $this->_model->updateUserStatus(array(1), null, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::updateUserStatus()
     *
     * Tests if the function updateUserStatus() throws Exception
     * if invalid userId is given.
     *
     * @return void
     * @expectedException Exception
     */
    public function testUpdateUserStatusWithInvalidUserId()
    {
        $this->_model->updateUserStatus(1, 'ABC', 'accountUser');
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::testCheckTrailExpiredOrNot()
     *
     * This test case tests the function with Un Expired UserId
     *
     * @return void
     */
    public function testCheckTrialExpiredOrNot()
    {
        $result = $this->_model->checkTrialExpiredOrNot(INVITED_USER_ID);

        $this->assertEquals(0, $result);
    }

    /**
     * Tests RapidFunnel_Model_AccountUser::testCheckTrailExpiredOrNot()
     *
     * This test case tests the function with Expired UserId
     *
     * @return void
     */
    public function testCheckTrialExpiredOrNotWithExpiredId()
    {
        $result = $this->_model->checkTrialExpiredOrNot(ID);

        $this->assertEquals(0, $result);
    }

    /**
     * Test to fetch user info using user id
     * Tests RapidFunnel_Model_AccountUser::getUserApiDashboardInfo()
     *
     * @return void
     */
    public function testGetUserApiDashboardInfoException()
    {
        $result = $this->_model->getUserApiDashboardInfo(null);
        $this->assertFalse($result);

    }

    /**
     * Test to fetch user info using user id
     * Tests RapidFunnel_Model_AccountUser::getUserApiDashboardInfo()
     *
     * @return void
     */
    public function testGetUserApiDashboardInfoForValidId()
    {
        //Fetches user info using a user id
        $result = $this->_model->getUserApiDashboardInfo(TEMP_USER_ID2);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result['response']);
    }

    /**
     * Functionality to test associateAllRecordsToUserApp, delete data
     *
     * @expectedException Exception
     */
    public function testSendForgetPasswordEmailWithInvalidUserId()
    {
        $this->_model->sendForgetPasswordEmail(
            null, CONCAT_EMAILS, ADMIN_FIRST_NAME, ADMIN_LAST_NAME
        );
    }

    /**
     * Functionality to test associateAllRecordsToUserApp, delete data
     *
     * @expectedException Exception
     */
    public function testSendForgetPasswordEmailWithInvalidEmail()
    {
        $this->_model->sendForgetPasswordEmail(
            ACCOUNT_USER_ID, null, ADMIN_FIRST_NAME, ADMIN_LAST_NAME
        );
    }

    /**
     * Functionality to test associateAllRecordsToUserApp, delete data
     *
     * @expectedException Exception
     */
    public function testSendForgetPasswordEmailWithInvalidFirstName()
    {
        $this->_model->sendForgetPasswordEmail(
            ACCOUNT_USER_ID, CONCAT_EMAILS, null, ADMIN_LAST_NAME
        );
    }

    /**
     * Functionality to test associateAllRecordsToUserApp, delete data
     */
    public function testSendForgetPasswordEmail()
    {
        $response = $this->_model->sendForgetPasswordEmail(
            ACCOUNT_USER_ID, CONCAT_EMAILS, ADMIN_FIRST_NAME, ADMIN_LAST_NAME
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $response);
        $this->assertNotEmpty($response['to']);
        $this->assertNotEmpty($response['subject']);
        $this->assertNotEmpty($response['html']);
        $this->assertNotEmpty($response['text']);
        $this->assertNotEmpty($response['from']);
    }

}
