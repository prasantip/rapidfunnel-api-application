<?php
/**
 * RapidFunnel_Model_AccountCampaignEmailTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountCampaignEmailTest
 * RapidFunnel/Model/AccountCampaignEmailTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountCampaignEmailTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;

    /**
     * RapidFunnel_Model_AccountCampaignEmailTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountCampaignEmail();
    }

    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountCampaign.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountCampaignEmail.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignEmail::__construct()
     *
     * @return void
     */
    public function testAccountCampaignEmailModel()
    {
        $this->_model = new RapidFunnel_Model_AccountCampaignEmail();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountCampaignEmail', $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignEmail::getMailsOfCampaign()
     *
     * @return void
     */
    public function testGetMailsOfCampaign()
    {
        $result = $this->_model->getMailsOfCampaign(CAMPAIGN_EMAIL_ID);
        $this->assertNotCount(0, $result);

    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignEmail::getMailsOfCampaign()
     *
     * @note: Tests getMailsOfCampaign() with wrong inputs
     *
     * @return void
     */
    public function testGetMailsOfCampaignWithWrongInput()
    {
        // if we providing wrong accountId
        $result = $this->_model->getMailsOfCampaign(WRONG_CAMPAIGN_EMAIL_ID);
        $this->assertCount(0, $result);

    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignEmail::getMailsOfCampaign()
     *
     * @note: Test case generates an Exception in getMailsOfCampaign()
     *
     * @return void
     */
    public function testGetMailsOfCampaignWithException()
    {
        // if there is wrong DB operation to go Catch block
        $result = $this->_model->getMailsOfCampaign(null);
        $this->assertEquals(false, $result);

    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignEmail::getOtherMailsOfCampaign()
     *
     * @return void
     */
    public function testGetOtherMailsOfCampaign()
    {
        $result = $this->_model->getOtherMailsOfCampaign(
            CAMPAIGN_ID,
            WRONG_CAMPAIGN_EMAIL_ID
        );
        $this->assertNotCount(0, $result);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignEmail::getOtherMailsOfCampaign()
     *
     * @note Tests if the function getOtherMailsOfCampaign() returns empty
     *       if incorrect input is given
     *
     * @return void
     */
    public function testGetOtherMailsOfCampaignWithIncorrectInput()
    {
        $result = $this->_model->getOtherMailsOfCampaign(WRONG_CAMPAIGN_ID);
        $this->assertEmpty($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignEmail::getOtherMailsOfCampaign()
     *
     * @note Tests for Exception in the function getOtherMailsOfCampaign()
     *
     * @return void
     */
    public function testGetOtherMailsOfCampaignWithException()
    {
        $result = $this->_model->getOtherMailsOfCampaign(null);
        $this->assertFalse($result);
    }

}
