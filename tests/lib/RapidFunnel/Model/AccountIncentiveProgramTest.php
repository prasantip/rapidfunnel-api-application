<?php
/**
 * RapidFunnel_Model_AccountIncentiveProgramTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountIncentiveProgramTest
 * RapidFunnel/Model/AccountIncentiveProgramTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountIncentiveProgramTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    private $_model;
    private $getParams
        = array(
            'incentiveTypeId' => '4',
            'incentiveId'     => INCENTIVE_ID,
            'sEcho'           => '1',
            'sSearch'         => FULL_NAME,
            'bSearchable_0'   => true,
            'iDisplayStart'   => 0,
            'iSortingCols'    => 1,
            'iDisplayLength'  => 1,
            'iSortCol_0'      => 0,
            'bSortable_0'     => true,
            'sSortDir_0'      => "asc"
        );
    private $altGetParams
        = array(
            'incentiveTypeId' => 1,
            'incentiveId'     => INCENTIVE_ID,
            'sEcho'           => '1',
            'sSearch'         => FULL_NAME,
            'bSearchable_0'   => true,
            'iDisplayStart'   => 0,
            'iSortingCols'    => 1,
            'iDisplayLength'  => 1,
            'iSortCol_0'      => 0,
            'bSortable_0'     => true,
            'sSortDir_0'      => "asc"
        );
    private $expectedArray
        = array(
            'sEcho'                => '1',
            'iTotalRecords'        => '1',
            'iTotalDisplayRecords' => '1',
            'aaData'               => array(
                array(
                    'incentiveId'  => INCENTIVE_ID,
                    'goal'         => 0,
                    'award'        => AWARD,
                    'awardTypeId'  => AWARD_TYPE_ID,
                    'userName'     => FULL_NAME,
                    'noOfLeads'    => NUMBER_OF_LEADS,
                    'awardsEarned' => AWARD_EARNED
                )
            )
        );
    private     $altExpectedArray
        = array(
            'sEcho'                => '1',
            'iTotalRecords'        => '1',
            'iTotalDisplayRecords' => '1',
            'aaData'               => array(
                array(
                    'incentiveId'  => INCENTIVE_ID_STRING,
                    'goal'         => '0',
                    'award'        => AWARD,
                    'awardTypeId'  => INCENTIVE_ID_STRING,
                    'topPercentage'  => INCENTIVE_ID_STRING,
                    'userName'     => FULL_NAME,
                    'noOfLeads'    => INCENTIVE_ID_STRING,
                    'awardsEarned' => INCENTIVE_ID_STRING
                )
            )
        );
    private $incentiveTypeIds
        = array(
            'achieveGoal' => 1,
            'mostLeads'   => 1,
            'perLead'     => 1,
            'xLeads'      => 1
        );
    private $altIncentiveTypeIds
        = array(
            'achieveGoal' => 1,
            'mostLeads'   => 0,
            'perLead'     => 1,
            'xLeads'      => 1
        );

    /**
     * RapidFunnel_Model_AccountIncentiveProgramTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountIncentiveProgram();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountIncentiveProgram.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds3 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUserGroup.xml'
        );
        $ds4 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUserIncentive.xml'
        );
        $ds5 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountContact.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);
        $compositeDs->addDataSet($ds3);
        $compositeDs->addDataSet($ds4);
        $compositeDs->addDataSet($ds5);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountIncentiveProgram::__Contractor()
     *
     * Tests the Contractor() of AccountIncentiveProgram Model
     *
     * @return void
     */
    public function testAccountSystemEmailUserEmailContractor()
    {
        $this->_model = new RapidFunnel_Model_AccountIncentiveProgram();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountIncentiveProgram',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountIncentiveProgram
     * ::getIncentivesByRunningStatus()
     *
     * Tests the function getIncentivesByRunningStatus()
     *
     * return void
     */
    public function testGetIncentivesByRunningStatus()
    {
        $returnResult = $this->_model->getIncentivesByRunningStatus(
            ACCOUNT_ID, ACCOUNT_USER_ID, RUNNING_STATUS
        );
        $this->assertNotEmpty($returnResult[0]['incentiveId']);
        $this->assertNotEmpty($returnResult[0]['name']);
        $this->assertNotEmpty($returnResult[0]['startDate']);
        $this->assertNotEmpty($returnResult[0]['endDate']);
        $this->assertNotEmpty($returnResult[0]['award']);
        $this->assertNotEmpty($returnResult[0]['topPercentage']);
        $this->assertNotEmpty($returnResult[0]['awardTypeId']);
        $this->assertNotEmpty($returnResult[0]['awardType']);
    }

}
