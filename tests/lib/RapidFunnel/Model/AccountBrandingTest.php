<?php
/**
 * RapidFunnel_Model_AccountBrandingTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountBrandingTest
 * RapidFunnel/Model/AccountBrandingTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountBrandingTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;

    /**
     * RapidFunnel_Model_AccountBrandingTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountBranding();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountBranding.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountBranding::__construct()
     *
     * @return void
     */
    public function testAccountBrandingModel()
    {
        $this->_model = new RapidFunnel_Model_AccountBranding();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountBranding',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountBranding::getDetails()
     *
     * Tests if the function getDetails() returns a record
     * associated with the given conditions.
     *
     * @return void
     */
    public function testGetDetails()
    {
        $returnVal = $this->_model
            ->getDetails(array('id' => ACCOUNT_BRANDING_ID));
        $this->assertArrayHasKey('id', $returnVal);
    }

    /**
     * Tests RapidFunnel_Model_AccountBranding::getDetails()
     *
     * Tests for Exception in the function getDetails()
     *
     * @return void
     */
    public function testGetDetailsWithException()
    {
        $returnVal = $this->_model
            ->getDetails(array('unknownColumn' => ACCOUNT_BRANDING_ID));
        $this->assertFalse($returnVal);
    }

    /**
     * Tests RapidFunnel_Model_AccountBranding::getBrandingDetails()
     *
     * Tests if the function getBrandingDetails() return a record
     * associated with the accountId.
     *
     * @return void
     */
    public function testGetBrandingDetails()
    {
        $returnVal = $this->_model->getBrandingDetails(ACCOUNT_ID);
        $this->assertArrayHasKey('id', $returnVal);
    }

    /**
     * Tests RapidFunnel_Model_AccountBranding::getBrandingDetails()
     *
     * Tests if the function getBrandingDetails() returns nothing
     * if incorrect input is given.
     *
     * @return void
     */
    public function testGetBrandingDetailsWithIncorrectInput()
    {
        $returnVal = $this->_model->getBrandingDetails(WRONG_ACCOUNT_ID);
        $expected = RapidFunnel_Configs::_instance('branding')->ToArray();

        $this->assertEquals($returnVal, $expected);
    }

    /**
     * Tests RapidFunnel_Model_AccountBranding::getBrandingDetails()
     *
     * Tests for exception in the function getBrandingDetails()
     *
     * @return void
     */
    public function testGetBrandingDetailsWithException()
    {
        $returnVal = $this->_model->getBrandingDetails(null);
        $this->assertFalse($returnVal);
    }

}