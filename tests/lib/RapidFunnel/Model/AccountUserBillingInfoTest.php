<?php
/**
 * RapidFunnel_Model_AccountBrandingTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountUserBillingInfoTest
 * RapidFunnel/Model/AccountUserBillingInfoTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountUserBillingInfoTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;

    /**
     * RapidFunnel_Model_AccountBrandingTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountUserBillingInfo();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountUserBillingInfo.xml'
        );

        $ds2 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/Account.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet(
        );
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountUserBillingInfo::__construct()
     *
     * @return void
     */
    public function testAccountUserBillingInfoModel()
    {
        // test that the model is instantiated properly
        $model = new RapidFunnel_Model_AccountUserBillingInfo();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountUserBillingInfo', $model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountUserBillingInfo::
     * getBillingDetailsByUserId()
     *
     * @return void
     */
    public function testGetBillingDetailsByUserId()
    {
        $result = $this->_model->getBillingDetailsByUserId(TEMP_USER_ID2);
        $this->assertNotNull($result['id']);
        $this->assertNotNull($result['accountId']);
        $this->assertNotNull($result['userId']);
        $this->assertNotNull($result['isForCompany']);
        $this->assertNotNull($result['stripeCustomerId']);
        $this->assertNotNull($result['lastChargeId']);
        $this->assertNotNull($result['name']);
        $this->assertNotNull($result['email']);
        $this->assertNotNull($result['address']);
        $this->assertNotNull($result['city']);
        $this->assertNotNull($result['stateId']);
        $this->assertNotNull($result['countryId']);
        $this->assertNotNull($result['zip']);
        $this->assertNotNull($result['expMonth']);
        $this->assertNotNull($result['expYear']);
        $this->assertNotNull($result['last4']);
        $this->assertNotNull($result['brand']);
        $this->assertNotNull($result['funding']);
        $this->assertNotNull($result['isSubscriptionActive']);
        $this->assertNotNull($result['subscriptionId']);
        $this->assertArrayHasKey('subscriptionPlanId', $result);
        $this->assertNotNull($result['subscriptionAmount']);
        $this->assertNotNull($result['subscriptionCreated']);
        $this->assertNotNull($result['subPaymentStartDate']);
        $this->assertNotNull($result['lastPaymentDoneOn']);
        $this->assertNotNull($result['lastPaidAmount']);
        $this->assertNotNull($result['nextPaymentOn']);
        $this->assertNotNull($result['failureCount']);
        $this->assertNotNull($result['inActiveOrIgnoreBilling']);
        $this->assertNotNull($result['created']);
        $this->assertNotNull($result['createdBy']);
        $this->assertNotNull($result['modified']);
        $this->assertNotNull($result['modifiedBy']);
        $this->assertNotNull($result['userStatus']);
        $this->assertNotNull($result['passThrough']);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserBillingInfo::
     * getBillingDetailsByUserId()
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetBillingDetailsByUserIdWithException()
    {
        $this->_model->getBillingDetailsByUserId(null);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserBillingInfo::
     * getBillingDetailsByUserId()
     *
     * @return void
     */
    public function testGetBillingDetailsByUserIdWithInvalidInput()
    {
        $result = $this->_model->getBillingDetailsByUserId(LARGE_NUMBER);
        $this->assertFalse($result);
    }

}
