<?php
/**
 * RapidFunnel_Model_AccountResourcesTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountResourcesTest
 * RapidFunnel/Model/AccountResourcesTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountResourcesTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_model;
    public $groupIds = array(1);
    public $adminRoleId = 1;
    public $userRoleId = 3;
    public $noGroupIds = null;
    public $accountId =1;
    public $userId =1;
    public $getParams = array(
            'sEcho'=>'1',
            'sSearch'=>'Resource',
            'bSearchable_0'=>true,
            'iDisplayStart'=>0,
            'iSortingCols'=>1,
            'iDisplayLength'=>1,
            'iSortCol_0'=>0,
            'bSortable_0'=>true,
            'sSortDir_0'=>"asc"
    );
    public $expectedArrayForApp = array(
        'sEcho'=>1,
        'iTotalRecords'=>1,
        'iTotalDisplayRecords'=>1,
        'aaData'=>array(
            array(
                'id'=>'1',
                'name'=>'Resources-hello',
                'description'=>'NEERAJ',
                'accountUserId'=>'1',
                'accountResourceId'=>'1',
                'categoryName'=>'Resource Category'
            )
        )
    );
    public $expectedArrayForAppWithUser = array(
        'sEcho'=>1,
        'iTotalRecords'=>1,
        'iTotalDisplayRecords'=>1,
        'aaData'=>array(
            array(
                'id'=>'1',
                'name'=>'Resources-hello',
                'description'=>'NEERAJ',
                'accountUserId'=>'1',
                'accountResourceId'=>'1',
                'categoryName'=>'Resource Category',
                'accountResourcesId'=>'1',
                'accountGroupId'=>'1'
            )
        )
    );
    public $expectedArrayForFreeUsers = array(
        'sEcho'=>1,
        'iTotalRecords'=>1,
        'iTotalDisplayRecords'=>1,
        'aaData'=>array(
            array(
                'id'=>'1',
                'name'=>'Resources-hello',
                'description'=>'NEERAJ',
                'accountId'=>'1',
                'accountResourceId'=>'1',
                'categoryName' => 'Resource Category'
            )
        )
    );

    /**
     * RapidFunnel_Model_AccountResourcesTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountResources();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountResources.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountResources::__construct()
     *
     * @return void
     */
    public function testAccountSystemEmailsModel()
    {
        $this->_model = new RapidFunnel_Model_AccountResources();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountResources',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountResources::getResourcesForApi()
     *
     * @return void
     */
    public function testGetResourcesForApi()
    {
        // by passing correct accountId
        $results = $this->_model
            ->getResourcesForApi(ACCOUNT_ID, ADMIN_ID);
        $this->assertGreaterThan(1, $results);
    }

    /**
     * Tests RapidFunnel_Model_AccountResources::getResourcesSentThroughEmail()
     *
     * @return void
     */
    public function testGetResourcesSentThroughEmail()
    {
        $emailContent = RESOURCE_URL;
        $result = $this->_model->getResourcesSentThroughEmail($emailContent);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
    }

    /**
     * Tests RapidFunnel_Model_AccountResources::saveResourceSentDate()
     *
     * @return void
     */
    public function testSaveResourceSentDate()
    {
        $result = $this->_model->saveResourceSentDate(ID, RESOURCE_ID, ACCOUNT_ID, 0);
        $this->assertTrue($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountResources::saveResourcesSentThroughEmail()
     *
     * @return void
     */
    public function testSaveResourcesSentThroughEmail()
    {
        $resources = array(RESOURCE_URL);
        $result = $this->_model->saveResourcesSentThroughEmail($resources, ACCOUNT_ID);
        $this->assertTrue($result);
    }


}
