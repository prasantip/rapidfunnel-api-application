<?php
/**
 * RapidFunnel_Model_AccountCampaignContactEmailTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountCampaignContactEmailTest
 * RapidFunnel/Model/AccountCampaignContactEmailTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountCampaignContactEmailTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    private $_model;

    /**
     * RapidFunnel_Model_AccountCampaignContactEmailTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountCampaignContactEmail();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountCampaignContactEmail',  $this->_model
        );
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountContact.xml'
        );
        $ds3 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/Account.xml'
        );
        $ds5 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountCampaignContactEmail.xml'
        );
        $ds6 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountCampaign.xml'
        );
        $ds7 = $this->createArrayDataSet(
            ['accountCampaignEmail' => unserialize(accountCampaignEmail)]
        );


        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);
        $compositeDs->addDataSet($ds3);
        $compositeDs->addDataSet($ds5);
        $compositeDs->addDataSet($ds6);
        $compositeDs->addDataSet($ds7);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContactEmail::__construct()
     *
     * @return void
     */
    public function testAccountCampaignContactEmail()
    {
        $this->_model = new RapidFunnel_Model_AccountCampaignContactEmail();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountCampaignContactEmail',
            $this->_model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContactEmail::getDetails()
     *
     * @return void
     */
    public function testGetDetails()
    {
        $returnVal = $this->_model->getMails();

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnVal);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnVal[0]);

        $this->assertNotEmpty($returnVal[0]['id']);
        $this->assertNotEmpty($returnVal[0]['campaignId']);
        $this->assertNotEmpty($returnVal[0]['title']);
        $this->assertNotEmpty($returnVal[0]['content']);
        $this->assertNotEmpty($returnVal[0]['textContent']);
        $this->assertNotEmpty($returnVal[0]['days']);
        $this->assertNotEmpty($returnVal[0]['status']);
        $this->assertNotEmpty($returnVal[0]['created']);
        $this->assertNotEmpty($returnVal[0]['createdBy']);
        $this->assertArrayHasKey('modified', $returnVal[0]);
        $this->assertArrayHasKey('isDeleted', $returnVal[0]);
        $this->assertArrayHasKey('type', $returnVal[0]);
        $this->assertNotEmpty($returnVal[0]['modifiedBy']);

        $returnVal = $this->_model->getMails(
            array('id'), array('ac.id = ?' =>null)
        );
        $this->assertEmpty($returnVal);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContactEmail::constructNewMail()
     *
     * @return void
     */
    public function testConstructNewMail()
    {
        $mailData = array(
            array(
                'contactMobile'=>'8142582889',
                'contactHome'=>'9849414565',
                'contactWork'=>'545',
                'contactOther'=>'123'
            ),
        );
        $expectedMail = array('8142582889', '9849414565', '545', '123');
        $returnArray=$this->_model->constructNewMail($mailData);

        $this->assertEquals($returnArray, $expectedMail);
    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContactEmail::constructNewMail()
     *
     * @return void
     */
    public function testCampaignSendMail()
    {
        $returnArray=$this->_model->campaignSendMail(CONTACT_ID, ACCOUNT_ID);
        $this->assertTrue($returnArray);

        $resultingTable = $this->getConnection()
            ->createQueryTable(
                "accountCampaignContactEmail",
                "SELECT id, sendStatus FROM accountCampaignContactEmail
                WHERE id = 1"
            );

        $expectedTable = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/expected.xml'
        )
            ->getTable("accountCampaignContactEmail");

        $this->assertTablesEqual($expectedTable, $resultingTable);

    }

    /**
     * Tests RapidFunnel_Model_AccountCampaignContactEmail::constructNewMail()
     *
     * @return void
     */
    public function testMarkMailsAsSendWithException()
    {
        $returnArray = $this->_model->markMailsAsSend(array("(null, null)"));
        $this->assertFalse($returnArray);

    }

}
