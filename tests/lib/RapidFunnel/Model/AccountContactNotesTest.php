<?php
/**
 * RapidFunnel_Model_AccountContactNotesTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountContactNotesTest
 * RapidFunnel/Model/AccountContactNotesTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountContactNotesTest extends
    RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    private $model;

    /**
     * Constructor method for class. Model wise required data initialized here
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new RapidFunnel_Model_AccountContactNote();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountContactNotes.xml'
        );

        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountContact.xml'
        );

        $ds3 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUserGroup.xml'
        );


        $compositeDs
            = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);
        $compositeDs->addDataSet($ds3);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::getContactNotes
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetContactNotesWithExceptionOne()
    {
        $this->model->getContactNotes(null, USER_ROLE_ID);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::getContactNotes
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetContactNotesWithExceptionTwo()
    {
        $this->model->getContactNotes(TEMP_USER_ID, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::getContactNotes
     *
     * @return void
     */
    public function testGetContactNotesWithExceptionThree()
    {
        $results = $this->model->getContactNotes(
            TEMP_USER_ID, USER_ROLE_ID, CONTACT_ID
        );
        $this->assertEmpty($results);
    }

    public function testGetContactNotesOfAdmin()
    {
        //Testing for valid inputs for which contact note exists
        $results = $this->model->getContactNotes(
            ALT_TEMP_USER_ID, MANAGER_ROLE_ID, OLD_ALT_CONTACT_ID,
            NOTE_ID
        );
        $this->assertNotEmpty($results[0]['note']);
        $this->assertNotEmpty($results[0]['noteTimeStamp']);
        $this->assertGreaterThan(0, $results[0]['noteId']);
        $this->assertEquals(OLD_ALT_CONTACT_ID, $results[0]['contactId']);
    }

    public function testGetContactNotesOfUser()
    {
        //Testing for valid inputs for which contact note exists
        $results = $this->model->getContactNotes(
            ALT_TEMP_USER_ID, USER_ROLE_ID, OLD_ALT_CONTACT_ID
        );

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertNotEmpty($results);

        $results = $results[0];
        // check array has necessary number of elements
        $this->assertCount(4, $results);

        // check that noteId key exists and it contains entities
        $this->assertArrayHasKey('noteId', $results);
        $this->assertGreaterThanOrEqual(1, $results['noteId']);

        // check that contactId exists and it is not empty
        $this->assertArrayHasKey('contactId', $results);
        $this->assertGreaterThanOrEqual(1, $results['contactId']);

        // check that contactNote exists and it is not empty
        $this->assertArrayHasKey('note', $results);
        $this->assertNotEmpty($results['note']);
        $this->assertNotEmpty($results['noteTimeStamp']);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::getNoteDetails
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetNoteDetailsWithException()
    {
        $this->model->getNoteDetails(null);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::getNoteDetails
     *
     * @return void
     */
    public function testGetNoteDetailsWithInvalidUserID()
    {
        $returnResult = $this->model->getNoteDetails(LARGE_NUMBER);
        $this->assertFalse($returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::getNoteDetails
     *
     * @return void
     */
    public function testGetNoteDetails()
    {
        //Testing for valid inputs for which contact note exists
        $results = $this->model->getNoteDetails(OLD_ALT_CONTACT_ID);

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertNotEmpty($results);
        $this->assertNotEmpty($results[0]['id']);
        $this->assertNotEmpty($results[0]['noteTimeStamp']);
        $this->assertNotEmpty($results[0]['contactNote']);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::addNotes
     *
     * @return void
     *
     * @expectedException Exception
     */
    public function testAddNotesException() {
        // test invalid input (force exception)
        // test with contact Id is null
        $this->model->addNotes(null, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::addNotes
     *
     * @return void
     *
     * @expectedException Exception
     */
    public function testAddNotesExceptionOne() {
        // test invalid input (force exception)
        // test with $noteParams is null
        $this->model->addNotes(CONTACT_ID, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::addNotes
     *
     * @return void
     * @expectedException Exception
     */
    public function testAddNotesExceptionTwo() {
        // test invalid input (force exception)
        // test with $noteParams is not in proper format
        // Initialize argument
        $noteParams = array();
        $noteParams['noteTimeStamps'] = NOTE_TIME_STAMP;
        $noteParams['contactNotes'] = CONTACT_NOTE;

        // Send contactNotes in array format
        $noteParams['contactNotes'] = array(CONTACT_NOTE);
        $result = $this->model->addNotes(CONTACT_ID, $noteParams);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::addNotes
     *
     * @return void
     */
    public function testAddNotesExceptionThree() {

        // Send contactNotes in array format
        $noteParams['contactNotes'] = array(CONTACT_NOTE);

        // Make noteTimeStamp empty
        $noteParams['noteTimeStamps'] = array('');
        $noteParams['noteTimeStamps'] = array(NOTE_TIME_STAMP);

        // test invalid input (force exception)
        // test with $noteParams is not in proper format
        $result = $this->model->addNotes(CONTACT_ID, $noteParams);
        $this->assertTrue($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::addNotes
     *
     * @return void
     */
    public function testAddNotes()
    {
        // Initialize argument
        $noteParams = array();
        $noteParams['noteTimeStamps'] = NOTE_TIME_STAMP;
        $noteParams['contactNotes'] = CONTACT_NOTE;

        // Send contactNotes in array format
        $noteParams['contactNotes'] = array(CONTACT_NOTE);

        // Make noteTimeStamp empty
        $noteParams['noteTimeStamps'] = array('');

        // test with $noteParams having empty data
        // As note is optional for empty data it will return true
        $this->assertTrue(
            $this->model->addNotes(
                CONTACT_ID, $noteParams
            )
        );

        // Make contactNotes as empty
        $noteParams['contactNotes'] = array('');

        // Send noteTimeStamp in array format
        $noteParams['noteTimeStamps'] = array(NOTE_TIME_STAMP);

        // test with $noteParams having empty data
        // As note is optional for empty data it will return true
        $this->assertTrue(
            $this->model->addNotes(
                CONTACT_ID, $noteParams
            )
        );

        // Test with all valid inputs
        // Send noteTimeStamp and contactNotes in array format
        $noteParams['noteTimeStamps'] = array(NOTE_TIME_STAMP);
        $noteParams['contactNotes'] = array(CONTACT_NOTE);
        $results = $this->model->addNotes(
            CONTACT_ID, $noteParams
        );
        $this->assertTrue($results);
    }

    /**
     * @expectedException Exception
     */
    public function testAddNotesWithException() {
        $noteParams['noteTimeStamps'] = NOTE_TIME_STAMP;
        $noteParams['contactNotes'] = array(CONTACT_NOTE);
        $results = $this->model->addNotes(
            CONTACT_ID, $noteParams
        );
        $this->assertFalse($results);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::updateNotes
     *
     * @return void
     */
    public function testUpdateNotesWithInvalidContactId()
    {
        $noteParams = array();
        $noteParams['noteIds'] = array(LARGE_NUMBER);
        $noteParams['updateTimeStamps'] = array(NOTE_TIME_STAMP);
        $noteParams['updateContactNotes'] = array(NOTE);
        $returnResult = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::updateNotes
     *
     * @return void
     */
    public function testUpdateNotesWithException()
    {
        $noteParams = array();
        $noteParams['noteIds'] = array();
        $noteParams['updateTimeStamps'] = array();
        $noteParams['updateContactNotes'] = array();
        $returnResult = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($returnResult);
    }
    /**
     * Tests RapidFunnel_Model_AccountContactNote::updateNotes
     *
     * @return void
     */
    public function testUpdateNotes()
    {
        // Initialize argument
        $noteParams = '';
        // test invalid input (force exception)
        // test with contact Id is null

        $result = $this->model->updateNotes(null, $noteParams);
        $this->assertFalse($result);
        // test invalid input (force exception)
        // test with $noteParams is null
        $result = $this->model->updateNotes(CONTACT_ID, null);
        $this->assertFalse($result);

        // test invalid input (force exception)
        // test with invalid argument of $noteParams

        $result = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($result);

        // Only set noteIds and updateTimeStamps
        $noteParams = array();
        $noteParams['noteIds'] = NOTE_ID;
        $noteParams['updateTimeStamps'] = NOTE_TIME_STAMP;

        $result = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($result);

        // Only set updateContactNotes and noteIds
        $noteParams = array();
        $noteParams['noteIds'] = NOTE_ID;
        $noteParams['updateContactNotes'] = CONTACT_NOTE;

        $result = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($result);

        // Only set updateContactNotes and updateTimeStamps
        $noteParams = array();
        $noteParams['updateContactNotes'] = CONTACT_NOTE;
        $noteParams['updateTimeStamps'] = NOTE_TIME_STAMP;

        $result = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($result);
        // Only send updateContactNotes and updateTimeStamps in array
        $noteParams = array();
        $noteParams['updateContactNotes'] = array(CONTACT_NOTE);
        $noteParams['updateTimeStamps'] = array(NOTE_TIME_STAMP);
        $noteParams['noteIds'] = NOTE_ID;

        $result = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($result);

        // Only send updateContactNotes and noteIds in array
        $noteParams = array();
        $noteParams['updateContactNotes'] = array(CONTACT_NOTE);
        $noteParams['updateTimeStamps'] = NOTE_TIME_STAMP;
        $noteParams['noteIds'] = array(NOTE_ID);

        $result = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($result);

        // Only send noteIds and updateTimeStamps in array
        $noteParams = array();
        $noteParams['updateContactNotes'] = CONTACT_NOTE;
        $noteParams['updateTimeStamps'] = array(NOTE_TIME_STAMP);
        $noteParams['noteIds'] = array(NOTE_ID);

        // test if updateContactNotes is set or not
        $result = $this->model->updateNotes(CONTACT_ID, $noteParams);
        $this->assertFalse($result);


        //Send valid inputs
        $noteParams = array();
        $noteParams['updateContactNotes'] = array(CONTACT_UPDATE_NOTE);
        $noteParams['updateTimeStamps'] = array(UPDATE_TIME_STAMP);
        $noteParams['noteIds'] = array(NOTE_ID);
        $this->assertTrue(
            $this->model->updateNotes(
                CONTACT_ID, $noteParams
            )
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::deleteNotes
     *
     * @return void
     */
    public function testDeleteNotes()
    {
        //Test with valid input
        $noteParams['deleteNoteIds'] = array(NOTE_ID);
        $this->assertTrue(
            $this->model->deleteNotes(
                CONTACT_ID, $noteParams
            )
        );

        //Test with valid input
        //It will delete all notes for that contact
        $this->assertTrue(
            $this->model->deleteNotes(
                CONTACT_ID, null, 1
            )
        );

    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::deleteNotes
     *
     * @return void
     * @expectedException Exception
     */
    public function testDeleteNotesWithException()
    {
        $this->model->deleteNotes(null, '');
    }

    /**
     * Tests RapidFunnel_Model_AccountContactNote::deleteNotes
     *
     * @return void
     * @expectedException Exception
     */
    public function testDeleteNotesWithExceptionOne()
    {
        // test invalid input (force exception)
        // test with deleteNoteIds is not in array format
        $noteParams['deleteNoteIds'] = NOTE_ID;
        $this->model->deleteNotes(CONTACT_ID, $noteParams);

    }


    public function testDeleteNoteTestWithException() {

        $deleteNoteParams = array();
        $deleteNoteParams['deleteNoteIds'] = array(LARGE_NUMBER);
        $returnResult = $this->model->deleteNotes(
            CONTACT_ID, $deleteNoteParams
        );
        $this->assertFalse($returnResult);
    }
}