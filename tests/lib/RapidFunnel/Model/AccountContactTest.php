<?php
/**
 * RapidFunnel_Model_AccountContactTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountContactTest
 * RapidFunnel/Model/AccountContactTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountContactTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    private $_model;
    public $userDb;

    /*
     * @var bool Flag to track if contact entity needs its opt data reset
     *           see tearDown().
     */ 
    private $_resetOptData = false;

    /**
     * RapidFunnel_Model_AccountContactTest::tearDown()
     *
     * @return void
     */
    public function tearDown()
    {
        /**
         * Checks if the reset flag for opt data has been set by the test
         * methods. This ensures that update database query is executed
         * only when needed.
         */
        if ($this->_resetOptData) {
            $data = array();

            // reset contact object after optin
            $data['optIn'] = 0;
            $data['optInSend'] = 0;
            $data['optInDate'] = null;

            // reset contact object after optout
            $data['optOut'] = 0;
            $data['optOutDate'] = null;

            $data['status'] = 0;

            $this->_model->db->update(
                'accountContact', $data, 'id = ' . CONTACT_ID
            );

            $this->_resetOptData = false;
        }
    }

    /**
     * RapidFunnel_Model_AccountContactTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountContact();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountContact.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds3 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountContactNotes.xml'
        );
        $ds4 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountCampaignContactEmail.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);
        $compositeDs->addDataSet($ds3);
        $compositeDs->addDataSet($ds4);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::__construct()
     * 
     * @return void
     */ 
    public function testAccountContactModel()
    {
        $model = new RapidFunnel_Model_AccountContact();
        $this->assertInstanceOf('RapidFunnel_Model_AccountContact', $model);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getContact()
     * 
     * @return void
     */
    public function testGetContact()
    {
        // if there is wrong DB operation to go Catch block
        $result = $this->_model->getContact(null, null, null);
        $this->assertEquals(false, $result);

        // if we providing correct accountId
        $result = $this->_model
            ->getContact(ACCOUNT_ID, ADMIN_ID, ADMIN_ROLE_ID);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertNotEmpty($result);

        $this->assertArrayHasKey('id', $result[0]);
        $this->assertArrayHasKey('campaignName', $result[0]);
        $this->assertArrayHasKey('phone', $result[0]);

        $result = $this->_model->getContact(
            ACCOUNT_ID, ADMIN_ID, MANAGER_ROLE_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertNotEmpty($result);

        $this->assertArrayHasKey('id', $result[0]);
        $this->assertArrayHasKey('phone', $result[0]);

        /*
         * Validate that API key parses output properly
         * 
         * - test object has two phone numbers separated by commas, when the API
         *   argument is passed, only the first phone would be displayed
         */
        $result = $this->_model->getContact(
            ACCOUNT_ID, ADMIN_ID, USER_ROLE_ID, '', null, true
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertNotEmpty($result);

        $this->assertArrayHasKey('id', $result[0]);
        $this->assertArrayHasKey('campaignName', $result[0]);
        $this->assertArrayHasKey('phone', $result[0]);

        $result = $this->_model->getContact(ACCOUNT_ID, ADMIN_ID, USER_ROLE_ID);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertNotEmpty($result);

        $this->assertArrayHasKey('id', $result[0]);
        $this->assertArrayHasKey('campaignName', $result[0]);
        $this->assertArrayHasKey('phone', $result[0]);

        
        // if we providing wrong accountId
        $result = $this->_model->getContact(
            WRONG_ACCOUNT_ID, ADMIN_WRONG_ID, WRONG_ROLE_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertEmpty($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getContact()
     *
     * @return void
     */
    public function testGetContactWithGroupIds()
    {
        $result = $this->_model->getContact(
            ACCOUNT_ID, ADMIN_ID, MANAGER_ROLE_ID, array(1), 'sachin@mindfiresolutions.com', null, 1
        );

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertNotEmpty($result);

        $this->assertNotEmpty($result[0]['id']);
        $this->assertNotEmpty($result[0]['accountId']);
        $this->assertNotEmpty($result[0]['firstName']);
        $this->assertNotEmpty($result[0]['email']);
        $this->assertNotEmpty($result[0]['phone']);
        $this->assertNotEmpty($result[0]['status']);
        $this->assertNotEmpty($result[0]['created']);
        $this->assertNotEmpty($result[0]['noteTimeStamp']);
        $this->assertNotEmpty($result[0]['note']);
        $this->assertArrayHasKey('lastName', $result[0]);
        $this->assertArrayHasKey('homeEmail', $result[0]);
        $this->assertArrayHasKey('workEmail', $result[0]);
        $this->assertArrayHasKey('otherEmail', $result[0]);
        $this->assertArrayHasKey('home', $result[0]);
        $this->assertArrayHasKey('work', $result[0]);
        $this->assertArrayHasKey('other', $result[0]);
        $this->assertArrayHasKey('mobile', $result[0]);
        $this->assertArrayHasKey('campaignId', $result[0]);
        $this->assertArrayHasKey('zip', $result[0]);
        $this->assertArrayHasKey('title', $result[0]);


    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getDetails()
     *
     * @return void
     */
    public function testGetDetails()
    {
        // if there is wrong DB operation to go Catch block
        $getDetail = $this->_model->getDetails(null);
        $this->assertEquals(false, $getDetail);

        // if we providing correct accountId
        $getDetail = $this->_model->getDetails(1);
        $this->assertArrayHasKey('id', $getDetail);

        // if we providing wrong accountId
        $get = $this->_model->getDetails(0);
        $this->assertEquals(null, $get);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::checkContactLimit()
     *
     * @return void
     */
    public function testCheckContactLimit()
    {
       // test for invalid input (force exception)
        $results = $this->_model->checkContactLimit(null, null);
        $this->assertFalse($results);

        // test for valid input (within limit)
        $results = $this->_model->checkContactLimit(ACCOUNT_ID, TEMP_USER_ID2);
        $this->assertTrue($results['isAllowed']);

        // test for valid input (at limit)
        $results = $this->_model->checkContactLimit(
            FULL_CONTACTS_ACCOUNT_ID, WRONG_USER_ID
        );
        $this->assertTrue($results['isAllowed']);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getContactForWeekMonthPreMonth()
     *
     * @return void
     */
    public function testGetContactForWeekMonthPreMonth()
    {
        $results = $this->_model
            ->getContactForWeekMonthPreMonth(ACCOUNT_ID);
        $this->assertGreaterThanOrEqual(0, $results);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getContactForWeekMonthPreMonth()
     *
     * @return void
     */
    public function testGetContactForWeekMonthPreMonthWithException()
    {
        $result = $this->_model->getContactForWeekMonthPreMonth(
            LARGE_NUMBER
        );
        $this->assertFalse($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getContactForWeekMonthPreMonth()
     *
     * @expectedException Exception
     *
     * @return void
     */
    public function testGetContactForWeekMonthPreMonthWithNullUserID()
    {
        $this->_model->getContactForWeekMonthPreMonth(null);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getContactForWeekMonthPreMonth()
     *
     * @expectedException Exception
     *
     * @return void
     */

    public function testGetContactForWeekMonthPreMonthWithBlankInputs()
    {
        $this->_model->getContactForWeekMonthPreMonth('');
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getContactForWeekMonthPreMonth()
     *
     * @expectedException Exception
     *
     * @return void
     */
    public function testGetContactForWeekMonthPreMonthWithInvalidInputs()
    {
        $this->_model->getContactForWeekMonthPreMonth(0);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::releaseContact()
     *
     * @return void
     */
    public function testReleaseContact()
    {
        $result = $this->_model->releaseContact(
            CONTACT_WRONG_EMAIL, ACCOUNT_ID, CONTACT_ID
        );
        $this->assertTrue($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::releaseContact()
     *
     * @return void
     */
    public function testReleaseContactWithException()
    {
        $result = $this->_model->releaseContact(
            CONTACT_WRONG_EMAIL, LARGE_NUMBER, CONTACT_ID
        );
        $this->assertFalse($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::setStatusAsNew()
     *
     * @return void
     */
    public function testSetStatusAsNew()
    {
        $result = $this->_model->setStatusAsNew(CONTACT_WRONG_EMAIL);
        $this->assertTrue($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::
     *  isDuplicateCampaignAssignedContact
     *
     * @return void
     * @expectedException exception
     */
    public function testIsDuplicateCampaignAssignedContactWithException()
    {
        $this->_model->isDuplicateContactOrCampaignAssigned(null, null, null);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::
     *  isDuplicateCampaignAssignedContact
     *
     * @return void
     * @expectedException exception
     */
    public function testIsDuplicateCampaignAssignedContactWithExce()
    {
        $this->_model->isDuplicateContactOrCampaignAssigned(
            ACCOUNT_ID, null, CONTACT_ID
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::
     *  isDuplicateCampaignAssignedContact
     *
     * @return void
     */
    public function testIsDuplicateCampaignAssignedContactException()
    {
        // to get details with valid details of account id, email and contact id
        $isDuplicateContact
            = $this->_model->isDuplicateContactOrCampaignAssigned(
            ACCOUNT_ID, CONTACT_EMAIL, ACCOUNT_USER_ID ,LARGE_NUMBER, true
        );
        $this->assertFalse($isDuplicateContact);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::
     *  isDuplicateCampaignAssignedContact
     *
     * @return void
     */
    public function testIsDuplicateCampaignAssignedContact()
    {
         // to get details with valid details of account id, email and contact id
         $isDuplicateContact
             = $this->_model->isDuplicateContactOrCampaignAssigned(
             ACCOUNT_ID, CONTACT_EMAIL, ACCOUNT_USER_ID ,CONTACT_ID, true
         );
         $this->assertEquals(0, $isDuplicateContact);
    }

     /** Tests RapidFunnel_Model_AccountContact
     * Tests RapidFunnel_Model_AccountContact::setStatusAsNew()
     *
     * @return void
     */
    public function testSetStatusAsNewWithException()
    {
        $result = $this->_model->setStatusAsNew(LARGE_NUMBER);
        $this->assertFalse($result);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getOptInRateOfUser()
     *
     * @return void
     */
    public function testGetOptInRateOfUserWithException()
    {
        $returnResult = $this->_model->getOptInRateOfUser(null, null);
        $this->assertFalse($returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getOptInRateOfUser() for null user id
     *
     * @return void
     */
    public function testGetOptInRateOfUserNullUserId()
    {
        $returnResult = $this->_model->getOptInRateOfUser(ACCOUNT_ID, null);
        $this->assertFalse($returnResult);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::getOptInRateOfUser() for valid input
     *
     * @return void
     */
    public function testGetOptInRateOfUser()
    {
        $response = $this->_model->getOptInRateOfUser(ACCOUNT_ID, USER_ID);
        $this->assertGreaterThanOrEqual(0, $response);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::deleteContact()
     *
     * This test case for editing an existing contact
     */
    public function testDeleteContact()
    {
        // checking accountContact
        $select = $this->_model->db->select()
            ->from('accountContact', array('id'))
            ->where('id = ?', CONTACT_ID);
        $stmt = $this->_model->db->query($select);
        $result = $stmt->fetch();
        $this->assertNotEmpty($result['id']);

        // checking accountCampaignContactEmail
        $select = $this->_model->db->select()
            ->from('accountCampaignContactEmail', array('id'))
            ->where('contactId = ?', CONTACT_ID);
        $stmt = $this->_model->db->query($select);
        $result = $stmt->fetch();
        $this->assertNotEmpty($result['id']);

        // checking accountMailLog
        $select = $this->_model->db->select()
            ->from('accountMailLog', array('recipientId'))
            ->where('recipientId = ?', CONTACT_ID);
        $stmt = $this->_model->db->query($select);
        $result = $stmt->fetch();
        $this->assertNotEmpty($result['recipientId']);


        $returnResult = $this->_model->deleteContact(CONTACT_ID);
        $this->assertTrue($returnResult);

        // checking accountContact
        $select = $this->_model->db->select()
            ->from('accountContact', array('id'))
            ->where('id = ?', CONTACT_ID);
        $stmt = $this->_model->db->query($select);
        $result = $stmt->fetch();
        $this->assertFalse($result);

        // checking accountCampaignContactEmail
        $select = $this->_model->db->select()
            ->from('accountCampaignContactEmail', array('id'))
            ->where('contactId = ?', CONTACT_ID);
        $stmt = $this->_model->db->query($select);
        $result = $stmt->fetch();
        $this->assertFalse($result);

        // checking accountMailLog
        $select = $this->_model->db->select()
            ->from('accountMailLog', array('recipientId'))
            ->where('recipientId = ?', CONTACT_ID)
            ->where('recipientType = ?', RECIPIENT_TYPE);
        $stmt = $this->_model->db->query($select);
        $result = $stmt->fetch();
        $this->assertFalse($result);

    }

    /**
     * Tests RapidFunnel_Model_AccountContact::deleteContact()
     *
     * This test case generates an exceptions and handles it
     *
     * @expectedException Exception
     */
    public function testDeleteContactWithException()
    {
        $this->_model->deleteContact(null);
    }

    /**
     * Tests RapidFunnel_Model_AccountContact::deleteContact()
     *
     * This test case generates an exception and checks the try catch
     * is working fine or not
     */
    public function testDeleteContactWithInvalidInput()
    {
        $returnResult = $this->_model->deleteContact(LARGE_NUMBER);
        $this->assertFalse($returnResult);
    }

}