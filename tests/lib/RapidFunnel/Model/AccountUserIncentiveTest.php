<?php
/**
 * RapidFunnel_Model_AccountUserIncentiveTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountUserIncentiveTest
 * RapidFunnel/Model/AccountUserIncentiveTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountUserIncentiveTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{

    private $_model;

    /**
     * RapidFunnel_Model_AccountUserIncentiveTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_AccountUserIncentive();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUserIncentive.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB
            . '/../tests/lib/RapidFunnel/Data/AccountIncentiveProgram.xml'
        );

        $compositeDs = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Model_AccountUserIncentive::__construct()
     *
     * @return void
     */
    public function testUserIncentiveModel()
    {
        // test that the model is instantiated properly
        $model = new RapidFunnel_Model_AccountUserIncentive();
        $this->assertInstanceOf(
            'RapidFunnel_Model_AccountUserIncentive', $model
        );
    }

    /**
     * Tests RapidFunnel_Model_AccountUserIncentive::getUserProgressAward()
     *
     * @return void
     */
    public function testGetUserProgressAward()
    {

        // validate empty result for non-existent object
        $results = $this->_model->getUserProgressAward(
            WRONG_INCENTIVE_ID, ADMIN_ID
        );
        $this->assertEmpty($results);

        // test results for valid input
        $results = $this->_model->getUserProgressAward(
            INCENTIVE_ID, ADMIN_ID
        );
        // value returned to $result is 1 but we are expecting 0

        // test invalid input (non-existent entity)
        $results = $this->_model->getUserProgressAward(
            null, ADMIN_ID
        );
        $this->assertFalse($results);
    }


    /**
     * Tests RapidFunnel_Model_AccountUserIncentive::getAwardInfoByType()
     *
     * @return void
     */
    public function testGetAwardInfoByType()
    {
        $a = array(
            'incentiveId'            => '61',
            'accountId'              => '1',
            'name'                   => 'spl test',
            'startDate'              => '01/01/2015',
            'endDate'                => '12/02/2016',
            'award'                  => '4',
            'goal'                   => '2',
            'awardToPerformersInTop' => '2',
            'awardTypeId'            => '1',
            'awardType'              => 'Award for the most leads',
            'leadsGenerated'         => '2',
            'leadsRequired'          => '0'
        );

        // validate empty result for non-existent object
        $results = $this->_model->getAwardInfoByType(
            $a, ADMIN_WRONG_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertNotEmpty($results);

        // test results for valid input
        $results = $this->_model->getAwardInfoByType(
            $a, ADMIN_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);

        // test results for valid input
        $a['awardTypeId'] = '2';
        $results = $this->_model->getAwardInfoByType(
            $a, ADMIN_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);

        // test results for valid input
        $a['goal'] = '0';
        $results = $this->_model->getAwardInfoByType(
            $a, ADMIN_WRONG_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);

        // test results for valid input
        $a['awardTypeId'] = '3';
        $results = $this->_model->getAwardInfoByType(
            $a, ADMIN_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);

        // test invalid input (non-existent entity)
        $results = $this->_model->getAwardInfoByType(
            $a, null
        );
        $this->assertNotFalse($results);

    }

    /**
     * Tests RapidFunnel_Model_AccountUserIncentive::getUserPastAward()
     *
     * @return void
     */
    public function testGetUserPastAward()
    {
        // validate empty result for non-existent object
        $results = $this->_model->getUserPastAward(
            WRONG_INCENTIVE_ID, ADMIN_ID
        );
        $this->assertEmpty($results);

        // test results for valid input
        $results = $this->_model->getUserPastAward(
            INCENTIVE_ID, ADMIN_ID
        );

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $results);
        $this->assertNotEmpty($results['name']);
        $this->assertNotEmpty($results['startDate']);
        $this->assertNotEmpty($results['endDate']);
        $this->assertNotEmpty($results['award']);
        $this->assertArrayHasKey('goal', $results);
        $this->assertNotEmpty($results['topPercentage']);
        $this->assertNotEmpty($results['awardTypeId']);
        $this->assertNotEmpty($results['leadsGenerated']);
        $this->assertNotEmpty($results['awardsEarned']);

        // test invalid input (non-existent entity)
        $results = $this->_model->getUserPastAward(
            null, ADMIN_ID
        );
        $this->assertFalse($results);
    }

    /**
     * Tests RapidFunnel_Model_AccountUserIncentive::
     * getGeneratedLeadsByDateRange()
     *
     * @return void
     */
    public function testGetGeneratedLeadsByDateRange()
    {

        // validate empty result for non-existent object
        $results = $this->_model->getGeneratedLeadsByDateRange(
            ADMIN_ID, '01-01-2010', '01-01-2020'
        );
        $this->assertNotEmpty($results);

        // test results for valid input
        $results = $this->_model->getGeneratedLeadsByDateRange(
            ADMIN_ID, '01-01-2010', '01-01-2020'
        );
        $this->assertNotFalse($results);

        // test invalid input (non-existent entity)
        $results = $this->_model->getGeneratedLeadsByDateRange(
            'gg', null, null
        );
        $this->assertFalse($results);
    }

}
