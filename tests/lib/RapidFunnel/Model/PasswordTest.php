<?php
/**
 * RapidFunnel_Model_PasswordTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_PasswordTest
 * RapidFunnel/Model/PasswordTest.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_PasswordTest extends PHPUNit_Framework_TestCase
{

    private $_model;

    /**
     * RapidFunnel_Model_PasswordTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new RapidFunnel_Model_Password();
    }

    /**
     * Tests RapidFunnel_Model_Password::__construct()
     *
     * @return void
     */
    public function testPasswordModel()
    {
        $this->_model = new RapidFunnel_Model_Password();
        $this->assertInstanceOf('RapidFunnel_Model_Password', $this->_model);
    }

    /**
     * Tests RapidFunnel_Model_Password::create()
     *
     * @return void
     */
    public function testCreate()
    {
        // if password is null
        $results = $this->_model->create(null);
        $this->assertEquals(true, $results);

        // if password is not null
        $results = $this->_model->create('password');
        $this->assertEquals(true, $results);
    }

    /**
     * Tests RapidFunnel_Model_Password::__set()
     *
     * @return void
     */
    public function test__set()
    {
        $results = $this->_model->__set('rawPass', 'abcde');
        $this->assertTrue($results);
        $this->setExpectedException('Exception');
        $results = $this->_model->__set('rawPassddddd', 'abcde');

    }

    /**
     * Tests RapidFunnel_Model_Password::__get()
     *
     * @return void
     */
    public function test__get()
    {
        $this->_model->__set('rawPass', 'abcde');
        $results = $this->_model->__get('rawPass');
        $this->assertEquals('abcde', $results);

        $this->setExpectedException('Exception');
        $results = $this->_model->__get('rawPassddddd');
    }
}
