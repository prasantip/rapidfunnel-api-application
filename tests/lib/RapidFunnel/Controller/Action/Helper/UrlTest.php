<?php

/**
 * RapidFunnel_Controller_Action_Helper_UrlTest
 * RapidFunnel/Controller/Action/Helper/UrlTest.php
 *
 * @category  Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @copyright 2016 RapidFunnel company
 * @license   Not licensed for external use
 */
class RapidFunnel_Controller_Action_Helper_UrlTest
    extends PHPUNit_Framework_TestCase
{
    private $_helper;

    /**
     * RapidFunnel_Controller_Action_Helper_UrlTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper = new RapidFunnel_Controller_Action_Helper_Url();
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_UrlTest
     * ::changeResourceUrlsInContent()
     * Tests the changeResourceUrlsInContent()
     *
     * @return void
     */
    public function testChangeResourceUrlsInContent()
    {
        $returnResult = $this->_helper->changeResourceUrlsInContent(
            USER_ID, EMAIL_CONTENT, ACCOUNT_USER_EMAIL
        );

        $this->assertEquals($returnResult, EMAIL_CONTENT);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_UrlTest
     * ::changeResourceUrlsInContentWithCampaign()
     * Tests the changeResourceUrlsInContent() with Campaign
     *
     * @return void
     */
    public function testChangeResourceUrlsInContentWithCampaign()
    {
        $returnResult = $this->_helper->changeResourceUrlsInContent(
            USER_ID, EMAIL_CONTENT, ACCOUNT_USER_EMAIL, true
        );

        $this->assertEquals($returnResult, EMAIL_CONTENT);

    }

}