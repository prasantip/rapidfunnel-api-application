<?php

/**
 * RapidFunnel_Controller_Action_Helper_AuthenticateTest
 * RapidFunnel/Controller/Action/Helper/AuthenticateTest.php
 *
 * @category  Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @copyright 2016 RapidFunnel company
 * @license   Not licensed for external use
 */
class RapidFunnel_Controller_Action_Helper_AuthenticateTest
    extends  RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_helper;

    /**
     * Constructor method for the class.
     * Sets up the attributes of the method by instantiating an object from the
     * Helper class being tested.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper = new RapidFunnel_Controller_Action_Helper_Authenticate(
        );
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/Account.xml'
        );

        $compositeDs
            = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AuthenticateTest::testGetUserAccountStatus()
     *
     * @returns void
     */
    public function testGetUserAccountStatus()
    {

        $returnResult = $this->_helper->getUserAccountStatus(TEMP_USER_ID2);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnResult);
        $this->assertEquals(CONTACT_STATUS, $returnResult['status']);
        $this->assertEquals(0   , $returnResult['passThrough']);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AuthenticateTest::testGetUserAccountStatus()
     *
     * @returns void
     */
    public function testGetUserAccountStatusWithIgnorePayment()
    {

        $returnResult = $this->_helper->getUserAccountStatus(NEW_USER_ID);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnResult);
        $this->assertEquals(CONTACT_STATUS, $returnResult['status']);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AuthenticateTest::testGetUserAccountStatus()
     *
     * @returns void
     */
    public function testGetUserAccountStatusOfDifferent()
    {

        $returnResult = $this->_helper->getUserAccountStatus(PAYMENT_USER_ID);

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnResult);
        $this->assertEquals(true, $returnResult['status']);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AuthenticateTest::
     * testGetUserAccountStatus()
     *
     * @returns void
     */
    public function testGetUserAccountStatusOfDifferentWithException()
    {
        $returnResult = $this->_helper->getUserAccountStatus(LARGE_NUMBER);
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnResult);
        $this->assertFalse($returnResult['status']);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AuthenticateTest
     * ::testAuthApiUser()
     *
     * @return void
     */
    public function testAuthApiUser()
    {

        $returnResult = $this->_helper->authApiUser(
            ACCOUNT_USER_EMAIL,
            ACCOUNT_USER_PASSWORD
        );

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnResult);
        $this->assertNotEmpty($returnResult);
        $this->assertEquals(FREE_ACCOUNT_ID, $returnResult['accountId']);
        $this->assertEquals(CONTACT_STATUS, $returnResult['accountInfo']['status']);
        $this->assertNotEmpty($returnResult['accessToken']);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AuthenticateTest
     * ::testAuthApiUser()
     *
     * @return void
     */
    public function testAuthApiUserWithWebView()
    {

        $returnResult = $this->_helper->authApiUser(
            ACCOUNT_USER_EMAIL,
            ACCOUNT_USER_PASSWORD, WEB_VIEW
        );
        $this->assertFalse($returnResult);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AuthenticateTest::testAuthUserToken()
     *
     * Functionality to test authUserToken()
     *
     * @return void
     */
    public function testAuthUserToken()
    {
        $returnResult = $this->_helper->authUserToken(
            ID,
            ALT_ACCESS_TOKEN
        );
        $this->assertTrue($returnResult);
    }

    public function testSetAuthResponse()
    {
        $result = $this->_helper->setAuthResponse(
            array(
                'accessToken' => ACCESS_TOKEN,
                'accessTokenExpiresOn' => date(
                    'Y-m-d', strtotime('+ 1 days')
                ),
                'userId' => USER_ID,
                'accountId' => ACCOUNT_ID
            ),
            0
        );

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertArrayHasKey('content', $result['response']);
    }

    public function testSetAuthResponseWithExpireTime()
    {
        $result = $this->_helper->setAuthResponse(
            array(
                'accessToken' => ACCESS_TOKEN,
                'accessTokenExpiresOn' => date(
                    'Y-m-d', strtotime('- 1 days')
                ),
                'userId' => USER_ID,
                'accountId' => ACCOUNT_ID
            ),
            0
        );

        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $result);
        $this->assertArrayHasKey('errorMessage', $result['response']);
    }

    public function testIsLSFreeEnterpriseProUser()
    {
        $result = $this->_helper->isLSFreeEnterpriseProUser(
            LS_ACCOUNT_ID, RapidFunnel_Configs::_instance(), '1',
            '3'
        );

        $this->assertFalse($result);
    }

    public function testIsLSFreeEnterpriseProUserWithNoTrial()
    {
        $result = $this->_helper->isLSFreeEnterpriseProUser(
            LS_ACCOUNT_ID, RapidFunnel_Configs::_instance(), '0',
            '3'
        );

        $this->assertTrue($result);
    }

    public function testIsLSFreeEnterpriseProUserForNonLSAccount()
    {
        $result = $this->_helper->isLSFreeEnterpriseProUser(
            ACCOUNT_ID, RapidFunnel_Configs::_instance(), '1',
            '3'
        );

        $this->assertTrue($result);
    }

}

