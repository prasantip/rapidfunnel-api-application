<?php
/**
 * RapidFunnel_Controller_Action_Helper_UserGroupTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Controller_Action_Helper_UserGroupTest
 * RapidFunnel/Controller/Action/Helper/UserGroupTest.php
 *
 * @category  Action_Helper
 * @package   Digitalassets_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Controller_Action_Helper_UserGroupTest
    extends PHPUNit_Framework_TestCase
{
    private $_helper;

    /**
     * RapidFunnel_Controller_Action_Helper_UserGroupTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper
            = new RapidFunnel_Controller_Action_Helper_UserGroup();
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_UserGroup
     * ::getAllAccessibleGroupIdsByUserId()
     *
     * Tests if the function getAllAccessibleGroupIdsByUserId()
     * returns an array of accessible groupIds for an userId
     *
     * @return void
     */
    public function testGetAllAccessibleGroupIdsByUserId()
    {
        $result = $this->_helper->getAllAccessibleGroupIdsByUserId(USER_ID);
        $this->assertNotEmpty($result);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_UserGroup
     * ::getAllAccessibleGroupIdsByUserId()
     *
     * @expectedException Exception
     */
    public function testGetAllAccessibleGroupIdsByUserIdWithInvalidUserId()
    {
        $this->_helper->getAllAccessibleGroupIdsByUserId(null);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_UserGroup
     * ::getAllAccessibleGroupIdsByUserId()
     *
     * @return void
     */
    public function testGetAllAccessibleGroupIdsByUserIdWithWrongUserId()
    {
        $returnResult = $this->_helper->getAllAccessibleGroupIdsByUserId(
            WRONG_USER_ID
        );
        $this->assertInternalType(INTERNAL_TYPE_ARRAY, $returnResult);
        $this->assertArrayNotHasKey('0', $returnResult);
    }
}