<?php

/**
 * RapidFunnel_Controller_Action_Helper_AccountUser
 * RapidFunnel/Controller/Action/Helper/AccountUserTest.php
 *
 * @category  Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @copyright 2016 RapidFunnel company
 * @license   Not licensed for external use
 */
class RapidFunnel_Controller_Action_Helper_AccountUserTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_helper;
    public $emails = array(ADMIN_WRONG_EMAIL,SUP_ADMIN_EMAIL,ADMIN_EMAIL);

    /**
     * Constructor method for the class.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper = new RapidFunnel_Controller_Action_Helper_AccountUser();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );

        $compositeDs
            = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AccountUser::isTrialUser()
     *
     * @return void
     */
    public function testIsTrialUserWithZero()
    {
        $returnResult = $this->_helper->isTrialUser(ID, 0);

        $this->assertEquals(0, $returnResult);

    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AccountUser::isTrialUser()
     *
     * @return void
     */
    public function testIsTrialUserWithInvalidUserId()
    {
        $result = $this->_helper->isTrialUser(null, null);
        $this->assertNull($result);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AccountUser::isTrialUser()
     *
     * @return void
     */
    public function testIsTrialUserWithException()
    {
        $result = $this->_helper->isTrialUser(
            null, ACCOUNT_ID_STRING, LARGE_NUMBER
        );

        $this->assertFalse($result);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AccountUser
     * ::isEnterpriseFreeUser()
     *
     * @return void
     */
    public function testIsEnterpriseFreeUser()
    {
        $userDetailsArray['ignoreUserPayment'] = IGNORE_USER_PAYMENT;
        $userDetailsArray['accountId'] = ACCOUNT_ID;
        $userDetailsArray['isTrial'] = IS_TRIAL;
        $userDetailsArray['status'] = SUSPEND_STATUS;
        $userDetailsArray['id'] = ADMIN_ID;
        $userDetailsArray['payThroughAuthNet'] = 1;

        $returnResult = $this->_helper->isEnterpriseFreeUser($userDetailsArray);

        $this->assertFalse($returnResult);

    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AccountUser
     * ::isTrialUser()
     *
     * @return void
     */
    public function testIsTrialUser()
    {
        $result = $this->_helper->isTrialUser(ID, USER_ID);
        $this->assertEquals(1, $result);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_AccountUser
     * ::isTrialUser()
     *
     * @return void
     */
    public function testIsTrialUserWithTrialExpireDate()
    {
        $result = $this->_helper->isTrialUser(ID, USER_ID, Date("Y-m-d"));
        $this->assertEquals(1, $result);
    }

}

