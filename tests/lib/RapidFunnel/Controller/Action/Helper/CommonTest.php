<?php
/**
 * RapidFunnel_Controller_Action_Helper_CommonTest File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Controller_Action_Helper_CommonTest
 * RapidFunnel/Controller/Action/Helper/CommonTest.php
 *
 * @category  Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Controller_Action_Helper_CommonTest
    extends RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
{
    private $_helper;

    /**
     * RapidFunnel_Controller_Action_Helper_CommonTest::__construct()
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper = new RapidFunnel_Controller_Action_Helper_Common();
    }

    /**
     * This function prepares the database before running test cases.
     *
     * @return PHPUnit_Extensions_Database_DataSet_CompositeDataSet
     */
    public function getDataSet()
    {
        $ds1 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUser.xml'
        );
        $ds2 = $this->createXmlDataSet(
            APP_LIB . '/../tests/lib/RapidFunnel/Data/AccountUserLegalshield.xml'
        );

        $compositeDs
            = new PHPUnit_Extensions_Database_DataSet_CompositeDataSet();
        $compositeDs->addDataSet($ds1);
        $compositeDs->addDataSet($ds2);

        return $compositeDs;
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::updateAccessToken()
     *
     * @return void
     */
    public function testUpdateAccessToken()
    {
        $result = $this->_helper->updateAccessToken(
            new RapidFunnel_Model_AccountUser(),
            SUP_ADMIN_WRONG_ID, TEST_ACCESS_TOKEN, 1
        );
        $this->assertEquals($result, 0);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::updateAccessToken()
     *
     * @expectedException 'No model provided'
     *
     * @return void
     */
    public function testUpdateAccessTokenWithNullAccountUserModel()
    {
        $this->_helper->updateAccessToken(null, null, null);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::updateAccessToken()
     *
     * @expectedException 'Not valid model'
     *
     * @return void
     */
    public function testUpdateAccessTokenWithInvalidAccountUserModel()
    {
        $this->_helper->updateAccessToken(123, null, null);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::updateAccessToken()
     *
     * @expectedException 'No User Id is provided'
     *
     * @return void
     */
    public function testUpdateAccessTokenWithNullUserId()
    {
        $this->_helper->updateAccessToken(
            new RapidFunnel_Model_AccountUser(),
            null, null
        );
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::updateAccessToken()
     *
     * @expectedException 'Not valid User Id'
     *
     * @return void
     */
    public function testUpdateAccessTokenWithInvalidUserId()
    {
        $this->_helper->updateAccessToken(
            new RapidFunnel_Model_AccountUser(),
            'abc', null
        );
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::updateAccessToken()
     *
     * @expectedException 'No token provided'
     *
     * @return void
     */
    public function testUpdateAccessTokenWithNullToken()
    {
        $this->_helper->updateAccessToken(
            new RapidFunnel_Model_AccountUser(),
            ADMIN_ID, null
        );
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::updateAccessToken()
     *
     * @expectedException 'Not valid token'
     *
     * @return void
     */
    public function testUpdateAccessTokenWithInvalidToken()
    {
        $this->_helper->updateAccessToken(
            new RapidFunnel_Model_AccountUser(),
            ADMIN_ID, 123
        );
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::addAccountDetailsInMail()
     *
     * @return void
     */
    public function testAddAccountDetailsInMailInValidAccountId()
    {
        $result  = $this->_helper->addAccountDetailsInMail(null, null, null);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('0', $result['status']);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::addAccountDetailsInMail()
     *
     * @return void
     */
    public function testAddAccountDetailsInMailValidAccountId()
    {
        $result  = $this->_helper->addAccountDetailsInMail(ACCOUNT_ID, null, null);
        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('htmlContent', $result);
        $this->assertArrayHasKey('textContent', $result);
        $this->assertEquals('1', $result['status']);
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::concatMultiField()
     *
     * @return void
     */
    public function testConcatMultiField()
    {
        $data = array(
            'element1', 'element2', 'element3', 'element4', 'element5'
        );
        $result = $this->_helper->concatMultiField($data);
        $this->assertInternalType('string', $result);
        $this->assertEquals(
            'element1,element2,element3,element4,element5',
            $result
        );
    }

    /**
     * Tests RapidFunnel_Controller_Action_Helper_Common::generateKey()
     *
     * @return void
     */
    public function testGenerateKey()
    {
        $result = $this->_helper->generateKey('apiLogin', ID, ADMIN_EMAIL);
        $this->assertInternalType('string', $result);

        $this->assertInternalType('string', $result);

        $result = $this->_helper->generateKey('apiContactList');
        $this->assertInternalType('string', $result);

        $result = $this->_helper->generateKey('apiAuthToken');
        $this->assertInternalType('string', $result);

        $result = $this->_helper->generateKey('apiDashboardInfo');
        $this->assertInternalType('string', $result);

    }

    /**
     * Functionality to test removeHyperLinkInEmail
     *
     * @expectedException Exception
     */
    public function testRemoveHyperLinkInEmailExceptionText()
    {
        $this->_helper->removeHyperLinkInEmail(null, null);
    }

    /**
     * Functionality to test removeHyperLinkInEmail
     *
     * @expectedException Exception
     */
    public function testRemoveHyperLinkInEmailExceptionEmailBody()
    {
        $this->_helper->removeHyperLinkInEmail('[SenderFacebookUrl]', null);
    }

    /**
     * Functionality to test for removeHyperLinkInEmail
     */
    public function testRemoveHyperLinkInEmail()
    {
        $result = $this->_helper->removeHyperLinkInEmail('[SenderFacebookUrl]',
            'Hello I am text');
        $this->assertInternalType(INTERNAL_TYPE_STRING, $result);
    }
}

