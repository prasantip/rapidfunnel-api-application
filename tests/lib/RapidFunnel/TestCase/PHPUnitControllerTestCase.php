<?php

/**
 */
//https://github.com/bskendig/zf1/commit/18327f18baa2fa0b6c8a47d2ad323859dc35040a
require_once 'ControllerTestCase.php';

class PHPUnit_ControllerTestCase extends ControllerTestCase
{
    public function setUp()
    {
        $this->bootstrap = new Zend_Application(APP_ENV,
                array(
            'config' => array(
                COMMON_CONFIG_INI_PATH,
                APP_INI_PATH
            )
                )
        );

        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function loginUser($user, $password)
    {
        $this->request->setMethod('POST')
                      ->setPost(array(
                          'username' => $user,
                          'password' => $password,
                      ));
        $this->dispatch('/admin/user/login');

        $this->resetRequest()
             ->resetResponse();
    }

    public function AccountloginUser($user, $password)
    {
        $this->request->setMethod('POST')
                      ->setPost(array(
                          'username' => $user,
                          'password' => $password,
                      ));
        $this->dispatch('/account/user/login');

        $this->resetRequest()
             ->resetResponse();
    }

    public function ApiloginUser($userId, $accessToken)
    {
        $this->request->setMethod('POST')
                      ->setPost(array(
                          'userId' => $userId,
                          'accessToken' => $accessToken,
                      ));
        $this->dispatch('/api/login');
        $this->resetRequest()
             ->resetResponse();
    }
}
