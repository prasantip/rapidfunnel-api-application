<?php

defined('TESTING') || define('TESTING', 1);

defined('SUP_ADMIN_EMAIL') || define('SUP_ADMIN_EMAIL', 'admin@email.org');
defined('SUP_ADMIN_ID') || define('SUP_ADMIN_ID', 1);
defined('SUP_ADMIN_WRONG_ID') || define('SUP_ADMIN_WRONG_ID', 99);
defined('SUP_ADMIN_PASSWORD') || define('SUP_ADMIN_PASSWORD', 'password1');
defined('SUP_ADMIN_WRONG_PASSWORD') ||
define('SUP_ADMIN_WRONG_PASSWORD', 'pass');

defined('ADMIN_EMAIL') ||
define('ADMIN_EMAIL', 'sanjeev.kumar1@mindfiresolutions.com');
defined('ADMIN_WRONG_EMAIL') ||
define('ADMIN_WRONG_EMAIL', 'sanjeev2.kumar@mindfiresolutions.com');
defined('STRIPE_MAIL') || define('STRIPE_MAIL', 'raj10@mailinator.com');
defined('ADMIN_PASSWORD') || define('ADMIN_PASSWORD', 'password1');
defined('ADMIN_ID') || define('ADMIN_ID', 65);
defined('ADMIN_WRONG_ID') || define('ADMIN_WRONG_ID', 64);
defined('ADMIN_FIRST_NAME') || define('ADMIN_FIRST_NAME', 'Sanjeev');
defined('ADMIN_LAST_NAME') || define('ADMIN_LAST_NAME', 'Kumar');
defined('ALT_ADMIN_LAST_NAME') || define('ALT_ADMIN_LAST_NAME', 'kumar');
defined('ADMIN_FULL_NAME') || define('ADMIN_FULL_NAME', 'Sanjeev ');
defined('FULL_NAME') || define('FULL_NAME', 'Sanjeev kumar');
defined('ADMIN_ROLE_ID') || define('ADMIN_ROLE_ID', 1);
defined('ADMIN_GROUP_ID') || define('ADMIN_GROUP_ID', 1);


defined('MANAGER_ROLE_ID') || define('MANAGER_ROLE_ID', 2);
defined('MANAGER_GROUP_ID') || define('MANAGER_GROUP_ID', 2);

defined('USER_ROLE_ID') || define('USER_ROLE_ID', 3);
defined('USER_GROUP_ID') || define('USER_GROUP_ID', 3);
defined('GROUP_LEADER') || define('GROUP_LEADER', 4);

defined('USER_ID') || define('USER_ID', 1);
defined('ALT_USER_ID') || define('ALT_USER_ID', 2);
defined('SECOND_ALT_USER_ID') || define('SECOND_ALT_USER_ID', 3);

defined('WRONG_ROLE_ID') || define('WRONG_ROLE_ID', 1e4);
defined('WRONG_GROUP_ID') || define('WRONG_GROUP_ID', 1e4);
defined('WRONG_USER_ID') || define('WRONG_USER_ID', 1e4);
defined('GROUP_NAME') || define('GROUP_NAME', 'Test Group');
defined('GROUP_DESCRIPTION') || define('GROUP_DESCRIPTION', 'Test Group');

defined('ACCOUNT_ID') || define('ACCOUNT_ID', 1);
defined('ACCOUNT_ID_STRING') || define('ACCOUNT_ID_STRING', '1');
defined('WRONG_ACCOUNT_ID') || define('WRONG_ACCOUNT_ID', 1111);
defined('ACCOUNT_NAME') || define('ACCOUNT_NAME', 'Mindfire');
defined('ACCOUNT_USER_EMAIL')
|| define('ACCOUNT_USER_EMAIL', 'free-user-1@email.org');
defined('ACCOUNT_USER_PASSWORD')
|| define('ACCOUNT_USER_PASSWORD', 'password1');
defined('ID') || define('ID', 58);
defined('ACCESS_TOKEN')
|| define('ACCESS_TOKEN', 'fada984c6f26bea9671673a7f0c280b2');
defined('ALT_ACCESS_TOKEN')
|| define('ALT_ACCESS_TOKEN', 'fada984c6f26bea9671673a7f0c280b2,d369d603be118aa689512c8771dd2c28,cb5bff66041ae8b71f4d473969e17a14,ea3532dc80925e17cc480890ea843802');



defined('FREE_ACCOUNT_ID') || define('FREE_ACCOUNT_ID', 2);
defined('FULL_CONTACTS_ACCOUNT_ID') || define('FULL_CONTACTS_ACCOUNT_ID', 3);
defined('TEMP_USER_ID') || define('TEMP_USER_ID', 77);
defined('ALT_TEMP_USER_ID') || define('ALT_TEMP_USER_ID', 70);
defined('TEMP_USER_ID2') || define('TEMP_USER_ID2', 65);
defined('INVITED_USER_ID') || define('INVITED_USER_ID', 66);
defined('INVITED_NEW_USER_ID') || define('INVITED_NEW_USER_ID', 67);
defined('NEW_USER_ID') || define('NEW_USER_ID', 68);
defined('INVITED_USER_EMAIL') ||
define('INVITED_USER_EMAIL', 'invited-user@email.org');
defined('USER_EMAIL') ||
define('USER_EMAIL', 'free-user-1@email.org');

defined('ADMIN_BILLING_FIRST_NAME') ||
define('ADMIN_BILLING_FIRST_NAME', 'Sanjeev');

defined('RESOURCE_CATEGORY_ID') || define('RESOURCE_CATEGORY_ID', 1);
defined('RESOURCE_CATEGORY_WRONG_ID') ||
define('RESOURCE_CATEGORY_WRONG_ID', 121212);

defined('ACCOUNT_BRANDING_ID') || define('ACCOUNT_BRANDING_ID', 1);

defined('CONTACT_ID') || define('CONTACT_ID', 1);
defined('NEW_CONTACT_ID') || define('NEW_CONTACT_ID', 2);
defined('CONTACT_WRONG_ID') || define('CONTACT_WRONG_ID', 11111);
defined('CONTACT_EMAIL') ||
define('CONTACT_EMAIL', 'sanjeev.kumar@mindfiresolutions.com');
defined('ALT_CONTACT_EMAIL') ||
define('ALT_CONTACT_EMAIL', 'Neeraj.das@mindfiresolutions.com');
defined('TEMP_CONTACT_EMAIL') ||
define('TEMP_CONTACT_EMAIL', 'postingcontact@gmail.com');

defined('CONTACT_PHONE') || define('CONTACT_PHONE', '555-555-555');
defined('CONTACT_WRONG_PHONE') || define('CONTACT_WRONG_PHONE', '666-555-555');
defined('MULTI_PHONE') || define('MULTI_PHONE', '555-555-555, 666-666-666');
defined('CONTACT_WRONG_EMAIL') ||
define('CONTACT_WRONG_EMAIL', 'sanjeev1.kumar@mindfiresolutions.com');


defined('CAMPAIGN_EMAIL_ID') || define('CAMPAIGN_EMAIL_ID', 1);
defined('ALT_CAMPAIGN_EMAIL_ID') || define('ALT_CAMPAIGN_EMAIL_ID', 2);
defined('WRONG_CAMPAIGN_EMAIL_ID') || define('WRONG_CAMPAIGN_EMAIL_ID', 1111);

defined('CAMPAIGN_ID') || define('CAMPAIGN_ID', 1);
defined('ALT_CAMPAIGN_ID') || define('ALT_CAMPAIGN_ID', 2);
defined('WRONG_CAMPAIGN_ID') || define('WRONG_CAMPAIGN_ID', 1111);

defined('RESOURCE_ID') || define('RESOURCE_ID', 1);
defined('WRONG_RESOURCE_ID') || define('WRONG_RESOURCE_ID', 1111);

defined('SYSTEM_EMAIL_ID') || define('SYSTEM_EMAIL_ID', 1);
defined('ALT_SYSTEM_EMAIL_ID') || define('ALT_SYSTEM_EMAIL_ID', 10);
defined('WRONG_SYSTEM_EMAIL_ID') || define('WRONG_SYSTEM_EMAIL_ID', 1111);
defined('SYSTEM_EMAIL_TYPE_ID') || define('SYSTEM_EMAIL_TYPE_ID', 1);

defined('ACCOUNT_GROUP_ID') || define('ACCOUNT_GROUP_ID', 1);
defined('ACCOUNT_GROUP_PARENT_ID') || define('ACCOUNT_GROUP_PARENT_ID', 1);

defined('INCENTIVE_ID') || define('INCENTIVE_ID', 1);
defined('INCENTIVE_ID_STRING') || define('INCENTIVE_ID_STRING', '1');
defined('WRONG_INCENTIVE_ID') || define('WRONG_INCENTIVE_ID', 1111);

defined('NO_OF_ROLES') || define('NO_OF_ROLES', 3);

defined('EDGE_USER_AGENT') || define('EDGE_USER_AGENT',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML,' .
    'like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10547');

defined('CHROME_USER_AGENT') || define('CHROME_USER_AGENT',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)' .
    'Chrome/48.0.2564.97 Safari/537.36');

defined('FIREFOX_USER_AGENT') || define('FIREFOX_USER_AGENT',
'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0');

defined('MAIL_TYPE_BROADCAST') || define('MAIL_TYPE_BROADCAST', 1);

defined('NOTE_TIME_STAMP') || define('NOTE_TIME_STAMP', '2016-02-23 05:32:24');
defined('NOTE') || define('NOTE', 'Hello
this is raj;i,want to test');

defined('UPDATE_TIME_STAMP')
|| define('UPDATE_TIME_STAMP', '2016-02-24 06:00:24');

defined('CONTACT_NOTE') || define('CONTACT_NOTE', 'This is a test note!');

defined('CONTACT_UPDATE_NOTE')
|| define('CONTACT_UPDATE_NOTE', 'This is a test update note!');

defined('ACCOUNT_USER_ID') || define('ACCOUNT_USER_ID', 65);
defined('ACCOUNT_USER_ID_STRING') || define('ACCOUNT_USER_ID_STRING', '65');
defined('ALT_ACCOUNT_USER_ID') || define('ALT_ACCOUNT_USER_ID', 64);
defined('ASSOCIATE_NUMBER') || define('ASSOCIATE_NUMBER', 111111111);
defined('ALT_ASSOCIATE_NUMBER') || define('ALT_ASSOCIATE_NUMBER', 2222222);

defined('REP_ID') || define('REP_ID', 'REP123');
defined('REP_ID2') || define('REP_ID2', 'REP2123');

defined('OLD_MAIL_UPDATE_TIME')
|| define('OLD_MAIL_UPDATE_TIME', '2016-02-29 00:00:00');

defined('NEW_MAIL_UPDATE_TIME')
|| define('NEW_MAIL_UPDATE_TIME', '2016-02-29 01:01:01');

defined('BROADCAST_TYPE') || define('BROADCAST_TYPE', '1');

defined('NOTE_ID') || define('NOTE_ID', 1);

defined('FIELD_RECORD_PER_PAGE')
|| define('FIELD_RECORD_PER_PAGE', 'contactRecordsPerPage');

defined('RECIPIENT_ID') || define('RECIPIENT_ID', 65);
defined('WRONG_RECIPIENT_ID') || define('WRONG_RECIPIENT_ID', 2222);
defined('RECIPIENT_TEMP_ID') || define('RECIPIENT_TEMP_ID', 2);

defined('RECIPIENT_TYPE') || define('RECIPIENT_TYPE', 1);
defined('RECIPIENT_TEMP_TYPE') || define('RECIPIENT_TEMP_TYPE', 2);

defined('RECIPIENT') || define('RECIPIENT', 'raj@mindfiresolutions.com');

defined('CONTACT_STATUS') || define('CONTACT_STATUS', 1);
defined('SUSPEND_STATUS') || define('SUSPEND_STATUS', 3);
defined('BOUNCED_STATUS') || define('BOUNCED_STATUS', '4');
defined('ALT_STATUS') || define('ALT_STATUS', 2);
defined('PASS_THROUGH') || define('PASS_THROUGH', 1);
defined('SPAM_COMPLAINT_STATUS') || define('SPAM_COMPLAINT_STATUS', 0);
defined('CONTACT_ALTER_STATUS') || define('CONTACT_ALTER_STATUS', 4);
defined('MESSAGE_ID') || define('MESSAGE_ID', '20160125231418.544.23715@broadcast.rapidfunnel.com');
defined('NEW_MESSAGE_ID') || define('NEW_MESSAGE_ID', '20160125231418.544.23775@broadcast.rapidfunnel.com');
defined('SUBJECT') || define('SUBJECT', 'hello');
defined('EMAIL_CONTENT') || define('EMAIL_CONTENT', 'I am eMail content');
defined('SENT_DATE') || define('SENT_DATE', '2016-04-04 00:00:00');
defined('MAIL_TYPE') || define('MAIL_TYPE', 'broadcastEmail');
defined('MAIL_ID') || define('MAIL_ID', 1);

defined('CREATED_BY') || define('CREATED_BY', 65);
defined('CREATED') || define('CREATED', '2015-10-24 20:00:49');
defined('USER_FIRST_NAME') || define('USER_FIRST_NAME', 'sanjeev ');
defined('TESTING_FIRST_NAME') || define('TESTING_FIRST_NAME', 'tester');
defined('USER_ID_STRING') || define('USER_ID_STRING', '1');
defined('ROLE_ID_STRING') || define('ROLE_ID_STRING', '1');


defined('CARD_TYPE') || define('CARD_TYPE', 'MASTER');
defined('ALT_CARD_TYPE') || define('ALT_CARD_TYPE', 'AX');
defined('CCV') || define('CCV', 123);
defined('WRONG_CCV') || define('WRONG_CCV', 123456677);
defined('ALT_CCV') || define('ALT_CCV', 3210);


defined('TEST_ACCESS_TOKEN') || define('TEST_ACCESS_TOKEN', 'testaccesstoken');
defined('KEY') || define('KEY', '20160125231418.544.23715@broadcast.rapidfunnel.com-2-2-raj@mindfiresolutions.com-2');

defined('FAILURE_NUMBER') || define('FAILURE_NUMBER', 1);
defined('FAILURE_NUMBER_ONE') || define('FAILURE_NUMBER_ONE', 2);
defined('FAILURE_NUMBER_TWO') || define('FAILURE_NUMBER_TWO', 3);
defined('FAILURE_NUMBER_THREE') || define('FAILURE_NUMBER_THREE', '-10');
defined('FAILURE_NUMBER_ZERO') || define('FAILURE_NUMBER_ZERO', 0);


defined('TEMP_GROUP_ID') || define('TEMP_GROUP_ID', 2);

defined('USER_TOKEN') || define('USER_TOKEN', 'be6691bf80450d35dd623c0d7b0283fc');
defined('WEB_VIEW_TOKEN') || define('WEB_VIEW_TOKEN', 'webviewtoken');
defined('WEB_VIEW') || define('WEB_VIEW', 1);
defined('RESET_PASS_HASH') || define('RESET_PASS_HASH', 12345);
defined('LINK') || define('LINK', 'www.example.com');

defined('TOTAL_CONTACTS') || define('TOTAL_CONTACTS', 100);
defined('OPT_IN_SEND_CONTACTS') || define('OPT_IN_SEND_CONTACTS', 100);
defined('OPTED_CONTACTS') || define('OPTED_CONTACTS', 30);
defined('NEW_OPTED_CONTACTS') || define('NEW_OPTED_CONTACTS', 15);
defined('LOWER_OPT_IN_STATUS') || define('LOWER_OPT_IN_STATUS', 11);
defined('LOW_OPT_IN_STATUS') || define('LOW_OPT_IN_STATUS', 10);
defined('TOTAL_ACTIVE_COTACTS') || define('TOTAL_ACTIVE_COTACTS', 80);
defined('IS_LATEST') || define('IS_LATEST', 1);
defined('IS_LATEST_ALT') || define('IS_LATEST_ALT', 0);

defined('ACCOUNT_CARD_PROFILE_ID') || define('ACCOUNT_CARD_PROFILE_ID', 1);
defined('TYPE') || define('TYPE', 5);
defined('DAYS_CHANGED') || define('DAYS_CHANGED', 1);
defined('DAYS') || define('DAYS', 2);
defined('NEW_PASSWORD') || define('NEW_PASSWORD', 'newpassword123');
defined('NEW_EMAIL') || define('NEW_EMAIL', 'sachin@gmail.com');
defined('WRONG_PASSWORD') || define('WRONG_PASSWORD', 12343);
defined('CONTACT_PASSWORD') || define('CONTACT_PASSWORD', 'f2f1cb32d652f90d5186a37a58b7b99b');
defined('PASSWORD_SALT') || define('PASSWORD_SALT', 'BBB4OB3TCiYy1KOIEvoLvg');

defined('IGNORE_USER_PAYMENT') || define('IGNORE_USER_PAYMENT', 1);
defined('IGNORE_USER_PAYMENT_STRING')
|| define('IGNORE_USER_PAYMENT_STRING', '1');
defined('NOT_IGNORE_USER_PAYMENT') || define('NOT_IGNORE_USER_PAYMENT', 0);

defined('ALIAS_GROUP_CODE') || define('ALIAS_GROUP_CODE', 'A1B2C3');
defined('LARGE_NUMBER') || define('LARGE_NUMBER', 1e1000);


defined('AWARD') || define('AWARD', 'FJSSHSHSNK');
defined('AWARD_EARNED') || define('AWARD_EARNED', 1);
defined('ALT_AWARD_EARNED') || define('ALT_AWARD_EARNED', 0);
defined('AWARD_TYPE_ID') || define('AWARD_TYPE_ID', 1);
defined('NUMBER_OF_LEADS') || define('NUMBER_OF_LEADS', 2);
defined('ALT_NUMBER_OF_LEADS') || define('ALT_NUMBER_OF_LEADS', 0);
defined('TOP_PERCENTAGE') || define('TOP_PERCENTAGE', 1);
defined('RUNNING_STATUS') || define('RUNNING_STATUS', 1);

defined('UPPER_LIMIT') || define('UPPER_LIMIT', 10);
defined('LOWER_LIMIT') || define('LOWER_LIMIT', 0);


defined('USER_PAYMENT') || define('USER_PAYMENT', '1');

defined('IS_TRIAL') || define('IS_TRIAL', 0);
defined('CUSTOM_VIDEO_TYPE_ID') || define('CUSTOM_VIDEO_TYPE_ID', 9);

defined('CONCAT_EMAILS')
|| define('CONCAT_EMAILS', 'sanjeev2.kumar@mindfiresolutions.com|admin@email.org|sanjeev.kumar1@mindfiresolutions.com');

defined('NEW_ALT_CONTACT_ID') || define('NEW_ALT_CONTACT_ID', 10);
defined('OLD_CONTACT_ID') || define('OLD_CONTACT_ID', 3);
defined('OLD_ALT_CONTACT_ID') || define('OLD_ALT_CONTACT_ID', 4);
defined('CUSTOM_CONTACT') || define('CUSTOM_CONTACT', 1);
defined('CUSTOM_CONTACT_FIRST_NAME') || define('CUSTOM_CONTACT_FIRST_NAME', 'sachin');
defined('NEW_CONTACT_EMAIL') || define('NEW_CONTACT_EMAIL', 'test1@gmail.com');
defined('NEW_FIRST_NAME') || define('NEW_FIRST_NAME', 'unit');
defined('NEW_ALT_FIRST_NAME') || define('NEW_ALT_FIRST_NAME', 'unitNew');
defined('NEW_LAST_NAME') || define('NEW_LAST_NAME', 'test');
defined('NEW_STATE') || define('NEW_STATE', 'Alabama');
defined('CHANNEL') || define('CHANNEL', 'web');

defined('EMBED_CODE') ||
define(
    'EMBED_CODE',
    '<iframe width="560" height="315" src="https://www.youtube.com/embed/1MF3HFO6fbM" '
    . 'frameborder="0" allowfullscreen></iframe>'
);

defined('WISTIA_EMBED_CODE') ||
define(
    'WISTIA_EMBED_CODE',
    '<iframe src="//fast.wistia.net/embed/iframe/gw5o8do29d?autoPlay='
    . 'true&playlistLoop=true&" class="wistia_embed" name="wistia_embed"></iframe>'
);

defined('MEDIA_PROVIDER') || define('MEDIA_PROVIDER', 'youtube');
defined('WISTIA_MEDIA_PROVIDER') || define('WISTIA_MEDIA_PROVIDER', 'wistia');
defined('WISTIA_PROJECT_ID') || define('WISTIA_PROJECT_ID', 'yk8uxjlxzv');
defined('MEDIA_HASH') || define('MEDIA_HASH', '1MF3HFO6fbM');
defined('WISTIA_MEDIA_HASH') || define('WISTIA_MEDIA_HASH', 'gw5o8do29d');
defined('VISITOR_KEY')
|| define('VISITOR_KEY', 'v20150227_fdda4703-50f7-4e7f-9fe2-152208bf2091');

defined('RE_WATCH_TOKEN')
|| define('RE_WATCH_TOKEN', '0.417978');

defined('DESCRIPTION') || define('DESCRIPTION', 'New Page for testing');
defined('AUTHOR') || define('AUTHOR', 'Tester');
defined('URL') || define('URL', 'https://testing.com');
defined('TITLE') || define('TITLE', 'Unit-Test');

defined('FOR_JS')
|| define('FOR_JS', '<jqueryscript></jqueryscript>-this link - custom-contact-us  - <jsscripts></jsscripts>');
defined('JS_CONTENT')
|| define('JS_CONTENT', '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script>-this link - custom-contact-us  - <script src="http://rfunnel.com/assets/builder_front/dist/elements/js/build/build.min.js?v=4.2"></script><script src="http://rfunnel.com/js/base.js?v=4.2"></script><script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script><script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script><script src="http://rfunnel.com/assets/builder_front/dist/js/contact.js?v=4.2"></script><script src="http://rfunnel.com/js/landing/analytic.js?v=4.2"></script>');

defined('EXPECTED_META_DATA')
|| define(
'EXPECTED_META_DATA', 'This is <meta name="description" content="New Page for testing" />
            <meta name="author" content="Tester" />
            <meta property="og:url" content="https://testing.com" />
            <meta property="og:title" content="Unit-Test" />
            <meta property="og:description" content="New Page for testing" />
            <meta property="og:site_name" content="Tester : Unit-Test" /> new'
);

defined('META_CONTENT') || define('META_CONTENT', 'This is <meta name="description" content="Custom page resource"> new');
defined('CONTENT_PLACEHOLDER')
|| define('CONTENT_PLACEHOLDER', 'Hi [firstName]-[lastName]-[rep-id]-[rep-id2]-[user-id]-[email]-[phoneNumber]-[resource-id]');
defined('EXPECTED_CONTENT_PLACEHOLDER')
|| define('EXPECTED_CONTENT_PLACEHOLDER', 'Hi Sanjeev-kumar-REP123-REP2123-65-sanjeev.kumar1@mindfiresolutions.com-NULL-0');

defined('ALT_CONTENT_PLACEHOLDER')
|| define('ALT_CONTENT_PLACEHOLDER', 'Hi [firstName] [lastName] [accountName] [password] [email] [rep-id] [rep-id2] are my details');
defined('CONTENT_SUBJECT') || define('CONTENT_SUBJECT', 'hello [firstName]');
defined('RESOURCE_LIST') || define('RESOURCE_LIST', 10);
defined('NEW_CAMPAIGN_ID') || define('NEW_CAMPAIGN_ID', 10);
defined('NEW_CAMPAIGN_EMAIL_ID') || define('NEW_CAMPAIGN_EMAIL_ID', 10);
defined('TIMESTAMP') || define('TIMESTAMP', '1472313678.3083');
defined('CUSTOM_DATA') || define('CUSTOM_DATA', '{"accountId":"1","recipientId":"3000147",'
    . '"subject":"RapidFunnel Contact","sendDate":"2016-08-27 '
    . '01:41:51","mailType":"optIn","recipientType":"2"}');

defined('ALT_CUSTOM_DATA') || define('ALT_CUSTOM_DATA', '{"accountId":"2","recipientId":"2",'
    . '"subject":"RapidFunnel Contact","sendDate":"2016-08-27 '
    . '01:41:51","mailType":"optIn","recipientType":"2"}');

defined('LS_ACCOUNT_ID') || define('LS_ACCOUNT_ID', 16);

defined('LOW_OPT_IN_DEACTIVATE') || define('LOW_OPT_IN_DEACTIVATE', 'lowOptInDeactivate');
defined('LOW_OPT_IN_WARNING') || define('LOW_OPT_IN_WARNING', 'lowOptInWarning');
defined('SPAM_COMPLAINT_DEACTIVATE') || define('SPAM_COMPLAINT_DEACTIVATE', 'spamComplaintDeactivate');
defined('SPAM_COMPLAINT_WARNING') || define('SPAM_COMPLAINT_WARNING', 'spamComplaintWarning');
defined('SPAM_COMPLAINTS') || define('SPAM_COMPLAINTS', 'spamComplaints');

defined('INTERNAL_TYPE_ARRAY') || define('INTERNAL_TYPE_ARRAY', 'array');
defined('INTERNAL_TYPE_STRING') || define('INTERNAL_TYPE_STRING', 'string');
defined('INTERNAL_TYPE_OBJECT') || define('INTERNAL_TYPE_OBJECT', 'object');

defined('NOT_PUBLISHED') || define('NOT_PUBLISHED', 0);
defined('FILE_SAVED_NAME') || define('FILE_SAVED_NAME', '2_6.jpg');
defined('FILE_RESOURCE_ID') || define('FILE_RESOURCE_ID', 6);
defined('COMPANY_NAME') || define('COMPANY_NAME', 'legalshield');
defined('APP_PUBLIC_PATH') || define('APP_PUBLIC_PATH', realpath(dirname(__FILE__)));
defined('FILE_LOCATION') || define('FILE_LOCATION', 'uploads/profile_image/account_user');
defined('WISTIA_API_KEY') || define('WISTIA_API_KEY', 'c855f5e17bf8e9ea403b8feee9e3dee9f3048501071df7c01b74a9af1655a7cd');
defined('TEST_FILE_NAME') || define('TEST_FILE_NAME', 'test.srt');
defined('APP_USER_ID') || define('APP_USER_ID', 9);
defined('PAYMENT_USER_ID') || define('PAYMENT_USER_ID', 100);
defined('NEW_PAYMENT_USER_ID') || define('NEW_PAYMENT_USER_ID', 101);
defined('APP_GROUP_ID') || define('APP_GROUP_ID', 9);
defined('PERCENTAGE_WATCHED') || define('PERCENTAGE_WATCHED', 0.312);

defined('GROUP_LEADER_CAMPAIGN') || define('GROUP_LEADER_CAMPAIGN', 1);
defined('GROUP_LEADER_RESOURCE') || define('GROUP_LEADER_RESOURCE', 2);
defined('GROUP_LEADER_INCENTIVE') || define('GROUP_LEADER_INCENTIVE', 3);
defined('AT_A_TIME') || define('AT_A_TIME', 10);
defined('TRIM_STRING') || define('TRIM_STRING', 'testing     purpose');
defined('TRIMED_STRING') || define('TRIMED_STRING', 'testing purpose');
defined('USER_BILLING_INFO_ID') || define('USER_BILLING_INFO_ID', 1);
defined('STRIPE_CUS_ID') || define('STRIPE_CUS_ID', 'cus_9GSPPkZVAhbt0d');

defined('RESOURCE_URL') || define('RESOURCE_URL', 'http://s1.rapidfunnel.com/res/70/43/[contactId]');
