<?php
defined('accountCampaignEmail')
|| define(
'accountCampaignEmail', serialize(
    [
        [
            'id'          => '1', 'campaignId' => '1',
            'title'       => 'Hello', 'content' => 'Html Bode',
            'textContent' => 'Text Body', 'days' => '1',
            'status'      => 'published', 'isDeleted' => '0',
            'created'     => '2015-10-24 20:00:49',
            'createdBy'   => '65', 'modified' => null,
            'modifiedBy'  => '65'
        ]
    ]
)
);

defined('ACCOUNT_CAMPAIGN_TEMP_EMAIL')
|| define(
'ACCOUNT_CAMPAIGN_TEMP_EMAIL', serialize(
    [
        [
            'id'              => '1',
            'accountId'       => '1',
            'campaignId'      => '1',
            'campaignEmailId' => '1',
            'days'            => '1'
        ]
    ]
)
);
