<?php
error_reporting(E_ALL | E_STRICT);

//set include path
ini_set('include_path', '.::/var/www/html/ZendFramework-1.12.0/library/');

//define if not already
defined('APP_PATH') || define('APP_PATH',
                realpath(dirname(__FILE__) . '/../../app'));

defined('APP_ENV') || define('APP_ENV', 'Testing');

defined('APP_LIB') || define('APP_LIB', realpath(dirname(__FILE__) . '/../../lib'));

defined('COMMON_CONFIG_INI_PATH') || define('COMMON_CONFIG_INI_PATH',
                APP_LIB . '/RapidFunnel/Config/commonConfig.ini');

switch (APP_ENV) {
    case 'Production' :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
                        APP_LIB . '/RapidFunnel/Config/app.ini');
        break;
    case 'Staging' :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
                        APP_LIB . '/RapidFunnel/Config/app_staging.ini');
        break;
    case 'QA' :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
                        APP_LIB . '/RapidFunnel/Config/app_qa.ini');
        break;
    case 'Development' :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
            APP_LIB . '/RapidFunnel/Config/app_development.ini');

    case 'Testing' :
    default :
        defined('APP_INI_PATH') || define('APP_INI_PATH',
            APP_LIB . '/RapidFunnel/Config/app_unit_test.ini');
}

//include constant 
require_once('Zend/Application.php');
//Bootstrap the application
require_once(APP_LIB.'/../tests/app/constant.php');
require_once(APP_LIB.'/../tests/app/tables.php');
//For controllers
require_once(APP_LIB.'/../tests/lib/RapidFunnel/TestCase/PHPUnitControllerTestCase.php');

$application = new Zend_Application(APP_ENV,
        array(
    'config' => array(
        COMMON_CONFIG_INI_PATH,
        APP_INI_PATH
    )
        )
);

$application->bootstrap();
clearstatcache();