<?php

/**
 * Test cases for login controller for API module
 */

class Api_AccountContactControllerTest extends PHPUnit_ControllerTestCase
{
    public function testAuthUser()
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('username' => ADMIN_EMAIL, 'password' => ADMIN_PASSWORD));
        $this->dispatch('/api/login/index');
        $response = json_decode($this->getResponse()->getBody());

        return $response->response->content;
    }

    /**
     * @depends testAuthUser
     */
    public function testGetContactAction($authData)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId, 'accessToken' => $authData->accessToken));
        $this->dispatch('/api/account-contact/get-contact');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
    }

    /**
     * @depends testAuthUser
     */
    public function testAddContactAction($authData)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId, 'accessToken' => $authData->accessToken, 'firstName' => 'fnmae', 'lastName' => 'lname', 'email' => 'phpunit@gmail.com', 'zip' => '12345'));
        $this->dispatch('/api/account-contact/add-contact');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
        return $response->response->content->contactId;
    }

    /**
     * @depends testAuthUser
     * @depends testAddContactAction
     */
    public function testUpdateContactAction($authData, $contactId)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId,
                                    'accessToken' => $authData->accessToken,
                                    'contactId' => $contactId,
                                    'firstName' => 'ffnmae',
                                    'lastName' => 'llname',
                                    'email' => 'phpunit@gmail.com',
                                    'zip' => '22222'));
        $this->dispatch('/api/account-contact/update-contact');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
        return $contactId;
    }

    /**
     * @depends testAuthUser
     * @depends testUpdateContactAction
     */
    public function testDeleteContactAction($authData, $contactId)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId,
                                    'accessToken' => $authData->accessToken,
                                    'contactId' => $contactId));
        $this->dispatch('/api/account-contact/delete-contact');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
    }
}

