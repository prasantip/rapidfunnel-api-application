<?php

/**
 * Test cases for login controller for API module
 */

class Api_AccountCampaignControllerTest extends PHPUnit_ControllerTestCase
{
    public function testAuthUser()
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('username' => ADMIN_EMAIL, 'password' => ADMIN_PASSWORD));
        $this->dispatch('/api/login/index');
        $response = json_decode($this->getResponse()->getBody());

        return $response->response->content;
    }

    /**
     * @depends testAuthUser
     */
    public function testGetCampaignsAction($authData)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId, 'accessToken' => $authData->accessToken));
        $this->dispatch('/api/account-campaign/get-campaigns');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
    }

    /**
     * @depends testAuthUser
     */
    public function testGetCampaignDetailsAction($authData)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId, 'accessToken' => $authData->accessToken, 'campaignId' => CAMPAIGN_ID));
        $this->dispatch('/api/account-campaign/get-campaign-details');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
    }

    /**
     * @depends testAuthUser
     */
    public function testAssignContactToCampaignAction($authData)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId,
                                    'accessToken' => $authData->accessToken,
                                    'contactId' => CONTACT_ID,
                                    'campaignId' => CAMPAIGN_ID));
        $this->dispatch('/api/account-campaign/assign-contact-to-campaign');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
    }
}

