<?php

/**
 * Test cases for login controller for API module
 */

class Api_LoginControllerTest extends PHPUnit_ControllerTestCase
{
    public function  testForgotPasswordAction()
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('email' => ADMIN_PASSWORD));
        $this->dispatch('/api/login/forgot-password');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');

        $this->assertModule('api');
        $this->assertController('login');
        $this->assertAction('forgot-password');

        return $response->response->content->temporaryPassword;
    }

    /**
     * @depends testForgotPasswordAction
     */
    public function testIndexAction($temporaryPassword)
    {
        // wrong input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('username' => 'wrong@email.org', 'password' => $temporaryPassword));
        $this->dispatch('/api/login/index');
        $response = $this->getResponse()->getBody();
        $this->assertJsonStringEqualsJsonString(
            $response, '{"response":{"status":"false","errorMessage":"Authentication Failed"}}'
        );

        $this->assertModule('api');
        $this->assertController('login');
        $this->assertAction('index');

        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('username' => ADMIN_EMAIL, 'password' => $temporaryPassword));
        $this->dispatch('/api/login/index');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');

        $this->assertModule('api');
        $this->assertController('login');
        $this->assertAction('index');

        return $response->response->content->accessToken;
    }

    /**
     * @depends testIndexAction
     */
    public function testChangePasswordAction($accessToken)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId' => 65,
                                      'accessToken' => $accessToken,
                                      'password' => ADMIN_PASSWORD,
                                      'confirmPassword' => ADMIN_PASSWORD
                                    )
                                );
        $this->dispatch('/api/login/change-password');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');

        $this->assertModule('api');
        $this->assertController('login');
        $this->assertAction('change-password');
    }

    public function testValidateUserRegistrationAction()
    {
        $email = ADMIN_EMAIL;
        $registrationCode = '12345';
        //update the registrationCode and perform test
        $accountUserModel =  new RapidFunnel_Model_AccountUser();
        //get the user by loading email
        $user = $accountUserModel->loadByEmail($email);
        $accountUserModel->load($user['id']);
        $accountUserModel->registrationCode = $registrationCode;
        $accountUserModel->update();

        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('email' => $email,
                                      'registrationCode' => $registrationCode
                                    )
                                );
        $this->dispatch('/api/login/validate-user-registration');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');

        $this->assertModule('api');
        $this->assertController('login');
        $this->assertAction('validate-user-registration');
    }

    public function testGetNewPasswordAction()
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('email' => ADMIN_EMAIL,
                                      'registrationCode' => '12345',
                                      'password' => ADMIN_PASSWORD,
                                      'confirmPassword' => ADMIN_PASSWORD
                                    )
                                );
        $this->dispatch('/api/login/get-new-password');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');

        $this->assertModule('api');
        $this->assertController('login');
        $this->assertAction('get-new-password');
    }
}

