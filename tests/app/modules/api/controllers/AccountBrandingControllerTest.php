<?php

/**
 * Test cases for account branding controller for API module
 */

class Api_AccountBrandingControllerTest extends PHPUnit_ControllerTestCase
{
    public function testAuthUser()
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('username' => ADMIN_EMAIL, 'password' => ADMIN_PASSWORD));
        $this->dispatch('/api/login/index');
        $response = json_decode($this->getResponse()->getBody());

        return $response->response->content;
    }

    /**
     * @depends testAuthUser
     */
    public function testGetDetailsAction($authData)
    {
        // correct input testcase
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(array('userId'=>$authData->userId, 'accessToken' => $authData->accessToken));
        $this->dispatch('/api/account-branding/get-details');
        $response = json_decode($this->getResponse()->getBody());
        $this->assertEquals($response->response->status, 'true');
    }
}

