This directory will have cache files.

******** Zend Db Table Meta Data Cache *********

Instructions
===============

Create Db Table Cache Directory
- Create table metadata cache dir as <projectDir>/cache/tables
- Give write permission to the directory.

===============

To clear db tables cache files: rm -r cache/tables/zend_*

*************************************************


