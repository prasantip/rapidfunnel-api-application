This directory will have the meta data of Db tables.
The Db Tables will get the meta data from here instead of requesting again to Db server.
It will stop the frequent "describe table_name" execution request to Db server.

After changing any Db schema cache files should be cleaned so the cache not have the
old meta data of tables;

