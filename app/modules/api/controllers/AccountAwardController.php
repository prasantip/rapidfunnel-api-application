<?php

/**
 * Api_AccountAwardController
 * Handle all actions related to awards
 *
 * PHP version 5.5.9
 *
 * @category  Controller
 * @package   Api
 * @author    rapid funnel <kraft.adam@gmail.com>
 * @copyright 2014 RapidFunnel, Inc.
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account/guest-book
 */
class Api_AccountAwardController extends Zend_Controller_Action
{

    //declare the response variable
    public $response
        = array(
            'response' => array('status' => 'false')
        );
    //declare the variables
    public $userIncentiveModel;
    public $userId;
    public $accountId;
    public $params;

    /**
     * Setting default parameters
     *
     * @return void
     */
    public function init()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->params = $this->_request->getParams();
        //get the post data
        $this->userId = $this->params['userId'];
        //load the collateral model
        $this->userIncentiveModel = new RapidFunnel_Model_AccountUserIncentive(
        );
        $userModel = new RapidFunnel_Model_AccountUser();

        $key = array('accountId');
        $userModel->load($this->userId,  $key);
        $this->accountId = $userModel->accountId;
    }

    /**
     * Functionality to get current reward details of an user
     *
     * @return object json response of current reward details
     */
    public function getCurrentAwardDetailAction()
    {
        $incentiveId = $this->params['incentiveId'];
        if (($this->userId > 0) && ($this->accountId > 0)
            && (is_numeric(
                $incentiveId
            ))
            && ($incentiveId > 0)
        ) {

            $resources = $this->userIncentiveModel->getUserProgressAward(
                $incentiveId, $this->userId
            );
            //check for contacts and return the response json
            if (!empty($resources) && is_array($resources)) {
                //To add few more info by award type
                $awardDetail = $this->userIncentiveModel->getAwardInfoByType(
                    $resources[0], $this->userId
                );

                $this->response['response']['status'] = 'true';
                $this->response['response']['content'] = $awardDetail;
            } else {
                $this->response['response']['errorMessage']
                    = 'No records found';
            }
        } else {
            $this->response['response']['errorMessage']
                = 'Not a valid input. Please try again';
        }
        echo json_encode($this->response);
    }

    /**
     * Get current and past rewards belong to the users
     *
     * @return object json response of rewards info
     * @throws Exception
     */
    public function getIncentivesAction()
    {
        $runningStatus = $this->params['runningStatus'];

        if (($this->userId > 0) && ($this->accountId > 0)) {
            //Incentive model
            $incentiveModel = new RapidFunnel_Model_AccountIncentiveProgram();
            //To fetch incentives by running status
            $resources = $incentiveModel->getIncentivesByRunningStatus(
                $this->accountId, $this->userId, $runningStatus
            );
            //check for contacts and return the response json
            if (!empty($resources) && is_array($resources)) {

                $this->response['response']['status'] = 'true';
                $this->response['response']['content'] = $resources;
            } else {
                $this->response['response']['errorMessage']
                    = 'No records found';
            }
        } else {
            $this->response['response']['errorMessage']
                = 'Not a valid input. Please try again';
        }
        echo json_encode($this->response);
    }

    /**
     * Functionality to get past reward details of an user
     *
     * @return object json response of past reward details
     */
    public function getPastAwardDetailAction()
    {
        $incentiveId = $this->params['incentiveId'];
        if (($this->userId > 0) && ($this->accountId > 0)
            && (is_numeric(
                $incentiveId
            ))
            && ($incentiveId > 0)
        ) {

            $resources = $this->userIncentiveModel->getUserPastAward(
                $incentiveId, $this->userId
            );
            //check for contacts and return the response json
            if (!empty($resources) && is_array($resources)) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content'] = $resources;
            } else {
                $this->response['response']['errorMessage']
                    = 'No records found';
            }
        } else {
            $this->response['response']['errorMessage']
                = 'Not a valid input. Please try again';
        }
        echo json_encode($this->response);
    }

    /**
     * Functionality to get generated leads for a specific date range for an user
     *
     * @return object json response of generated leads
     */
    public function getGeneratedLeadsByDateRangeAction()
    {
        $startDate = $this->params['startDate'];
        $endDate = $this->params['endDate'];

        //check for required inputs
        if ((0 < $this->userId) && (0 < $this->accountId)
            && ('' != $startDate)
            && ('' != $endDate)
        ) {

            $resources
                = $this->userIncentiveModel->getGeneratedLeadsByDateRange(
                    $this->userId, $startDate, $endDate
                );
            //check for contacts and return the response json
            if ($resources) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content'] = $resources;
            } else {
                $this->response['response']['errorMessage']
                    = $this->userIncentiveModel->getMessage;
            }
        } else {
            $this->response['response']['errorMessage']
                = 'Not a valid input. Please try again';
        }
        echo json_encode($this->response);
    }

}
