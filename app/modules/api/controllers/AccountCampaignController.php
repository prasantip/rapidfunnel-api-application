<?php

/**
 * Api_AccountCampaignController
 *
 * @category  Controller
 * @package   Admin
 * @copyright 2014 Digital Assets, Inc.
 * @license   Not licensed for external use
 */
class Api_AccountCampaignController extends Zend_Controller_Action
{

    //declare the response variable
    public $response = array('response' =>
        array('status' => 'false')
    );
    public $model;
    public $userId;
    public $accountId;
    public $userRoleId;
    public $ignoreUserPayment;
    public $isTrial;
    public $status;
    public $userDetailsArray;

    /**
     * @function: init()
     * @purpose: setting default parameters
     * @params : nothing
     */
    public function init()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //get the post data
        $this->userId = $this->_request->getPost('userId');
        //load the contacts model
        $this->model = new RapidFunnel_Model_AccountCampaign();
        $userModel = new RapidFunnel_Model_AccountUser();

        $key = array('accountId', 'roleId', 'ignoreUserPayment',
                     'isTrial', 'status', 'payThroughAuthNet');
        $userModel->load($this->userId, $key);

        $this->accountId = $userModel->accountId;
        $this->userRoleId = $userModel->roleId;
        $this->ignoreUserPayment = $userModel->ignoreUserPayment;
        $this->isTrial = $userModel->isTrial;
        $this->status = $userModel->status;
        $this->payThroughAuthNet = $userModel->payThroughAuthNet;

        $this->userDetailsArray = array();
        $this->userDetailsArray['ignoreUserPayment'] = $this->ignoreUserPayment;
        $this->userDetailsArray['accountId'] = $this->accountId;
        $this->userDetailsArray['isTrial'] = $this->isTrial;
        $this->userDetailsArray['status'] = $this->status;
        $this->userDetailsArray['payThroughAuthNet'] = $this->payThroughAuthNet;
        $this->userDetailsArray['id'] = $this->userId;
    }

    /**
     * Get campaign details for the campaign belong to the app of an user
     *
     * @return object
     * @throws Exception
     */
    public function getCampaignsAction()
    {
        //get config data
        $accountRolesCnf = RapidFunnel_Configs::_instance('accountRoles');

        // if not admin,  have only access to same group campaign
        if ($accountRolesCnf->admin != $this->userRoleId) {
            // user then allow to get campaign belongs to user grp
            $roleId = '';
            if($accountRolesCnf->user === $this->userRoleId) {
                $roleId = $accountRolesCnf->user;
            }

            // if not admin,  have only access to same group campaign
            $accessibleGroupIds
                = Zend_Controller_Action_HelperBroker::getStaticHelper(
                'UserGroup'
            )->getAllAccessibleGroupIdsByUserId($this->userId,
                $roleId);

            //Calling a model function to generate the query
            $response = $this->model->getAllCampaignsForApp(
                $this->userId, $this->accountId,
                $this->userDetailsArray,  $accessibleGroupIds
            );
        } else {
            //Calling a model function to generate the query
            $response = $this->model->getAllCampaignsForApp(
                $this->userId, $this->accountId, $this->userDetailsArray
            );
        }

        //check for campaigns and return the response json
        if (!empty($response) && is_array($response)) {
            $this->response['response']['status'] = 'true';
            $this->response['response']['content']['campaigns'] = $response;
        } else {
            $this->response['response']['errorMessage']
                = 'No campaign found with this account';
        }
        echo json_encode($this->response);
    }


    /**
     * Get specific campaign details
     *
     * @return object
     * @throws Exception
     */
    public function getCampaignDetailsAction()
    {
        $userId = $this->_request->getPost('userId');
        $campaignId = $this->_request->getPost('campaignId');

        if (!is_numeric($campaignId)) {
            $this->response['response']['errorMessage'] = 'Invalid campaign id';
        } elseif (!is_numeric($userId)) {
            $this->response['response']['errorMessage'] = 'Invalid user id';
        } else {
            $campaignDetails = $this->model->getCampaignDetails(
                $userId, $campaignId
            );
            //check for contacts and return the response json
            if (!empty($campaignDetails) && is_array($campaignDetails)) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content'] = $campaignDetails;
            } else {
                $this->response['response']['errorMessage']
                    = 'No details found with this campaign id';
            }
        }
        echo json_encode($this->response);
    }

    /**
     * functionality to update contact list to memcache
     *
     * @param : void
     *
     * @return boolean
     */
    public function updateMemcacheAction()
    {
        //add or update latest contact to memcache
        //generate key to get data
        $config = RapidFunnel_Configs::_instance();
        $accountRoles = $config->accountRoles;
        $key = $this->_helper->Common->generateKey(
            $config->memcache->contactKey, $this->userId);

        //get contacts as user
        $contactModel = new RapidFunnel_Model_AccountContact();
        $contacts = $contactModel->getContact(
            $this->accountId, $this->userId,
            $accountRoles->user, null, null, true
        );

        // Encode data to support utf8
        $contacts = $this->_helper->Common->utf8_converter($contacts);

        $contactDetailsArray['response']['status'] = 'true';
        $contactDetailsArray['response']['content']['contacts'] = $contacts;

        $contactDetailsArray = json_encode(
            $contactDetailsArray, JSON_HEX_QUOT | JSON_HEX_TAG
        );

        $memCached = new RapidFunnel_Memcache_Memcached();
        //check if key exist in memcache then get data
        $dataExist = $memCached->checkKeyExistWithData($key);

        if (!empty($dataExist)) {
            //update new data to memcache
            $memCached->Update($key, $contactDetailsArray);
        } else {
            //add data to memcache
            $memCached->setItem($key, $contactDetailsArray);
        }
    }


    /**
     * Assign contact to a particular campaign
     * @return object json response containing status of assigning campaign
     */
    public function assignContactToCampaignAction()
    {
        $allowed = true;

        $campaignId = $this->_request->getPost('campaignId');
        $contactId = $this->_request->getPost('contactId');
        $modelAccountCampaign = new RapidFunnel_Model_AccountCampaign();

        // if campaignId is 0 then remove campaign and campaign email for
        //contact
        if ('0' === $campaignId) {
            $status = $modelAccountCampaign->removeCampaign($contactId);

            if ($status) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content']['message']
                    = 'Contact has been assign successfully';

                $this->updateMemcacheAction();
            } else {
                $this->response['response']['errorMessage']
                    = 'Error during the process';
            }
        } else {
            $accessibleGroupIds
                = Zend_Controller_Action_HelperBroker::getStaticHelper(
                'UserGroup'
            )->getAllAccessibleGroupIdsByUserId($this->userId);

            //Get all the campaigns belong to the user
            $response = $this->model->getAllCampaignsForApp(
                $this->userId, $this->accountId,
                $this->userDetailsArray, $accessibleGroupIds
            );

            if (!empty(array_filter($response))) {
                //Extract campaign Ids
                $campaignsBelongToUser = array_map(
                    function ($var) {
                        return $var['id'];
                    }, $response
                );

                //Check if campaign belongs to the user or not
                if (!empty($campaignsBelongToUser)
                    && in_array(
                        $campaignId, $campaignsBelongToUser
                    )
                ) {
                    if (!is_numeric($contactId) || !is_numeric($campaignId)) {
                        $this->response['response']['errorMessage']
                            = 'Invalid campaign id or contact id';
                    } else {

                        //get contact details
                        $accountContactModel = new RapidFunnel_Model_AccountContact(
                        );
                        $accountContactModel->load($contactId);

                        if (isset($accountContactModel->email)
                            && !empty($accountContactModel->email)
                        ) {

                            $modelAccountCampaign->load($campaignId);

                            // check if enterprise free user
                            $freeEnterpriseAccount
                                = $this->_helper->accountUser->isEnterpriseFreeUser(
                                $this->userDetailsArray
                            );

                            // check if has access to this campaign
                            if ($freeEnterpriseAccount) {
                                if (!$modelAccountCampaign->campaignAccessPermissionForFreeUser(
                                    $campaignId,
                                    $modelAccountCampaign->accountId
                                )
                                ) {
                                    $this->response['response']['errorMessage']
                                        = 'Sorry, not allowed';
                                    $allowed = false;
                                }
                            }

                            if ($allowed) {
                                $modelAccountCampaignContact
                                    = new RapidFunnel_Model_AccountCampaignContact(
                                );
                                $isCampaignChangedOrNew = false;
                                $oldAccountCampaignId = false;

                                //check if contact campaign changed, when changed and optIn is 0 send mail
                                if ($contactId) {
                                    $oldCampaignId
                                        = $accountContactModel->campaignId;

                                    $isCampaignChangedOrNew = true;
                                    // make is campaign to false when campaign
                                    // not changed
                                    if ($campaignId === $oldCampaignId) {
                                        $isCampaignChangedOrNew = false;
                                    }
                                }

                            if ($isCampaignChangedOrNew) {
                                // create array to pass contact and campaign details
                                $contactArray['accountId']
                                    = $accountContactModel->accountId;
                                $contactArray['email']
                                    = $accountContactModel->email;
                                $contactArray['status']
                                    = $accountContactModel->status;
                                $contactArray['optIn']
                                    = $accountContactModel->optIn;
                                $contactCampaignArray['oldCampaignId']
                                    = $oldAccountCampaignId;
                                $contactCampaignArray['changedBy']
                                    = $this->userId;

                                $contactCampaignArray['campaign'] = $campaignId;
                                $dbStatus
                                    = $modelAccountCampaignContact->assignContactToCampaign(
                                    $contactId, $contactArray, $contactCampaignArray
                                );
                            } else {
                                $dbStatus['message'] = 'true';
                            }
                                //check for contacts and return the response json
                                if ('true' === $dbStatus['message']) {
                                    if (isset($dbStatus['BouncedStatus'])
                                        && 1 ===
                                        $dbStatus['BouncedStatus']
                                    ) {
                                        $this->response['response']
                                        ['errorMessage'] = 'The email you entered'
                                            . ' appears to be invalid, please'
                                            . ' check it and try again.';
                                    } else {
                                        $this->response['response']['status']
                                            = 'true';
                                        $this->response['response']['content']
                                        ['message'] =
                                            'Contact has been assigned successfully';
                                    }

                                    $this->updateMemcacheAction();

                                } else {
                                    if ('error' === $dbStatus['message']) {
                                        $this->response['response']['errorMessage']
                                            = 'Error during the process';
                                    } elseif ('notAllowed'
                                        === $dbStatus['message']
                                    ) {
                                        $this->response['response']['errorMessage']
                                            = $dbStatus['errorMessage'];
                                    } elseif ('duplicateContact'
                                        === $dbStatus['message']
                                    ) {
                                        $this->response['response']['errorMessage']
                                            = 'Another user in your company has already '
                                            . 'assigned '
                                            . $accountContactModel->email
                                            . ' to a campaign.';
                                    }
                                }
                            }
                        } else {
                            $this->response['response']['errorMessage']
                                = 'Contact should have a email to assign Campaign.';
                        }
                    }
                } else {
                    $this->response['response']['errorMessage']
                        = 'Invalid campaign id';
                }
            } else {
                $this->response['response']['errorMessage']
                    = 'Invalid campaign id';
            }
        }

        echo json_encode($this->response);
    }

}
