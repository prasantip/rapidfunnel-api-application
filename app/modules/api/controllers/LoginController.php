<?php

/**
 * Api_LoginController
 *
 * @category  Controller
 * @package   Api
 * @copyright 2014 Digital Assets, Inc.
 * @license   Not licensed for external use
 */
class Api_LoginController extends Zend_Controller_Action
{

    //prepared the response data to send after processing login
    public $response = array('response' =>
        array('status' => 'false')
    );

    /**
     * @function: init()
     * @purpose: setting default parameters
     * @params : nothing
     */
    public function init()
    {
        //load the account user model
        $this->model = new RapidFunnel_Model_AccountUser();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->cmnHelper = new RapidFunnel_Controller_Action_Helper_Common();
    }

    /**
     * Login to the user account through API
     *
     * @return string a JSON encoded string containing authenticate
     *                  details and branding color details
     */
    public function indexAction()
    {
        $response = $this->response;
        //login form
        $this->form = new RapidFunnel_Form_Login();
        $config = RapidFunnel_Configs::_instance();

        //checking for post method and valid form
        if ($this->_request->isPost()) {

            $postParams = $this->_request->getParams();
            $webViewToken = isset($postParams['webViewToken'])
                ? $postParams['webViewToken'] : '';
            $getPost = $this->_request->getPost();
            $memcacheModel = new RapidFunnel_Model_Memcache();
            // If $webViewToken found authenticate user based on the token
            if (!empty($webViewToken)) {
                $this->model->load(
                    $webViewToken, array('email', 'password'), 'webViewToken'
                );

                if ($this->model->isLoaded) {
                    //Memcache checking
                    $authenticated = $memcacheModel->authUser(
                        $this->model->email, $this->model->password,
                        $webViewToken
                    );
                    if (!empty($authenticated)) {
                        echo json_encode($authenticated);
                        exit;
                    }

                    //checking for authentication
                    $authenticate = $this->_helper->Authenticate->authApiUser(
                        $this->model->email, $this->model->password, 1
                    );

                    if ($authenticate) {
                        $response = $this->_helper->Authenticate->setAuthResponse(
                            $authenticate
                        );
                    } else {
                        $response['response']['errorMessage']
                            = 'Authentication Failed';
                    }
                } else {
                    $response['response']['errorMessage']
                        = 'Authentication Failed';
                }
            } elseif ($this->form->isValid($getPost)) {
                $username = trim($this->form->getValue('username'));
                $password = trim($this->form->getValue('password'));

                //Memcache checking
                $authenticated = $memcacheModel->authUser(
                    $username, $password
                );
                if (!empty($authenticated)) {
                    echo json_encode($authenticated);
                    exit;
                }

                //checking for authentication
                $authenticate = $this->_helper->Authenticate->authApiUser(
                    $username, $password
                );
                if ($authenticate && (!isset($authenticate['inactiveStatus']))) {
                    $statusDeactivate = $config->autoSuspend->status->deactivate;

                    //Check if user is autoSuspended
                    if ($statusDeactivate == $authenticate['autoSuspendStatus']) {
                        $response['response']['errorMessage']
                            = $config->autoSuspend->message->deactivate;
                    }
                    //check if free user
                    elseif(!$authenticate['freeUser']) {
                        //Check if the user belongs to Legalshield check
                        // if advantage status is 1
                        //or 0 and if 0 make user Free and if 1 make user Paid
                        $this->cmnHelper->updateAdvantageStatus(
                            $authenticate['accountId'], $authenticate['userId']
                        );

                        $rememberMe = 0;
                        if (!empty($getPost['remember'])) {
                            $rememberMe = 1;
                        }

                        $response = $this->_helper->Authenticate->setAuthResponse(
                            $authenticate, $rememberMe
                        );
                    } else {
                        $FreeUserConfig =
                            RapidFunnel_Configs::_instance('freeUser');
                        $response['response']['errorMessage']
                            = $FreeUserConfig->errorMessage;
                    }
                } else {
                    if (isset($authenticate['inactiveStatus'])
                        && $authenticate['inactiveStatus'] === false
                    ) {
                        $response['response']['errorMessage']
                            =  $config->message->logIn->inactiveApp;
                    }
                    else {
                        $response['response']['errorMessage']
                            = 'Authentication Failed';
                    }
                }
            } else {
                //get the error message
                $response['response']['errorMessage']
                    = $this->form->getMessages();
            }
        } else {
            //get the error message
            $response['response']['errorMessage']
                = "Invalid user's details";
        }

        //send the json data to the client
        echo json_encode($response);
    }

    /**
     * Send link to reset password when forget password
     *
     * @return object json response containing status and user details
     */
    public function forgotPasswordAction()
    {
        //get the post email
        $email = $this->_request->getPost('email');
        //checking for post method and valid form
        if (!empty($email) && $this->model->isEmailAvailable($email)) {
            //get the user by loading email
            $user = $this->model->loadByEmail($email);
            $this->model->load($user['id']);

            $mailStatus = $this->model->sendForgetPasswordEmail(
                $user['id'], $email, $user['firstName'], $user['lastName']
            );

            if ($mailStatus) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content']['userId'] = $user['id'];
                $this->response['response']['content']['email']
                    = $user['email'];
                $this->response['response']['content']['firstName']
                    = $user['firstName'];
                $this->response['response']['content']['lastName']
                    = $user['lastName'];
            } else {
                $this->response['response']['errorMessage']
                    = 'Error during password retrieving, please try after sometime';
            }

        } else {
            $this->response['response']['errorMessage']
                = 'Please enter a valid email id';
        }

        //get the response
        echo json_encode($this->response);
    }

    /**
     * @function: changePasswordAction()
     * @purpose: to change pawword and make the foce change 1->0
     * @param: none
     * @return: json $response
     */
    public function changePasswordAction()
    {
        $changePassForm = new RapidFunnel_Form_ChangePassword();
        // below field is not needed in api section
        $changePassForm->removeElement('oldPassword');

        //checking for post method and valid form
        if ($this->_request->isPost() && $changePassForm->isValid($this->_request->getPost())) {
            //get the post parameters
            $password = $changePassForm->getValue('password');
            $accessToken = $this->_request->getPost('accessToken');
            $userId = $this->_request->getPost('userId');

            //checking for authontication
            $isValidToken = $this->_helper->Authenticate->authUserToken($userId,
                    $accessToken);

            if ($isValidToken) {
                //load account user model
                $this->model->load($userId);

                $passwordModel = new RapidFunnel_Model_Password();
                $passwordModel->create($password);
                //get the password details and set it on account user table
                $this->model->password = $passwordModel->password;
                $this->model->salt = $passwordModel->salt;
                $this->model->forceChange = 0;

                if ($this->model->update()) {
                    $this->response['response']['status'] = 'true';
                    $this->response['response']['content']['userId'] = $userId;
                    $this->response['response']['content']['forceChange'] = '0';
                } else {
                    $this->response['response']['errorMessage'] = 'Error during change password, please try after sometime';
                }
            } else {
                $this->response['response']['errorMessage'] = 'Invalid access';
            }
        } else {
            //get the error message
            $this->response['response']['errorMessage'] = $changePassForm->getMessages();
        }

        //send the json data to the client
        echo json_encode($this->response);
    }

    /**
     * @function: validateUserRegistrationAction()
     * @purpose: to check wheather the given email id and registration code is matching or not.
     * @param: none
     * @return: json $response
     */
    public function validateUserRegistrationAction()
    {
        $setPassForm = new RapidFunnel_Form_NewUserSetPassword();
        $setPassForm->removeElement('password');
        $setPassForm->removeElement('confirmPassword');

        //checking for post method and valid form
        if ($this->_request->isPost() && $setPassForm->isValid($this->_request->getPost())) {
            //get the post parameters
            $email = $setPassForm->getValue('email');
            $registrationCode = $setPassForm->getValue('registrationCode');
            $status = $this->model->validateRegistration($email,
                    $registrationCode);
            if ($status) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content']['message'] = "User's details validated successfully";
            } else {
                $this->response['response']['errorMessage'] = "Invalid user's details";
            }
        } else {
            //get the error message
            $this->response['response']['errorMessage'] = $setPassForm->getMessages();
        }

        echo json_encode($this->response);
    }

    /**
     * @function: getNewPassword()
     * @purpose: to create new password for the newly created user
     * @param: none
     * @return: json $response
     */
    public function getNewPasswordAction()
    {
        $setPassForm = new RapidFunnel_Form_NewUserSetPassword();
        //checking for post method and valid form
        if ($this->_request->isPost() && $setPassForm->isValid($this->_request->getPost())) {

            //get the post parameters
            $email = $setPassForm->getValue('email');
            $registrationCode = $setPassForm->getValue('registrationCode');
            $password = $setPassForm->getValue('password');
            $status = $this->model->validateRegistration($email,
                    $registrationCode);

            if ($status) {
                //get the user by loading email
                $user = $this->model->loadByEmail($email);
                $this->model->load($user['id']);

                $passwordModel = new RapidFunnel_Model_Password();
                $passwordModel->create($password);
                //get the password details and set it on account user table
                $this->model->password = $passwordModel->password;
                $this->model->salt = $passwordModel->salt;
                $this->model->registrationCode = null;
                if ($this->model->update()) {
                    $this->response['response']['status'] = 'true';
                    $this->response['response']['content']['message'] = "Password created successfully";
                } else {
                    $this->response['response']['errorMessage'] = 'Error during create password, please try after sometime';
                }
            } else {
                $this->response['response']['errorMessage'] = "Invalid user's details";
            }
        } else {
            $this->response['response']['errorMessage'] = $setPassForm->getMessages();
        }

        echo json_encode($this->response);
    }

    /**
     * function logoutUsersAction()
     * @purpose to logout the users by ajax
     * @param none
     * @return none
     */
    public function logoutUsersAction()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $status = false;

        //checking for post method and valid form
        if ($this->_request->isPost()) {
            //get the post parameters
            $accessToken = $this->_request->getPost('accessToken');
            $userId = $this->_request->getPost('userId');

            //checking for authentication
            $isValidToken = $this->_helper->Authenticate->authUserToken($userId,
                $accessToken);

            if ($isValidToken) {
                $status = $this->_helper->Authenticate->logout();
                if($status) {
                    $this->response['response']['status'] = 'true';
                }
            }
        }

        if(!$status) {
            $this->response['response']['errorMessage'] = 'Invalid access';
        }

        echo json_encode($this->response);
    }

    /**
     * To get user contact, resource, campaign and reward stats
     *
     * @param none
     * @return json $response
     */
    public function contactResourceCampaignRewardCountAction()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $response =  $this->response;
        $response['response']['status'] = 'false';

        if (is_numeric($this->_request->getPost('userId'))) {
            $userId = $this->_request->getPost('userId');

            //checking for authentication
            $isValidToken = $this->_helper->Authenticate->authUserToken(
                $userId, $this->_request->getPost('accessToken')
            );

            if ($isValidToken) {
                //Check if the info in memcache
                //To check access token in memcache
                $memcacheModel = new RapidFunnel_Model_Memcache();
                //Memcache checking
                $dashboardInfo = $memcacheModel->getUserDashboardInfo(
                    $userId
                );
                //Return if data is not empty
                if (!empty($dashboardInfo)) {
                    $response = $dashboardInfo;
                } else {
                    $response = $this->model->getUserApiDashboardInfo($userId);
                    //Update the data to memcache
                    $memcacheModel = new RapidFunnel_Model_Memcache();
                    $memcacheModel->setApiDashboardInfo($userId);
                }
                if (!($response)) {
                    $response['response']['errorMessage']
                        = 'Invalid Data';
                }
            } else {
                $response['response']['errorMessage']
                    = 'Invalid access token';
            }
        } else {
            $response['response']['errorMessage'] = 'Invalid user id';
        }

        //send the json data to the client
        echo json_encode($response);
    }

}
