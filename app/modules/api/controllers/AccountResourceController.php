<?php

/**
 * Api_AccountResourceController
 *
 * @category  Controller
 * @package   Api
 * @copyright 2014 Digital Assets, Inc.
 * @license   Not licensed for external use
 */
class Api_AccountResourceController extends Zend_Controller_Action
{
    //declare the response variable
    public $response = array('response' =>
        array('status' => 'false')
    );
    //declare the variables
    public $model;
    public $userId;
    public $accountId;
    public $repId;
    public $repId2;
    public $roleId;
    public $ignoreUserPayment;
    public $isTrial;
    public $status;
    public $firstName;
    public $lastName;
    public $email;
    public $contactId;
    public $phoneNumber;
    public $payThroughAuthNet;

    /**
     * @function: init()
     * @purpose: setting default parameters
     * @params : nothing
     */
    public function init()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->params = $this->_request->getParams();
        //get the post data
        $this->userId = $this->params['userId'];
        //load the collateral model
        $this->model = new RapidFunnel_Model_AccountResources();
        $userModel = new RapidFunnel_Model_AccountUser();
        $key = array('firstName', 'lastName', 'email', 'phoneNumber',
                     'accountId', 'roleId', 'repId', 'repId2',
                     'ignoreUserPayment', 'isTrial', 'status',
                     'payThroughAuthNet');
        $userModel->load($this->userId, $key);

        $this->accountId = $userModel->accountId;
        $this->roleId = $userModel->roleId;
        $this->ignoreUserPayment = $userModel->ignoreUserPayment;
        $this->isTrial = $userModel->isTrial;
        $this->status = $userModel->status;
        $this->repId = isset($userModel->repId) ? $userModel->repId : "";
        $this->repId2 = isset($userModel->repId2) ? $userModel->repId2 : "";
        $this->firstName = $userModel->firstName;
        $this->lastName = isset($userModel->lastName) ? $userModel->lastName : "";
        $this->email = $userModel->email;
        $this->phoneNumber = $userModel->phoneNumber;
        $this->payThroughAuthNet = $userModel->payThroughAuthNet;
    }

    /**
     * @function: getResourceAction()
     * @purpose: functionality to fetch all resource associated with the user account.
     * @param: void
     */
    public function getResourceAction()
    {
        //get config data
        $config = RapidFunnel_Configs::_instance();
        $accountRolesCnf = $config->accountRoles;
        $contentPlaceholders = $config->contentPlaceholders;

        $userDetailsArray['ignoreUserPayment'] = $this->ignoreUserPayment;
        $userDetailsArray['accountId'] = $this->accountId;
        $userDetailsArray['isTrial'] = $this->isTrial;
        $userDetailsArray['status'] = $this->status;
        $userDetailsArray['id'] = $this->userId;
        $userDetailsArray['payThroughAuthNet'] = $this->payThroughAuthNet;

        // if not admin,  have only access to same group resource
        if ($accountRolesCnf->admin != $this->roleId) {
            $roleId = '';
            if($accountRolesCnf->user === $this->roleId) {
                $roleId = $accountRolesCnf->user;
            }

            // if not admin,  have only access to same group campaign
            $accessibleGroupIds
                = Zend_Controller_Action_HelperBroker::getStaticHelper(
                'UserGroup'
            )->getAllAccessibleGroupIdsByUserId($this->userId,
                $roleId);

            //Calling a model function to generate the query
            $resources = $this->model->getResourcesForApi($this->accountId,
                    $this->userId, $accessibleGroupIds, $userDetailsArray);

        } else {
            //Calling a model function to generate the query
            $resources = $this->model->getResourcesForApi($this->accountId,
                    $this->userId, '', $userDetailsArray);
        }

        //check for contacts and return the response json
        if (!empty($resources) && is_array($resources)) {
            $baseUrl = $config->serverInfo->domain;
            $aRConfig = $config->accountResourceTypeId;
            $aRTypeLink = $aRConfig->link;
            $aRTypeCustomVideoPage = $aRConfig->customVideoPage;
            $aRTypeCustomPage = $aRConfig->customPage;
            $aRTypePlainText = $aRConfig->plainText;

            //Getting the remote domain for custom video url
            $accountBrandingModel = new RapidFunnel_Model_AccountBranding();
            $key = array('domainName');
            $accountBrandingModel->load($this->accountId, $key,
                'accountId');

            $accountBrandingDomain = (isset($accountBrandingModel->domainName) ?
                $accountBrandingModel->domainName : '');

            //check for all files other than link and add baseurl in it
            foreach ($resources as $key => $item) {

                //when resource type is link
                if ($aRTypeLink == $item['accountResourceTypeId']) {

                    $resources[$key]['link']
                        = $baseUrl . str_replace(
                            array('[resourceId]', '[userId]'),
                            array($item['id'], $this->userId),
                            $config->accountResource->links->publicLink
                        );
                    //When resource type is custom video
                } elseif (($aRTypeCustomVideoPage == $item['accountResourceTypeId'])
                || ($aRTypeCustomPage == $item['accountResourceTypeId'])) {

                    if (!empty($accountBrandingDomain)) {
                        $customSiteUrl = $accountBrandingDomain;
                    } else {
                        $customSiteUrl = $baseUrl;
                    }
                    //Remove the trailing "/"
                    $customSiteUrl = rtrim($customSiteUrl, '/');
                    $resources[$key]['link'] = $customSiteUrl . str_replace(
                        array('[resourceId]', '[userId]'),
                        array($item['id'], $this->userId),
                        $config->accountResource->links->publicLink
                    );

                    //For custom pages
                    if ($aRTypeCustomPage == $item['accountResourceTypeId']) {
                        $resources[$key]['content'] = '';
                    }
                } elseif ($aRTypePlainText == $item['accountResourceTypeId']) {

                    $templateVars = array(
                        $contentPlaceholders->senderFirstName,
                        $contentPlaceholders->senderLastName,
                        $contentPlaceholders->senderEmail,
                        $contentPlaceholders->senderPhone,
                        $contentPlaceholders->repId,
                        $contentPlaceholders->repId2
                    );
                    $replaceTemplateVars = array(
                        $this->firstName, $this->lastName,
                        $this->email, $this->phoneNumber,
                        $this->repId,
                        $this->repId2
                    );


                    if (isset($this->params['contactId'])
                        && !empty($this->params['contactId'])
                    ) {
                        $this->contactId = $this->params['contactId'];
                        $contactModel = new RapidFunnel_Model_AccountContact();
                        $contactDetails = array('firstName', 'lastName',
                                                'email',
                                                'phone', 'home', 'work',
                                                'other');

                        $contactModel->load(
                            $this->contactId, $contactDetails
                        );
                        if (isset($contactModel->firstName)
                            && !empty($contactModel->firstName)
                        ) {
                            $contactPhone = array();
                            if (isset($contactModel->phone)) {
                                array_push(
                                    $contactPhone, $contactModel->phone
                                );
                            }
                            if (isset($contactModel->home)) {
                                array_push(
                                    $contactPhone, $contactModel->home
                                );
                            }
                            if (isset($contactModel->work)) {
                                array_push(
                                    $contactPhone, $contactModel->work
                                );
                            }
                            if (isset($contactModel->other)) {
                                array_push(
                                    $contactPhone, $contactModel->other
                                );
                            }

                            $contactPhone
                                = $this->_helper->Common->concatMultiField(
                                $contactPhone
                            );
                            $contactModel->phone = $contactPhone;
                            $contactTemplate
                                = array($contentPlaceholders->firstName,
                                        $contentPlaceholders->lastName,
                                        $contentPlaceholders->email,
                                        $contentPlaceholders->phoneNumber);
                            $contactValues = array(
                                $contactModel->firstName,
                                $contactModel->lastName,
                                $contactModel->email, $contactModel->phone,
                            );

                            $templateVars = array_merge(
                                $templateVars, $contactTemplate
                            );
                            $replaceTemplateVars = array_merge(
                                $replaceTemplateVars, $contactValues
                            );
                        }
                    }

                    $item['content'] = str_replace(
                        $templateVars, $replaceTemplateVars, $item['content']
                    );

                    $item['name'] = str_replace(
                        $templateVars, $replaceTemplateVars, $item['name']
                    );

                    $resources[$key]['content'] = $item['content'];
                    $resources[$key]['name'] = $item['name'];
                    $resources[$key]['link'] = '';
                } else {
                    $resources[$key]['link'] = $baseUrl . '/' . $config->uploadPath->accountResourceFile . '/' . $item['link'];
                }
            }
            $this->response['response']['status'] = 'true';
            $this->response['response']['content'] = $resources;
        } else {
            $this->response['response']['errorMessage'] = 'No records found';
        }
        echo json_encode($this->response);
    }

}
