<?php

/**
 * Api_AccountContactController
 *
 * @category  Controller
 * @package   Api
 * @copyright 2014 Digital Assets, Inc.
 * @license   Not licensed for external use
 */
class Api_AccountContactController extends Zend_Controller_Action
{

    //declare the response variable
    public $response = array(
        'response' => array('status' => 'false')
    );
    public $model;
    public $userId;
    public $accessToken;
    public $accountId;
    public $roleId;
    public $apiCall;
    public $country;
    public $stateId;


    /**
     * @function: init()
     * @purpose: setting default parameters
     * @params : nothing
     */
    public function init()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //get the post data
        $this->userId = $this->_request->getPost('userId');
        if (!is_numeric($this->userId)) {
            $this->response['response']['errorMessage'] = 'Invalid user id';
            //send the json data to the client
            echo json_encode($this->response);
        }

        $this->accessToken = $this->_request->getPost('accessToken');

        //load the contacts model
        $this->model = new RapidFunnel_Model_AccountContact();
        $userModel = new RapidFunnel_Model_AccountUser();

        $key = array('accountId', 'roleId', 'countryId', 'stateId');
        $userModel->load($this->userId, $key);

        $this->accountId = $userModel->accountId;
        $this->roleId = $userModel->roleId;
        $this->countryId = isset($userModel->countryId)
            ? $userModel->countryId : "";
        $this->stateId = isset($userModel->stateId)
            ? $userModel->stateId : "";

    }

    /**
     * @function: indexAction()
     * @purpose: to show the dashboard screen
     * @param: none
     * @return: none
     */
    public function indexAction()
    {
        //code for getting all the contacts details for a particular person
    }

    /**
     * functionality to update contact list to memcache
     *
     * @param : void
     *
     * @return boolean
     */
    public function updateMemcacheAction()
    {
        //add or update latest contact to memcache
        //generate key to get data
        $config = RapidFunnel_Configs::_instance();
        $accountRoles = $config->accountRoles;
        $contactStatus = $config->accountContactStatus;

        $key = $this->_helper->Common->generateKey(
            $config->memcache->contactKey, $this->userId);

        //get contacts as user
        $contacts = $this->model->getContact(
            $this->accountId, $this->userId,
            $accountRoles->user, null, null, true
        );

        // Encode data to support utf8
        $contacts = $this->_helper->Common->utf8_converter($contacts);

        //when contact status is pre-released then display as new
        foreach ($contacts as $contact => $values) {
            if (($contactStatus->preRelease === $values['status'])
                && ($accountRoles->admin !== $this->roleId)
            ) {
                $contacts[$contact]['status'] = $contactStatus->new;
            }
        }

        $contactDetailsArray['response']['status'] = 'true';
        $contactDetailsArray['response']['content']['contacts'] = $contacts;

        $contactDetailsArray = json_encode(
            $contactDetailsArray, JSON_HEX_QUOT | JSON_HEX_TAG
        );

        $memCached = new RapidFunnel_Memcache_Memcached();
        //check if key exist in memcache then get data
        $dataExist = $memCached->checkKeyExistWithData($key);

        if (!empty($dataExist)) {
            //update new data to memcache
            $memCached->Update($key, $contactDetailsArray);
        } else {
            //add data to memcache
            $memCached->setItem($key, $contactDetailsArray);
        }
    }

    /**
     * @function: getContactAction()
     * @purpose: functionality to fetch all contact associated with the user account.
     * @param: void
     * @return: json $response
     */
    public function getContactAction()
    {
        //first check in memcache if found key and data then return data
        $memCached = new RapidFunnel_Memcache_Memcached();
        $config = RapidFunnel_Configs::_instance();

        //generate key to get data
        $key = $this->_helper->Common->generateKey(
            $config->memcache->contactKey, $this->userId);

        //check if key exist in memcache then get data
        $dataExist = $memCached->checkKeyExistWithData($key);

        if (!empty($dataExist)) {
            echo $dataExist;
        } else {
            $contactStatus = $config->accountContactStatus;
            $accountRoles = $config->accountRoles;

            //get contacts as user, even if the user is admin / manager
            $contacts = $this->model->getContact(
                $this->accountId, $this->userId,
                $accountRoles->user, null, null, true
            );

            // Encode data to support utf8
            $contacts = $this->_helper->Common->utf8_converter($contacts);

            //when contact status is pre-released then display as new
            foreach ($contacts as $contact => $values) {
                if (($contactStatus->preRelease === $values['status'])
                    && ($accountRoles->admin !== $this->roleId)
                ) {
                    $contacts[$contact]['status'] = $contactStatus->new;
                }
            }

            //check for contacts and return the response json
            if (!empty($contacts) && is_array($contacts)) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content']['contacts'] = $contacts;
            } else {
                $this->response['response']['errorMessage']
                    = 'No contacts found with this account';
            }

            $this->response = json_encode(
                $this->response, JSON_HEX_QUOT | JSON_HEX_TAG
            );
            //add new data to memcache
            $memCached->setItem($key, $this->response);

            echo $this->response;
        }

    }

    /**
     * functionality to search contact and return list of searched contact
     * @param: void
     *
     * @return string $response
     */
    public function searchContactAction()
    {
        $config = RapidFunnel_Configs::_instance();
        $accountRoles = $config->accountRoles;
        $contactStatus = $config->accountContactStatus;

        $keyWord = $this->_request->getPost('contactSearchKey');

        //get contacts as user, even if the user is admin / manager
        $contacts = $this->model->getContact(
            $this->accountId, $this->userId,
            $accountRoles->user, null, $keyWord, true
        );

        // Encode data to support utf8
        $contacts = $this->_helper->Common->utf8_converter($contacts);

        //when contact status is pre-released then display as new
        foreach ($contacts as $contact => $values) {
            if (($contactStatus->preRelease === $values['status'])
                && ($accountRoles->admin !== $this->roleId)
            ) {
                $contacts[$contact]['status'] = $contactStatus->new;
            }
        }

        //check for contacts and return the response json
        if (!empty($contacts) && is_array($contacts)) {
            $this->response['response']['status'] = 'true';
            $this->response['response']['content']['contacts'] = $contacts;
        } else {
            $this->response['response']['errorMessage'] = 'No contacts found with this account';
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    /**
     * @function: getContactDetailsAction()
     * @purpose: functionality to fetch particular contact details.
     * @param: void
     * @return: json $response
     */
    public function getContactDetailsAction()
    {
        $contactId = $this->_request->getPost('contactId');
        if (!is_numeric($contactId)) {
            $this->response['response']['errorMessage'] = 'Invalid contact id';
        } else {
            $contactDetails = $this->model->getDetails($contactId);

            // Encode data to support utf8
            $contactDetails = $this->_helper->Common->utf8_converter($contactDetails);

            if (('6' === $contactDetails['status'])
                && ('1' !== $this->roleId)
            ) {
                $contactDetails['status'] = '1';
            }

            //check for contacts and return the response json
            if (!empty($contactDetails) && is_array($contactDetails)) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content'] = $contactDetails;
            } else {
                $this->response['response']['errorMessage']
                    = 'No details found with this contact id';
            }
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    /**
     * functionality to add contact through API
     *
     * @param: void
     *
     * @return string $response
     */
    public function addContactAction()
    {
        //contact form
        $addContactForm = new RapidFunnel_Form_AccountContact($this->accountId);
        $addContactForm->removeElement('campaign');

        $config = RapidFunnel_Configs::_instance();

        //checking for post method and valid form
        if ($this->_request->isPost()
            && $addContactForm->isValid(
                $this->_request->getPost()
            )
        ) {
            $getParams = $this->_request->getParams();

                if (!empty(trim($getParams['email']))) {
                    //Check duplicate Email if email is not empty
                    $contactStatus
                        = $this->model->isDuplicateContactOrCampaignAssigned(
                        $this->accountId, trim($getParams['email']),
                        $this->userId
                    );
                } else {
                    $contactStatus = 0;
                }

                // if false then error during the process
                if (0 === $contactStatus) {
                    $this->model->accountId = $this->accountId;
                    $postValues = $addContactForm->getValues();
                    $postValues = $this->assignMultiPhnEml(
                        $postValues, $getParams
                    );

                    foreach ($postValues as $key => $val) {
                        if (in_array(
                            $key, array('stateUsa', 'stateCanada', 'state',
                                        'country')
                        )) {
                            continue;
                        } elseif ('note' === $key) {
                            $this->model->$key = trim($val);
                        } else {
                            $this->model->$key = trim(
                                preg_replace('/\s+/', ' ', $val)
                            );
                        }
                    }

                    $this->model->countryId = $this->countryId;
                    $this->model->stateId = $this->stateId;
                    $this->model->channel = 'Mobile';
                    $this->model->created = date("Y-m-d H:i:s");
                    $this->model->createdBy = $this->userId;
                    $this->model->modifiedBy = $this->userId;

                    // optInSendDate is used to determine the
                    // number of days since, optInMail was sent by the user.
                    $this->model->optInSendDate = date('y-m-d H:i:s');

                    $dbStatus = $this->model->save();
                    if ($dbStatus) {

                        //update memcache with contact list
                        $this->updateMemcacheAction();

                        $this->response['response']['status'] = 'true';
                        $this->response['response']['content']['contactId']
                            = $this->model->id;

                        //Add note to accountContactNotes table
                        $contactId = $this->model->id;
                        $acntContNote
                            = new RapidFunnel_Model_AccountContactNote();
                        $vars['contactNotes']
                            = isset($getParams['note'])
                            ? array($getParams['note']) : array('');

                        //For old API add server time as note time stamp
                        $vars['noteTimeStamps'] = array(date('Y-m-d H:i:s'));

                        $noteAddStatus = true;
                        If (!empty(array_filter($vars['contactNotes']))) {
                            $noteAddStatus = $acntContNote->addNotes(
                                $contactId, $vars
                            );
                        }

                        if ($noteAddStatus) {
                            $this->response['response']['content']['message']
                                = 'Contact added successfully';
                        } else {
                            $this->response['response']['errorMessage']
                                = 'Error during the process';
                        }

                        //Update the data to memcache
                        $memcacheModel = new RapidFunnel_Model_Memcache();
                        $memcacheModel->setApiDashboardInfo($this->userId);

                    } else {
                        $this->response['response']['errorMessage']
                            = "Error during the process";
                    }
                } elseif (1 === $contactStatus) {
                    $this->response['response']['errorMessage']
                        = $config->contact->duplicateContact->message;
                } elseif (false === $contactStatus) {
                    $this->response['response']['errorMessage']
                        = 'Error during the process';
                }
        } else {
            $this->response['response']['errorMessage']
                = $addContactForm->getMessages();
        }

        echo json_encode($this->response);
    }

    /**
     * functionality to update contact through API.
     *
     * @param: void
     *
     * @return string $response
     */
    public function updateContactAction()
    {
        //contact form
        $updateContactForm = new RapidFunnel_Form_AccountContact(
            null, null,
            null, null, null, true
        );

        $config = RapidFunnel_Configs::_instance();
        $contactStatusConfig = $config->accountContactStatus;

        //checking for post method and valid form
        if ($this->_request->isPost()
            && $updateContactForm->isValid(
                $this->_request->getPost()
            )
        ) {
            $getParams = $this->_request->getParams();

            //get the contact Id
            $contactId = $this->_request->getPost('contactId');

            if (!is_numeric($contactId)) {
                $this->response['response']['errorMessage']
                    = 'Invalid contact id';
                echo json_encode($this->response);
                exit;
            }

            if (!empty(trim($getParams['email']))
                || !empty(trim(
                    $getParams['phone']
                ))
            ) {
                $this->model->load($contactId);
                if (isset($this->model->id)) {

                    $previousEmail = $this->model->email;
                    $newEmail = trim($getParams['email']);
                    $contactStatus = 0;

                    //check whether any campaign is assigned to contact
                    $accContCampModel
                        = new RapidFunnel_Model_AccountCampaignContact();
                    $campaignDetails = $accContCampModel->getCampaignByContact(
                        $contactId
                    );

                    if (0 < $campaignDetails['accountCampaignId']) {
                        $contactStatus
                            = $this->model->isDuplicateContactOrCampaignAssigned(
                            $this->accountId, $newEmail, $this->model->createdBy,
                            $contactId, true
                        );
                    } else {
                        if (!empty($newEmail)) {
                            $contactStatus
                                = $this->model->isDuplicateContactOrCampaignAssigned(
                                $this->accountId, $newEmail,
                                $this->model->createdBy,
                                $contactId
                            );
                        } else {
                            $this->model->status
                                = $contactStatusConfig->notSent;
                        }
                    }

                    // 1 : contact exist else 0
                    // if false then error during the process
                    if (0 === $contactStatus) {
                        $contactPhone = $this->model->phone;

                        //Contacts having multiple phone number the first number
                        // will be updated through mobile APP
                        if (strpos($contactPhone, ',') !== false) {
                            $phone = explode(',', $contactPhone);
                            $phone[0] = $getParams['phone'];
                            $getParams['phone'] = implode(',', $phone);
                        }

                        $updateValues = $updateContactForm->getValues();
                        $updateValues = $this->assignMultiPhnEml(
                            $updateValues, $getParams
                        );
                        unset($updateValues['campaign']);
                        foreach ($updateValues as $key => $val) {
                            if (in_array(
                                $key, array('stateUsa', 'stateCanada', 'state',
                                    'country')
                            )) {
                                continue;
                            } else {
                                $this->model->$key = trim(
                                    preg_replace('/\s+/', ' ', $val)
                                );
                            }
                        }

                        // check if email is not empty and campaign is assigned
                        // update status to 1 if not optIn
                        $modelAccountCampaignContact
                            = new RapidFunnel_Model_AccountCampaignContact();

                        $campaignId
                            = $modelAccountCampaignContact->isCampaignAssignToContact(
                            $contactId
                        );

                        $optInMailStatus = true;
                        $dbStatus = false;
                        $reSendOptInEmail = false;
                        // when campaign is assigned contact and email got changed
                        // resend optIn email and change optIn status and status
                        if ($previousEmail !== $newEmail
                            && !empty($campaignId)
                            && $contactStatusConfig->notOptIn !==
                            $this->model->optIn
                        ) {
                            $reSendOptInEmail = true;
                        }

                        if ((!empty($newEmail)
                                && $contactStatusConfig->notOptIn
                                === $this->model->optIn)
                            || ($reSendOptInEmail)
                        ) {
                            if (!empty($campaignId)) {
                                $this->model->status = $contactStatusConfig->new;
                            } else {
                                $this->model->status
                                    = $contactStatusConfig->notSent;
                            }

                            if ($reSendOptInEmail) {
                                $optInMailStatus
                                    = $modelAccountCampaignContact->sendOptInEmail(
                                    $modelAccountCampaignContact->createOptInEmail(
                                        $this->model
                                    )
                                );
                                $this->model->optIn
                                    = $contactStatusConfig->notOptIn;
                                $this->model->campaignAssignTime = date(
                                    "Y-m-d H:i:s"
                                );
                                $this->model->optInSend = $config->optin->EmailSent;
                            }

                        }

                        $this->model->modifiedBy = $this->userId;

                        if ($optInMailStatus) {
                            $dbStatus = $this->model->update();
                        }

                        //Update note to accountContactNotes table
                        $acntContNote
                            = new RapidFunnel_Model_AccountContactNote();

                        $contactNotes = array_filter(
                            $acntContNote->getNoteDetails($contactId)
                        );

                        $vars['noteIds'] = isset($contactNotes[0]['id'])
                            ? array($contactNotes[0]['id']) : 0;
                        $vars['updateContactNotes']
                            = isset($getParams['note']) ? array($getParams['note'])
                            : array('');

                        //For old API add server time as note time stamp
                        $vars['updateTimeStamps']
                            = array(date('Y-m-d H:i:s'));

                        If (!empty(array_filter($vars['updateTimeStamps']))
                            && !empty(array_filter($vars['updateContactNotes']))
                            && !empty(array_filter($vars['noteIds']))
                        ) {
                            $acntContNote->updateNotes($contactId, $vars);
                        }

                        if ($dbStatus) {
                            //update memcache with contact list
                            $this->updateMemcacheAction();

                            $this->response['response']['status'] = 'true';
                            $this->response['response']['content']['message']
                                = 'Contact updated successfully';
                        } else {
                            $this->response['response']['errorMessage']
                                = 'Error during the process';
                        }
                    } elseif (1 === $contactStatus) {
                        $this->response['response']['errorMessage']
                            = $config->contact->duplicateContact->message;
                    } elseif (false === $contactStatus) {
                        $this->response['response']['errorMessage']
                            = 'Error during the process';
                    }
                } else {
                    $this->response['response']['errorMessage'] = 'Invalid contact id';
                }
            } else {
                $this->response['response']['errorMessage']
                    = 'Email or Phone is required';
            }
        } else {
            $this->response['response']['errorMessage']
                = $updateContactForm->getMessages();
        }

        echo json_encode($this->response);
    }


    /**
     * @function: deleteContactAction()
     * @purpose: functionality to delete contact.
     * @param: void
     * @return: json $response
     */
    public function deleteContactAction()
    {
        $contactId = $this->_request->getPost('contactId');
        $userId = $this->_request->getPost('userId');

        if (!is_numeric($contactId)) {
            $this->response['response']['errorMessage'] = 'Invalid contact id';
        } else {
            $this->model->load($contactId);

            if (isset($this->model->id) && $this->model->createdBy === $userId) {
                if ($this->model->deleteContact($contactId)) {
                    $acntContNote
                        = new RapidFunnel_Model_AccountContactNote();
                    $acntContNote->deleteNotes($contactId, null, 1);

                    //update memcache with contact list
                    $this->updateMemcacheAction();
                    $memcacheModel = new RapidFunnel_Model_Memcache();
                    $userId = $this->_request->getPost('userId');
                    $memcacheModel->setApiDashboardInfo($userId);

                    $this->response['response']['status'] = 'true';
                    $this->response['response']['content']['message']
                        = 'Contact deleted successfully';
                } else {
                    $this->response['response']['errorMessage']
                        = 'Error during delete';
                }
            } else {
                $this->response['response']['errorMessage']
                    = 'You are not permitted to delete this contact';
            }
        }

        echo json_encode($this->response);
    }

    /**
     * Construct post value array for phone and email type
     *
     * @param <Array> $postValues post parameter
     * @param <Array> $getParams  contains parameter value
     *
     * @return mixed
     */
    public function assignMultiPhnEml($postValues, $getParams)
    {
        //Add key and value to post variable for saving
        //multiple phone data
        $postValues['phone'] = isset($getParams['phone']) ? $getParams['phone']
            : '';
        $postValues['home'] = isset($getParams['home']) ? $getParams['home']
            : '';
        $postValues['work'] = isset($getParams['work']) ? $getParams['work']
            : '';
        $postValues['other'] = isset($getParams['other']) ? $getParams['other']
            : '';

        //Add key and value to post variable for saving
        //multiple email data
        $postValues['email'] = isset($getParams['email']) ? $getParams['email']
            : '';
        $postValues['homeEmail'] = isset($getParams['homeEmail'])
            ? $getParams['homeEmail'] : '';
        $postValues['workEmail'] = isset($getParams['workEmail'])
            ? $getParams['workEmail'] : '';
        $postValues['otherEmail'] = isset($getParams['otherEmail'])
            ? $getParams['otherEmail'] : '';

        return $postValues;
    }
}
