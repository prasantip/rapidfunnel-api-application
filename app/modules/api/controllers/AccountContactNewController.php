<?php

/**
 * Api_AccountContactNewController
 *
 * @category  Controller
 * @package   Api
 * @copyright 2014 Digital Assets, Inc.
 * @license   Not licensed for external use
 */
class Api_AccountContactNewController extends Zend_Controller_Action
{

    //declare the response variable
    public $response = array(
        'response' => array('status' => 'false')
    );
    public $model;
    public $userId;
    public $accessToken;
    public $accountId;
    public $roleId;
    public $countryId;
    public $stateId;

    /**
     * Setting default parameters
     *
     * @return object | void json encoded response if validation fails
     */
    public function init()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //get the post data
        $this->userId = $this->_request->getPost('userId');
        if (!is_numeric($this->userId)) {
            $this->response['response']['errorMessage'] = 'Invalid user id';
            //send the json data to the client
            echo json_encode($this->response);
        }

        //load the contacts model
        $this->model = new RapidFunnel_Model_AccountContact();
        $userModel = new RapidFunnel_Model_AccountUser();

        $key = array('accountId', 'roleId', 'countryId', 'stateId');
        $userModel->load($this->userId, $key);

        $this->accountId = $userModel->accountId;
        $this->roleId = $userModel->roleId;
        $this->countryId = isset($userModel->countryId)
            ? $userModel->countryId : "";
        $this->stateId = isset($userModel->stateId)
            ? $userModel->stateId : "";
    }

    /**
     * @function: indexAction()
     * @purpose : to show the dashboard screen
     *
     * @param   : none
     */
    public function indexAction()
    {
        //code for getting all the contacts details for a particular person
    }

    /**
     * functionality to update contact list to memcache
     *
     * @param : void
     *
     * @return boolean
     */
    public function updateMemcacheAction()
    {
        //add or update latest contact to memcache
        //generate key to get data
        $config = RapidFunnel_Configs::_instance();
        $accountRoles = $config->accountRoles;
        $contactStatus = $config->accountContactStatus;

        $key = $this->_helper->Common->generateKey(
            $config->memcache->contactKey, $this->userId);

        //get contacts as user
        $contacts = $this->model->getContact(
            $this->accountId, $this->userId,
            $accountRoles->user, null, null, true
        );

        // Encode data to support utf8
        $contacts = $this->_helper->Common->utf8_converter($contacts);

        //when contact status is pre-released then display as new
        foreach ($contacts as $contact => $values) {
            if (($contactStatus->preRelease === $values['status'])
                && ($accountRoles->admin !== $this->roleId)
            ) {
                $contacts[$contact]['status'] = $contactStatus->new;
            }
        }

        $contactDetailsArray['response']['status'] = 'true';
        $contactDetailsArray['response']['content']['contacts'] = $contacts;
        $contactDetailsArray = json_encode(
            $contactDetailsArray, JSON_HEX_QUOT | JSON_HEX_TAG
        );

        $memCached = new RapidFunnel_Memcache_Memcached();
        //check if key exist in memcache then get data
        $dataExist = $memCached->checkKeyExistWithData($key);

        if (!empty($dataExist)) {
            //update new data to memcache
            $memCached->Update($key, $contactDetailsArray);
        } else {
            //add data to memcache
            $memCached->setItem($key, $contactDetailsArray);
        }
    }

    /**
     * @function: getContactAction()
     * @purpose : functionality to fetch all contact associated with the user account.
     *
     * @param   : void
     *
     * @return object json encoded associated contact details
     */
    public function getContactAction()
    {
        //first check in memcache if found key and data then return data
        $memCached = new RapidFunnel_Memcache_Memcached();
        $config = RapidFunnel_Configs::_instance();

        //generate key to get data
        $key = $this->_helper->Common->generateKey(
            $config->memcache->contactKey, $this->userId);

        //check if key exist in memcache then get data
        $dataExist = $memCached->checkKeyExistWithData($key);

        if (!empty($dataExist)) {
            echo $dataExist;
        } else {
            $contactStatus = $config->accountContactStatus;
            $accountRoles = $config->accountRoles;

            //get contacts as user, even if the user is admin / manager
            $contacts = $this->model->getContact(
                $this->accountId, $this->userId, $accountRoles->user,
                null, null, true
            );

            // Encode data to support utf8
            $contacts = $this->_helper->Common->utf8_converter($contacts);

            //when contact status is pre-released then display as new
            foreach ($contacts as $contact => $values) {
                if (($contactStatus->preRelease === $values['status'])
                    && ($accountRoles->admin !== $this->roleId)
                ) {
                    $contacts[$contact]['status'] = $contactStatus->new;
                }
            }

            //check for contacts and return the response json
            if (!empty($contacts) && is_array($contacts)) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content']['contacts'] = $contacts;
            } else {
                $this->response['response']['errorMessage']
                    = 'No contacts found with this account';
            }

            $this->response = json_encode(
                $this->response, JSON_HEX_QUOT | JSON_HEX_TAG
            );
            //add new data to memcache
            $memCached->setItem($key, $this->response);

            echo $this->response;
        }

    }

    /**
     * functionality to search contact and return list of searched contact
     * @param: void
     *
     * @return string $response
     */
    public function searchContactAction()
    {
        $config = RapidFunnel_Configs::_instance();
        $accountRoles = $config->accountRoles;
        $contactStatus = $config->accountContactStatus;

        $keyWord = $this->_request->getPost('contactSearchKey');

        //get contacts as user, even if the user is admin / manager
        $contacts = $this->model->getContact(
            $this->accountId, $this->userId,
            $accountRoles->user, null, $keyWord, true
        );

        // Encode data to support utf8
        $contacts = $this->_helper->Common->utf8_converter($contacts);

        //when contact status is pre-released then display as new
        foreach ($contacts as $contact => $values) {
            if (($contactStatus->preRelease === $values['status'])
                && ($accountRoles->admin !== $this->roleId)
            ) {
                $contacts[$contact]['status'] = $contactStatus->new;
            }
        }

        //check for contacts and return the response json
        if (!empty($contacts) && is_array($contacts)) {
            $this->response['response']['status'] = 'true';
            $this->response['response']['content']['contacts'] = $contacts;
        } else {
            $this->response['response']['errorMessage'] = 'No contacts found with this account';
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    /**
     * Fetch particular contact details.
     *
     * @return object json encoded contact details
     */
    public function getContactDetailsAction()
    {
        $contactId = $this->_request->getPost('contactId');
        if (!is_numeric($contactId)) {
            $this->response['response']['errorMessage'] = 'Invalid contact id';
        } else {
            $contactDetails = $this->model->getDetails($contactId);

            // Encode data to support utf8
            $contactDetails = $this->_helper->Common->utf8_converter($contactDetails);

            if (('6' === $contactDetails['status'])
                && ('1' !== $this->roleId)
            ) {
                $contactDetails['status'] = '1';
            }

            //check for contacts and return the response json
            if (!empty($contactDetails) && is_array($contactDetails)) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content'] = $contactDetails;
            } else {
                $this->response['response']['errorMessage']
                    = 'No details found with this contact id';
            }
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    /**
     * Add Contacts
     *
     * @return object json response
     * @throws Exception
     */
    public function addContactAction()
    {
        //contact form
        $addContactForm = new RapidFunnel_Form_AccountContact(
            null, null, null, null, null, true);
        $addContactForm->removeElement('campaign');

        $config = RapidFunnel_Configs::_instance();
        //checking for post method and valid form
        if ($this->_request->isPost()
            && $addContactForm->isValid(
                $this->_request->getPost()
            )
        ) {
            $getParams = $this->_request->getParams();

                if (!empty(trim($getParams['email']))) {
                    //Check duplicate Email if email is not empty
                    $contactStatus
                        = $this->model->isDuplicateContactOrCampaignAssigned(
                        $this->accountId, trim($getParams['email']),
                        $this->userId
                    );
                } else {
                    $contactStatus = 0;
                }

                // 1 : contact exist else 0
                // if false then error during the process
                if (0 === $contactStatus) {
                    $this->model->accountId = $this->accountId;
                    $postValues = $addContactForm->getValues();
                    $postValues = $this->assignMultiPhnEml(
                        $postValues, $getParams
                    );

                    //Get multiple notes from contactNotes
                    $acntContNote
                        = new RapidFunnel_Model_AccountContactNote();
                    $vars['contactNotes']
                        = isset($getParams['note'])
                        ? array($getParams['note']) : array('');

                    $vars['noteTimeStamps']
                        = isset($getParams['noteTimeStamps'])
                        ? $getParams['noteTimeStamps'] : array('');

                    foreach ($postValues as $key => $val) {
                        if (in_array(
                            $key,
                            array('stateUsa', 'stateCanada', 'state', 'country')
                        )) {
                            continue;
                        } elseif ('note' === $key) {
                            $this->model->$key = trim($val);
                        } else {
                            $this->model->$key = trim(
                                preg_replace('/\s+/', ' ', $val)
                            );
                        }
                    }
                    $this->model->countryId = $this->countryId;
                    $this->model->stateId = $this->stateId;
                    $this->model->channel = 'Mobile';
                    $this->model->created = date("Y-m-d H:i:s");
                    $this->model->createdBy = $this->userId;
                    $this->model->modifiedBy = $this->userId;
                    $this->model->status
                    = $config->accountContactStatus->notSent;

                    $dbStatus = $this->model->save();

                    if ($dbStatus) {
                        $this->response['response']['status'] = 'true';
                        $this->response['response']['content']['contactId']
                            = $this->model->id;

                        $contactId = $this->model->id;
                        If (!empty(array_filter($vars['noteTimeStamps']))
                            && !empty(array_filter($vars['contactNotes']))
                        ) {
                            $acntContNote->addNotes($contactId, $vars);
                        }

                        $this->response['response']['content']['message']
                            = 'Contact added successfully';

                        //update memcache with contact list
                        $this->updateMemcacheAction();

                        //Update the data to memcache
                        $memcacheModel = new RapidFunnel_Model_Memcache();
                        $memcacheModel->setApiDashboardInfo($this->userId);
                    }
                } elseif (1 === $contactStatus) {
                    $this->response['response']['errorMessage']
                        = $config->contact->duplicateContact->message;
                } elseif (false === $contactStatus) {
                    $this->response['response']['errorMessage']
                        = 'Error during the process';
                }
        } else {
            $this->response['response']['errorMessage']
                = $addContactForm->getMessages();
        }
        echo json_encode($this->response);
    }

    /**
     * Update Contacts
     *
     * @return object json response
     */
    public function updateContactAction()
    {
        //contact form
        $updateContactForm = new RapidFunnel_Form_AccountContact(
            null, null, null, null, null, true
        );

        $config = RapidFunnel_Configs::_instance();
        $contactStatusConfig = $config->accountContactStatus;

        //checking for post method and valid form
        if ($this->_request->isPost()
            && $updateContactForm->isValid(
                $this->_request->getPost()
            )
        ) {
            $getParams = $this->_request->getParams();
            //get the contact Id
            $contactId = $this->_request->getPost('contactId');

            if (!is_numeric($contactId)) {
                $this->response['response']['errorMessage'] = 'Invalid contact id';
            } else {

                $this->model->load($contactId);
                if(isset($this->model->id)){

                    $previousEmail = $this->model->email;
                    $newEmail = isset($getParams['email']) ? trim($getParams['email']) : '';
                    $contactStatus = 0;

                    //check whether any campaign is assigned to contact
                    $accContCampModel = new RapidFunnel_Model_AccountCampaignContact();
                    $campaignDetails = $accContCampModel->getCampaignByContact(
                        $contactId
                    );


                    if (!empty($newEmail)) {
                        if (0 < $campaignDetails['accountCampaignId']) {
                            $contactStatus = $this->model->isDuplicateContactOrCampaignAssigned(
                                $this->accountId, $newEmail, $this->model->createdBy, $contactId, true
                            );
                        } else {
                            $contactStatus = $this->model->isDuplicateContactOrCampaignAssigned(
                                $this->accountId, $newEmail, $this->model->createdBy, $contactId
                            );
                        }
                    } else {
                        $this->model->status = $contactStatusConfig->notSent;
                    }

                    // 1 : contact exist else 0
                    // if false then error during the process
                    if (0 === $contactStatus) {

                        $updateValues = $updateContactForm->getValues();
                        $updateValues = $this->assignMultiPhnEml(
                            $updateValues, $getParams
                        );

                        unset($updateValues['campaign']);
                        foreach ($updateValues as $key => $val) {
                            if (in_array(
                                $key, array('stateUsa', 'stateCanada', 'state',
                                    'country')
                            )) {
                                continue;
                            } else {
                                $this->model->$key = trim(
                                    preg_replace('/\s+/', ' ', $val)
                                );
                            }
                        }

                        // check if email is not empty and campaign is assigned
                        // update status to 1 if not optIn
                        $modelAccountCampaignContact = new RapidFunnel_Model_AccountCampaignContact();

                        $campaignId = $modelAccountCampaignContact->isCampaignAssignToContact(
                            $contactId
                        );

                        $optInMailStatus = true;
                        $dbStatus = false;
                        $reSendOptInEmail = false;
                        // when campaign is assigned contact and email got changed
                        // resend optIn email and change optIn status and status
                        if (strtolower($previousEmail) !== strtolower($newEmail)
                            && !empty($campaignId)
                        ) {
                            $reSendOptInEmail = true;
                        }

                        if ((!empty($newEmail) &&
                                $contactStatusConfig->notOptIn === $this->model->optIn) || ($reSendOptInEmail)
                        ) {
                            if (!empty($campaignId)) {
                                $this->model->status = $contactStatusConfig->new;
                            } else {
                                $this->model->status = $contactStatusConfig->notSent;
                            }

                            if ($reSendOptInEmail) {
                                $optInMailStatus = $modelAccountCampaignContact->sendOptInEmail(
                                    $modelAccountCampaignContact->createOptInEmail(
                                        $this->model
                                    )
                                );
                                $this->model->optIn = $contactStatusConfig->notOptIn;
                                $this->model->campaignAssignTime = date(
                                    "Y-m-d H:i:s"
                                );
                                $this->model->optInSend = $config->optin->EmailSent;
                            }
                        }

                        $this->model->modifiedBy = $this->userId;

                        if ($optInMailStatus) {
                            $dbStatus = $this->model->update();
                        }

                        //Update note to accountContactNotes table
                        $acntContNote = new RapidFunnel_Model_AccountContactNote();

                        $contactNotes = array_filter(
                            $acntContNote->getNoteDetails($contactId)
                        );

                        $vars['noteIds'] = isset($contactNotes[0]['id'])
                            ? array($contactNotes[0]['id']) : 0;

                        //If already there is note
                        if (!empty($vars['noteIds'])) {
                            $vars['updateContactNotes']
                                = isset($getParams['note'])
                                ? array($getParams['note']) : array('');

                            //For old API add server time as note time stamp
                            $vars['updateTimeStamps']
                                = isset($getParams['noteTimeStamps'])
                                ? $getParams['noteTimeStamps'] : array('');

                            If (!empty(array_filter($vars['updateTimeStamps'])) &&
                                !empty(array_filter($vars['updateContactNotes']))
                            ) {
                                $acntContNote->updateNotes(
                                    $contactId, $vars
                                );
                            }
                        } else { //If no note is there then add note
                            $vars['contactNotes']
                                = isset($getParams['note'])
                                ? array($getParams['note']) : array('');

                            //For old API add server time as note time stamp
                            $vars['noteTimeStamps']
                                = isset($getParams['noteTimeStamps'])
                                ? $getParams['noteTimeStamps'] : array('');

                            If (!empty(array_filter(
                                    $vars['noteTimeStamps']
                                ))
                                && !empty(array_filter(
                                    $vars['contactNotes']
                                ))
                            ) {
                                $acntContNote->addNotes($contactId, $vars);
                            }
                        }

                        if ($dbStatus) {
                            //update memcache with contact list
                            $this->updateMemcacheAction();

                            // strcasecmp() is used to compare email ids while
                            // ignoring case
                            if (strcasecmp(trim($previousEmail), trim($getParams['email']))
                            ) {
                                $this->model->setStatusAsNew(
                                    $getParams['email']
                                );
                            }

                            $this->response['response']['status'] = 'true';
                            $this->response['response']['content']['message']
                                = 'Contact updated successfully';

                            // Set campaign to None if email is not available.
                            if (empty(trim($getParams['email']))
                            ) {
                                $modelAccCampCont = new RapidFunnel_Model_AccountCampaignContact();
                                $modelAccCampCont->channel = 'Mobile';
                                $modelAccCampCont->createdBy = $this->userId;
                                $modelAccCampCont->accountCampaignId = '0';
                                $modelAccCampCont->accountContactId = $contactId;
                            }

                            //update memcache with contact list
                            $this->updateMemcacheAction();
                        }
                    } elseif (1 === $contactStatus) {
                        $this->response['response']['errorMessage']
                            = $config->contact->duplicateContact->message;
                    } elseif (false === $contactStatus) {
                        $this->response['response']['errorMessage']
                            = 'Error during the process';
                    }
                } else{
                    $this->response['response']['errorMessage'] = 'Invalid contact id';
                }
            }
        } else {
            $this->response['response']['errorMessage']
                = $updateContactForm->getMessages();
        }

        echo json_encode($this->response);
    }

    /**
     * Delete Contact and delete notes of that contact
     *
     * @return object json encode response
     * @throws Exception
     */
    public function deleteContactAction()
    {
        $contactId = $this->_request->getPost('contactId');
        $userId = $this->_request->getPost('userId');

        if (!is_numeric($contactId)) {
            $this->response['response']['errorMessage'] = 'Invalid contact id';
        } else {
            $this->model->load($contactId);
            if (isset($this->model->id) && $this->model->createdBy === $userId) {
                if ($this->model->deleteContact($contactId)) {
                //Delete all notes related to contact id
                $acntContNote
                    = new RapidFunnel_Model_AccountContactNote();
                $acntContNote->deleteNotes($contactId, null, 1);
                //update memcache with contact list
                $this->updateMemcacheAction();

                //Update the data to memcache
                $memcacheModel = new RapidFunnel_Model_Memcache();
                $memcacheModel->setApiDashboardInfo($this->userId);

                $this->response['response']['status'] = 'true';
                $this->response['response']['content']['message']
                    = 'Contact deleted successfully';
                } else {
                    $this->response['response']['errorMessage']
                    = 'Error during delete';
                }
            } else {
                $this->response['response']['errorMessage']
                    = 'You are not permitted to delete this contact';
            }
        }

        echo json_encode($this->response);
    }

    /**
     * Construct post value array for phone and email type
     *
     * @param <Array> $postValues post parameter
     * @param <Array> $getParams  contains parameter value
     *
     * @return mixed
     */
    public function assignMultiPhnEml($postValues, $getParams)
    {
        //Add key and value to post variable for saving
        //multiple phone data
        $postValues['phone'] = isset($getParams['phone']) ? $getParams['phone']
            : '';
        $postValues['home'] = isset($getParams['home']) ? $getParams['home']
            : '';
        $postValues['work'] = isset($getParams['work']) ? $getParams['work']
            : '';
        $postValues['other'] = isset($getParams['other']) ? $getParams['other']
            : '';

        //Add key and value to post variable for saving
        //multiple email data
        $postValues['email'] = trim(
            isset($getParams['email']) ? $getParams['email'] : ''
        );
        $postValues['homeEmail'] = trim(
            isset($getParams['homeEmail']) ? $getParams['homeEmail'] : ''
        );
        $postValues['workEmail'] = trim(
            isset($getParams['workEmail']) ? $getParams['workEmail'] : ''
        );
        $postValues['otherEmail'] = trim(
            isset($getParams['otherEmail']) ? $getParams['otherEmail'] : ''
        );

        return $postValues;
    }

    /**
     * Get Contact Notes for all contact number
     *
     * @return object json encoded notes
     */
    public function getContactNotesAction()
    {
        $accountRoles = RapidFunnel_Configs::_instance('accountRoles');

        //get contacts as user, even if the user is admin / manager
        $acntContNote
            = new RapidFunnel_Model_AccountContactNote();
        $contactNotes = $acntContNote->getContactNotes(
            $this->userId, $accountRoles->user
        );

        // Encode data to support utf8
        $contactNotes = $this->_helper->Common->utf8_converter($contactNotes);

        //check for contacts and return the response json
        if (!empty($contactNotes) && is_array($contactNotes)) {
            $this->response['response']['status'] = 'true';
            $this->response['response']['content']['contactNotes'] = $contactNotes;
        } else {
            $this->response['response']['errorMessage']
                = 'No contact notes found with this account';
        }

        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    /**
     * Get Contact Notes for particular contact number
     *
     * @return object json encoded notes
     */
    public function getContactNoteDetailsAction()
    {
        $contactId = $this->_request->getPost('contactId');

        if (!is_numeric($contactId)) {
            $this->response['response']['errorMessage'] = 'Invalid contact id';
        } else {
            //get contacts as user, even if the user is admin / manager
            $acntContNote
                = new RapidFunnel_Model_AccountContactNote();
            $contactNotes = $acntContNote->getNoteDetails($contactId);

            // Encode data to support utf8
            $contactNotes = $this->_helper->Common->utf8_converter($contactNotes);

            //check for contacts and return the response json
            if (!empty($contactNotes) && is_array($contactNotes)) {
                $this->response['response']['status'] = 'true';
                $this->response['response']['content']['contactNotes']
                    = $contactNotes;
            } else {
                $this->response['response']['errorMessage']
                    = 'No contact notes found with this account';
            }
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    /**
     * Add contact notes for a contact
     *
     * @return object json encoded notes
     * @throws Exception
     */
    public function addContactNotesAction()
    {
        if ($this->_request->isPost()) {
            $getParams = $this->_request->getParams();
            $vars = array();
            $acntContNote
                = new RapidFunnel_Model_AccountContactNote();

            //Ensure contactId is not empty
            if (empty($getParams['contactId'])) {
                $this->response['response']['errorMessage']
                    = 'Invalid contact id';
            } else {
                $contactId = $this->_request->getPost('contactId');
                $vars['contactNotes'] = isset($getParams['contactNotes'])
                    ? $getParams['contactNotes'] : array('');
                $vars['noteTimeStamps']
                    = isset($getParams['noteTimeStamps'])
                    ? $getParams['noteTimeStamps'] : array('');

                If (!empty(array_filter($vars['noteTimeStamps']))
                    && !empty(array_filter($vars['contactNotes']))
                ) {
                    if ($acntContNote->addNotes($contactId, $vars)) {
                        $this->response['response']['status'] = 'true';
                        $this->response['response']['content']['message']
                            = 'Notes Added successfully.';
                    } else {
                        $this->response['response']['errorMessage']
                            = 'Error during add contact notes.';
                    }
                }
            }
        } else {
            $this->response['response']['errorMessage']
                = 'Error during add contact notes.';
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);

    }

    /**
     * Update contact notes for a contact
     *
     * @return object json encoded notes
     * @throws Exception
     */
    public function updateContactNotesAction()
    {
        if ($this->_request->isPost()) {
            $getParams = $this->_request->getParams();
            $vars = array();
            $acntContNote
                = new RapidFunnel_Model_AccountContactNote();

            //Ensure contactId is not empty
            if (empty($getParams['contactId'])) {
                $this->response['response']['errorMessage']
                    = 'Invalid contact id';
            } else {
                if (empty(array_filter($getParams['noteIds']))) {
                    $this->response['response']['errorMessage']
                        = 'Invalid note ids supplied';
                } else {
                    $contactId = $this->_request->getPost('contactId');
                    $vars['noteIds'] = isset($getParams['noteIds'])
                        ? $getParams['noteIds'] : array('');
                    $vars['updateContactNotes']
                        = isset($getParams['contactNotes'])
                        ? $getParams['contactNotes'] : array('');
                    $vars['updateTimeStamps'] = isset($getParams['noteTimeStamps'])
                        ? $getParams['noteTimeStamps'] : array('');

                    If (!empty(array_filter($vars['updateTimeStamps']))
                        && !empty(array_filter($vars['updateContactNotes']))
                        && !empty(array_filter($vars['noteIds']))
                    ) {
                        $acntContNote->updateNotes($contactId, $vars);

                        if ($acntContNote->updateNotes($contactId, $vars)) {
                            $this->response['response']['status'] = 'true';
                            $this->response['response']['content']['message']
                                = 'Notes updated successfully.';
                        } else {
                            $this->response['response']['errorMessage']
                                = 'Error during update contact notes.';
                        }
                    }
                }
            }
        } else {
            $this->response['response']['errorMessage']
                = 'Error during update contact notes.';
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);

    }

    /**
     * Update contact notes for a contact
     *
     * @return object json encoded notes
     * @throws Exception
     */
    public function deleteContactNotesAction()
    {
        if ($this->_request->isPost()) {
            $getParams = $this->_request->getParams();
            $acntContNote
                = new RapidFunnel_Model_AccountContactNote();

            //Ensure contactId is not empty
            if (empty($getParams['contactId'])) {
                $this->response['response']['errorMessage']
                    = 'Invalid contact id';
            } else {
                if (empty(array_filter($getParams['noteIds']))) {
                    $this->response['response']['errorMessage']
                        = 'Invalid note ids supplied';
                } else {
                    $contactId = $this->_request->getPost('contactId');
                    $vars['deleteNoteIds'] = $getParams['noteIds'];

                    if ($acntContNote->deleteNotes($contactId, $vars)) {
                        $this->response['response']['status'] = 'true';
                        $this->response['response']['content']['message']
                            = 'Notes deleted successfully.';
                    } else {
                        $this->response['response']['errorMessage']
                            = 'Error during update contact notes.';
                    }
                }
            }
        } else {
            $this->response['response']['errorMessage']
                = 'Error during delete contact notes.';
        }
        echo json_encode($this->response, JSON_HEX_QUOT | JSON_HEX_TAG);
    }

}
