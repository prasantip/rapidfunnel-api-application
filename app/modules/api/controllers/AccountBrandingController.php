<?php

/**
 * Api_AccountBrandingController
 *
 * @category  Controller
 * @package   Api
 * @copyright 2014 Digital Assets, Inc.
 * @license   Not licensed for external use
 */
class Api_AccountBrandingController extends Zend_Controller_Action
{

    //declare the response variable
    public $response = array('response' =>
        array('status' => 'false')
    );
    //declare the variables
    public $model;
    public $userId;
    public $accessToken;
    public $accountId;

    /**
     * @function: init()
     * @purpose: setting default parameters
     * @params : nothing
     */
    public function init()
    {
        //disabled the view as response here are in json format
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //get the post data
        $this->userId = $this->_request->getPost('userId');
        //load the collateral model
        $this->model = new RapidFunnel_Model_AccountBranding();
        $userModel = new RapidFunnel_Model_AccountUser();
        $userModel->load($this->userId);
        $this->accountId = $userModel->accountId;
    }

    /**
     * @function: getDetailsAction()
     * @purpose: get branding details associated with the account
     * @param: void
     */
    public function getDetailsAction()
    {
        $resources = $this->model->getDetails(array('accountId =?' => $this->accountId));
        //check for contacts and return the response json
        if (!empty($resources) && is_array($resources)) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $baseUrl = $request->getScheme() . '://' . $request->getHttpHost();
            $config = RapidFunnel_Configs::_instance();
            $path = $config->uploadPath->branding->account . '/' . $this->accountId;

            //set the mobile logo url
            if (isset($resources['mobileLogo']) && !empty($resources['mobileLogo'])) {
                $resources['mobileLogoPath'] = $path . '/' . $resources['mobileLogo'];
                $resources['mobileLogoUrl'] = $baseUrl . '/' . $resources['mobileLogoPath'];
            }

            //set the dashboard logo url
            if (isset($resources['dashboardLogo']) && !empty($resources['dashboardLogo'])) {
                $resources['dashboardLogoPath'] = $path . '/' . $resources['dashboardLogo'];
                $resources['dashboardLogoUrl'] = $baseUrl . '/' . $resources['dashboardLogoPath'];
            }

            $resources['colorScheme'] = (isset($resources['colorScheme']) && !empty($resources['colorScheme']))
                        ? $resources['colorScheme'] : '';
            $this->response['response']['status'] = 'true';
            $this->response['response']['content'] = $resources;
        } else {
            $this->response['response']['errorMessage'] = 'No record found with this account';
        }
        echo json_encode($this->response);
    }

}
