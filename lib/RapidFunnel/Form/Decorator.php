<?php

/**
 * Decorate form element
 *
 * @category  Form
 * @package   Form
 * @author    rapid funnel <support@rapidfunnel.com>
 * @copyright 2014 RapidFunnel, Inc.
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com
 */
class InputDecorator extends Zend_Form_Decorator_Abstract
{
    protected $_format
        = '<div class="form-group">
        <input id="%s" name="%s" type="%s" class="form-input" placeholder="%s"/><i class="%s"></i></div>';

    /**
     * Return zend form element with custom format
     *
     * @param $content
     * @return string
     */
    public function render($content)
    {
        $element = $this->getElement();
        $name = htmlentities($element->getFullyQualifiedName());
        $id = htmlentities($element->getId());
        $type = htmlentities($element->getAttrib('type'));
        $placeholder = htmlentities($element->getAttrib('placeholder'));
        $glyphIcon = htmlentities($element->getAttrib('fa-icon'));

        $markup = sprintf(
            $this->_format, $id, $name, $type, $placeholder, $glyphIcon
        );

        return $markup;
    }
}