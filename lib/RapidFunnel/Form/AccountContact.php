<?php

/**
 * RapidFunnel_Form_AccountContact
 *
 * For adding an account contact
 *
 * @category  Form
 * @package RapidFunnel_Form
 * @copyright 2012 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Form_AccountContact extends RapidFunnel_Form_Abstract
{

    private $__campaign = null;

    public function __construct($accountId = null, $userId = null,
            $roleId = null, $groupIds = null, $freeUser = false, $isApi = null)
    {
        if(!$isApi) {
            $campaignModel = new RapidFunnel_Model_AccountCampaign();
            $campaignAll = $campaignModel->getAllCampaignsToSend(
                $accountId,
                $userId, $roleId, $groupIds, $freeUser
            );

            $campaignOptionArray = array();
            if (is_array($campaignAll)) {
                foreach ($campaignAll as $campaign) {
                    $campaignOptionArray[$campaign['id']] = $campaign['name'];
                }
            }
            $campaignOptionArray['0'] = 'None';
            $this->__campaign = $campaignOptionArray;
        }
            $this->init($isApi);
    }

    /**
     * function init()
     * @purpose Initial set ups for the functionality
     * @param none
     * @return none
     */
    public function init($isApi)
    {

        parent::init();

        $this->addElementPrefixPath(
                'RapidFunnel_Validate_', APP_LIB . '/RapidFunnel/Validate/',
                'validate'
        );


        $noteDecorator = array(
            array('ViewHelper'), array(
                'Errors', array(
                    'placement' => 'append', 'class' => 'alert-danger',
                    'style' => 'list-style: none; padding-left: 10px;'
                )
            ), array('HtmlTag', array('tag' => 'div', 'class' => 'col-sm-7')),
            array(
                'Label', array(
                'placement' => 'prepend', 'class' => 'control-label col-sm-3',
                'escape' => false,
                'requiredSuffix' => '<span class="warn">*</span>'
            )
            ), array(
                'Description',
                array('escape' => false, 'class' => 'description')
            )
        );

        //Adding for first name field
        $this->addElement('text', 'firstName',
                array(
            'required' => true,
            'class' => 'form-control',
            'label' => 'First Name',
            'validators' => array(
                array('NotEmpty', true,
                    array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'First Name is required')))
            )
        ));

        //Adding for last name field
        $this->addElement('text', 'lastName',
                array(
            'required' => false,
            'class' => 'form-control',
            'label' => 'Last Name',
        ));

        $this->addElement('text', 'address1',
                array(
            'required' => false,
            'class' => 'form-control',
            'label' => 'Address 1'
        ));

        $this->addElement('text', 'address2',
                array(
            'required' => false,
            'class' => 'form-control',
            'label' => 'Address 2'
        ));

        $this->addElement('text', 'city',
                array(
            'required' => false,
            'class' => 'form-control',
            'label' => 'City'
        ));

        $countryList = $this->getOptions(
            RapidFunnel_Data_Source::COUNTRIES, null, null,
            array('value' => 'id', 'display' => 'name')
        );
        $countryList = array_merge(array(0 => ' None '), $countryList);
        //To add other option
        $this->addElement('select', 'country',
            array(
                'class' => 'form-control',
                'label' => 'Country',
                'onchange' => 'changeStateList()',
                'multiOptions' => $countryList
            ));
        $this->addElement(
            'hidden', 'stateId',
            array('decorators' => array(array('ViewHelper')))
        );
        $this->addElement('select', 'state',
            array(
                'onchange' => 'updateState()',
                'registerInArrayValidator' => false,
                'class' => 'form-control',
                'label' => 'State or Province'
            )
        );

        $this->addElement('text', 'zip',
                array(
            'required' => false,
            'maxlength' => 10,
            'class' => 'form-control ',
            'label' => 'Postal Code',
            'validators' => array(
                array('ZipCode'),
                array('NotEmpty', false, array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Required'))),
                array('StringLength', false, array(0, 10, 'messages' => array(Zend_Validate_StringLength::TOO_LONG => 'Max length should be 10')))
            )
        ));

        $this->addElement('text', 'company',
                array(
            'required' => false,
            'class' => 'form-control',
            'label' => 'Company Name'
        ));

        $this->addElement('text', 'title',
                array(
            'required' => false,
            'class' => 'form-control',
            'label' => 'Title'
        ));

        $this->addElement('select', 'interest',
                array(
            'required' => false,
            'class' => 'form-control',
            'label' => 'Interest',
            'multiOptions' => array('1' => 'low', '2' => 'medium', '3' => 'high')
        ));

        $this->addElement(
            'textarea', 'note', array(
                'required' => false, 'class' => 'form-control',
                'label' => 'Note', 'rows' => '5',
                'decorators' => $noteDecorator, 'ignore' => true
            )
        );

        if(!$isApi) {
            if ('' != $this->__campaign && !empty($this->__campaign)) {
                $this->addElement(
                    'select', 'campaign',
                    array(
                        'class'        => 'form-control',
                        'label'        => 'Assign / Change Campaign',
                        'multiOptions' => $this->__campaign,
                        'value'        => '0',
                        'validators'   => array(
                            array('NotEmpty', false,
                                  array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Required')))
                        )
                    )
                );
            }

            if ('' != $this->__campaign && !empty($this->__campaign)) {
                $this->addElement(
                    'text', 'campaignAlias',
                    array('class'   => 'form-control', 'label' => 'Campaign',
                          'value'   => 'Please enter a valid primary email address to assign a campaign.',
                          'title'   => 'Please enter a valid primary email address to assign a campaign.',
                          'attribs' => array('disabled' => 'disabled'),
                          'ignore'  => true
                    )
                );
            }
        }

        //submit button
        $this->addElement('button', 'submit',
                array(
            'type' => 'submit',
            'label' => 'Submit',
            'class' => 'btn btn-primary',
            'decorators' => array(array('ViewHelper'))
        ));
    }

}
