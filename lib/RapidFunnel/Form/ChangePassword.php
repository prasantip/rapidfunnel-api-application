<?php

/**
 * RapidFunnel_Form_ChangePassword
 *
 * @category  Form
 * @package RapidFunnel_Form
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Form_ChangePassword extends RapidFunnel_Form_Abstract
{

    //variables decalration
    public $defaultTextFieldDecorator;

    /**
     * function init()
     * @purpose Initial set ups for the functionality
     * @param none
     * @return none
     */
    public function init()
    {

        parent::init();

        $this->addElementPrefixPath(
                'RapidFunnel_Validate_', APP_LIB . '/RapidFunnel/Validate/',
                'validate'
        );

        //Default decorators for text field
        $this->defaultTextFieldDecorator = array(
            array('ViewHelper'),
            array('Errors', array('placement' => 'append', 'class' => 'alert-danger',
                    'style' => 'list-style: none; padding-left: 10px;')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'control-label col-sm-2',
                    'escape' => false, 'requiredSuffix' => '<span class="warn">*</span>')),
            array('Description', array('escape' => false, 'class' => 'description'))
        );

        $this->addElement('password', 'oldPassword',
                array(
            'required' => true,
            'class' => 'form-control ',
            'label' => 'Existing Password',
            'decorators' => $this->defaultTextFieldDecorator,
            'validators' => array(
                array('NotEmpty', false, array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Required')))
            )
        ));

        $this->addElement('password', 'password',
                array(
            'required' => true,
            'class' => 'form-control ',
            'label' => 'New Password',
            'decorators' => $this->defaultTextFieldDecorator,
            'validators' => array(
                array('NotEmpty', false, array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Required'))),
                array('PasswordStrength')
            )
        ));

        $this->addElement('password', 'confirmPassword',
                array(
            'required' => true,
            'class' => 'form-control',
            'label' => 'Confirm New Password',
            'decorators' => $this->defaultTextFieldDecorator,
            'validators' => array(
                array('Identical', false, array('token' => 'password', 'messages' => array(
                            Zend_Validate_Identical::NOT_SAME => 'Password didn\'t match. Please try again.',
                            Zend_Validate_Identical::MISSING_TOKEN => 'No Password was provided to match against')))
            )
        ));

        //submit button
        $this->addElement('button', 'changePassword',
                array(
            'type' => 'submit',
            'label' => 'Change Password',
            'class' => 'btn btn-primary',
            'decorators' => array(array('ViewHelper'))
        ));
    }

}
