<?php

/**
 * RapidFunnel_Form_Login
 *
 * @category  Form
 * @package RapidFunnel_Form
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 */
require_once 'Decorator.php';

class RapidFunnel_Form_Login extends RapidFunnel_Form_Abstract
{

    //variables declaration
    public $defaultTextFieldDecorator;

    /**
     * function init()
     * @purpose Initial set ups for the functionality
     * @param none
     */
    public function init()
    {
        parent::init();

        $inputDecorator = new InputDecorator();
        //Default decorators for text field
        $this->defaultTextFieldDecorator = array(
            array('ViewHelper'), array(
                'Label', array(
                    'placement' => 'prepend', 'class' => 'control-label',
                    'escape' => false,
                    'requiredSuffix' => '<span class="warn">*</span>'
                )
            ), array(
                'Errors', array(
                    'placement' => 'append', 'class' => 'alert-danger',
                    'style' => 'list-style: none; padding-left: 10px;'
                )
            ), array(
                'Description',
                array('escape' => false, 'class' => 'description')
            )
        );

        //Adding for email field
        $this->addElement(
            'text', 'username', array(
                'required' => true, 'placeholder' => 'Enter email',
                'fa-icon' => 'fa fa-envelope-o loginIcon',
                'decorators' => array($inputDecorator),
                'type' => 'text',
                'validators' => array(
                    array(
                        'NotEmpty', true,
                        array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Email is Required'))
                    )
                )
            )
        );

        //Adding for email field
        $this->addElement(
            'password', 'password', array(
                'required' => true, 'placeholder' => 'Password',
                'fa-icon' => 'fa fa-eye-slash loginIcon',
                'type' => 'password',
                'decorators' => array($inputDecorator), 'validators' => array(
                    array(
                        'NotEmpty', false,
                        array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Password is Required'))
                    )
                )
            )
        );

        //submit button
        $this->addElement(
            'button', 'sign_in', array(
                'type' => 'submit', 'label' => 'Login',
                'class' => 'btn loginBtn',
                'decorators' => array(array('ViewHelper'))
            )
        );
    }

}
