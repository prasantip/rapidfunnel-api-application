<?php

/**
 * This basically blows away any default formatting
 * and also implements a getValues method.
 *
 * The get values is an override of Zend_Forms get values
 * which allows you to exclude form items if necessary.
 *
 * RapidFunnel_Form_Abstract
 * RapidFunnel/Form/Abstract.php
 *
 * @category  Form
 * @package   RapidFunnel_Form
 * @copyright 2012 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Form_Abstract extends Zend_Form
{

    /**
     * these are the standard default decorators
     * and this is usually set in the extending class if desired
     * you can set your own for each class or just use these
     *
     * @var array $defaultDecorataor
     */
    protected $defaultDecorator = array(
        array('ViewHelper'),
        array('Errors', array('placement' => 'append', 'class' => 'alert-danger',
                'style' => 'list-style: none; padding-left: 10px;')),
        array('HtmlTag', array('tag' => 'div', 'class' => 'col-sm-9')),
        array('Label', array('placement' => 'prepend', 'class' => 'control-label col-sm-3',
                'escape' => false, 'requiredSuffix' => '<span class="warn">*</span>')),
        array('Description', array('escape' => false, 'class' => 'description'))
    );

    public $defaultDateFieldDecorator = array(
        array('ViewHelper'),
        array('Errors', array('placement' => 'append', 'class' => 'alert-danger',
                'style' => 'list-style: none; padding-left: 10px;')),
        array('HtmlTag', array('tag' => 'div', 'class' => 'col-sm-4')),
        array('Label', array('placement' => 'prepend', 'class' => 'control-label col-sm-3',
                'escape' => false, 'requiredSuffix' => '<span class="warn">*</span>')),
        array('Description', array('escape' => false, 'class' => 'description'))
    );
    public $defaultMultiCheckDecorator = array(
            array('ViewHelper'),

            array('HtmlTag', array('tag' => 'li', 'class' => 'col-sm-6')),
            array(
                'decorator' => array('LiTag' => 'HtmlTag'),
                'options' => array('tag' => 'ul', 'class' => 'multi-check')),
            array('Errors', array('placement' => 'append', 'class' => 'alert-danger',
                    'style' => 'list-style: none; padding-left: 10px;')),
            array('Description', array('escape' => false, 'class' => 'description'))
        );
    public $defaultFileDecorator = array(
        'File',
        array('Errors', array('placement' => 'append', 'class' => 'alert-danger',
                'style' => 'list-style: none; padding-left: 10px;')),
        array('Description', array('escape' => false, 'class' => 'description')),
        array('HtmlTag', array('tag' => 'div', 'class' => 'col-lg-9')),
        array('Label', array('placement' => 'prepend', 'class' => 'control-label col-sm-3'))
    );

    public function init()
    {
        // set form method POST
        $this->setMethod('post');

        $this->setElementDecorators($this->defaultDecorator);
    }

    public function getValues($names = array())
    {
        $data = parent::getValues();
        foreach ($names as $val) {
            unset($data[$val]);
        }
        return $data;
    }


    /**
     *It returns the multi-options array from the specified source
     *
     * @param string $source string from data source class
     * @param string | null $default string that will be the
     *                      selection drop down default
     * @param array | null $where array('id = ?' => $Id, 'name != ?' => $name)
     * @param array | null $optionDbField array( 'value' => 'id',
     *                                    'display' => array(
     *                                     array('', 'name', '-'),
     *                                     ...
     *                                     array('[', 'id', ']')) );
     *                                     OR
     *                                     array('value' => 'id',
     *                                     'display' => 'name');
     * @param array | null $order orderBy clause
     *
     * @return array
     */
    public function getOptions(
        $source, $default = null, $where = null, $optionDbField = null,
        $order = null
    ) {

        $source = new $source();
        $select = $source->select();
        $valueStr = 'value';
        $displayStr = 'display';

        if ($where !== null) {
            foreach ($where as $field => $val) {
                $select->where($field, $val);
            }
            // to make condition for order by
            if ($order != null) {
                $select->order($order);
            }
            $sourceResult = $source->fetchAll($select);
        } else {
            // to make condition for order by
            if ($order != null) {
                if (is_array($order)) {
                    $select->order($order['column'], $order['orderBy']);
                } else {
                    $select->order($order);
                }
            }
            $sourceResult = $source->fetchAll($select);
        }

        //add the default display item if it's passed
        if ($default != null && is_array($default)) {
            $returnData[$default[$valueStr]] = $default[$displayStr];
        }

        //apply everything else
        if ($optionDbField != null && is_array($optionDbField)) {
            // if display field is array, make display part with multiple fields
            if (is_array($optionDbField[$displayStr])) {
                $displayArr = $optionDbField[$displayStr];

                foreach ($sourceResult as $item) {
                    $displayPart = '';
                    foreach ($displayArr as $eachDisplay) {
                        $displayPart .= $eachDisplay[0] . $item->$eachDisplay[1]
                            . $eachDisplay[2];
                    }
                    $returnData[$item->$optionDbField[$valueStr]] = $displayPart;
                }
            } else {
                foreach ($sourceResult as $item) {
                    $returnData[$item->$optionDbField[$valueStr]]
                        = $item->$optionDbField[$displayStr];
                }
            }
        } else {
            foreach ($sourceResult as $item) {
                if (!isset($item->name)) {
                    $returnData[$item->id] = $item->firstName . ' '
                        . $item->lastName;
                } else {
                    $returnData[$item->id] = $item->name;
                }
            }
        }

        return ((isset($returnData) && is_array($returnData))
            ? $returnData : array());
    }

}