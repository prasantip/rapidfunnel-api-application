<?php

/**
 * RapidFunnel_Form_NewUserSetPassword
 *
 * @category  Form
 * @package RapidFunnel_Form
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Form_NewUserSetPassword extends RapidFunnel_Form_Abstract
{

    //variables decalration
    public $defaultTextFieldDecorator;

    /**
     * function init()
     * @purpose Initial set ups for the functionality
     * @param none
     * @return none
     */
    public function init()
    {

        parent::init();

        //Default decorators for text field
        $this->defaultTextFieldDecorator = array(
            array('ViewHelper'),
            array('Label', array('placement' => 'prepend', 'class' => 'control-label',
                    'escape' => false, 'requiredSuffix' => '<span class="warn">*</span>')),
            array('Errors', array('placement' => 'append', 'class' => 'alert-danger',
                    'style' => 'list-style: none; padding-left: 10px;')),
            array('Description', array('escape' => false, 'class' => 'description'))
        );

        $this->addElement('text', 'email',
                array(
            'required' => true,
            'class' => 'form-control',
            'label' => 'Email address',
            'validators' => array(
                array('NotEmpty', true,
                    array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Email is Required'))),
                array('EmailAddress')
            )
        ));

        //Adding for first name field
        $this->addElement('text', 'registrationCode',
                array(
            'required' => true,
            'class' => 'form-control',
            'label' => 'Registration Code',
            'validators' => array(
                array('NotEmpty', true,
                    array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Registration Code is required')))
            )
        ));

       $this->addElement('password', 'password',
                array(
            'required' => true,
            'class' => 'form-control ',
            'label' => 'Password',
            'validators' => array(
                array('NotEmpty', false, array('messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => 'Required')))
            )
        ));

        $this->addElement('password', 'confirmPassword',
                array(
            'required' => true,
            'class' => 'form-control',
            'label' => 'Confirm Password',
            'validators' => array(
                array('Identical', false, array('token' => 'password'))
            )
        ));

        //submit button
        $this->addElement('button', 'setPassword',
                array(
            'type' => 'submit',
            'label' => 'Set Password',
            'class' => 'btn btn-primary',
            'decorators' => array(array('ViewHelper'))
        ));
    }

}
