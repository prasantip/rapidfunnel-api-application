<?php

/**
 * RapidFunnel_Validate_ZipCode
 *
 * @category  Validator
 * @package RapidFunnel_Validate
 * @copyright 2015 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Validate_ZipCode extends Zend_Validate_Abstract
{

    const NOT_MATCH = 'notMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Only numeric, space, character and "-" are allowed.'
    );

    public function isValid($value)
    {
        $value = (string) $value;
        $this->_setValue($value);

        // check other criteria using regexp
        // must be atleast one number and minimum 8 character long
        $regMatchStr = '/^[a-zA-Z0-9- ]+$/';
        if (!preg_match($regMatchStr, $value)) {
            $this->_error(self::NOT_MATCH);
            return false;
        }

        return true;
    }

}
