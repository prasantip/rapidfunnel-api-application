<?php

/**
 * RapidFunnel_Validate_EmailAddress
 *
 * @category  Validator
 * @package RapidFunnel_Validate
 * @copyright 2015 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Validate_EmailAddress extends Zend_Validate_Abstract
{

    const NOT_MATCH = 'notMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Please enter valid email address'
    );

    public function isValid($value)
    {
        $value = (string) $value;
        $this->_setValue($value);

        // check other criteria using regexp
        // must be atleast one number and minimum 8 character long
        $regMatchStr = '/^( )*([\w+-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w+-]{0,66})\.([a-z0-9]{2,}(?:\.[a-z]{2})?)( )*$/';
        if (!preg_match($regMatchStr, $value)) {
            $this->_error(self::NOT_MATCH);
            return false;
        }

        return true;
    }

}
