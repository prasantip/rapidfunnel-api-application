<?php

/**
 * RapidFunnel_Validate_PasswordStrength
 *
 * @category  Validator
 * @package RapidFunnel_Validate
 * @copyright 2012 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Validate_PasswordStrength extends Zend_Validate_Abstract
{

    const NOT_MATCH = 'notMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Password must be a minimum of 6 characters long.'
    );

    public function isValid($value)
    {
        $value = (string) $value;
        $this->_setValue($value);

        // must be minimum 6 character long
        $regMatchStr = '/^.{6,}$/';
        if (!preg_match($regMatchStr, $value)) {
            $this->_error(self::NOT_MATCH);
            return false;
        }

        return true;
    }

}
