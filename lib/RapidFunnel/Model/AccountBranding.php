<?php
/**
 * RapidFunnel_Model_AccountBranding
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/branding
 */

/**
 * RapidFunnel_Model_AccountBranding
 * RapidFunnel/Model/AccountBranding.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/branding
 */
class RapidFunnel_Model_AccountBranding extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountId;
    protected $dashboardLogo = '';
    protected $mobileLogo = '';
    protected $isShowColorToMobile = 0;
    protected $primaryColor;
    protected $primaryColorOffset;
    protected $secondaryColor;
    protected $secondaryColorOffset;
    protected $tertiaryColor;
    protected $tertiaryColorOffset;
    protected $domainName;
    protected $supportLink;
    protected $reBrand;
    protected $reBrandAdditionalNotificationEmail;
    protected $reBrandRepIdToolTip;
    protected $reBrandAdditionalNotificationEmailToolTip;
    protected $reBrandRepId2;
    protected $reBrandRepId2ToolTip;
    protected $upgradeInformationForFreeUsers;

    /**
     * Constructor method for the RapidFunnel_Model_AccountBranding class.
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the  database by
     * setting the appropriate table, database adapter.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountBranding());
        $this->db = $this->table->getAdapter();
    }

    /**
     * RapidFunnel_Model_AccountBranding::getDetails()
     *
     * Functionality to get details of account branding
     *
     * @param array $conditions is an array of condition of an unique accountId
     *                          provided when user account is created in a group
     *
     *
     * @return array all the accountBranding details
     */
    public function getDetails($conditions)
    {
        try {
            $select = $this->db->select()
                ->from(array('accountBranding'));

            if (is_array($conditions) && !empty($conditions)) {
                foreach ($conditions as $key => $val) {
                    $select->where($key, $val);
                }
            }

            $stmt = $this->db->query($select);
            return $stmt->fetch();
        } catch (Exception $zde) {
            error_log($zde->getMessage());
            $this->setMessage($zde->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountBranding::getBrandingDetails()
     *
     * Functionality to get profile details details of the user
     * To populate details of the user when the user clicked on edit profile
     *
     * @param int $accountId is an unique id provided when user account
     *                       is created in a group
     *
     * @return array all the accountBranding details
     */
    public function getBrandingDetails($accountId)
    {
        try {
            $select = $this->db->select()
                ->from(array('accountBranding'))
                ->where('accountId =?', $accountId);

            $row = $this->db->fetchRow($select);

            if (!empty($row)) {
                return $row;
            } else {
                return RapidFunnel_Configs::_instance('branding')->ToArray();
            }

        } catch (Exception $zde) {
            error_log($zde->getMessage());
            $this->setMessage($zde->getMessage());
            return false;
        }
    }

}
