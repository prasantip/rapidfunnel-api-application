<?php
/**
 * RapidFunnel_Model_Abstract File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_Abstract
 * RapidFunnel/Model/Abstract.php
 *
 * Abstract enforces encapsulation through the
 * __set() and __get() php methods
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
abstract class RapidFunnel_Model_Abstract
{

    private $reflect;
    private $table;
    private $isLoaded = false;
    private $message;
    private $record;  //the current 'active' record.

    /**
     * RapidFunnel_Model_Abstract::__construct()
     *
     * Instantiates the parent class by calling its constructor method.
     */
    public function __construct()
    {
        $this->reflect = new ReflectionClass($this);
    }

    /**
     * RapidFunnel_Model_Abstract::save()
     *
     * This function is used to save a model to the database.
     *
     * @return boolean
     */
    public function save()
    {

        foreach ($this->reflect->getProperties(ReflectionProperty::IS_PROTECTED) as $property) {
            $property->setAccessible(true);
            $saveData[$property->getName()] = $property->getValue($this);
        }

        try {
            $this->id = $this->table->createRow($saveData)->save();
            $this->message = array(
                'type' => 'alert-success',
                'string' => '<strong>Save complete</strong>'
            );
            $this->isLoaded = true;
            return true;
        } catch (Zend_Db_Exception $zde) {
            error_log($zde->getMessage());
            $this->message = $zde->getMessage();
            return false;
        }
    }

    /**
     * This function is used to Load an object with the particular data
     * for the passed id
     *
     * @param mixed $value value of field to load
     * @param array $keys array of keys to fetch data
     * @param string $field field name to fetch data from table
     *
     * @return boolean
     * @throws exception
     */
    public function load($value, $keys = null, $field = null)
    {
        if (empty($value)) {
            throw new InvalidArgumentException('Invalid value supplied.');
        }

        if (!is_array($keys) && !empty($keys)) {
            throw new InvalidArgumentException('Invalid keys supplied.');
        }

        if (!is_string($field) && !empty($field)) {
            throw new InvalidArgumentException('Invalid field supplied.');
        }

        try {
            $this->isLoaded = false;

            if (!$keys) {
                $keys = '*';
            }

            if (!$field) {
                $field = 'id';
            }

            $select = $this->table->select()
                ->from($this->table, ($keys))
                ->where($field . ' = ?', $value);

            //get data
            $this->record = $this->table->fetchRow($select);

            if ('' != ($this->record)) {
                //load the instance vars
                foreach (
                    $this->reflect->getProperties(
                        ReflectionProperty::IS_PROTECTED
                    ) as
                    $property
                ) {
                    if ('*' === $keys or in_array($property->name, $keys)) {
                        $property->setAccessible(true);
                        $property->setValue(
                            $this,
                            is_null($this->record->{$property->getName()}) ? ''
                                : $this->record->{$property->getName()}
                        );
                    }
                }
                $this->isLoaded = true;
            }

            return $this->isLoaded;
        } catch (Zend_Db_Exception $zde) {
            error_log($zde->getMessage());
            $this->message = $zde->getMessage();
            return false;
        }
    }

    /**
     * RapidFunnel_Model_Abstract::update()
     *
     * This function is used to update a model in the database
     *
     * @return boolean
     */
    public function update()
    {
        if ($this->isLoaded) {

            foreach ($this->reflect->getProperties(ReflectionProperty::IS_PROTECTED) as $property) {
                $property->setAccessible(true);
                $updateData[$property->getName()] = $property->getValue($this);
            }

            try {
                $this->table->update($updateData, $this->getWhere($this->id));
                $this->message = 'Update successful';
                return true;
            } catch (Zend_Db_Exception $zde) {
                error_log($zde->getMessage());
                $this->message = $zde->getMessage();
                return false;
            }
        } else {
            $this->message = 'Model must be loaded before this operation';
            return false;
        }
    }

    /**
     * RapidFunnel_Model_Abstract::remove()
     *
     * This function is used to remove a model from the database.
     *
     * @return bool
     * @throws Exception
     */
    public function remove()
    {
        if ($this->isLoaded) {
            try {
                $this->table->delete($this->getWhere($this->id));
                $this->message = 'Successfully Deleted';
                return true;
            } catch (Zend_Db_Exception $zde) {
                error_log($zde->getMessage());
                $this->message = $zde->getMessage();
                return false;
            }
        } else {
            throw new Exception(
                'Model must be loaded before calling '
                . __METHOD__ . ' in ' . $this->reflect->getName()
            );
        }
    }

    /**
     * RapidFunnel_Model_Abstract::setTable()
     *
     * Set the table to be associated with
     * this model
     *
     * @param Zend_Db_Table_Abstract $table table to be set
     *
     * @return void
     */
    protected function setTable(Zend_Db_Table_Abstract $table)
    {
        $this->table = $table;
    }

    /**
     * RapidFunnel_Model_Abstract::getTable()
     *
     * Get handle to the table associated
     * with this model.
     *
     * @return mixed Zend_Db_Table_Abstract
     */
    protected function getTable()
    {
        return $this->table;
    }

    /**
     * RapidFunnel_Model_Abstract::isLoaded()
     *
     * Accessor - has the model been loaded
     * already?
     *
     * @return boolean
     */
    protected function isLoaded()
    {
        return $this->isLoaded;
    }

    /**
     * RapidFunnel_Model_Abstract::getMessage()
     *
     * Access for the message stored
     * in this model
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * RapidFunnel_Model_Abstract::setMessage()
     *
     * Access to set the current models
     * message
     *
     * @param string $message message to be set
     *
     * @return void
     */
    protected function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * RapidFunnel_Model_Abstract::getWhere()
     *
     * Utility method to return a standard where
     * clause that includes the record id
     *
     * NOTE: This is left over from a multi-tenant system
     * This may no longer be necessary and could be factored out
     * in this application
     *
     * @param int $id record id
     *
     * @return array
     */
    protected function getWhere($id)
    {
        return array('id = ?' => $id);
    }

    /**
     * RapidFunnel_Model_Abstract::__set()
     *
     * The __set magic method implementaion
     * used to enforce encapsulation.
     * If the var is not declared.  You cannot set it.
     *
     * @param string $name the class property name to set
     * @param mixed $value the value to set the class property to.
     *
     * @return bool
     * @throws Exception
     */
    public function __set($name, $value)
    {
        $isProperty = false;

        foreach ($this->reflect->getProperties() as $property) {
            if ($property->getName() == $name) {
                $this->{$name} = $value;
                $isProperty = true;
                break;
            }
        }

        if (!$isProperty) {
            throw new Exception(
                'You cannot set an undeclared property "'
                . $name . '" in ' . $this->reflect->getName()
                . ' called from ' . __METHOD__
            );
        } else {
            return true;
        }
    }

    /**
     * RapidFunnel_Model_Abstract::__get()
     *
     * The __get magic method implementation
     * used to enforce encapsulation
     * if the var/property does not exist throw an exception
     * when trying retrieve it.
     *
     * @param string $name property name to return
     *
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        if (!isset($this->{$name})) {
            throw new Exception(
                'Undefined property "' . $name . '" in '
                . $this->reflect->getName() . ' called from ' . __METHOD__
            );
        } else {
            return $this->{$name};
        }
    }

    /**
     * RapidFunnel_Model_Abstract::__isset()
     *
     * The __isset magic method implementiaton
     * used to enforce encapsulation
     * if the var/property does not exist throw an exception
     * when trying retrieve it.
     *
     * @param string $name property name to return
     *
     * @return bool
     * @throws Exception
     */
    public function __isset($name)
    {
        if (isset($this->{$name})) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * RapidFunnel_Model_Abstract::useNewSlaveDBConnection
     *
     * Functionality to use new slave DB connection adapter
     * with this model.
     *
     * @return object(new connection object)
     */
    public function useNewSlaveDBConnection()
    {
        $slaveDb = Zend_Registry::get('slaveDb');
        $slaveDb->closeConnection();
        $slaveDb->getConnection();
        Zend_Db_Table_Abstract::setDefaultAdapter($slaveDb);

        return Zend_Db_Table_Abstract::getDefaultAdapter();

    }

}
