<?php
/**
 * RapidFunnel_Model_AccountUserGroup File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountUserGroup
 * RapidFunnel/Model/AccountUserGroup.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountUserGroup extends RapidFunnel_Model_Abstract
{

    public $db;
    public $dtHelper;

    public function __construct()
    {
        /**
         * Constructor method for the RapidFunnel_Model_AccountUserGroup
         *
         * Instantiates the parent class by calling its constructor method. Also,
         * configures the model for interactions with the database by setting the
         * appropriate table, database adapter, query helper.
         *
         */
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountUserGroup());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
    }

    /**
     * RapidFunnel_Model_AccountUserGroup::getGroupByUserId()
     *
     * Gets the group to which a user is associated.
     *
     * @param int $userId user id
     *
     * @return bool / array
     */
    public function getGroupByUserId($userId)
    {
        try {
            $select = $this->db->select()
                    ->from('accountUserGroup',
                            array('accountGroupId, GetGroupFamily(accountGroupId) AS childs'))
                    ->where('accountUserId = ?', $userId);

            $stmt = $this->db->query($select);
            $row = $stmt->fetchAll();

            return $row;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

}
