<?php
/**
 * RapidFunnel_Model_AccountUserCardProfile
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/admin
 */

/**
 * RapidFunnel_Model_AccountUserCardProfile
 * RapidFunnel/Model/AccountUserCardProfile.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/admin
 */
class RapidFunnel_Model_AccountUserCardProfile extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountId;
    protected $userId;
    protected $firstName;
    protected $middleName;
    protected $lastName;
    protected $email;
    protected $address;
    protected $suite;
    protected $city;
    protected $stateId;
    protected $countryId;
    protected $zip;
    protected $maskedCcNo;
    protected $ccType;
    protected $ccExpMonth;
    protected $ccExpYear;
    protected $maskedCvv;
    protected $authorizeProfileToken;
    protected $authorizePaymentProfileToken;
    protected $created;
    protected $createdBy;
    protected $modified;
    protected $modifiedBy;
    protected $firstCycleStartDate;
    protected $lastPaymentDoneOn;
    protected $nextPaymentOn;
    protected $failureCount;
    protected $lastAmountPaid;
    protected $lastTransactionId;

    /**
     * Constructor method for the RapidFunnel_Model_AccountCardProfile
     *
     * Instantiates the parent class by calling its constructor method. Also,
     * configures the model for interactions with the database by setting the
     * appropriate table, database adapter, Account and AccountUser models.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountUserCardProfile());
        $this->db = $this->table->getAdapter();

    }

    /**
     * RapidFunnel_Model_AccountUserCardProfile::getDetailByUserId()
     *
     * Functionality to get card profile detail by user id
     *
     * @param int $userId used to get user card details
     *
     * @return array with user card details
     */
    public function getDetailByUserId($userId)
    {
        try {
            $select = $this->db->select()
                    ->from(array('aucp' => 'accountUserCardProfile'))
                    ->joinInner(array('au' => 'accountUser'),
                            'au.id = aucp.userId',
                            array('userStatus' => 'status'))
                    ->joinInner(array('a' => 'account'),
                            'a.id = aucp.accountId', array('passThrough'))
                    ->where('aucp.userId = ?', $userId);

            $stmt = $this->db->query($select);
            return $stmt->fetch();
        } catch (Exception $zde) {
            error_log($zde->getMessage());
            $this->setMessage($zde->getMessage());
            return false;
        }
    }

}
