<?php
/**
 * RapidFunnel_Model_Memcache
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2016 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/admin
 */

/**
 * RapidFunnel_Model_Memcache
 * RapidFunnel/Model/Memcache.php
 *
 * This model will communicate with Memcache server using Memcache library
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @copyright 2016 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Model_Memcache
{

    public $memcache;
    public $commonHelper;

    /**
     * For default initializations
     */
    public function __construct()
    {
        $this->memcache = new RapidFunnel_Memcache_Memcached();
        $this->commonHelper = new RapidFunnel_Controller_Action_Helper_Common();
    }

    /**
     * Functionality to authenticate users
     *
     * @param string $email    user's email
     * @param string $password password of user
     * @param string $token    view token
     * @param string $type     type of login
     *
     * @return array / boolean $users info as json string
     */
    public function authUser($email, $password, $token = '', $type = 'apiLogin')
    {
        $response = false;
        try {
            $config = RapidFunnel_Configs::_instance();
            $passwordLength = $config->accountUser->passwordLength;
            //generate key from user info
            $dataKey = $this->commonHelper->generateKey(
                $type, null, $email
            );
            $loginInfo =  $this->memcache->getItem($dataKey);

            if ((isset($loginInfo['userInfo'])
                    && !empty($loginInfo['userInfo']))
                && (isset($loginInfo['response'])
                    && !empty($loginInfo['response']))
            ) {
                //Validating by webViewToken
                if (!empty($token)
                    && ($token === $loginInfo['userInfo']['webViewToken'])
                ) {
                    $response = $loginInfo['response'];
                } else if (strlen($loginInfo['userInfo']['password'])
                    == $passwordLength
                ) {
                    //Validating using password
                    $md5Password = hash(
                        'sha512',
                        $password . $loginInfo['userInfo']['salt']
                    );
                } else {
                    $md5Password = md5(
                        $password.$loginInfo['userInfo']['salt']
                    );
                }
                if (isset($md5Password)
                    && $md5Password === $loginInfo['userInfo']['password']
                ) {
                    $response = $loginInfo['response'];
                }
            }
            return $response;

        } catch(Exception $mce) {
            error_log($mce->getMessage());
            return false;
        }
    }

    /**
     * Functionality to validate users access token
     *
     * @param int    $userId      user id
     * @param string $accessToken access token of user to access through api
     *
     * @return bool status
     */
    public function validateAccessToken($userId, $accessToken)
    {
        $response = false;
        try {
            //generate key from user info
            $dataKey = $this->commonHelper->generateKey(
                'apiAuthToken', $userId
            );
            $authToken =  $this->memcache->getItem($dataKey);

            //When it is same
            if (!empty($authToken) && ($accessToken === $authToken)) {
                $response = true;
            }

        } catch(Exception $mce) {
            error_log($mce->getMessage());
            $response = false;
        }
        return $response;
    }

    /**
     * Functionality to fetch users api dashboard info
     *
     * @param int    $userId user id
     * @param string $type   information type
     *
     * @return array users dashboard info
     */
    public function getUserDashboardInfo($userId, $type = 'apiDashboardInfo')
    {
        $response = false;
        try {
            //generate key from user info
            $dataKey = $this->commonHelper->generateKey(
                $type, $userId
            );
            $info =  $this->memcache->getItem($dataKey);

            //When it is same
            if (!empty($info)) {
                $response = $info;
            }

        } catch(Exception $mce) {
            error_log($mce->getMessage());
            $response = false;
        }
        return $response;
    }

    /**
     * Functionality to set users api dashboard info to memcache
     *
     * @param int $userId users id
     *
     * @return boolean
     */
    public function setApiDashboardInfo($userId)
    {
        $response = false;
        try {
            $userModel = new RapidFunnel_Model_AccountUser();
            //Get user's dashboard info
            $dashboardInfo = $userModel->getUserApiDashboardInfo($userId);

            if ('true' === $dashboardInfo['response']['status']) {
                //To set users authentication token to memcache
                $apiDashboardInfoKey = $this->commonHelper->generateKey(
                    'apiDashboardInfo', $userId
                );
                $response = $this->memcache->setItem(
                    $apiDashboardInfoKey, $dashboardInfo
                );
            }

            return $response;
        } catch (Exception $mce) {
            error_log($mce->getMessage());
            $response = false;
        }

        return $response;
    }

    /**
     * Functionality to delete user's login and dashboard info
     *
     * @param int $userId users id
     *
     * @return boolean
     */
    public function deleteLoginDashboardInfo($userId)
    {
        $response = false;
        try {
            $keys[] = $this->commonHelper->generateKey(
                'apiLogin', $userId
            );
            $keys[] = $this->commonHelper->generateKey(
                'apiDashboardInfo', $userId
            );

            //Clear remaining page
            $this->memcache->deleteMultiple($keys);

            return $response;
        } catch (Exception $mce) {
            error_log($mce->getMessage());
            $response = false;
        }

        return $response;
    }

}
