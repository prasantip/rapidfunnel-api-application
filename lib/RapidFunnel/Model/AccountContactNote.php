<?php

/**
 * RapidFunnel_Model_AccountContactNote
 * RapidFunnel/Model/AccountContactNote.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @copyright 2013 DigitalAssets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Model_AccountContactNote extends RapidFunnel_Model_Abstract
{

    public $dbAdapter;
    protected $id;
    protected $accountContactId;
    protected $contactNote;
    protected $noteTimeStamp;

    /**
     * RapidFunnel_Model_AccountContactNote constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountContactNote());
        $this->dbAdapter = $this->table->getAdapter();
    }

    /**
     * Get contact notes for user or specific to one contact
     *
     * @param int        $userId    user id
     * @param int        $roleId    role id
     * @param int | null $contactId contact id
     * @param int | null $groupIds  group id
     *
     * @return array contact note details
     * @throws InvalidArgumentException
     */
    public function getContactNotes(
        $userId, $roleId, $contactId = null, $groupIds = null
    ) {
        //Ensure userId is valid and not empty
        if (!is_numeric($userId) || empty($userId)) {
            throw new InvalidArgumentException('Invalid User Id provided.');
        }

        //Ensure roleId is valid and not empty
        if (!is_numeric($roleId) || empty($roleId)) {
            throw new InvalidArgumentException('Invalid Role Id provided.');
        }

        $select = $this->table->select()->setIntegrityCheck(false)
            ->from(
                array(
                    'acn' => 'accountContactNotes'
                ), array(
                    'noteId'    => 'acn.id',
                    'contactId' => 'acn.accountContactId',
                    'note'      => 'acn.contactNote',
                    'acn.noteTimeStamp'
                )
            )
            ->join(
                array('ac' => 'accountContact'),
                'acn.accountContactId = ac.id', array('')
            )
            ->joinLeft(
                array('au' => 'accountUser'), "au.id = ac.createdBy ",
                array()
            );

        // for normal user fetch only his contacts
        if (!empty($contactId)) {
            $select->where('acn.accountContactId = ?', $contactId);
        } elseif (3 == $roleId) {
            $select->where('ac.createdBy = ?', $userId);
        } elseif ((2 == $roleId) && !empty($groupIds)) {//for role manager
            // get group of manager
            // -> find ancestors of the groups
            // -> find all users of these groups
            // -> find all contacts of these groups
            $select
                ->joinLeft(
                    array('aug' => 'accountUserGroup'),
                    "aug.accountUserId = au.id "
                )
                ->where('aug.accountGroupId IN (?)', $groupIds)->where(
                    'au.roleId != ?', 1
                );
        }

        return $this->dbAdapter->fetchAll($select);
    }

    /**
     * Add notes to contact id
     *
     * @param int   $contactId contact id
     * @param array $vars      contains note values and timestamps
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function addNotes($contactId, $vars)
    {

        //Ensure contactId is not empty
        if (empty($contactId)) {
            throw new InvalidArgumentException('No Contact Id is provided.');
        }

        // ensure that $vars is an array
        if (!is_array($vars)) {
            throw new InvalidArgumentException('Invalid argument supplied.');
        }

        // ensure that $vars['noteTimeStamps'] is an array
        if (!is_array($vars['noteTimeStamps'])) {
            throw new InvalidArgumentException('Note TimeStamp should be in array format.');
        }

        // ensure that $vars['contactNotes'] is an array
        if (!is_array($vars['contactNotes'])) {
            throw new InvalidArgumentException('Contact Notes should be in array format.');
        }

        $varNoteTmStamps = array_filter($vars['noteTimeStamps']);
        $varContNotes = array_filter($vars['contactNotes']);

     // ensure that noteTimeStamps and contactNotes contains appropriate keys
        if (!empty($varNoteTmStamps) && !empty($varContNotes)) {
            try {
                //Insert new notes
                $data = [];
                $values = implode(
                    ',', array_fill(
                        0, count($varNoteTmStamps),
                        '(?, ?, ?, ?)'
                    )
                );

                foreach (
                    $varNoteTmStamps as $key => $noteTime
                ) {
                    $contactNote = isset($vars['contactNotes'][$key])
                        ? preg_replace(
                            '#<script(.*?)>(.*?)</script>#is', '',
                            $vars['contactNotes'][$key]
                        ) : '';

                    $guestNote = 0;
                    if (isset($vars['guestNote'])
                        && (1 == $vars['guestNote'])
                    ) {
                        $guestNote = 1;
                    }

                    //form the data for insert query
                    $data[] = $contactId;
                    $data[] = $contactNote;
                    $data[] = $noteTime;
                    $data[] = $guestNote;
                }
                //form the insert query
                $stmt = $this->dbAdapter->prepare(
                    'INSERT INTO `accountContactNotes`'
                    . ' (accountContactId, contactNote, '
                    . 'noteTimeStamp, guestNote) VALUES ' . $values
                );
                return $stmt->execute($data);

            } catch (Exception $e) {
                error_log($e->getMessage());
                $this->setMessage($e->getMessage());
                return false;
            }
        } else {
            return true;
        }

    }

    /**
     * Update notes respective to each note id
     *
     * @param int   $contactId contact id
     * @param array $vars      contains note ids, note values, timestamps
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function updateNotes($contactId, $vars)
    {
        try {
            //Ensure contactId is not empty
            if (!is_numeric($contactId) || empty($contactId)) {
                throw new InvalidArgumentException('No Contact Id is provided.');
            }

            // Ensure that $vars is an array
            if (!is_array($vars)) {
                throw new InvalidArgumentException('Invalid argument supplied.');
            }

            // ensure that $vars['noteIds'] is set
            if (!isset($vars['noteIds'])) {
                throw new InvalidArgumentException('noteIds is not set.');
            }

            // ensure that $vars['updateContactNotes'] is set
            if (!isset($vars['updateContactNotes'])) {
                throw new InvalidArgumentException('updateContactNotes is not set.');
            }

            // ensure that $vars['updateTimeStamps'] is set
            if (!isset($vars['updateTimeStamps'])) {
                throw new InvalidArgumentException('updateTimeStamps is not set.');
            }

            // ensure that $vars['noteIds'] is an array
            if (!is_array($vars['noteIds'])) {
                throw new InvalidArgumentException('Note Id should be in array format.');
            }

            // ensure that $vars['updateContactNotes'] is an array
            if (!is_array($vars['updateContactNotes'])) {
                throw new InvalidArgumentException('Contact Notes should be in array format.');
            }

            // ensure that $vars['updateTimeStamps'] is an array
            if (!is_array($vars['updateTimeStamps'])) {
                throw new InvalidArgumentException('Note TimeStamp should be in array format.');
            }

            // ensure that $vars contains appropriate keys
            if (empty(array_filter($vars['noteIds']))
                || empty(array_filter($vars['updateContactNotes']))
                || empty(array_filter($vars['updateTimeStamps']))
            ) {
                throw new InvalidArgumentException(
                    'Invalid argument supplied.'
                );
            }

            //Update existing notes
            foreach (array_filter($vars['noteIds']) as $noteKey => $id) {
                $updateData = array();
                $where = array();

                $updateData['accountContactId'] = $contactId;
                $updateData['contactNote']
                    = preg_replace(
                    '#<script(.*?)>(.*?)</script>#is', '',
                    $vars['updateContactNotes'][$noteKey]
                );
                $updateData['noteTimeStamp']
                    = $vars['updateTimeStamps'][$noteKey];
                $where[] = "id = $id";
                $where[] = "accountContactId = $contactId";

                $this->dbAdapter->update(
                    'accountContactNotes', $updateData, $where
                );
            }
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }

    }

    /**
     * Delete all notes of contact id or individual note
     *
     * @param int          $contactId contact id
     * @param array | null $vars      note id
     * @param int          $deleteAll deleteAll notes or not
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function deleteNotes($contactId, $vars = null, $deleteAll = 0)
    {
        //Ensure contactId is not empty
        if (!is_numeric($contactId) || empty($contactId)) {
            throw new InvalidArgumentException('Invalid Contact Id provided.');
        }

        if (isset($vars['deleteNoteIds'])
            && !empty($contactId)
        ) {
            // ensure that $vars['noteIds'] is an array
            if (!is_array($vars['deleteNoteIds'])) {
                throw new InvalidArgumentException('Note Id should be in array format.');
            }

        }
        try {
            //Delete contact notes
            if (0 == $deleteAll
                && isset($vars['deleteNoteIds'])
            ) {

                $this->dbAdapter->delete(
                    'accountContactNotes',
                    array('id IN (?)' => $vars['deleteNoteIds'])
                );
                return true;
            } elseif (1 == $deleteAll) {
                $where = "accountContactId = $contactId";
                $this->dbAdapter->delete('accountContactNotes', $where);
                return true;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * Get all notes for a contact
     *
     * @param mixed $contactId contact Id
     *
     * @return mixed arrays of note details
     * @throws InvalidArgumentException
     */
    public function getNoteDetails($contactId)
    {
        //Ensure contactId is not empty
        if (!is_numeric($contactId) || empty($contactId)) {
            throw new InvalidArgumentException('Invalid Contact Id provided.');
        }

        try {
            $selectQry = $this->dbAdapter->select()
                ->from(
                    array('ac' => 'accountContact'), array('')
                )
                ->join(
                    array('acn' => 'accountContactNotes'),
                    'ac.id=acn.accountContactId',
                    array('acn.id', 'acn.contactNote', 'acn.noteTimeStamp')
                )
                ->where('ac.id = ?', $contactId)
                ->order('acn.noteTimeStamp DESC');
            return $this->dbAdapter->fetchAll($selectQry);
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }
}
