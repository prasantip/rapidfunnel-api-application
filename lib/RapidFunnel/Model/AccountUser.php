<?php
/**
 * RapidFunnel_Model_AccountUser File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account/users
 */

/**
 * RapidFunnel_Model_AccountUser
 * RapidFunnel/Model/AccountUser.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account/users
 */
class RapidFunnel_Model_AccountUser extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountId;
    protected $email;
    protected $new = '1';
    protected $firstName;
    protected $lastName;
    protected $phoneNumber = NULL;
    protected $created;
    protected $createdBy;
    protected $modified;
    protected $modifiedBy;
    protected $password;
    protected $salt;
    protected $forceChange = '0'; //default
    protected $passwordResetSendTime;
    protected $profileImage;
    protected $address = NULL;
    protected $suite;
    protected $city = NULL;
    protected $stateId = NULL;
    protected $countryId = NULL;
    protected $zip = NULL;
    protected $status = '1';
    protected $isTrial = 0;
    protected $trialExpiresOn = NULL;
    protected $isDelete = 0;
    protected $autoSuspendStatus = 0;
    protected $roleId;
    protected $customRoleId;
    protected $isBillingContact = 0;
    protected $accessToken;
    protected $accessTokenExpiresOn;
    protected $registrationCode;
    protected $lastLogin = NULL;
    protected $ignoreUserPayment = 0;
    protected $linkTrackingNotify = '1';
    protected $additionalEmailNotification = null;
    protected $isCancelPremiumService = 0;
    protected $repId = NULL;
    protected $repId2 = NULL;
    protected $facebookUrl;
    protected $instagramUrl;
    protected $twitterUrl;
    protected $contactRecordsPerPage;
    protected $resourceRecordsPerPage;
    protected $userRecordsPerPage;
    protected $groupRecordsPerPage;
    protected $campaignRecordsPerPage;
    protected $broadcastRecordsPerPage;
    protected $currProgramRecordsPerPage;
    protected $pastProgramRecordsPerPage;
    protected $systemEmailsRecordsPerPage;
    protected $newUserEmailsRecordsPerPage;
    protected $contactDefSort;
    protected $resetPassHash;
    protected $webViewToken;
    protected $rewardNotifications = 1 ;
    protected $emailsFromAdministrators = 1;
    protected $myWeeklyStats = 1;
    protected $sqsMessageId = null;
    protected $payThroughAuthNet = 0;
    public $dtHelper;
    public $commonHelper;
    public $accountUserHelper;
    public $userStatusForAccountAdmin;
    public $accountContact;

    /**
     * Constructor method for the RapidFunnel_Model_AccountUser
     *
     * Instantiates the parent class by calling its constructor method. Also,
     * configures the model for interactions with the database by setting the
     * appropriate table, database adapter, query helper and Url helper.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountUser());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
        $this->commonHelper = new RapidFunnel_Controller_Action_Helper_Common();
        $this->accountUserHelper = new RapidFunnel_Controller_Action_Helper_AccountUser();
        $this->accountContact = new RapidFunnel_Model_AccountContact();

    }

    /**
     * RapidFunnel_Model_AccountUser::loadByEmail()
     *
     * This function is used to load user details by email
     *
     * @param string $email email id of the user
     *
     * @throws Exception
     *
     * @return boolean
     */
    public function loadByEmail($email)
    {
        try {
            $selectQuery = $this->db->select()->from(
                array('au' => 'accountUser')
            )->join(
                array('ar' => 'accountRoles'), 'au.roleId = ar.id',
                array('roleId' => 'id', 'roleName' => 'name',
                      'roleDescription' => 'description')
            )->where('au.email = ?', $email);

            $stmt = $this->db->query($selectQuery);
            $row = $stmt->fetch();

            return $row;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountUser::isEmailAvailable()
     *
     * This function checks if a users email is available and user is not
     * deleted
     *
     * @param string     $email      email id of the user
     * @param bool|false $forId      id of the user
     * @param bool|false $chkDeleted flag to check if user is deleted or not
     *
     * @throws Exception
     *
     * @return bool|array List of available users
     */
    public function isEmailAvailable($email, $forId = false , $chkDeleted = false)
    {
        try {
            $select = $this->db->select()
                ->from('accountUser', array('id', 'sqsMessageId'))
                ->where('email = ?', $email);

            if ($forId && (0 < $forId)) {
                $select->where('id != ?', $forId);
            }

            // If $chkDeleted is set to "Yes"
            // Then check whether the user is deleted or not
            if ($chkDeleted) {
                $select->where('isDelete = ?', 0);
            }

            $stmt = $this->db->query($select);
            $row = $stmt->fetch();

            // if email exist for other user then return $row
            return ($row);
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountUser::sendForgetPasswordEmail()
     *
     * This function is used to Send link to reset password
     *
     * @param int    $userId    user id
     * @param string $email     email id
     * @param string $firstName first name
     * @param string $lastName  last name
     *
     * @return bool
     * @throws Exception
     */
    public function sendForgetPasswordEmail($userId, $email, $firstName, $lastName) {

        // $userId should not be empty
        if (!is_numeric($userId) || empty($userId)) {
            throw new InvalidArgumentException('Empty $userId not allowed.');
        }

        // $email should not be empty
        if (!is_string($email) || empty($email)) {
            throw new InvalidArgumentException('Empty $email not allowed.');
        }

        // $firstName should not be empty
        if (!is_string($firstName) || empty($firstName)) {
            throw new InvalidArgumentException('Empty $firstName not allowed.');
        }

        $subject = 'Password Notification';
        $baseUrl = RapidFunnel_Configs::_instance(
            'serverInfo'
        )->domain;
        $user = $userId;

        //Random number generated
        $number = substr(md5(microtime()), rand(0, 26), 6);

        //Save random number as resetPassHash in db to validate
        //against the number passed with reset password link
        $updateData['resetPassHash'] = $number;
        $where = "id = $userId";
        $this->db->update(
            'accountUser', $updateData, $where
        );


        $loginLink = $baseUrl . 'registration/reset-password/' . $user
            . '&number=' . $number . "\n\n";
        $loginLinkHref = "<a href=" . $baseUrl
            . '/registration/reset-password?user=' . $user . '&number='
            . $number . ">" . "Click here to change your password. </a>" ."\n\n";

        $supportLink = 'http://support.rapidfunnel.com';
        $supportLinkHref
            = "<a href='http://support.rapidfunnel.com'>contact support</a>";

        $mailBody1 = 'Hi ' . $firstName . ' ' . $lastName . ", \n\n"
            . 'Someone requested a new password for your RapidFunnel account. '
            . 'If this request was not made by you, please ';

        $mailBody2 = ' and we\'ll be happy to help!' . "\n\n";

        $mailBody3 = 'Account login: ' . $email . "\n\n"
            . 'Regards,' . "\n" . 'The RapidFunnel Team';

        $mailTextBody = $mailBody1 . $supportLink . $mailBody2 . $loginLink
            . $mailBody3;

        $mailHtmlBody = nl2br(
            $mailBody1 . $supportLinkHref . $mailBody2 . $loginLinkHref
            . $mailBody3
        );

        // call mailgun library to send mails to those users.
        // $emailList will always be an array
        $libMailgun = new RapidFunnel_Email_Mailgun_Mailgun();

        $status = $libMailgun->sendMail(
            $email, $subject, $mailHtmlBody, $mailTextBody, ''
        );

        return $status;
    }

    /**
     * RapidFunnel_Model_AccountUser::validateRegistration()
     *
     * Validates user registration details with email and registration code
     *
     * @param string $email            email id of the registering user
     * @param string $registrationCode registration code of the registering user
     *
     * @return bool
     */
    public function validateRegistration($email, $registrationCode)
    {
        try {
            $select = $this->db->select()
                    ->from('accountUser')
                    ->where('email = ?', $email)
                    ->where('registrationCode = ?', $registrationCode);

            $qry = $this->db->query($select);
            $row = $qry->fetch();

            // if row exist then true else false
            return ($row) ? true : false;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountUser::updateUserStatus()
     *
     * Update User status
     *
     * @param array  $data   data needs to be updated
     * @param string $where  Conditions
     * @param string $table  For which tables data will be updated
     *
     * @return bool update status
     * @throws Exception
     */
    public function updateUserStatus($data, $where, $table)
    {
        //Ensure data is not empty
        if (empty($data)) {
            throw new InvalidArgumentException('No data provided.');
        }

        //Ensure valid data
        if (!is_array($data)) {
            throw new InvalidArgumentException('Not valid data.');
        }

        //Ensure $where details not empty
        if (empty($where)) {
            throw new InvalidArgumentException('No conditions provided.');
        }

        //Ensure valid $where details
        if (!is_string($where)) {
            throw new InvalidArgumentException('Not valid conditions.');
        }

        //Ensure $tables is not empty
        if (empty($table)) {
            throw new InvalidArgumentException('No tables provided.');
        }

        //Ensure valid tables
        if (!is_string($table)) {
            throw new InvalidArgumentException('Not valid data.');
        }

        try {
            $this->db->update($table, $data, $where);
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }

    }

    /**
     * RapidFunnel_Model_AccountUser::checkTrialExpiredOrNot()
     *
     * Functionality to check if user's trial period is expired or not
     *
     * @param  int $userId user Id
     *
     * @return bool status of trial period expiration
     *
     * @throws Exception
     */
    public function checkTrialExpiredOrNot($userId)
    {
        //Ensure $userId not empty
        if (empty($userId)) {
            throw new InvalidArgumentException('No userId provided.');
        }

        //Ensure valid $userId
        if (!is_numeric($userId)) {
            throw new InvalidArgumentException('No valid userId provided.');
        }

        try {
            $select = $this->table->select()
                ->from(
                    array('accountUser'), array('trialExpiresOn')
                )
                ->where('id = ?', $userId);

            $expireDate = $this->db->fetchAll($select)[0]['trialExpiresOn'];

            if (!empty($expireDate) && $expireDate >= date("Y-m-d H:i:s")) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * This function calculates the data for the dashboard page on api
     *
     * @param int $userId users id
     *
     * @return array | bool dashboard info
     */
    public function getUserApiDashboardInfo($userId)
    {
        try {
            // $userId should not be empty
            if (empty($userId) || ! is_numeric($userId)) {
                throw new InvalidArgumentException('Invalid $userId argument supplied.');
            }

            $response = false;
            $userModel = new RapidFunnel_Model_AccountUser();

            //load the id
            if ($userModel->load($userId)) {
                $accountId = $userModel->accountId;
                $roleId = $userModel->roleId;

                //get contacts for current week, current month, previous month and total
                $contactModel = new RapidFunnel_Model_AccountContact();

                $contactStats = $contactModel->getContactForWeekMonthPreMonth(
                    $userId
                );

                $response['response']['content']['ContactCurrentMonth']
                    = isset($contactStats['0']['ContactCurMonth'])
                    ? $contactStats['0']['ContactCurMonth'] : 0;
                $response['response']['content']['ContactLastMonth']
                    = isset($contactStats['0']['ContactLastMonth'])
                    ? $contactStats['0']['ContactLastMonth'] : 0;
                $response['response']['content']['ContactCurWeek']
                    = isset($contactStats['0']['ContactCurWeek'])
                    ? $contactStats['0']['ContactCurWeek'] : 0;
                $response['response']['content']['TotalContact']
                    = isset($contactStats['0']['TotalContact'])
                    ? $contactStats['0']['TotalContact'] : 0;

                //get config data
                $config = RapidFunnel_Configs::_instance();
                $accountRolesCnf = $config->accountRoles;

                $resourceModel = new RapidFunnel_Model_AccountResources();
                $campaignModel = new RapidFunnel_Model_AccountCampaign();
                $incentiveModel = new RapidFunnel_Model_AccountIncentiveProgram(
                );

                $userDetailsArray = array();
                $userDetailsArray['ignoreUserPayment']
                    = $userModel->ignoreUserPayment;
                $userDetailsArray['accountId'] = $userModel->accountId;
                $userDetailsArray['isTrial'] = $userModel->isTrial;
                $userDetailsArray['status'] = $userModel->status;
                $userDetailsArray['id'] = $userModel->id;
                $userDetailsArray['payThroughAuthNet'] = $userModel->payThroughAuthNet;

                //if not admin,  have only access to same group resource
                if ($accountRolesCnf->admin !== $roleId) {
                    $accessibleGroupIds = Zend_Controller_Action_HelperBroker::
                    getStaticHelper('UserGroup')
                        ->getAllAccessibleGroupIdsByUserId(
                            $userId
                        );

                    //getting resources list for user
                    $resources = $resourceModel->getResourcesForApi(
                        $accountId, $userId, $accessibleGroupIds,
                        $userDetailsArray
                    );

                    //getting campaign list for user
                    $campaignList = $campaignModel->getAllCampaignsForApp(
                        $userId, $accountId,
                        $userDetailsArray, $accessibleGroupIds
                    );
                } else {
                    //getting resources list for user
                    $resources = $resourceModel->getResourcesForApi(
                        $accountId, $userId, null, $userDetailsArray
                    );

                    //getting campaign list for user
                    $campaignList = $campaignModel->getAllCampaignsForApp(
                        $userId, $accountId, $userDetailsArray
                    );
                }

                //getting reward program for account
                $rewardCount = $incentiveModel->getIncentivesByRunningStatus(
                    $accountId, $userId, '1'
                );
                $response['response']['content']['TotalReward'] = count(
                    $rewardCount
                );
                $response['response']['content']['TotalResources'] = count(
                    $resources
                );
                $response['response']['content']['TotalCampaign'] = count(
                    $campaignList
                );
                $response['response']['status'] = 'true';
            }

            return $response;
        } catch (Exception $zde) {
            error_log($zde->getMessage());

            return false;
        }
    }

}
