<?php
/**
 * RapidFunnel_Model_AccountUserIncentive File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountUserIncentive
 * RapidFunnel/Model/AccountUserIncentive.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountUserIncentive
    extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountIncentiveProgramId;
    protected $userId;
    protected $leadsGenerated;
    protected $awardsEarned;
    public $dtHelper;
    public $incentiveTypeIds;
    public $requiredUserId;

    /**
     * RapidFunnel_Model_AccountUserIncentive::__construct()
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountUserIncentive());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
        $this->incentiveTypeIds = RapidFunnel_Configs::_instance(
            'accountIncentiveTypeIds'
        )->toArray();
    }

    /**
     * RapidFunnel_Model_AccountUserIncentive::getUserProgressAward()
     *
     * API functionality to fetch incentive details of an user
     *
     * @param int $incentiveId incentive id
     * @param int $userId      user id
     *
     * @return array | bool array of incentive details
     */
    public function getUserProgressAward($incentiveId, $userId)
    {
        $accIncentXType = RapidFunnel_Configs::_instance(
            'accountIncentiveTypeIds'
        )->xLeads;

        try {
            $todayDate = date("Y-m-d");
            $select = $this->table->select()->setIntegrityCheck(false)
                ->from(
                    array('aip' => 'accountIncentiveProgram'),
                    array(
                        'incentiveId' => 'id', 'accountId', 'name',
                        'startDate' => 'DATE_FORMAT(startDate,"%m/%d/%Y")',
                        'endDate' => "IF((accountAwardTypeId != $accIncentXType
                        ), DATE_FORMAT(endDate, '%m/%d/%Y'), '')",
                        'award',
                        'goal' => 'IFNULL(goal,0)',
                        'awardToPerformersInTop' => 'IFNULL(topPercentage,0)'
                    )
                )
                ->joinInner(
                    array('aat' => 'accountAwardType'),
                    'aat.id = aip.accountAwardTypeId',
                    array('awardTypeId' => 'id', 'awardType' => 'name')
                )
                ->joinLeft(
                    array(
                        "EDrvdTable" => new Zend_Db_Expr(
                            "(SELECT COUNT(ac.id) AS leadCount, "
                            . "accountIncentiveProgram.id "
                            . "FROM accountIncentiveProgram "
                            . "INNER JOIN accountContact ac "
                            . "ON (accountIncentiveProgram.accountId "
                            . "= ac.accountId) "
                            . "AND accountIncentiveProgram.startDate "
                            . "<= DATE(ac.optInDate) "
                            . "AND IF((accountAwardTypeId != $accIncentXType), "
                            . "accountIncentiveProgram.endDate "
                            . ">= DATE(ac.optInDate), 1 = ac.optIn) "
                            . "AND IF(EXISTS(SELECT accountCampaignId "
                            . "FROM accountIncentiveCampaign "
                            . "WHERE accountIncentiveProgramId="
                            . "accountIncentiveProgram.id) ,"
                            . "(ac.campaignId in (SELECT "
                            . "accountCampaignId FROM accountIncentiveCampaign "
                            . "WHERE accountIncentiveProgramId="
                            . "accountIncentiveProgram.id)), 1) "
                            . "AND ((ac.campaignId IS NOT NULL) AND ac.campaignId != 0)"
                            . "INNER JOIN accountUser au ON au.id=ac.createdBy "
                            . "AND IF(EXISTS(SELECT accountGroupId "
                            . "FROM accountIncentiveGroup "
                            . "WHERE accountIncentiveProgramId="
                            . "accountIncentiveProgram.id) ,"
                            . "(au.id IN (SELECT accountUserId "
                            . "FROM accountUserGroup aug INNER JOIN "
                            . "accountIncentiveGroup aig "
                            . "ON aug.accountGroupId=aig.accountGroupId "
                            . "WHERE aig.accountIncentiveProgramId="
                            . "accountIncentiveProgram.id)), 1) "
                            . "WHERE ac.createdBy = $userId "
                            . "AND accountIncentiveProgram.id= $incentiveId)"
                        )
                    ),
                    'aip.id = EDrvdTable.id',
                    array('leadsGenerated' => 'IFNULL(leadCount,0)')
                )
                ->where('aip.startDate <= ?', $todayDate)
                ->where(
                    "aip.endDate >= '$todayDate' "
                    . "OR aip.endDate = '0000-00-0'"
                )
                ->where('aip.id = ?', $incentiveId);
            $stmt = $this->db->query($select);
            $row = $stmt->fetchAll();
            return $row;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountUserIncentive::getAwardInfoByType()
     *
     * Api functionality to calculate award specific information based on type
     *
     * @param array $awardDetail array of award details
     * @param int   $userId      user id
     *
     * @return array award details along with calculated value
     */
    public function getAwardInfoByType($awardDetail, $userId)
    {
        //calculate days left
        $endDateTime = new DateTime($awardDetail['endDate']);
        $awardDetail['daysLeft'] = $endDateTime
            ->diff(new DateTime(date('Y-m-d')))->format("%a");

        //calculate data specific to incentive type
        if ($awardDetail['awardTypeId']
            == $this->incentiveTypeIds['achieveGoal']
        ) {
            if (0 != $awardDetail['goal']) {
                $progress = (
                    $awardDetail['leadsGenerated'] / $awardDetail['goal']
                );
                $awardDetail['leadsRequired'] = (
                    $awardDetail['goal'] - $awardDetail['leadsGenerated']
                );
            } else {
                $progress = 0;
                $awardDetail['leadsRequired'] = $awardDetail['goal'];
            }
            $awardDetail['progressPercentage'] = ($progress * 100);
            //Per lead award type
        } else {
            if ($awardDetail['awardTypeId']
                == $this->incentiveTypeIds['perLead']
            ) {
                $awardDetail['moneyEarned'] = (
                    $awardDetail['leadsGenerated'] * $awardDetail['award']
                );
                //For Most leads
            } else {
                if ($awardDetail['awardTypeId']
                    == $this->incentiveTypeIds['mostLeads']
                ) {
                    // calculating the rank of user by number of leads
                    $usersRankInfo = $this->getUsersProgressRank(
                        $awardDetail['incentiveId'], $awardDetail['accountId'],
                        $userId, $awardDetail['awardToPerformersInTop']
                    );
                    //assign values from response
                    $awardDetail['rank'] = $usersRankInfo['rank'];
                    $awardDetail['rankPercentage']
                        = $usersRankInfo['rankPercentage'];
                    //when leads required is set
                    if (isset($usersRankInfo['leadsRequired'])
                        && 0 < $usersRankInfo['leadsRequired']
                    ) {
                        $awardDetail['leadsRequired']
                            = $usersRankInfo['leadsRequired'];
                    }
                }
            }
        }
        return $awardDetail;
    }

    /**
     * RapidFunnel_Model_AccountUserIncentive::getUsersProgressRank()
     *
     * To calculate the rank of users based on award
     *
     * @param int $incentiveId       incentive id
     * @param int $accountId         account id
     * @param int $userId            user id
     * @param int $awardedPercentage award percentage
     *
     * @return array rank info
     */
    public function getUsersProgressRank($incentiveId, $accountId, $userId,
        $awardedPercentage
    )
    {
        //This is used in finding the users info in array_filter function
        $this->requiredUserId = $userId;

        try {
            $response['rank'] = '0';
            $response['rankPercentage'] = '0';
            $select = $this->table->select()->setIntegrityCheck(false)
                ->from(array('au' => 'accountUser'), array('userId' => 'id'))
                ->joinLeft(
                    array(
                        "EDrvdTable" => new Zend_Db_Expr(
                            "(SELECT COUNT(ac.id) AS leadsGenerated, "
                            . "ac.createdBy AS userId "
                            . "FROM accountIncentiveProgram "
                            . "INNER JOIN accountContact ac "
                            . "ON (accountIncentiveProgram.accountId "
                            . "= ac.accountId) "
                            . "AND (accountIncentiveProgram.startDate "
                            . "<= DATE(ac.optInDate) AND "
                            . "accountIncentiveProgram.endDate "
                            . ">= DATE(ac.optInDate)) "
                            . "AND IF(EXISTS(SELECT accountCampaignId "
                            . "FROM accountIncentiveCampaign "
                            . "WHERE accountIncentiveProgramId="
                            . "accountIncentiveProgram.id) ,"
                            . "(ac.campaignId in (SELECT "
                            . "accountCampaignId FROM accountIncentiveCampaign "
                            . "WHERE accountIncentiveProgramId="
                            . "accountIncentiveProgram.id)), 1) "
                            . "AND ((ac.campaignId IS NOT NULL) AND ac.campaignId != 0)"
                            . "INNER JOIN accountUser au "
                            . "ON au.id=ac.createdBy "
                            . "AND IF(EXISTS(SELECT accountGroupId "
                            . "FROM accountIncentiveGroup WHERE "
                            . "accountIncentiveProgramId="
                            . "accountIncentiveProgram.id) ,"
                            . "(au.id IN (SELECT accountUserId "
                            . "FROM accountUserGroup aug INNER JOIN "
                            . "accountIncentiveGroup aig ON "
                            . "aug.accountGroupId=aig.accountGroupId WHERE "
                            . "aig.accountIncentiveProgramId="
                            . "accountIncentiveProgram.Id)), 1)"
                            . "WHERE accountIncentiveProgram.id = $incentiveId "
                            . "GROUP BY ac.createdBy "
                            . "ORDER BY COUNT(ac.id) DESC ) "
                        )
                    ), 'au.id = EDrvdTable.userId', array('leadsGenerated')
                )
                ->where('au.accountId = ?', $accountId)
                ->where('au.isDelete = ?', 0)
                ->order(
                    new Zend_Db_Expr(
                        ' EDrvdTable.leadsGenerated DESC, EDrvdTable.userId = '
                        . $this->requiredUserId . ' DESC '
                    )
                );

            $stmt = $this->db->query($select);
            $allUserInfo = $stmt->fetchAll();

            //When all user found
            if (!empty($allUserInfo) && (0 < $awardedPercentage)) {
                if (0 < $allUserInfo[0]['leadsGenerated']) {
                    $totalUsers = count($allUserInfo);

                    //Finding the curr users info
                    $currUser = array_filter(
                        $allUserInfo, function ($user) {
                            return ($user['userId'] == $this->requiredUserId);
                        }
                    );

                    $currUserKey = key($currUser);
                    $currUsersPosition = key($currUser) + 1;

                    if (0 < $allUserInfo[$currUserKey]['leadsGenerated']) {
                        //users position is one
                        $response['rank'] = $currUsersPosition;
                        $response['rankPercentage'] = ceil(
                            (($totalUsers - $currUserKey) * 100) / $totalUsers
                        );
                    }
                    //calculating key of last awarded user
                    $posnOfLastAwardedUser = ceil(
                        ($awardedPercentage / 100) * $totalUsers
                    );
                    //Calculate leads Required
                    if ($currUsersPosition <= $posnOfLastAwardedUser) {
                        $response['leadsRequired'] = '0';
                    } else {
                        $keyOfLastAwardedUser = ($posnOfLastAwardedUser - 1);
                        $response['leadsRequired']
                            = $allUserInfo[$keyOfLastAwardedUser]['leadsGenerated']
                            - $currUser[key($currUser)]['leadsGenerated'];
                    }
                }
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
        }
        return $response;
    }

    /**
     * RapidFunnel_Model_AccountUserIncentive::getUserPastAward()
     *
     * Api functionality to fetch past incentive details of an user
     *
     * @param int $incentiveId incentive id
     * @param int $userId      user id
     *
     * @return array incentive details
     */
    public function getUserPastAward($incentiveId, $userId)
    {
        try {
            $todayDate = date("Y-m-d");
            $select = $this->table->select()->setIntegrityCheck(false)
                ->from(
                    array('aip' => 'accountIncentiveProgram'),
                    array(
                        'name',
                        'startDate' => 'DATE_FORMAT(startDate,"%m/%d/%Y")',
                        'endDate'   => 'DATE_FORMAT(endDate,"%m/%d/%Y")',
                        'award',
                        'goal',
                        'topPercentage'
                    )
                )
                ->joinInner(
                    array('aat' => 'accountAwardType'),
                    'aat.id = aip.accountAwardTypeId',
                    array('awardTypeId' => 'id', 'awardType' => 'name')
                )
                ->joinLeft(
                    array('aui' => 'accountUserIncentive'),
                    'aui.accountIncentiveProgramId = aip.id',
                    array('leadsGenerated', 'awardsEarned')
                )
                ->where('aip.endDate < ?', $todayDate)
                ->where('aip.id = ?', $incentiveId)
                ->where('aui.userId = ?', $userId);
            $stmt = $this->db->query($select);
            $row = $stmt->fetch();
            return $row;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountUserIncentive::getGeneratedLeadsByDateRange()
     *
     * Api functionality to fetch lead count in a date range of an user
     *
     * @param int    $userId    user id
     * @param string $startDate starting date for the range
     * @param string $endDate   ending date for the range
     *
     * @return array row / boolean
     */
    public function getGeneratedLeadsByDateRange($userId, $startDate, $endDate)
    {
        try {
            $select = $this->db->select()
                ->from(
                    array('ac' => 'accountContact'),
                    array('leadCount' => 'COUNT(ac.id)')
                )
                ->where('ac.createdBy = ?', $userId)
                ->where('DATE(ac.optInDate) >= ?', $startDate)
                ->where('DATE(ac.optInDate) <= ?', $endDate);

            $stmt = $this->db->query($select);
            $row = $stmt->fetch();
            return $row;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

}
