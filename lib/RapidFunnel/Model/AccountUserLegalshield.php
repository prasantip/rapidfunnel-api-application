<?php

/**
 * RapidFunnel_Model_AccountUserLegalshield
 * RapidFunnel/Model/AccountUserLegalshield.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */

use League\OAuth2\Client\Provider\GenericProvider;
class RapidFunnel_Model_AccountUserLegalshield extends
    RapidFunnel_Model_Abstract
{

    public $db;
    protected $accountUserId;
    protected $associateNumber;
    protected $associateName;
    protected $associateEmail;
    protected $advantageStatus;
    protected $token;
    protected $isTokenExpired;

    public function __construct()
    {
        parent::__construct();
        $this->setTable(new RapidFunnel_Data_Table_AccountUserLegalshield());
        $this->db = $this->table->getAdapter();
    }

    /**
     * Get user info related to Legalshield account
     *
     * @param int $userId user id
     *
     * @return array | boolean containing associateName, associateEmail,
     *                         advantageStatus
     * @throws InvalidArgumentException
     */
    public function getLegalshieldInfo($userId)
    {
        //Ensure userId is not empty and is numeric
        if (empty($userId) || !is_numeric($userId)) {
            throw new InvalidArgumentException('Invalid User Id provided.');
        }

        $select = $this->db->select()->from(
            array('accountUserLegalshield'), array(
                'associateName', 'associateEmail', 'advantageStatus',
                'accessToken', 'associateNumber'
            )
        )->where('accountUserId = ?', $userId);

        $result = $this->db->fetchAll($select);
        if (!empty($result)) {
            return $this->db->fetchAll($select)[0];
        } else {
            return false;
        }
    }

    /**
     * Get advantage status from Legalshield and check
     * advantage status true or false
     *
     * @param string $accessToken serialized object
     * @param int    $lsInfo if 1 return ls details else return 1 or 0
     *
     * @return int array of legalshield details of the user or 0 to check
     *         if user will be free or paid
     * @throws InvalidArgumentException
     */
    public function getAdvantageStatus($accessToken, $lsInfo = 0)
    {
        //Ensure access token is not empty and is string
        if (empty($accessToken) || !is_string($accessToken)) {
            throw new InvalidArgumentException('Invalid access token provided.');
        }

        try {
            $legalshieldConfig = RapidFunnel_Configs::_instance('legalshield');
            $provider = new GenericProvider(
                [
                    'clientId' => $legalshieldConfig->clientId,
                    'clientSecret' => $legalshieldConfig->clientSecret,
                    'redirectUri' => $legalshieldConfig->redirectUri,
                    'urlAuthorize' => $legalshieldConfig->urlAuthorize,
                    'urlAccessToken' => $legalshieldConfig->urlAccessToken,
                    'urlResourceOwnerDetails' => $legalshieldConfig->urlResourceOwnerDetails
                ]
            );

            //Store legalshield user info
            $resourceOwner = $provider->getResourceOwner(
                unserialize($accessToken)
            );
            if (false !== $resourceOwner) {
                $resourceOwner = $resourceOwner->toArray();
                if (1 === $lsInfo) {
                    $lsDetails = array(
                        'can_have_crm' => !empty($resourceOwner['access_control']['can_have_crm'])
                            ? 1 : 0, 'repId2' => $resourceOwner['id'],
                        'repId' => $resourceOwner['login']
                    );
                    return $lsDetails;
                } else {
                    return !empty($resourceOwner['access_control']['can_have_crm'])
                        ? 1 : 0;
                }
            } else {
                return -1;
            }

        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return -1;
        }
    }

}