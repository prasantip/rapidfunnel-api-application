<?php
/**
 * RapidFunnel_Model_AccountCampaign File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account/campaign
 */

/**
 * RapidFunnel_Model_AccountCampaign
 * RapidFunnel/Model/AccountCampaign.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account/campaign
 */
class RapidFunnel_Model_AccountCampaign extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountId;
    protected $name;
    protected $description;
    protected $showInApp;
    protected $sendFromEmail;
    protected $status;
    protected $created;
    protected $createdBy;
    protected $modifiedBy;
    public $dtHelper;
    public $accountUserHelper;
    public $authenticateHelper;

    /**
     * RapidFunnel_Model_AccountCampaign::__construct()
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the database by
     * setting the appropriate table, database adapter and a query helper.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountCampaign());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
        $this->accountUserHelper
            = new RapidFunnel_Controller_Action_Helper_AccountUser();
        $this->authenticateHelper
            = new RapidFunnel_Controller_Action_Helper_Authenticate();
    }


    /**
     * RapidFunnel_Model_AccountCampaign::getAllCampaignsToSend()
     *
     * Get all the campaigns associated with an user account id
     *
     * @param int     $accountId account id
     * @param int     $userId    user id
     * @param int     $roleId    role id
     * @param array   $groupIds  array of group ids
     * @param boolean $freeUser  determines if the account is a free account
     *
     * @return array $response
     */
    public function getAllCampaignsToSend($accountId, $userId, $roleId,
        $groupIds = null, $freeUser = false
    ) {
        try {
            $select = $this->table->select()->setIntegrityCheck(false)
                ->from(
                    array('c' => 'accountCampaign'),
                    array('id', 'name', 'status')
                )
                ->where('c.accountId = ?', $accountId)
                ->where('c.status = ?', '1');

            if ((1 != $roleId)) {//for other than admin
                if ($freeUser) {
                    $select
                        ->joinInner(
                            array('afUc' => 'accountFreeUserCampaigns'),
                            "afUc.accountCampaignId = c.id "
                        );
                } else {
                    if (!empty($groupIds)) {
                        $select
                            ->joinLeft(
                                array('acg' => 'accountCampaignGroup'),
                                "acg.accountCampaignId = c.id "
                            )
                            ->where(
                                '(acg.accountGroupId IN (' . implode(
                                    ',',
                                    $groupIds
                                ) . ') OR c.createdBy = ' . $userId . ')'
                            );
                    } else {
                        $select->orWhere('c.createdBy', $userId);
                    }
                }
            }

            $qry = $this->db->query($select);
            $response = $qry->fetchAll();
            return $response;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaign::getAllCampaignsForApp()
     *
     * Get campaign details for the campaign belong to the app of an user
     *
     * @param mixed  $userId      user id
     * @param mixed  $accountId   account id
     * @param string $groupIds    associated group id
     * @param array  $userDetails user details
     *
     * @return array of campaign details
     * @throws Exception
     */
    public function getAllCampaignsForApp($userId, $accountId, $userDetails,
        $groupIds = null
    ) {
        if (!is_numeric($userId) || empty($userId)) {
            throw new InvalidArgumentException('No user id specified.');
        }

        if (!is_numeric($accountId) || empty($accountId)) {
            throw new InvalidArgumentException('No account id specified.');
        }

        if (!is_array($userDetails) || empty($userDetails)) {
            throw new InvalidArgumentException('No user details specified.');
        }

        try {
            // check of enterprise free user
            $accountUserHelper
                = new RapidFunnel_Controller_Action_Helper_AccountUser();
            $freeEntpAcc = $accountUserHelper->isEnterpriseFreeUser(
                $userDetails
            );

            $select = $this->db->select()
                ->from(
                    array('ac' => 'accountCampaign'), array(
                        'id', 'accountId', 'name', 'description', 'status',
                        'created', 'createdBy', 'modified', 'modifiedBy'
                    )
                );

            // handle free enterprise user
            if ($freeEntpAcc) {
                $select
                    ->joinInner(
                        array('afuc' => 'accountFreeUserCampaigns'),
                        'ac.id = afuc.accountCampaignId', array()
                    )
                    ->where(
                        new Zend_Db_Expr(
                            'EXISTS(SELECT 1 FROM accountUserGroup aug '
                            . 'WHERE aug.accountUserId = ' . $userId . ')'
                        )
                    )
                    ->where('afuc.accountId = ?', $accountId);
            } else {
                if (!empty($groupIds)) {
                    //Validating for proper associated group id
                    $select
                        ->joinLeft(
                            array('acg' => 'accountCampaignGroup'),
                            "acg.accountCampaignId = ac.id "
                        )
                        ->where(
                            '(acg.accountGroupId IN (' . implode(
                                ',', $groupIds
                            ) . '))'
                        )
                        ->group('ac.id');
                }

                $select
                    ->joinInner(
                        array('auac' => 'accountUserAppCampaign'),
                        'ac.id = auac.accountCampaignId', array()
                    )
                    ->where(
                        new Zend_Db_Expr(
                            'EXISTS(SELECT 1 FROM accountUserGroup aug '
                            . 'WHERE aug.accountUserId = ' . $userId . ')'
                        )
                    )
                    ->where('auac.accountUserId = ?', $userId);
            }

            $select
                ->joinLeft(
                    array(
                        "totCntTbl" => $this->db->select()
                            ->from(
                                array('ac' => 'accountContact'),
                                array(
                                    'totalContacts' => 'COUNT( ac.id )',
                                    'accountCampaignId' => 'ac.campaignId'
                                )
                            )
                            ->where('ac.createdBy = ?', $userId)
                            ->where(
                                '(ac.campaignId IS NOT NULL) AND ac.campaignId != 0'
                            )
                            ->group('ac.campaignId')
                    ), 'ac.id = totCntTbl.accountCampaignId',
                    array('totalContacts' => 'IFNULL(totalContacts, 0)')
                );

            $select->where('ac.status = ?', '1');

            $qry = $this->db->query($select);
            $response = $qry->fetchAll();
            return $response;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaign::getCampaignDetails()
     *
     * Get specific campaign details
     *
     * @param mixed $userId     user id
     * @param mixed $campaignId campaign id
     *
     * @return array
     * @throws Exception
     */
    public function getCampaignDetails($userId, $campaignId)
    {
        if (!is_numeric($userId) || empty($userId)) {
            throw new InvalidArgumentException('No user id specified.');
        }

        if (!is_numeric($campaignId) || empty($campaignId)) {
            throw new InvalidArgumentException('No campaign id specified.');
        }

        try {
            $select = $this->db->select()
                ->from(
                    'accountCampaign',
                    array(
                        'id', 'accountId', 'name', 'description', 'status',
                        'created', 'createdBy', 'modified', 'modifiedBy'
                    )
                )
                ->joinLeft(
                    array(
                        "totCntTbl" => $this->db->select()
                            ->from(
                                array('ac' => 'accountContact'),
                                array(
                                    'totalContacts' => 'COUNT( ac.id )',
                                    'accountCampaignId' => 'ac.campaignId'
                                )
                            )
                            ->where('ac.createdBy = ?', $userId)
                            ->where('(ac.campaignId IS NOT NULL) AND ac.campaignId != 0')
                            ->group('ac.campaignId')
                    ), 'accountCampaign.id = totCntTbl.accountCampaignId',
                    array('totalContacts' => 'IFNULL(totalContacts, 0)')
                )
                ->where('accountCampaign.id = ?', $campaignId);

            $qry = $this->db->query($select);
            $response = $qry->fetch();
            return $response;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaign::campaignAccessPermissionForFreeUser()
     *
     * Checks if access is permitted for free user
     *
     * @param int $campaignId campaign id
     * @param int $accountId  account id
     *
     * @return boolean|int
     */
    public function campaignAccessPermissionForFreeUser($campaignId, $accountId)
    {
        try {
            $select = $this->db->select()
                ->from(array('ac' => 'accountCampaign'), array('id'))
                ->joinInner(
                    array('afuc' => 'accountFreeUserCampaigns'),
                    "afuc.accountCampaignId = ac.id ", array()
                )
                ->where('ac.id = ?', $campaignId)
                ->where('ac.accountId = ?', $accountId);

            $qry = $this->db->query($select);
            $row = $qry->fetch();

            // if 1, contact exist else 0
            if ($row) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaignContact::removeCampaign()
     *
     * Functionality to remove campaign contact and campaign emails
     *
     * @param int $contactId contact id
     *
     * @return boolean
     */
    public function removeCampaign($contactId)
    {
        $deleteCampaignStatus = true;
        $deleteCampaignMailStatus = true;

        //get campaign for contact
        $accountCamEmail = new RapidFunnel_Model_AccountCampaignContactEmail();
        $accountContact = new RapidFunnel_Model_AccountContact();
        $ContactStatusConfig = RapidFunnel_Configs::_instance(
            'accountContactStatus'
        );

        try {
            if (!is_numeric($contactId) || empty($contactId)) {
                throw new InvalidArgumentException('Invalid contact id provided.');
            }

            $accountCamEmail->load(
                $contactId, array('campaignId'),
                'contactId'
            );

            $accountContact->load(
                $contactId, array('campaignId', 'optIn')
            );

            // keep whole process in transaction
            $this->db->beginTransaction();

            if (isset($accountContact->campaignId)
                && !empty($accountContact->campaignId)
            ) {
                $updateData = array('campaignAssignTime' => null,
                                    'campaignId'         => null);
                if ($ContactStatusConfig->notOptIn == $accountContact->optIn) {
                    $updateData['status'] = $ContactStatusConfig->notSent;
                    $updateData['optInSend'] = $ContactStatusConfig->notSent;
                    $updateData['optInResend'] = $ContactStatusConfig->notSent;
                }

                $deleteCampaignStatus = $this->db->update(
                    'accountContact',$updateData,
                    array('id = ?' => $contactId)
                );

            }

            if (isset($accountCamEmail->campaignId)
                && !empty($accountCamEmail->campaignId)
            ) {
                $deleteCampaignMailStatus = $this->db->delete(
                    'accountCampaignContactEmail', 'contactId =' .
                    $contactId
                );
            }

            if ($deleteCampaignStatus && $deleteCampaignMailStatus) {
                $this->db->commit();

                return true;
            } else {
                $this->db->rollback();

                return false;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

}
