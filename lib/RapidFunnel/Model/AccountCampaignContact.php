<?php
/**
 * RapidFunnel_Model_AccountCampaignContact
 *
 * PHP version 5
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/campaign
 */

/**
 * RapidFunnel_Model_AccountCampaignContact
 * RapidFunnel/Model/AccountCampaignContact.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/campaign
 */
class RapidFunnel_Model_AccountCampaignContact
    extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $accountCampaignId;
    protected $accountContactId;
    protected $channel;
    protected $created;
    protected $createdBy;
    public $cmnHelper;

    /**
     * Constructor method for the RapidFunnel_Model_AccountCampaignContact
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the database by
     * setting the appropriate table, database adapter and a common helper.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountCampaignContact());
        $this->db = $this->table->getAdapter();
        $this->cmnHelper = new RapidFunnel_Controller_Action_Helper_Common();
    }

    /**
     * RapidFunnel_Model_AccountCampaignContact::assignContactToCampaign()
     *
     * Functionality to  insert contact and campaignId
     *
     * @param int   $contactId   contact id of the contact created
     * @param array $contactVars contact various attributes
     * @param array $contactCampaignVars contact campaign attributes
     *
     * @return boolean/ array
     */
    public function assignContactToCampaign(
        $contactId, $contactVars, $contactCampaignVars
    )
    {
        $returnArray = '';
        $isDuplicateContact = 0;
        $isValidEmail = 1;
        $config = RapidFunnel_Configs::_instance();

        //Check for duplicate assigned active contact
        $accountContactModel = new RapidFunnel_Model_AccountContact();
        $accountContactModel->load($contactId);

        if(!empty($contactVars['email'])) {
            $isDuplicateContact
                = $accountContactModel->isDuplicateContactOrCampaignAssigned(
                $contactVars['accountId'], $contactVars['email'],
                null, $contactId, true
            );
        }

        if (0 === $isDuplicateContact) {
            $isAllowedArray['isAllowed'] =  true;
            $updateCampaignAssignTime = false;

            //check whether campaign assigning first time or changing to other
            //campaign ot changing to none

            $campaignAssignConfig = $config->campaignAssigning;

            if((empty($contactCampaignVars['oldCampaignId'])
                || (0 === $contactCampaignVars['oldCampaignId'])) &&
                (!empty($contactCampaignVars['campaign'])
                    && (0 < $contactCampaignVars['campaign']))
            ) {
                $campaignAssign = $campaignAssignConfig->noneToNew;
            } else if((!empty($contactCampaignVars['oldCampaignId'])
                    || (0 < $contactCampaignVars['oldCampaignId'])) &&
                (!empty($contactCampaignVars['campaign'])
                    && (0 < $contactCampaignVars['campaign']))
            ) {
                $campaignAssign = $campaignAssignConfig->oldToNew;
            } else if((!empty($contactCampaignVars['oldCampaignId'])
                    || (0 < $contactCampaignVars['oldCampaignId'])) &&
                (empty($contactCampaignVars['campaign'])
                    || (0 === $contactCampaignVars['campaign']))
            ) {
                $campaignAssign = $campaignAssignConfig->oldToNone;
            }

            //campaign changed from none to any or assigning first time
            // check is allowed to assign campaign
            if ($campaignAssignConfig->noneToNew === $campaignAssign) {
                $isAllowedArray = $accountContactModel->checkContactLimit(
                    $contactVars['accountId'], $contactCampaignVars['changedBy']
                );
                $accountContactModel->campaignAssignTime = date("Y-m-d H:i:s");
                $updateCampaignAssignTime = true;
            } elseif ($campaignAssignConfig->oldToNone === $campaignAssign) {
                $accountContactModel->campaignAssignTime = null;
                $accountContactModel->campaignId = null;
                $accountContactModel->optInSendDate = null;

                //if contact is not opted then change status to blank
                if($config->accountContactStatus->notOptIn ===
                    $accountContactModel->optIn) {
                    $accountContactModel->status
                        = $config->accountContactStatus->notSent;
                    $accountContactModel->optInSend = 0;
                    $accountContactModel->optInResend = 0;
                }
            }

            if($updateCampaignAssignTime ||
                $campaignAssignConfig->oldToNew === $campaignAssign) {
                $accountContactModel->campaignId = $contactCampaignVars['campaign'];
                $accountContactModel->campaignAssignTime = date("Y-m-d H:i:s");
            }

            if ($isAllowedArray['isAllowed']) {
                $returnArray['message'] = $this->saveCampaignChanges(
                    $contactId, $contactCampaignVars['campaign'],
                    $accountContactModel, $contactVars, $isValidEmail,
                    $campaignAssign
                );
            } else {
                $returnArray['message'] = 'notAllowed';
                $returnArray['errorMessage'] = $isAllowedArray['errorMessage'];
            }
        } elseif (1 === $isDuplicateContact) {
            $returnArray['message'] = 'duplicateContact';
        }

        return $returnArray;
    }

    /**
     * RapidFunnel_Model_AccountCampaignContact::getCampaignByContact()
     *
     * Functionality to get campaignId using contactId
     *
     * @param int $contactId use to filter accountCampaignContact
     *                       and get campaignId
     *
     * @return boolean
     */
    public function getCampaignByContact($contactId)
    {
        try {
            $select = $this->db->select()
                ->from(
                    array('accountContact'),
                    array('accountCampaignId' => 'campaignId')
                )
                ->where('id = ?', $contactId);
            $qry = $this->db->query($select);
            $contactDetails = $qry->fetch();
            return $contactDetails;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaignContact::sendOptInEmail()
     *
     * Functionality to send the email
     *
     * @param array $optInArr array of details to send email
     *
     * @return boolean
     * @throws exception
     */
    public function sendOptInEmail($optInArr)
    {
        if(empty($optInArr) || !is_array($optInArr)) {
            throw new InvalidArgumentException('Invalid optInArr supplied.');
        }

        $config = RapidFunnel_Configs::_instance();
        $recipientType = $config->broadcastEmail->recipientType;
        $campEmailOptInType = $config->campaign->email;

        if (!empty($optInArr) && is_array($optInArr)
            && $optInArr['contactId']
        ) {
            // set from address
            $fromAddress = '';

            // if campaign is set, use sendFromEmail of assigned campaign
            if (isset($optInArr['accountCampaignId'])
                && ('' != $optInArr['accountCampaignId'])
            ) {
                $accountCampaignModel = new RapidFunnel_Model_AccountCampaign();
                $accountCampaignModel->load($optInArr['accountCampaignId']);
                if (isset($accountCampaignModel->sendFromEmail)
                    && ('' != $accountCampaignModel->sendFromEmail)
                ) {
                    $fromAddress = $accountCampaignModel->sendFromEmail;
                    $senderName = $optInArr['senderFirstName'] . ' '
                        . $optInArr['senderLastName'];
                }
            }
            $accountContactModel = new RapidFunnel_Model_AccountContact();
            $accountContactModel->load($optInArr['contactId']);

            $accountUserModel = new RapidFunnel_Model_AccountUser();
            $accountUserModel->load($accountContactModel->createdBy);

            // if still it is null, populate creator email
            if (empty($fromAddress)) {
                $fromAddress = $accountUserModel->email;
                $senderName = $accountUserModel->firstName . ' '
                    . $accountUserModel->lastName;
            }

            $baseUrl = $config->serverInfo->domain;
            $param = array(
                'contactId' => $optInArr['contactId'],
                'userId' => $optInArr['senderId']
            );
            $encodeUrl = urlencode(base64_encode(serialize($param)));
            $url = $baseUrl . '/system/opt-in/confirm-email/p/' . $encodeUrl;
            $mailHtmlBodyLink = $url;
            $mailTextBodyLink = $url;

            //first check in campaign optIn else get it from system email
            $campEmailModel = new RapidFunnel_Model_AccountCampaignEmail();
            $mailDetails = $campEmailModel->getOtherMailsOfCampaign(
                $optInArr['accountCampaignId'], $campEmailOptInType->optIn
            );

            if(empty($mailDetails)) {
                //get system mail details for optIn
                $sysEmailModel = new RapidFunnel_Model_AccountSystemEmails();
                $mailDetails = $sysEmailModel->getEmailDetails(
                    $optInArr['accountId'], "1"
                );
            } else {
                $mailDetails['subject'] = $mailDetails['title'];
                $mailDetails['htmlBody'] = $mailDetails['content'];
                $mailDetails['textBody'] = $mailDetails['textContent'];
            }

            $placeHolders = $config->contentPlaceholders;

            // creating array for replacing content placeholder variable
            $templateVars = array($placeHolders->accountName,
                                  $placeHolders->firstName,
                                  $placeHolders->lastName,
                                  $placeHolders->company, $placeHolders->email,
                                  $placeHolders->senderFirstName,
                                  $placeHolders->senderLastName,
                                  $placeHolders->senderEmail,
                                  $placeHolders->senderPhone,
                                  $placeHolders->password,
                                  $placeHolders->contactFirstName,
                                  $placeHolders->contactLastName,
                                  $placeHolders->contactLinkClicked,
                                  $placeHolders->contactEmail,
                                  $placeHolders->contactCampaign,
                                  $placeHolders->contactPhone,
                                  $placeHolders->contactMobileNumber,
                                  $placeHolders->phoneNumber,
                                  $placeHolders->title,
                                  $placeHolders->repId, $placeHolders->repId2,
                                  $placeHolders->userId);

            $replaceTemplateVars = array(
                $optInArr['accountName'], $optInArr['firstName'],
                $optInArr['lastName'], $optInArr['company'], $optInArr['email'],
                $optInArr['senderFirstName'], $optInArr['senderLastName'],
                $optInArr['senderEmail'], $optInArr['phone'], "", "", "", "",
                "", "", $optInArr['contactPhone'], "", $optInArr['phone'],
                $optInArr['title'], $optInArr['repId'], $optInArr['repId2'],
                $optInArr['senderId']
            );

            /**
             * http:// is being added by html editor when creating link
             * so replace it by empty string
             */
            $mailDetails['htmlBody'] = str_replace(
                'http://[activationLink]', '[activationLink]',
                $mailDetails['htmlBody']
            );

            // for activation link
            $mailHtmlBody = str_replace(
                $placeHolders->activationLink, $mailHtmlBodyLink,
                $mailDetails['htmlBody']
            );
            $mailTextBody = str_replace(
                $placeHolders->activationLink, $mailTextBodyLink,
                $mailDetails['textBody']
            );

            $mailHtmlBody = str_replace(
                $templateVars, $replaceTemplateVars, $mailHtmlBody
            );
            $mailTextBody = str_replace(
                $templateVars, $replaceTemplateVars, $mailTextBody
            );

            // Track opt in link
            if ('1' === $accountUserModel->linkTrackingNotify) {
                $urlHelper = new RapidFunnel_Controller_Action_Helper_Url();
                $mailHtmlBody = $urlHelper->changeResourceUrlsInContent(
                    $optInArr['senderId'], $mailHtmlBody,
                    $optInArr['email'], false
                );
                $mailTextBody = $urlHelper->changeResourceUrlsInContent(
                    $optInArr['senderId'], $mailTextBody,
                    $optInArr['email'], false
                );
            }

            // create unsubscribe code to use in unsubscribe link
            $unsubscribeCode = urlencode(
                base64_encode($optInArr['contactId'])
            );

            $serverDomain = $config->serverInfo->domain;
            $unsubscribeMessages = $config->messages->unsubscribe->optin;

            // add unsubscribe link and domain name
            $unsubscribeHtml = '<br /><br />' . str_replace(
                    array('<%serverDomain%>', '<%unsubscribeCode%>'),
                    array($serverDomain, $unsubscribeCode),
                    $unsubscribeMessages->html
                );
            $mailHtmlBody = $mailHtmlBody . $unsubscribeHtml;

            //create unsubscribe text message
            $unsubscribeText = "\r\n\r\n" . str_replace(
                    array('<%serverDomain%>', '<%unsubscribeCode%>'),
                    array($serverDomain, $unsubscribeCode),
                    $unsubscribeMessages->text
                );

            $mailTextBody = $mailTextBody . $unsubscribeText;

            // remove http:// from social URL
            // replace social media url
            $templateHttpVars = array('http://' . $placeHolders->facebookUrl,
                                      'http://' . $placeHolders->twitterUrl,
                                      'http://' . $placeHolders->instagramUrl);

            // replace social media url
            $replaceHttpVars = array($placeHolders->facebookUrl,
                                     $placeHolders->twitterUrl, $placeHolders->instagramUrl);

            $mailHtmlBody = str_replace(
                $templateHttpVars, $replaceHttpVars, $mailHtmlBody
            );

            $mailTextBody = str_replace(
                $templateHttpVars, $replaceHttpVars, $mailTextBody
            );

            $commonHelper = new RapidFunnel_Controller_Action_Helper_Common();
            if(empty($optInArr['senderInstagramUrl'])) {
                $mailHtmlBody
                    = $commonHelper->removeHyperLinkInEmail(
                    $placeHolders->instagramUrl, $mailHtmlBody
                );
            }

            if(empty($optInArr['senderFacebookUrl'])) {
                $mailHtmlBody
                    = $commonHelper->removeHyperLinkInEmail(
                    $placeHolders->facebookUrl, $mailHtmlBody
                );
            }

            if(empty($optInArr['senderTwitterUrl'])) {
                $mailHtmlBody
                    = $commonHelper->removeHyperLinkInEmail(
                    $placeHolders->twitterUrl, $mailHtmlBody
                );
            }

            // replace social media url
            $templateMediaVars = array($placeHolders->facebookUrl,
                                       $placeHolders->twitterUrl,
                                       $placeHolders->instagramUrl);

            $replaceMediaVars = array(
                $optInArr['senderFacebookUrl'],
                $optInArr['senderTwitterUrl'], $optInArr['senderInstagramUrl']
            );

            $mailHtmlBody = str_replace(
                $templateMediaVars, $replaceMediaVars, $mailHtmlBody
            );

            $mailTextBody = str_replace(
                $templateMediaVars, $replaceMediaVars, $mailTextBody
            );

            $subject = str_replace(
                $templateVars, $replaceTemplateVars, $mailDetails['subject']
            );

            $subject = str_replace(
                $templateHttpVars, $replaceHttpVars, $subject
            );

            $subject = str_replace(
                $templateMediaVars, $replaceMediaVars, $subject
            );

            // call mailgun library to send mails to those users.
            // $emailList will always be an array
            $libMailgun = new RapidFunnel_Email_Mailgun_Mailgun();

            // Fill Custom Data with type of email and contactid
            $customData = array(
                'accountId'     => $optInArr['accountId'],
                'recipientId'   => $optInArr['contactId'],
                'subject'       => $subject,
                'sendDate'      => date("Y-m-d H:i:s"),
                'mailType'      => 'optIn',
                'mailId'        => $mailDetails['id'],
                'recipientType' => $recipientType->contact,
            );

            //check email ending with yahoo.com or aol.com
            $emailAddress = explode("@", $fromAddress);
            $userEmailConfig = $config->userEmail->mail->toArray();

            $replyTo = '';

            if (in_array($emailAddress[1], $userEmailConfig)) {
                $replyTo = $fromAddress;
                $fromAddress = $emailAddress[0] . '.' . $emailAddress[1]
                    . '@rapidfunnel.com';
            }

            // change from email, add first and last name before email
            $fromAddress = $senderName . ' <' . $fromAddress . '>';

            // Send mail
            $status = $libMailgun->sendMail(
                $optInArr['email'], $subject, $mailHtmlBody, $mailTextBody,
                $fromAddress, $customData, $replyTo, $config->mailType->optIn
            );

            return $status;
        } else {
            return false;
        }
    }

    /**
     * Function to check if campaign assigned to contact
     * @param int $contactId
     *
     * @return int | false
     * @throws exception
     */
    public function isCampaignAssignToContact($contactId)
    {
        if(empty($contactId) || !is_numeric($contactId)) {
            throw new InvalidArgumentException('Invalid contactId supplied.');
        }

        $campaignDetails = $this->getCampaignByContact($contactId);

        if (!empty($campaignDetails['accountCampaignId'])) {
            return $campaignDetails['accountCampaignId'];
        } else {
            return false;
        }
    }

    /**
     * Function to send optIn email to contact
     * @param mixed $accountContactModel contactModel
     *
     * @return array
     */
    public function createOptInEmail($accountContactModel)
    {
        $optInArr['firstName']
            = $accountContactModel->firstName;
        $optInArr['lastName']
            = $accountContactModel->lastName;
        $optInArr['contactId'] = isset($this->accountContactId) ?
            $this->accountContactId : $accountContactModel->id;
        $optInArr['accountId']
            = $accountContactModel->accountId;
        $optInArr['email'] = $accountContactModel->email;
        $optInArr['company']
            = isset($accountContactModel->company)
            ? $accountContactModel->company : '';
        $optInArr['title']
            = isset($accountContactModel->title)
            ? $accountContactModel->title : '';

        $contactPhone = array();
        if (isset($accountContactModel->phone)) {
            array_push(
                $contactPhone,
                $accountContactModel->phone
            );
        }
        if (isset($accountContactModel->home)) {
            array_push(
                $contactPhone,
                $accountContactModel->home
            );
        }
        if (isset($accountContactModel->work)) {
            array_push(
                $contactPhone,
                $accountContactModel->work
            );
        }
        if (isset($accountContactModel->other)) {
            array_push(
                $contactPhone,
                $accountContactModel->other
            );
        }

        $contactPhone
            = $this->cmnHelper->concatMultiField(
            $contactPhone
        );
        $optInArr['contactPhone'] = $contactPhone;

        $userModel
            = new RapidFunnel_Model_AccountUser();
        $userModel->load(
            $accountContactModel->createdBy
        );
        $optInArr['senderFirstName']
            = $userModel->firstName;
        $optInArr['repId']
            = isset($userModel->repId) ?
            $userModel->repId : '';
        $optInArr['repId2']
            = isset($userModel->repId2) ?
            $userModel->repId2 : '';
        $optInArr['senderLastName']
            = $userModel->lastName;
        $optInArr['senderEmail']
            = $userModel->email;
        $optInArr['senderFacebookUrl']
            = isset($userModel->facebookUrl) ?
            $userModel->facebookUrl : '';
        $optInArr['senderTwitterUrl']
            = isset($userModel->twitterUrl) ?
            $userModel->twitterUrl : '';
        $optInArr['senderInstagramUrl']
            = isset($userModel->instagramUrl) ?
            $userModel->instagramUrl : '';
        $optInArr['senderId'] = $userModel->id;
        $optInArr['phone']
            = isset($userModel->phoneNumber) ?
            $userModel->phoneNumber : '';

        $optInArr['accountCampaignId']
            = $accountContactModel->campaignId;
        $accountModel = new RapidFunnel_Model_Account();
        $accountModel->load($accountContactModel->accountId
        );
        $optInArr['accountName'] = $accountModel->name;
        $optInArr['accountCompanyName']
            = isset($accountModel->companyName)
            ? $accountModel->companyName : '';

        return $optInArr;
    }

    /**
     * RapidFunnel_Model_AccountCampaignContact::saveCampaignChanges()
     *
     * @param int    $contactId           This is unique id for a contact
     * @param int    $campaignId          Id which is used to manage contact's
     *                                     campaign details
     * @param object $accountContactModel This object of contact's latest values
     * @param array  $contactVars         This is a array of contact details
     * @param bool   $isValidEmail        true if email is valid else false
     * @param int $campaignAssign          tell what kind of campaign assignment
     *                                     it is
     *
     * @return bool
     */
    public function saveCampaignChanges(
        $contactId, $campaignId, $accountContactModel,
        $contactVars, $isValidEmail, $campaignAssign
    ) {
        try {
            $config = RapidFunnel_Configs::_instance();
            $contactStatusConfig = $config->accountContactStatus;
            $campaignAssignConfig = $config->campaignAssigning;
            //clear the log from accountCampaignContactEmail
            $this->db->delete(
                'accountCampaignContactEmail',
                'contactId = ' . $contactId
            );

            $status = $accountContactModel->update();

            if ($status) {
                //do only when campaign is changed
                //OptIn Mail only send when optIn is 0

                if ($isValidEmail
                    && '0' != $campaignId
                ) {
                    if ($contactStatusConfig->notOptIn ===
                        $accountContactModel->optIn
                    ) {
                        //create optIn email
                        $optInArr = $this->createOptInEmail(
                            $accountContactModel
                        );

                        if ($this->sendOptInEmail($optInArr)) {
                            $accountContactModel->optInSend = 1;
                            $accountContactModel->status
                                = $contactStatusConfig->new;

                            $accountContactModel->optInSendDate
                                = date('y-m-d H:i:s');
                        }
                    } else {
                        $accCamContEmailModel
                            = new RapidFunnel_Model_AccountCampaignContactEmail(
                        );

                        $accountId = $accountContactModel->accountId;
                        $created = date("Y-m-d H:i:s");

                        //get emails of campaign
                        $accCamEmailModel
                            = new RapidFunnel_Model_AccountCampaignEmail(
                        );
                        $emails = $accCamEmailModel->getMailsOfCampaign(
                            $campaignId, true
                        );

                        if (0 < count($emails)) {
                            //calculate the sendOnDate and update
                            // the accountCampaignContactEmail table
                            $insertQuery = '';
                            foreach ($emails as $email) {
                                // "On day 1"  means same day (+0 days)
                                if (0 < $email['days']) {
                                    $days = ($email['days'] - 1);
                                } else {
                                    $days = 0;
                                }

                                $emailId = $email['id'];
                                $sendOnDate = date(
                                    'Y-m-d', strtotime(
                                        date('Y-m-d') . "+" . $days
                                        . " days"
                                    )
                                );

                                //form the insert query
                                $insertQuery .= '('
                                    . $accCamContEmailModel->db->quote(
                                        $accountId
                                    ) . ','
                                    . $accCamContEmailModel->db->quote(
                                        $campaignId
                                    ) . ','
                                    . $accCamContEmailModel->db->quote(
                                        $contactId
                                    ) . ','
                                    . $accCamContEmailModel->db->quote(
                                        $emailId
                                    ) . ','
                                    . $accCamContEmailModel->db->quote(
                                        $sendOnDate
                                    ) . ','
                                    . $accCamContEmailModel->db->quote(
                                        $created
                                    ) . '),';
                            }

                            //update the table
                            $insertQuery = rtrim($insertQuery, ",");
                            $accCamContEmailModel->db->query(
                                'INSERT INTO `accountCampaignContactEmail` '
                                . '(`accountId`, `campaignId`, `contactId`, '
                                . '`emailId`, `sendOnDate`, `created`) VALUES '
                                . $insertQuery
                            );

                            //send campaign mail for 1st mail
                            $accCamContEmailModel->campaignSendMail(
                                $contactId, $accountId
                            );
                        }
                    }

                    // Update contact to released if any pre-released
                    // with same email
                    $accountContactModel->releaseContact(
                        $contactVars['email'],
                        $contactVars['accountId'], $contactId
                    );

                    // If contacts status was PreRelease before its
                    // campaign was changed, then change it to New.
                    // To make sure its not treated as 20 days old.
                    if ($contactStatusConfig->preRelease
                        === $contactVars['status']
                    ) {
                        $accountContactModel->setStatusAsNew(
                            $contactVars['email']
                        );
                    }

                    $accountContactModel->load($contactId,
                        array('status', 'optIn'));
                    // check if optIn is 0 and status is 0 update status to 1
                    if ($contactStatusConfig->notSent
                        === $accountContactModel->status
                        && $contactStatusConfig->notOptIn
                        === $accountContactModel->optIn
                    ) {
                        $accountContactModel->status
                            = $contactStatusConfig->new;
                    }
                }

                //update account table
                $accountContactModel->update();

                $returnResult = 'true';
            } else {
                if ($campaignAssignConfig->oldToNone == $campaignAssign) {
                    $returnResult = 'true';
                } else {
                    $returnResult = 'error';
                }
            }
            return $returnResult;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());

            return false;
        }
    }
}
