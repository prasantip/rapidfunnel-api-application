<?php
/**
 * RapidFunnel_Model_AccountCampaignEmail File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account/campaign-email
 */

/**
 * RapidFunnel_Model_AccountCampaignEmail
 * RapidFunnel/Model/AccountCampaignEmail.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account/campaign-email
 */
class RapidFunnel_Model_AccountCampaignEmail extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $campaignId;
    protected $title;
    protected $content;
    protected $textContent;
    protected $type =  0;
    protected $days;
    protected $status;
    protected $isDeleted = '0';
    protected $created;
    protected $createdBy;
    protected $modified;
    protected $modifiedBy;
    public $dtHelper;

    /**
     * RapidFunnel_Model_AccountCampaignEmail::__construct()
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the database by
     * setting the appropriate table, database adapter and a query helper.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountCampaignEmail());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
    }

    /**
     * RapidFunnel_Model_AccountCampaignEmail::getMailsOfCampaign()
     *
     * To get all the mails of the campaign
     *
     * @param int $campaignId campaign id
     * @param boolean $flag
     *
     * @return array, boolean false in case of error
     */
    Public function getMailsOfCampaign($campaignId, $flag = null)
    {
        try {


            $select = $this->db->select()->from(
                'accountCampaignEmail',
                array('id', 'title', 'content', 'textContent', 'status', 'days')
            )->where('campaignId = ?', $campaignId)->where(
                'isDeleted = ?', '0');
                if($flag) {
                    $campaignEmailConfig = RapidFunnel_Configs::_instance(
                        'campaign'
                    )->email;
                    $select->where('type != ?', $campaignEmailConfig->optIn);
                }

            $select->order('days');

            $stmt = $this->db->query($select);

            return $stmt->fetchAll();
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());

            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaignEmail::getOtherMailsOfCampaign()
     *
     * To get all others mails of a campaign
     *
     * @param int    $campaignId campaign id
     * @param int    $type       email type
     *
     * @return boolean
     */
    Public function getOtherMailsOfCampaign($campaignId, $type = null)
    {
        $campEmailOptInType = RapidFunnel_Configs::_instance('campaign')->email;

        try {
            $select = $this->db->select()
                ->from(
                    'accountCampaignEmail',
                    array('id', 'title', 'content', 'status',
                          'textContent', 'days')
                )
                ->where('campaignId = ?', $campaignId)
                ->where('isDeleted = ?', '0')
                ->order('days');

            if (isset($type) && $campEmailOptInType->optIn === $type) {
                $campEmailOptInType = RapidFunnel_Configs::_instance('campaign')
                    ->email;
                $select->where('type = ?', $type)
                        ->where('status = ?', $campEmailOptInType->published);

                return $this->db->fetchRow($select);
            } else {
                return $this->db->query($select)->fetchAll();
            }

        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

}
