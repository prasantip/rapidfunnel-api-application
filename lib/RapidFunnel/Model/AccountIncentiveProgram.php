<?php
/**
 * RapidFunnel_Model_AccountIncentiveProgram File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_AccountIncentiveProgram
 * RapidFunnel/Model/AccountIncentiveProgram.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_AccountIncentiveProgram
    extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountId;
    protected $name;
    protected $accountAwardTypeId;
    protected $startDate;
    protected $endDate;
    protected $leadsGenerated;
    protected $award;
    protected $goal;
    protected $topPercentage;
    protected $subject;
    protected $emailContent;
    protected $from;
    public $dtHelper;
    public $incentiveTypeIds;

    /**
     * RapidFunnel_Model_AccountIncentiveProgram::__construct()
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the database by
     * setting the appropriate table, database adapter and a query helper.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountIncentiveProgram());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
        $this->incentiveTypeIds = RapidFunnel_Configs::_instance(
            'accountIncentiveTypeIds'
        )->toArray();
    }

    /**
     * RapidFunnel_Model_AccountIncentiveProgram::getIncentivesByRunningStatus()
     *
     * Fetch the incentives by running status basing on requirement
     *
     * @param int    $accountId     account id
     * @param string $runningStatus current incentive or past incentive
     * @param int    $userId        user id
     *
     * @return array array of incentives available as per the running status
     * @throws Exception
     */
    public function getIncentivesByRunningStatus(
        $accountId, $userId, $runningStatus = '1'
    ) {
        try {
            $todayDate = date("Y-m-d");
            $select = $this->table->select()->setIntegrityCheck(false)
                ->from(
                    array('au' => 'accountUser'), array()
                )
                ->join(
                    array('aip' => 'accountIncentiveProgram'),
                    'au.accountId = aip.accountId', array(
                        'incentiveId' => 'aip.id', 'name',
                        'startDate'   => 'DATE_FORMAT(startDate,"%m/%d/%Y")',
                        'endDate'     => 'DATE_FORMAT(endDate,"%m/%d/%Y")',
                        'award',
                        'goal', 'topPercentage'
                    )
                )
                ->join(
                    array('aug' => 'accountUserGroup'),
                    'aug.accountUserId = au.id', array()
                )
                ->joinLeft(
                    array('aig' => 'accountIncentiveGroup'),
                    'aig.accountIncentiveProgramId = aip.id', array()
                )
                ->join(
                    array('aat' => 'accountAwardType'),
                    'aat.id = aip.accountAwardTypeId',
                    array('awardTypeId' => 'id', 'awardType' => 'name')
                )
                ->where('aip.accountId = ?', $accountId)
                ->where('au.id = ?', $userId)
                ->where(
                    'IF( aig.accountGroupId IS NOT NULL, ' .
                    'aig.accountGroupId = aug.accountGroupId, 1)'
                );

            //When fetching inactive incentives
            if ('0' == $runningStatus) {
                $select->where(
                    "aip.endDate < '$todayDate' AND aip.endDate != '0000-00-00'"
                );
            } else {
                //If the running status is active means the award is running
                $select
                    ->where(
                        "aip.endDate >=  '$todayDate' OR aip.endDate = '0000-00-00'"
                    )
                    ->where('aip.startDate <= ?', $todayDate);
            }
            $select->group(['aip.id']);

            $result = $this->db->fetchAll($select);
        } catch (Exception $e) {
            error_log($e->getMessage());
            throw new Exception($e->getMessage());
        }

        return $result;
    }

}
