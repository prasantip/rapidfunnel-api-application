<?php
/**
 * RapidFunnel_Model_AccountSystemEmails
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/system-emails
 */

/**
 * RapidFunnel_Model_AccountSystemEmails
 * RapidFunnel/Model/AccountSystemEmails.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/system-emails
 */
class RapidFunnel_Model_AccountSystemEmails extends RapidFunnel_Model_Abstract
{

    public $db;
    public $dtHelper;
    protected $id;
    protected $accountId;
    protected $type;
    protected $description;
    protected $status;
    protected $subject;
    protected $htmlBody;
    protected $textBody;
    protected $createdBy;
    protected $modifiedBy;
    protected $days;

    /**
     * Constructor method for the RapidFunnel_Model_AccountSystemEmails
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the database by
     * setting the appropriate table, database adapter and a query helper.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountSystemEmails());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
    }

    /**
     * RapidFunnel_Model_AccountSystemEmails::getEmailDetails()
     *
     * Functionality to get system emails details
     *
     * @param int $accountId is an unique id of a group under which
     *                       this user belong to
     * @param int $type      type of the system emails
     *
     * @return array with id
     */
    public function getEmailDetails($accountId, $type)
    {
        try {
            $select = $this->db->select()
                ->from(
                    array('se' => 'accountSystemEmails'),
                    array('id', 'subject', 'htmlBody',
                          'textBody', 'accountId', 'status', 'type')
                )
                ->where('se.type = ?', $type)
                ->where(
                    'se.accountId = "' . $accountId . '" or se.accountId = "0"'
                )
                ->where('se.status = "2" or se.status = "1"')
                ->order(new zend_db_expr("accountId = 0 ASC"));

            $qry = $this->db->query($select);
            $row = $qry->fetchAll();

            // if custom mail found, send that. else default mail
            $selectedMail = '';
            if (!empty($row)) {
                $selectedMail = $row[0];
            }

            return $selectedMail;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

}
