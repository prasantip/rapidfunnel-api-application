<?php
/**
 * RapidFunnel_Model_AccountCampaignContactEmail
 *
 * PHP version 5
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account
 */

/**
 * RapidFunnel_Model_AccountCampaignContactEmail
 * RapidFunnel/Model/AccountCampaignContactEmail.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account
 */
class RapidFunnel_Model_AccountCampaignContactEmail
    extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountId;
    protected $campaignId;
    protected $contactId;
    protected $emailId;
    protected $sendOnDate;
    protected $created;
    public $cmnHelper;
    public $accountResourcesModel;

    /**
     * Constructor method for the RapidFunnel_Model_AccountCampaignContactEmail
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the database by
     * setting the appropriate table, database adapter and a common helper.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(
            new RapidFunnel_Data_Table_AccountCampaignContactEmail()
        );
        $this->db = $this->table->getAdapter();
        $this->cmnHelper = new RapidFunnel_Controller_Action_Helper_Common();
        $this->accountResourcesModel = new RapidFunnel_Model_AccountResources();
    }

    /**
     * RapidFunnel_Model_AccountCampaignContactEmail::getMails()
     *
     * Functionality to get mails according to the conditions
     *
     * @param array $fields     contains the filed names which we want
     *                          to fetch from the accountCampaignEmail
     * @param array $conditions is an associative array contains conditions
     *
     * @return array of values
     */
    Public function getMails($fields = array(), $conditions = array())
    {
        try {
            $select = $this->db->select()
                ->joinInner(
                    array('ac' => 'accountCampaign'),
                    'ac.id = ace.campaignId', array()
                )
                ->joinInner(
                    array('acce' => 'accountCampaignContactEmail'),
                    'acce.emailId = ace.id', array()
                )
                ->joinInner(
                    array('a' => 'account'),
                    'a.id = acce.accountId', array()
                )
                ->joinInner(
                    array('aco' => 'accountContact'),
                    'acce.contactId = aco.id AND acce.contactId = aco.id',
                    array()
                )
                ->joinInner(
                    array('au' => 'accountUser'),
                    'aco.createdBy = au.id', array()
                )
                ->where('ace.isDeleted = ?', '0')
                ->where('ace.status = ?', 'published')
                ->where('ac.status = ?', '1');

            if (is_array($fields) && !empty($fields)) {
                $select->from(array('ace' => 'accountCampaignEmail'), $fields);
            } else {
                $select->from(array('ace' => 'accountCampaignEmail'));
            }

            if (is_array($conditions) && !empty($conditions)) {
                foreach ($conditions as $key => $val) {
                    $select->where($key, $val);
                }
            }

            $stmt = $this->db->query($select);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaignContactEmail::markMailsAsSend()
     *
     * Functionality to mark mails as already sent
     *
     * @param array $updateArr an array of id and sendStatus to
     *                         insert a new row in accountCampaignContactEmail
     *
     * @return true if row inserted successfully else false
     */
    Public function markMailsAsSend($updateArr)
    {
        try {
            $insertQuery
                = 'INSERT INTO accountCampaignContactEmail (id,sendStatus)
                    VALUES ' . implode(',', $updateArr) . ' ON DUPLICATE KEY UPDATE
                    sendStatus=VALUES(sendStatus)';

            $this->db->query($insertQuery);
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountCampaignContactEmail::campaignSendMail()
     *
     * Functionality to send campaign email to user
     *
     * @param int $contactId used as conditions for getMails()
     * @param int $accountId used as conditions for getMails()
     *
     * @return boolean
     */
    public function campaignSendMail($contactId, $accountId)
    {
        $ret = false;

        $urlHelper = new RapidFunnel_Controller_Action_Helper_Url();
        $recipientType = RapidFunnel_Configs::_instance(
            'broadcastEmail'
        )->recipientType;

        $placeHolders = RapidFunnel_Configs::_instance(
            'contentPlaceholders'
        );
        // creating array for replacing content placeholder variable
        $templateVars = array($placeHolders->title, $placeHolders->firstName,
                              $placeHolders->lastName, $placeHolders->company,
                              $placeHolders->email,
                              $placeHolders->senderFirstName,
                              $placeHolders->senderLastName,
                              $placeHolders->senderEmail,
                              $placeHolders->senderPhone, $placeHolders->userId,
                              $placeHolders->repId, $placeHolders->contactPhone,
                              $placeHolders->repId2,
                              $placeHolders->contactId
                              );

        //get all the mails to be send on today.
        $mails = $this->getMails(
            array(
                'aco.firstName', 'aco.lastName', 'aco.email',
                'contactTitle'                         => 'aco.title',
                'a.companyName as company',
                'acce.emailId', 'acce.contactId', 'acce.accountId',
                'aco.campaignId as accountCampaignId', 'acce.sendOnDate',
                'ace.title', 'ace.content', 'ace.textContent', 'ace.days',
                'accCamConEmailId'                     => 'acce.id',
                'senderFirstName'                      => 'au.firstName',
                'senderLastName'                       => 'au.lastName',
                'senderEmailId'                        => 'au.email',
                'senderId'                             => 'au.id',
                'senderPhoneNumber'                    => 'au.phoneNumber',
                'ac.sendFromEmail',
                'contactMobile'                        => 'aco.phone',
                'contactHome'                          => 'aco.home',
                'contactWork'                          => 'aco.work',
                'contactOther'                         => 'aco.other',
                'au.linkTrackingNotify', 'senderRepId' => 'au.repId',
                'senderRepId2'                         => 'au.repId2',
                'senderFacebookUrl'                    => 'au.facebookUrl',
                'senderTwitterUrl'                     => 'au.twitterUrl',
                'senderInstagramUrl'                   => 'au.instagramUrl'
            ), array(
                'sendOnDate <= ?'     => date('Y-m-d'),
                'acce.sendStatus = ?' => '0', 'aco.optOut != ?' => '1',
                'acce.contactId = ?'  => $contactId,
                'acce.accountId = ?'  => $accountId,
            )
        );

        if (!empty($mails) && is_array($mails)) {
            /* Group all the contact grouped by email to
             send using batch mail method*/
            $markAsSentArr = array();
            $resultArray = array();
            foreach ($mails as $data) {
                $id = $data['contactId'];
                if (isset($resultArray[$id])) {
                    $resultArray[$id][] = $data;
                } else {
                    $resultArray[$id] = array($data);
                }
            }

            /* removed 4th or more mail for contact if exist
                and create new array of mails*/
            $newMailsArray = array();
            foreach ($resultArray as $key => $value) {
                foreach ($value as $key => $values) {
                    if (3 < sizeof($value)) {
                        if (2 < $key) {
                            array_push(
                                $markAsSentArr,
                                "('" . $values['accCamConEmailId'] . "','2')"
                            );
                        } else {
                            array_push($newMailsArray, $values);
                        }
                    } else {
                        array_push($newMailsArray, $values);
                    }
                }
            }

            $contactPhone = $this->constructNewMail(
                $newMailsArray
            );

            foreach ($newMailsArray as $key => $value) {
                $newMailsArray[$key]['contactPhone']
                    = $this->cmnHelper->concatMultiField($contactPhone);

            }

            $value['senderEmailId'] = !empty($value['sendFromEmail']) ?
                $value['sendFromEmail'] : $value['senderEmailId'];

            foreach ($newMailsArray as $key => $value) {
                $senderPhone = isset($value["senderPhoneNumber"])
                    ? $value["senderPhoneNumber"] : '';
                $senderFacebookUrl = isset($value['senderFacebookUrl'])
                    ? $value['senderFacebookUrl'] : '';
                $senderTwitterUrl = isset($value['senderTwitterUrl'])
                    ? $value['senderTwitterUrl'] : '';
                $senderInstagramUrl = isset($value['senderInstagramUrl'])
                    ? $value['senderInstagramUrl'] : '';

                $replaceTemplateVars = array(
                    $value["contactTitle"], $value["firstName"],
                    $value["lastName"], $value["company"], $value["email"],
                    $value["senderFirstName"], $value["senderLastName"],
                    $value["senderEmailId"], $senderPhone, $value["senderId"],
                    $value["senderRepId"], $value["contactPhone"],
                    $value["senderRepId2"], $contactId
                );

                $value['content'] = str_replace(
                    $templateVars, $replaceTemplateVars, $value['content']
                );
                $value['textContent'] = str_replace(
                    $templateVars, $replaceTemplateVars, $value['textContent']
                );

                $content = $value['content'];

                /* Funtionality to change the links in the campaign
                   email to a link which can send notification to sender user
                   once the contact clicks on any link on the email
                   to change the urls to content in content */
                if ('1' === $value['linkTrackingNotify']) {
                    $value['content'] = $urlHelper->changeResourceUrlsInContent(
                        $value['senderId'], $value['content'], $value['email'],
                        false
                    );
                    $value['textContent']
                        = $urlHelper->changeResourceUrlsInContent(
                        $value['senderId'], $value['textContent'],
                        $value['email'],
                        false
                    );
                }

                // create unsubscribe code to use in unsubscribe link
                $value['unsubscribeCode'] = urlencode(
                    base64_encode($value['contactId'])
                );
                $config = RapidFunnel_Configs::_instance();
                $serverDomain = $config->serverInfo->domain;
                $unsubscribeMessages = $config->messages->unsubscribe;

                // add unsubscribe link and domain name
                $unsubscribeHtml = '<br /><br />' . str_replace(
                        array('<%serverDomain%>', '<%unsubscribeCode%>'),
                        array($serverDomain, $value['unsubscribeCode']),
                        $unsubscribeMessages->html
                    );
                $mailHtmlBody = $value['content'] . $unsubscribeHtml;

                //create text message
                $unsubscribeText = "\r\n\r\n" . str_replace(
                        array('<%serverDomain%>', '<%unsubscribeCode%>'),
                        array($serverDomain, $value['unsubscribeCode']),
                        $unsubscribeMessages->text
                    );
                $mailTextBody = $value['textContent'] . $unsubscribeText;

                // remove http:// from social URL
                // replace social media url
                $templateHttpVars = array('http://' . $placeHolders->facebookUrl,
                                          'http://' . $placeHolders->twitterUrl,
                                          'http://' . $placeHolders->instagramUrl);

                // replace social media url
                $replaceHttpVars = array($placeHolders->facebookUrl,
                                         $placeHolders->twitterUrl, $placeHolders->instagramUrl);

                $mailHtmlBody = str_replace(
                    $templateHttpVars, $replaceHttpVars, $mailHtmlBody
                );

                $mailTextBody = str_replace(
                    $templateHttpVars, $replaceHttpVars, $mailTextBody
                );

                // remove links if social media values are empty
                $commonHelper = new RapidFunnel_Controller_Action_Helper_Common();
                if(empty($senderInstagramUrl)) {
                    $mailHtmlBody
                        = $commonHelper->removeHyperLinkInEmail(
                        $placeHolders->instagramUrl, $mailHtmlBody
                    );
                }

                if(empty($senderFacebookUrl)) {
                    $mailHtmlBody
                        = $commonHelper->removeHyperLinkInEmail(
                        $placeHolders->facebookUrl, $mailHtmlBody
                    );
                }

                if(empty($senderTwitterUrl)) {
                    $mailHtmlBody
                        = $commonHelper->removeHyperLinkInEmail(
                        $placeHolders->twitterUrl, $mailHtmlBody
                    );
                }

                // replace social media url
                $templateMediaVars = array($placeHolders->facebookUrl,
                                           $placeHolders->twitterUrl, $placeHolders->instagramUrl);

                $replaceMediaVars = array(
                    $senderFacebookUrl, $senderTwitterUrl, $senderInstagramUrl
                );

                $mailHtmlBody = str_replace(
                    $templateMediaVars, $replaceMediaVars, $mailHtmlBody
                );

                $mailTextBody = str_replace(
                    $templateMediaVars, $replaceMediaVars, $mailTextBody
                );

                $subject = str_replace(
                    $templateVars, $replaceTemplateVars, $value['title']
                );

                $subject = str_replace(
                    $templateHttpVars, $replaceHttpVars, $subject
                );

                $subject = str_replace(
                    $templateMediaVars, $replaceMediaVars, $subject
                );

                $toEmail = $value['email'];

                $fromEmail = !empty($value['sendFromEmail']) ?
                    $value['sendFromEmail'] : $value['senderEmailId'];

                // Fill Custom Data with type of email and contactId
                $customData = array(
                    'accountId'     => $value['accountId'],
                    'recipientId'   => $value['contactId'],
                    'subject'       => $subject,
                    'sendDate'      => date("Y-m-d H:i:s"),
                    'mailType'      => 'campaign',
                    'mailId'        => $value['emailId'],
                    'recipientType' => $recipientType->contact,
                );

                //check email ending with yahoo.com or aol.com or ymail.com
                $emailAddress = explode("@", $fromEmail);
                $config = RapidFunnel_Configs::_instance();
                $userEmailConfig = $config->userEmail->mail->toArray();
                $replyTo = '';
                $mailType = $config->accountMailLog->toArray();

                if (in_array($emailAddress[1], $userEmailConfig)) {
                    $replyTo = $fromEmail;
                    $fromEmail = $emailAddress[0] . '.' . $emailAddress[1]
                        . '@rapidfunnel.com';
                }

                // change from email, add first and last name before email
                $fromEmail = $value["senderFirstName"] . ' '
                    . $value["senderLastName"] . ' <' . $fromEmail . '>';

                if ('' != $value['senderEmailId']) {
                    // check if account details found then add to email
                    $helper = new RapidFunnel_Controller_Action_Helper_Common();
                    $mailDetails = $helper
                        ->addAccountDetailsInMail(
                            $value['accountId'], $mailHtmlBody, $mailTextBody
                        );

                    if (1 === $mailDetails['status']) {
                        $mailHtmlBody = $mailDetails['htmlContent'];
                        $mailTextBody = $mailDetails['textContent'];
                    }

                    $libMailgun = new RapidFunnel_Email_Mailgun_Mailgun();

                    $previewStatus = $libMailgun->sendMail(
                        $toEmail, $subject, $mailHtmlBody, $mailTextBody,
                        $fromEmail,
                        $customData, $replyTo, $mailType['mailType'][1]
                    );
                }

                if ($previewStatus) {
                    array_push(
                        $markAsSentArr,
                        "('" . $value['accCamConEmailId'] . "','1')"
                    );

                    // Get resources sent through emails and save its details.
                    $resources = $this->accountResourcesModel
                        ->getResourcesSentThroughEmail(
                            $content
                        );

                    if ($resources) {
                        $this->accountResourcesModel
                            ->saveResourcesSentThroughEmail(
                                $resources, $value['accountId']
                            );
                    }
                }
            }
            //update accountCampaignContactEmail for send = 1
            if ($previewStatus) {
                $status = $this->markMailsAsSend($markAsSentArr);
                if ($status) {
                    $ret = true;
                }
            }
        }

        return $ret;
    }

    /**
     * RapidFunnel_Model_AccountCampaignContactEmail::constructNewMail()
     *
     *  Create new placeholder parameter contactPhone by combining
     *     all type of phone number and push to $newMailsArray
     *
     * @param Array $newMailsArray contains content placeholder values
     *
     * @return mixed array contains different types of contacts
     */
    public function constructNewMail($newMailsArray)
    {
        $contactPhone = array();

        foreach ($newMailsArray as $key => $value) {
            if ($key == 'contactMobile'
                && !empty($value["contactMobile"])
            ) {
                array_push($contactPhone, $value['contactMobile']);
            }
            if ($key == 'contactHome'
                && !empty($value["contactHome"])
            ) {
                array_push($contactPhone, $value['contactHome']);
            }
            if ($key == 'contactWork'
                && !empty($value["contactWork"])
            ) {
                array_push($contactPhone, $value['contactWork']);
            }
            if ($key == 'contactOther'
                && !empty($value["contactOther"])
            ) {
                array_push($contactPhone, $value['contactOther']);
            }
        }

        return $contactPhone;
    }

}
