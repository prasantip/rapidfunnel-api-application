<?php
/**
 * RapidFunnel_Model_AccountContact
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/contact
 */

/**
 * RapidFunnel_Model_AccountContact
 * RapidFunnel/Model/AccountContact.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/contact
 */
class RapidFunnel_Model_AccountContact extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $accountId;
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $homeEmail;
    protected $workEmail;
    protected $otherEmail;
    protected $phone;
    protected $mobile;
    protected $home;
    protected $work;
    protected $other;
    protected $address1;
    protected $address2;
    protected $city;
    protected $stateId;
    protected $countryId;
    protected $zip;
    protected $company;
    protected $title;
    protected $interest;
    protected $note;
    protected $channel = 'Web Application';
    protected $created;
    protected $createdBy;
    protected $modifiedBy;
    protected $optIn = 0;
    protected $optInSend = 0;
    protected $optInDate = null;
    protected $optInResend = 0;
    protected $optOut = 0;
    protected $optOutDate = null;
    protected $status = 0;
    protected $customContact;
    protected $campaignId;
    protected $optInSendDate = null;
    protected $campaignAssignTime;
    public $dtHelper;
    public $cmnHelper;

    /**
     * Constructor method for the RapidFunnel_Model_AccountContact
     *
     * Instantiates the parent class by calling its constructor method. Also,
     * configures the model for interactions with the database by setting
     * the appropriate table, database adapter, query helper and common helper.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountContact());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
        $this->cmnHelper = new RapidFunnel_Controller_Action_Helper_Common();
    }

    /**
     * Functionality to check if the contact is a duplicate(based on
     * campaign assigned and status)
     *
     * @param int    $accountId          id for which contact belongs to
     * @param string $email              email address of contact
     * @param int    $createdBy          user id associated with creation
     *                                   of contact
     * @param int    $contactId          contact id
     * @param bool   $isCampaignAssigned campaign is assigned or not
     *
     * @return bool
     * @throws exception
     */
    public function isDuplicateContactOrCampaignAssigned($accountId, $email,
        $createdBy = null, $contactId = null, $isCampaignAssigned = null)
    {
        if(empty($accountId) || !is_numeric($accountId)) {
            throw new InvalidArgumentException('Invalid accountId supplied.');
        }

        if(empty($email) || !is_string($email)) {
            throw new InvalidArgumentException('Invalid email supplied.');
        }
        try {
            $accountContactStatus = RapidFunnel_Configs::_instance(
                'accountContactStatus'
            );

            $select = $this->db->select()
                ->from(array('ac' => 'accountContact'), array('id'));

            if ($isCampaignAssigned) {
                $select->where(
                    '(ac.campaignId IS NOT NULL) AND ac.campaignId != 0 '
                    . 'OR ac.status =' . $accountContactStatus->active
                );
            }

            if($contactId) {
                $select->where('ac.id != ?', $contactId);
            }

            if($createdBy) {
                $select->where('ac.createdBy = ?', $createdBy);
            }

            $select->where(
                "ac.status != $accountContactStatus->preRelease "
                . "AND ac.status != $accountContactStatus->unsubscribed"
            )
                ->where('ac.email = ?', $email)
                ->where('ac.accountId = ?', $accountId);

            $qry = $this->db->query($select);
            $row = $qry->fetchAll();

            // if 1 : contact exist else 0
            if ($row) {
                $return = 1;
            } else {
                $return = 0;
            }

            return $return;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * Functionality to get all the contact associated with an account
     *
     * @param int    $accountId    is an unique id of a group under which this
     *                             user belong to
     * @param int    $userId       unique integer value created when the new user
     *                             created
     * @param int    $roleId       is used to specify get the contacts of which
     *                             type of user like (user, admin, manager..etc)
     *                             is created when the user created
     * @param array  $groupIds     contains the group
     *                             ids (accountUserGroup.accountGroupID)
     * @param string $searchKey    search contact with this key
     * @param bool   $isApi        is calling from API
     *
     * @return array | bool all contact details associated with account
     */
    public function getContact($accountId, $userId, $roleId, $groupIds = null,
        $searchKey = null, $isApi = null
    ) {
        $config = RapidFunnel_Configs::_instance();
        $accountRoles = $config->accountRoles;

        try {
            //Select account contact details with first note created by contact
            //So that it will be compatible with current version of API
            //having single note
            $select = $this->table->select()
                ->setIntegrityCheck(false)
                ->from(
                    array(
                        'ac' => 'accountContact'
                    ), array(
                        'id', 'accountId', 'firstName', 'lastName', 'email',
                        'homeEmail', 'workEmail', 'otherEmail', 'phone', 'home',
                        'work', 'other', 'mobile', 'state' => 'stateId', 'zip', 'title',
                        'status', 'created'
                    )
                )
                ->joinLeft(
                    array('acn' => 'accountContactNotes'),
                    'acn.accountContactId = ac.id', array(
                        'noteTimeStamp',
                        'note' => 'contactNote'
                    )
                )
                ->joinLeft(
                    array('bcn' => 'accountContactNotes'),
                    'acn.accountContactId = bcn.accountContactId AND '
                    . '(acn.noteTimeStamp < bcn.noteTimeStamp '
                    . 'OR (acn.noteTimeStamp = bcn.noteTimeStamp '
                    . 'AND acn.accountContactId < bcn.accountContactId))',
                    array('')
                )
                ->where('bcn.accountContactId IS NULL');
            // for roles user and admin
            if ($roleId != $accountRoles->manager) {
                $select
                    ->joinLeft(
                        array('acam' => 'accountCampaign'),
                        'ac.campaignId = acam.id',
                        array(
                            'campaignName' => 'acam.name',
                            'campaignId' => 'acam.id'
                        )
                    );

                if ($roleId == $accountRoles->user) {
                    $select->where('ac.createdBy = ?', $userId);
                } else {
                    $select->where('ac.accountId = ?', $accountId);
                }

            } else {//for role manager
                // get group of manager
                // -> find ancestors of the groups
                // -> find all users of these groups
                // -> find all contacts of these groups
                $select
                    ->joinLeft(
                        array('acam' => 'accountCampaign'),
                        'ac.campaignId = acam.id',
                        array('campaignId' => 'acam.id')
                    );
                if (!empty($groupIds)) {
                    $select
                        ->joinLeft(
                            array('au' => 'accountUser'),
                            "au.id = ac.createdBy ", array()
                        )
                        ->joinLeft(
                            array('aug' => 'accountUserGroup'),
                            "aug.accountUserId = au.id ", array()
                        )
                        ->where('aug.accountGroupId IN (?)', $groupIds)
                        ->where(
                            'au.roleId != ?', $accountRoles->admin
                        )->where(
                            'ac.accountId = ?', $accountId
                        );
                } else {
                    $select->where('ac.createdBy = ?', $userId)
                        ->where('ac.accountId = ?', $accountId);
                }
            }

            if ($searchKey) {
                $select->where(
                    "ac.email LIKE '$searchKey%'
                    OR ac.firstName LIKE '$searchKey%'
                    OR ac.lastName LIKE '$searchKey%'
                    OR concat(ac.firstName, ' ' ,ac.lastName) LIKE '$searchKey%'"
                );
            }

            $select
                ->group('ac.id');

            if ($isApi) {
                $select->order('created DESC')
                    ->limit($config->contact->limit->api);
            }

            $qry = $this->db->query($select);
            $contactList = $qry->fetchAll();
            return $contactList;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountContact::getDetails()
     *
     * Functionality to get Contact Details
     *
     * @param int $contactId is an unique id(accountId of a group) given when
     *                       the user is created under a group
     *
     * @return array | bool all contact details associated with account
     */
    public function getDetails($contactId)
    {
        try {
            $select = $this->db->select()
                ->from(
                    array('accountContact'), array(
                        'id', 'accountId', 'firstName', 'lastName', 'email',
                        'homeEmail', 'workEmail', 'otherEmail', 'phone', 'home',
                        'work', 'other', 'mobile', 'state' => 'stateId', 'zip', 'title',
                        'note', 'status', 'created'
                    )
                )
                ->joinLeft(
                    array('acn' => $this->table->select()
                        ->setIntegrityCheck(false)
                        ->from(
                            array(
                                'aCN' => 'accountContactNotes'
                            ), array(
                                'noteId'    => 'aCN.id',
                                'note'      => 'aCN.contactNote',
                                'aCN.noteTimeStamp',
                                'contactId' => 'accountContactId'
                            )
                        )
                        ->join(
                            array(
                                'bCN' => $this->table->select()
                                    ->setIntegrityCheck(
                                        false
                                    )->from(
                                        array(
                                            'accountContactNotes'
                                        ), array(
                                            'minId' => 'MIN(id)',
                                            'accountContactId',
                                        )
                                    )->group('accountContactId'),
                            ), 'aCN.id = bCN.minId AND '
                            . 'aCN.accountContactId = bCN.accountContactId',
                            array('minId', 'accountContactId')
                        )
                        ->where('aCN.accountContactId = ?', $contactId)
                    ), 'acn.contactId = accountContact.id',
                    array('note')
                )
                ->where('accountContact.id = ?', $contactId)
                ->joinLeft(
                    array('acam' => 'accountCampaign'),
                    'accountContact.campaignId = acam.id',
                    array(
                        'campaignName' => 'acam.name',
                        'campaignId'   => 'acam.id'
                    )
                );

            $qry = $this->db->query($select);
            $contactDetails = $qry->fetch();
            return $contactDetails;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * Functionality to calculate the opt-in rate of users
     *
     * @param int $accountId account id of user
     * @param int $userId    user's id
     *
     * @return int optIn rate
     */
    public function getOptInRateOfUser($accountId, $userId)
    {
        try{
            if (empty($accountId) || !is_numeric($accountId)) {
                throw new InvalidArgumentException('Invalid accountId argument supplied.');
            }

            if (empty($userId) || !is_numeric($userId)) {
                throw new InvalidArgumentException('Invalid userId argument supplied.');
            }
            $optInPercent = 0;
            //query to check maximum contacts for the account
            $query = $this->db->select()
                ->from(array('ac' => 'accountContact'),
                    array('optInSendContacts' => 'sum(ac.optInSend)',
                          'optedInContacts' => 'sum(ac.optIn)')
                )
                ->where('ac.accountId = ?', $accountId)
                ->where('ac.createdBy = ?', $userId);

            $data = $this->db->fetchRow($query);
            //When opt in data exists
            if (!empty($data) && ($data['optInSendContacts'] > 0)) {
                $optInPercent = (($data['optedInContacts'] * 100) / $data['optInSendContacts']);
            }
            return $optInPercent;

        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());

            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountContact::checkContactLimit()
     *
     * Functionality to check limitation according to account level
     *
     * @param int $accountId    is an unique id of a group under which this
     *                          user belong to
     * @param int $userId is an unique id for user
     *
     * @return array
     */
    public function checkContactLimit($accountId, $userId)
    {
        $returnArray['isAllowed'] = false;

        try {
            $config = RapidFunnel_Configs::_instance();
            $configMessages = $config->messages->maxAllowedContacts;
            $postingCode = $config->accountContact->channel->postingCode;
            $minPercent = $config->maxAllowedContacts->minPercent;
            //To get opt-in rate of user
            $optInRate = $this->getOptInRateOfUser($accountId, $userId);

            //Check maximum number of contacts based on optInRate
            if ($minPercent->min80 <= $optInRate) {
                $maxAllowedContacts = $config->maxAllowedContacts->optInRate80;
            } elseif ($minPercent->min60 <= $optInRate) {
                $maxAllowedContacts = $config->maxAllowedContacts->optInRate60;
            } else {
                $maxAllowedContacts = $config->maxAllowedContacts->default;
            }

            //query to check maximum contacts for the account
            $select = $this->db->select()
                ->from(
                    array('a' => 'account'), array()
                )
                ->Join(
                    array('al' => 'accountLevel'), 'a.accountLevel = al.id',
                    array('maxContacts')
                )
                ->JoinLeft(
                    array('ac' => 'accountContact'), 'ac.accountId = a.id',
                    array('contactCount' => 'count(ac.id)')
                )
                ->where('a.id = ?', $accountId)
                ->where("ac.channel != '" . $postingCode . "'");

            $stmt = $this->db->query($select);
            $row = $stmt->fetch();

            // max contact 0 = unlimited contacts
            if ((0 == $row['maxContacts'])
                || (0 < ($row['maxContacts'] - $row['contactCount']))
            ) {
                //Functionality to check for this user
                $select = $this->db->select()
                    ->from(
                        array('ac' => 'accountContact'), array('id',
                        'campaignAssignTime')
                    )
                    ->where('ac.createdBy = ?', $userId)
                    ->where("ac.channel != '" . $postingCode . "'")
                    ->where('ac.campaignAssignTime IS NOT NULL')
                    ->order('ac.campaignAssignTime DESC')
                    ->limit($maxAllowedContacts->week);

                $stmt = $this->db->query($select);
                $rows = $stmt->fetchAll();

                //If some records fetched
                if (is_array($rows) && (0 < count($rows))) {
                    //As the data are already sorted so just do check
                    $allowedContacts = $maxAllowedContacts->toArray();

                    foreach ($allowedContacts as $noOfContacts) {
                        $exitForEach['status'] = false;

                        //When the last record is not present
                        if (isset($rows[$noOfContacts - 1])) {
                            $diffInMinute = round(
                                abs(
                                    time() - strtotime(
                                        $rows[$noOfContacts - 1]
                                        ['campaignAssignTime']
                                    )
                                ) / 60, 2
                            );

                            switch ($noOfContacts) {
                                case $maxAllowedContacts->hour:
                                    //check using minute
                                    if (60 >= $diffInMinute) {
                                        $exitForEach['time'] = '60 minutes';
                                        $exitForEach['contactCount']
                                            = $maxAllowedContacts->hour;
                                        $exitForEach['status'] = true;
                                    }
                                    break;
                                case $maxAllowedContacts->day:
                                    //check using hour for the day(1440minutes)
                                    if (1440 >= $diffInMinute) {
                                        $exitForEach['time'] = '24 hours';
                                        $exitForEach['contactCount']
                                            = $maxAllowedContacts->day;
                                        $exitForEach['status'] = true;
                                    }
                                    break;
                                case $maxAllowedContacts->week:
                                    //check using day for the week(10080minutes)
                                    if (10080 >= $diffInMinute) {
                                        $exitForEach['time'] = '7 days';
                                        $exitForEach['contactCount']
                                            = $maxAllowedContacts->week;
                                        $exitForEach['status'] = true;
                                    }
                                    break;
                            }

                            //if exitforeach then go out of foreach
                            if ($exitForEach['status']) {
                                $returnArray['errorMessage'] =
                                    str_replace(array("<contact>", "<time>"),
                                        array($exitForEach['contactCount'],
                                              $exitForEach['time']),
                                        $configMessages->time);


                                $returnArray['isAllowed'] = false;
                                break;
                            }
                        }
                        $returnArray['isAllowed'] = true;
                    }
                } else {
                    $returnArray['isAllowed'] = true;
                }
            } else {
                $returnArray['errorMessage'] = $configMessages->account;
            }

            return $returnArray;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());

            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountContact::getContactForWeekMonthPreMonth()
     *
     * Functionality To get all the contact created by user for current week,
     *               current month and previous month
     *
     * @param int $userId     unique integer value created when the user
     *                        account created
     *
     * @return array of contact details of current week,
     *                  current month and previous month
     *
     * @throws Exception
     */
    public function getContactForWeekMonthPreMonth($userId)
    {
        if (!is_numeric($userId) || empty($userId)) {
            throw new InvalidArgumentException(
                'Invalid $userId argument supplied.'
            );
        }

        try {
            $select = $this->table->select()->setIntegrityCheck(false)
                ->from(
                    array(
                        'ac' => 'accountContact'), array('SUM(DATE_FORMAT(created, "%Y%m") =
                        DATE_FORMAT(CURDATE(),"%Y%m")) "ContactCurMonth",
                    SUM(DATE_FORMAT(created, "%Y%m") =
                        DATE_FORMAT( CURDATE() - INTERVAL 1 MONTH, "%Y%m" )) "ContactLastMonth",
                    SUM(YEARWEEK(created) = YEARWEEK(CURDATE())) "ContactCurWeek",
		            COUNT(id) "TotalContact"')
                )
                ->where('ac.createdBy = ?', $userId);

            $qry = $this->db->query($select);
            $contactList = $qry->fetchAll();

            return $contactList;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

    /**
     * RapidFunnel_Model_AccountContact::releaseContact()
     *
     * This function is used to change the status of contact from
     * Pre Release to Released, if the contact has been added by
     * another user.
     *
     * @param string $emailId   Email id of the contact, whose status
     *                          is being changed.
     * @param int    $accountId account association id
     * @param int    $contactId id of the contact to be skipped from releasing
     *
     * @return bool
     * @throws Exception
     */
    public function releaseContact($emailId, $accountId, $contactId)
    {
        if (!empty($emailId)) {
            $config = RapidFunnel_Configs::_instance();
            $accountContactStatus = $config->accountContactStatus;
            try {
                $this->db->update(
                    'accountContact',
                    array(
                        'status' => $accountContactStatus->released,
                        'campaignId' => null,
                        'campaignAssignTime' => null
                    ),
                    array(
                        'email = ?' => trim($emailId),
                        'status = ?' => $accountContactStatus->preRelease,
                        'accountId = ?' => $accountId,
                        'id != ?' => $contactId
                    )
                );

                return true;
            } catch (Exception $exception) {
                error_log($exception->getMessage());
                return false;
            }
        }
    }

    /**
     * RapidFunnel_Model_AccountContact::setStatusAsNew()
     *
     * This function is used to change the status of contact from
     * Pre Release to New, if the contacts Email is changed.
     *
     * @param string $emailId Email id of the contact, whose status
     *                        is being changed.
     *
     * @return bool
     * @throws Exception
     */
    public function setStatusAsNew($emailId)
    {
        if (!empty($emailId)) {
            $config = RapidFunnel_Configs::_instance();
            $accountContactStatus = $config->accountContactStatus;
            try {
                $this->db->update(
                    'accountContact',
                    array(
                        'status'        => $accountContactStatus->new
                    ),
                    array(
                        'email = ?' => $emailId,
                        'status != 0',
                        'status != 1'
                    )
                );

                return true;
            } catch (Exception $exception) {
                error_log($exception->getMessage());
                return false;
            }
        }
    }

    /**
     * To delete the contact and related data Permanently from DB
     *
     * @param int $contactId contact Id
     *
     * @return boolean
     * @throws Exception
     */
    public function deleteContact($contactId)
    {
        if (empty($contactId) || !is_numeric($contactId)) {
            throw new InvalidArgumentException('Invalid $userId argument supplied.');
        }

        try {
            $recipientTypeConfig = RapidFunnel_Configs::_instance('broadcastEmail')
                ->recipientType;

            // keep whole process in transaction
            $this->db->beginTransaction();

            if ($contactId) {
                //DELETE Contact  Related data
                $this->db->delete('accountContact', 'id = ' . $contactId);

                $this->db->delete(
                    'accountCampaignContactEmail',
                    'contactId = ' . $contactId
                );

                $this->db->delete(
                    'accountMailLog',
                    'recipientId = ' . $contactId . ' and recipientType = ' .
                    $recipientTypeConfig->contact
                );
            }

            $this->db->commit();
            return true;
        } catch (Exception $e) {
            $this->db->rollback();
            error_log($e->getMessage());
            $this->setMessage($e->getMessage());
            return false;
        }
    }

}
