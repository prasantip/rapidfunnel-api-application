<?php
/**
 * RapidFunnel_Model_AccountResources
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/resource
 */

/**
 * RapidFunnel_Model_AccountResources
 * RapidFunnel/Model/AccountResources.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/account/resource
 */
class RapidFunnel_Model_AccountResources extends RapidFunnel_Model_Abstract
{

    public $db;
    public $dtHelper;
    protected $id;
    protected $accountId;
    protected $accountResourceTypeId;
    protected $accountResourceCategoryId;
    protected $name;
    protected $description;
    protected $content;
    protected $link;
    protected $fileSavedName;
    protected $fileOriginalName;
    protected $status = 0;
    protected $isPublished = 0;
    protected $created;
    protected $createdBy;
    protected $modifiedBy;
    protected $showInApp;

    /**
     * Constructor method for the RapidFunnel_Model_AccountResources
     *
     * Instantiates the parent class by calling its constructor method. Also,
     * configures the model for interactions with the database by setting
     * the appropriate table, database adapter, file helper and query helper.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountResources());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
    }

    /**
     * RapidFunnel_Model_AccountResources::getResourcesForApi()
     *
     * Functionality to fetch resources of a user for api
     *
     * @param int   $accountId   is an unique id of a group under which
     *                           this user belong to
     * @param int   $userId      unique integer value created when
     *                           the user created
     * @param array $groupIds    ids of accessible groups
     * @param array $userDetails user various details
     *
     * @return array with resources details
     */
    public function getResourcesForApi($accountId, $userId,
        $groupIds = null, $userDetails = null
    ) {
        $config = RapidFunnel_Configs::_instance();
        $accountResourceLinkValue = $config->accountResourceTypeId->link;
        // check of enterprise free user
        $accountUserHelper
            = new RapidFunnel_Controller_Action_Helper_AccountUser();
        $freeEnterpriseAccount = $accountUserHelper->isEnterpriseFreeUser(
            $userDetails
        );

        $select = $this->table->select()->setIntegrityCheck(false)
            ->from(
                array('ar' => 'accountResources'),
                array('id'          => 'DISTINCT(ar.id)', 'name',
                      'accountResourceTypeId', 'content',
                      "description" => "IF((ar.description IS NOT NULL AND ar.description != ' '), ar.description, 'null')",
                      'link'        => new zend_db_expr(
                          'IF(ar.accountResourceTypeId = '
                          . $accountResourceLinkValue . ', ar.link,'
                          . 'ar.fileSavedName)'
                      ))
            )
            ->join(
                array('art' => 'accountResourceType'),
                'art.id = ar.accountResourceTypeId',
                array('typeImage' => 'typeImage')
            )
            ->joinLeft(
                array('arc' => 'accountResourceCategory'),
                'ar.accountResourceCategoryId = arc.id',
                array('category' => 'arc.name')
            )
            ->where('ar.status = ?', '1');

        // handle free enterprise user
        if ($freeEnterpriseAccount) {

            // If user is not associated to any group then he should not get any resources
            $select->joinInner(
                array('afur' => 'accountFreeUserResources'),
                'ar.id = afur.accountResourceId', array()
            )->where(
                new Zend_Db_Expr(
                    'EXISTS(SELECT 1 FROM accountUserGroup aug '
                    . 'WHERE aug.accountUserId = ' . $userId . ')'
                )
            )->where('afur.accountId = ?', $accountId);
        } else {
            if (!empty($groupIds)) {
                //Validating for proper associated group id
                $select->joinLeft(
                    array('arg' => 'accountResourceGroup'),
                    "arg.accountResourcesId = ar.id "
                )
                    ->where(
                        '(arg.accountGroupId IN (' . implode(
                            ',',
                            $groupIds
                        ) . '))'
                    )
                    ->group('ar.id');
            }
            $select->joinInner(
                array('auar' => 'accountUserAppResource'),
                'ar.id = auar.accountResourceId', array()
            )
                ->where('auar.accountUserId = ?', $userId);
        }
        $select->group('ar.id');

        $query = $this->db->query($select);
        $resource = $query->fetchAll();

        return $resource;
    }

    /**
     * Function to get resources sent through emails.
     *
     * @param string $emailContent emails html
     *
     * @return string
     */
    public function getResourcesSentThroughEmail($emailContent)
    {
        try {
            $config = RapidFunnel_Configs::_instance();
            $url = $config->regExpr->url;
            preg_match_all($url, $emailContent, $match);

            return $match['0'];
        } catch (Exception $exception) {
            error_log($exception->getMessage());
            return false;
        }
    }

    /**
     * Function to save resources sent date
     *
     * @param int $userId     user id
     * @param int $resourceId resource id
     * @param int $accountId  account id
     * @param int $type      Type of the mail
     *
     * @return bool
     */
    public function saveResourceSentDate($userId, $resourceId, $accountId, $type)
    {
        try {
            if (!is_numeric($userId) || (0 >= $userId)) {
                throw new InvalidArgumentException('Invalid user id provided.');
            }

            if (!is_numeric($resourceId) || (0 >= $resourceId)) {
                throw new InvalidArgumentException('Invalid resource id provided.');
            }

            $this->db->insert(
                'userResourcesSentLog',
                array('userId'     => $userId, 'accountId' => $accountId,
                      'resourceId' => $resourceId, 'type' => $type)
            );
            return true;
        } catch (Exception $exception) {
            error_log($exception->getMessage());
            return false;
        }
    }

    /**
     * Function to save resources sent through emails.
     *
     * @param array $resources array of resource id and user id
     * @param int   $accountId id of the account which user belong to
     *
     * @return bool
     */
    public function saveResourcesSentThroughEmail($resources, $accountId)
    {
        try {
            $config = RapidFunnel_Configs::_instance();
            foreach ($resources as $resource) {
                $resource = explode('/', $resource);
                $this->saveResourceSentDate(
                    $resource[5], $resource[4], $accountId, $config->resourceMail->campaign
                );
            }
            return true;
        } catch (Exception $exception) {
            error_log($exception->getMessage());
            return false;
        }
    }

}
