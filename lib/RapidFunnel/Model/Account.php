<?php
/**
 * RapidFunnel_Model_Account File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account
 */

/**
 * RapidFunnel_Model_Account
 * RapidFunnel/Model/Account.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/account
 */
class RapidFunnel_Model_Account extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;
    protected $name;
    protected $companyName;
    protected $address;
    protected $suite;
    protected $city;
    protected $countryId;
    protected $stateId;
    protected $zip;
    protected $url;
    protected $status;
    protected $accountLevel;
    protected $ignoreAccountPayment = 0;
    protected $passThrough = 0;
    protected $trial = 0;
    protected $trialPeriod = 0;
    protected $testAccount = 0;
    protected $trialUserEnabled = 0;
    protected $userTrialPeriod = 0;
    protected $enableMyWeeklyStats = 1;
    protected $dateCreated;
    protected $createdBy;
    protected $modifiedBy;
    protected $wistiaProjectId;
    protected $basePrice;
    protected $basePricePlanId;
    protected $subscriptionPlanId;
    protected $payThroughAuthNet = 0;
    public $dtHelper;

    /**
     * Constructor method for the RapidFunnel_Model_AccountBranding class.
     *
     * Instantiates the parent class by calling its constructor method.
     * Also, configures the model for interactions with the  database by
     * setting the appropriate table, database adapter and  DtQuery Helper.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_Account());
        $this->db = $this->table->getAdapter();
        $this->dtHelper = new RapidFunnel_Controller_Action_Helper_DtQuery();
    }

    /**
     * RapidFunnel_Model_Account::getAccountDetail()
     *
     * get account complete detail
     *
     * @param int $id    account id
     *
     * @return boolean | array
     *
     * @throws Exception
     */
    public function getAccountDetail($id)
    {
        // $id should not be empty or should be numeric
        if (!is_numeric($id) || empty($id)) {
            throw new InvalidArgumentException(
                'account id is either empty or not numeric'
            );
        }
        
        try {
            $select = $this->db->select()
                ->from(array('a' => 'account'))
                ->joinLeft(
                    array('st' => 'states'), 'a.stateId = st.id',
                    array('stateCode' => 'name')
                );

            $select->where('a.id = ?', $id)
                ->having('a.id > 0');

            $stmt = $this->db->query($select);
            return $stmt->fetch();
        } catch (Exception $zde) {
            error_log($zde->getMessage());
            $this->setMessage($zde->getMessage());
            return false;
        }
    }

}
