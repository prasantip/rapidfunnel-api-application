<?php
/**
 * RapidFunnel_Model_AccountUserBillingInfo
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/admin
 */

/**
 * RapidFunnel_Model_AccountUserBillingInfo
 * RapidFunnel/Model/AccountUserBillingInfo.php
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid  Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      https://my.rapidfunnel.com/admin
 */
class RapidFunnel_Model_AccountUserBillingInfo extends RapidFunnel_Model_Abstract
{

    public $db;
    protected $id;

    /**
     * Constructor method for the RapidFunnel_Model_AccountUserBillingInfo
     *
     * Instantiates the parent class by calling its constructor method. Also,
     * configures the model for interactions with the database by setting the
     * appropriate table, database adapter, Account and AccountUser models.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTable(new RapidFunnel_Data_Table_AccountUserBillingInfo());
        $this->db = $this->table->getAdapter();
    }

    /**
     * RapidFunnel_Model_AccountUserBillingInfo::getBillingDetailsByUserId()
     *
     * Functionality to get card profile detail by user id
     *
     * @param int $userId used to get user card details
     *
     * @return array with user card details
     */
    public function getBillingDetailsByUserId($userId)
    {
        if (!is_numeric($userId) || empty($userId)) {
            throw new InvalidArgumentException('Invalid user id supplied.');
        }

        try {
            $select = $this->db->select()
                ->from(
                    array('aubi' => 'accountUserBillingInfo')
                )
                ->joinInner(
                    array('au' => 'accountUser'), 'au.id = aubi.userId',
                    array('userStatus' => 'status')
                )->joinInner(
                    array('a' => 'account'), 'a.id = aubi.accountId',
                    array('passThrough', 'subscriptionPlanId')
                )->where('aubi.userId = ?', $userId);

            $stmt = $this->db->query($select);
            return $stmt->fetch();
        } catch (Exception $zde) {
            error_log($zde->getMessage());
            $this->setMessage($zde->getMessage());
            return false;
        }
    }

}
