<?php
/**
 * RapidFunnel_Model_Password File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Model_Password
 * RapidFunnel/Model/Password.php
 *
 * Password model is used compositionally
 * by other models it implements its own
 * setter and getter instead of using the abstract.
 *
 * Password is a stand alone model in that sense.
 * This was done to avoid some conflicts with models
 * that need the user to be logged in.  This is used
 * by both the change password and registration flows.
 *
 * @category  Model
 * @package   RapidFunnel_Model
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets.com
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Model_Password
{
    protected $id;
    protected $password;
    protected $rawPass;
    protected $salt;
    protected $forceChange = true;
    private $reflect;

    /**
     * RapidFunnel_Model_Password::__construct()
     */
    public function __construct()
    {
        $this->reflect = new ReflectionClass($this);
    }

    /**
     * Override create to generate
     * and has the pass before setting.
     *
     * @param string $pass password
     *
     * @return boolean
     */
    public function create($pass = null)
    {
        if ($pass == null) {
            $password = $this->generateRandomPassword();
        } else {
            $password = $pass;
        }

        $this->rawPass = $password;
        $this->salt = $this->generateSalt();
        $this->password = $this->hashPassword($password, $this->salt);

        return true;
    }

    /**
     * Generate a random password not hashed yet.
     *
     * @return string
     */
    private function generateRandomPassword()
    {
        $length = 8;
        $pwd = "";
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

        $maxlength = strlen($possible);

        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);

            if (!strstr($pwd, $char)) {
                $pwd .= $char;
                $i++;
            }
        }
        return $pwd;
    }

    /**
     * Hash an an existing password and apply the salt
     *
     * @param string $password password string
     * @param string $salt     salt string
     *
     * @return string
     */
    public function hashPassword($password,$salt)
    {
        return hash('sha512', $password.$salt);
    }

    /**
     * Generate salt for the password
     *
     * @return string
     */
    private function generateSalt()
    {
        return substr(
            str_replace(
                '+', '.',
                base64_encode(
                    pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand())
                )
            ), 0, 22
        );
    }

    /**
     * The __set magic method implementation
     * used to enforce encapsulation.
     * If the var is not declared.  You cannot set it.
     *
     * @param string $name  the class property name to set
     * @param mixed  $value the value to set the class property to.
     *
     * @throws Exception
     *
     * @return boolean
     */
    public function __set($name, $value)
    {
        $isProperty = false;

        foreach ($this->reflect->getProperties() as $property) {
            if ($property->getName() == $name) {
                $this->{$name} = $value;
                $isProperty = true;
                break;
            }
        }

        if (!$isProperty) {
            throw new UnexpectedValueException(
                'You cannot set an undeclared property "' . $name . '" in '
                . $this->reflect->getName() . ' called from ' . __METHOD__
            );
        } else {
            return true;
        }

    }

    /**
     * The __get magic method implementation
     * used to enforce encapsulation
     * if the var/property does not exist throw an exception
     * when trying retrieve it.
     *
     * @param string $name property name to return
     *
     * @throws Exception
     *
     * @return string
     */
    public function __get($name)
    {
        if (!isset($this->{$name})) {
            throw new InvalidArgumentException(
                'Undefined property "' . $name . '" in '
                . $this->reflect->getName() . ' called from ' . __METHOD__
            );
        } else {
            return $this->{$name};
        }
    }

}
