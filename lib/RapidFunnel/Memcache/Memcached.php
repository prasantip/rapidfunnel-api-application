<?php

/**
 * RapidFunnel_Memcache_Memcached
 * Memcache/Memcached.php
 *
 * @category  library
 * @copyright 2016 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Memcache_Memcached
{
    public $memcache;
    public $errorMsg = '';
    public $emptyKeyMsg = 'Key cannot be empty';
    public $emptyItemMsg = 'Items cannot be empty';
    public $expireTimeNotNumMsg = 'Expire time should be numeric value.';

    /**
     * Basic initialisations of the class
     * Calls a method to establish memcache connection
     */
    public function __construct()
    {
        //Get memcache server info from config
        $memcacheConfig = RapidFunnel_Configs::_instance('memcache');
        //If memcache enabled
        if (1 == $memcacheConfig->enable) {
            //Get connection
            $this->memcache = $this->connect();
        } else {
            $this->errorMsg = 'Memcache disabled!!';
        }
    }

    /**
     * Functionality to connect to memcache server
     *
     * @return object Memcached object
     * @throws Exception if connection error
     */
    private function connect()
    {
        //Get memcache server info from config
        $memcacheConfig = RapidFunnel_Configs::_instance('memcache');

        //Connect to memcache
        $memcache = new Memcached();
        $memcache->addServer(
            $memcacheConfig->server, $memcacheConfig->port
        );

        $status = $memcache->getStats();
        $processId = isset(
            $status[$memcacheConfig->server . ':' . $memcacheConfig->port]['pid']
        ) ? ($status[$memcacheConfig->server . ':' . $memcacheConfig->port]['pid']) : -1;

        //When memcache created a process
        if ($processId <= 0) {
            $this->errorMsg = 'Unable to connect to Memcache server.';
            $memcache = false;
        }

        return $memcache;
    }

    /**
     * Functionality to get an item
     *
     * @param string $key key for memcache storage
     *
     * @return string stored data against the key
     * @throws Exception while empty key or any error
     */
    public function getItem($key)
    {
        try {
            if (!empty($this->errorMsg)) {
                throw new InvalidArgumentException($this->errorMsg);
            }
            if (empty($key)) {
                throw new InvalidArgumentException($this->emptyKeyMsg);
            }

            return $this->memcache->get($key);
        } catch (Exception $mce) {
            return false;
        }
    }

    /**
     * Functionality to set an item
     *
     * @param string $key        key for memcache storage
     * @param mixed  $value      mixed data type value
     * @param int    $expiration time of expiration in second
     *
     * @return string stored data against the key
     * @throws Exception while empty key or any error
     */
    public function setItem($key, $value, $expiration = null)
    {
        try {
            if (!empty($this->errorMsg)) {
                throw new InvalidArgumentException($this->errorMsg);
            }
            if (empty($key)) {
                throw new InvalidArgumentException($this->emptyKeyMsg);
            }
            if (empty($value)) {
                throw new InvalidArgumentException($this->emptyKeyMsg);
            }
            if (!empty($expiration) && !is_numeric($expiration)) {
                throw new InvalidArgumentException($this->expireTimeNotNumMsg);
            }

            $expirationTime = $this->getExpirationTime($expiration);

            return $this->memcache->set($key, $value, $expirationTime);
        } catch (Exception $mce) {
            return false;
        }
    }

    /**
     * Function to calculate expire time
     *
     * @param int $expiration time of expiration in second
     *
     * @return int expire time in second
     */
    private function getExpirationTime($expiration = null)
    {
        if (!empty($expiration)) {
            //calculate time if expiration time sent
            $expirationTime = time() + $expiration;
        } else {
            //calculate from config value
            $memcacheConfig = RapidFunnel_Configs::_instance('memcache');
            $expirationTime = time() + $memcacheConfig->expireTime;
        }

        return $expirationTime;
    }

    /**
     * Functionality to check if key exist
     *
     * @param string $key key to be search in memcache
     *
     * @return string | array | false
     * @throws Exception while empty key or any error
     */
    public function checkKeyExistWithData($key)
    {
        try {
            if (!empty($this->errorMsg)) {
                throw new Exception($this->errorMsg);
            }
            if (empty($key)) {
                throw new InvalidArgumentException($this->emptyKeyMsg);
            }

            return $this->memcache->get($key);
        } catch (Exception $mce) {
            return false;
        }
    }

    /**
     * Functionality to update data
     *
     * @param string $key        for memcache delete
     * @param mixed  $value      mixed data type value
     * @param int    $expiration time of expiration in second
     *
     * @return boolean
     * @throws Exception while empty key or any error
     */
    public function Update($key, $value, $expiration = null)
    {
        try {
            if (!empty($this->errorMsg)) {
                throw new Exception($this->errorMsg);
            }
            if (empty($key)) {
                throw new InvalidArgumentException($this->emptyKeyMsg);
            } elseif (empty($value)) {
                throw new InvalidArgumentException($this->emptyItemMsg);
            } elseif (!empty($expiration) && !is_numeric($expiration)) {
                throw new InvalidArgumentException($this->expireTimeNotNumMsg);
            }

            $expirationTime = $this->getExpirationTime($expiration);

            return $this->memcache->replace(
                $key, $value, $expirationTime
            );
        } catch (Exception $mce) {
            return false;
        }
    }

    /**
     * Functionality to delete data if key exist
     *
     * @param string $key key to be search in memcache
     *
     * @return boolean
     * @throws Exception while empty key or any error
     */
    public function delete($key)
    {
        try {
            if (!empty($this->errorMsg)) {
                throw new Exception($this->errorMsg);
            }
            if (empty($key)) {
                throw new InvalidArgumentException($this->emptyKeyMsg);
            }

            return $this->memcache->delete($key);
        } catch (Exception $mce) {
            return false;
        }
    }

    /**
     * Functionality to delete data if key exist
     *
     * @param array $keys key to be search in memcache
     *
     * @return boolean
     * @throws Exception while empty key or any error
     */
    public function deleteMultiple($keys)
    {
        try {
            if (!empty($this->errorMsg)) {
                throw new Exception($this->errorMsg);
            }
            if (empty($keys)) {
                throw new InvalidArgumentException($this->emptyKeyMsg);
            }

            return $this->memcache->deleteMulti($keys);
        } catch (Exception $mce) {
            return false;
        }
    }

}
