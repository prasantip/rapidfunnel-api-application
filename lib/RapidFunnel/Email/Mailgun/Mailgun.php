<?php

use Mailgun\Mailgun;

/**
 * RapidFunnel_Email_Mailgun_Mailgun
 * Mailgun/Mailgun.php
 *
 * @category  library
 * @package   Mailgun
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Email_Mailgun_Mailgun
{

    private $_domain;
    private $_mgClient;
    public $message;

    public function __construct()
    {
        //get the mailgun informations
        $mailgunConfig = RapidFunnel_Configs::_instance('mailgun')->api;

        $this->_mgClient = new Mailgun($mailgunConfig->apikey);
        $this->_domain = $mailgunConfig->domain;
    }

    /**
     * @function _isValidEmail
     * @purpose to check email format
     * @param string $input
     * @return boolean
     */
    private function _isValidEmail($input)
    {
        if ('' != $input) {
            $validator = new RapidFunnel_Validate_EmailAddress();

            if ($validator->isValid($input)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * @function _getDetail
     * @purpose Check from and To to get detail from those
     * @param type $input
     * @return type
     */
    private function _getDetail($input)
    {
        $response = false;

        if ('' != $input) {
            $detail = array();

            // if $from is array, extract all detail, else it will be only email
            if (is_array($input) && (isset($input['email']))) {
                $email = $input['email'];

                // set first and last name
                if (isset($input['firstname']) && ('' != $input['firstname'])) {
                    $detail['first'] = $input['firstname'];
                }
                if (isset($input['lastname']) && ('' != $input['lastname'])) {
                    $detail['last'] = $input['lastname'];
                }
            } else {
                $email = $input;
            }

            // if it is a valid email, set from email
            if ($this->_isValidEmail($email)) {
                $response['email'] = $email;
                $response['detail'] = $detail;
            }
        }
        return $response;
    }

    /**
     * Functionality to sendMail
     *
     * @param string $to         email id
     * @param string $subject    email subject
     * @param string $html       html content of email
     * @param string $text       text content of email
     * @param string $from       from email
     * @param array  $customData custom data sent with email
     * @param string $replyTo    email that to be shown in reply to
     * @param string $mailType   mail type
     *
     * @return boolean
     */
    public function sendMail($to, $subject, $html = null, $text = null,
        $from = null, $customData = array(), $replyTo = null, $mailType = null
    ) {
        $config = RapidFunnel_Configs::_instance();

        if ('campaign' === $mailType) {
            //get the campaign mailgun info
            $campaignMailConfig = $config->mailgun->campaign;
            $this->_mgClient = new Mailgun($campaignMailConfig->apikey);
            $this->_domain = $campaignMailConfig->domain;
        } elseif($config->mailType->optIn === $mailType) {
            //get the optIn mailgun info
            $campaignMailConfig = $config->mailgun->optIn;
            $this->_mgClient = new Mailgun($campaignMailConfig->apikey);
            $this->_domain = $campaignMailConfig->domain;
        }

        try {
            // set all the params
            $params = array(
                'to' => $to,
                'subject' => $subject,
                'html' => $html,
                'text' => $text
            );

            $params['from'] = 'support@rapidfunnel.com';
            if (isset($from) && '' !== $from) {
                // Check and replace comma from sender address with space
                if (strpos($from, ',')) {
                    $from = str_replace(',', ' ', $from);
                }
                $params['from'] = $from;
            }

            if ('' != $replyTo) {
                $params['h:Reply-To'] = $replyTo;
            }

            // send custom data
            if (is_array($customData)) {
                $params['v:custom-data'] = json_encode($customData);
            }

            if(defined('TESTING')) {
                $result = $params;
            } else {
                // Now, compose and send your message.
                $result = $this->_mgClient->sendMessage($this->_domain, $params);
            }

            return $result;
        } catch (Exception $ex) {
            error_log($ex->getMessage());
            $this->message = $ex->getMessage();
            return false;
        }
    }

    /**
     * @function messageBuilder
     * @purpose sending message with additional flexibility
     * @param string $subject
     * @param array $toArr
     * @param string $textBody
     * @param string $htmlBody
     * @param array $ccArr
     * @param string $from
     * @param string $replyTo
     *
     * @return array|string
     */
    public function messageBuilder($subject, $toArr, $textBody = '',
            $htmlBody = '', $ccArr = array(), $from = '', $replyTo = '')
    {
        if (defined('TESTING')) {
            return true;
        }

        $status = false;
        $messageBldr = $this->_mgClient->MessageBuilder();

        // Define the from address.
        if ('' != $from) {
            $fromDetail = $this->_getDetail($from);
        } else {
            $fromDetail['email'] = 'support@rapidfunnel.com';
            $fromDetail['detail'] = array();
        }
        $messageBldr->setFromAddress($fromDetail['email'], $fromDetail['detail']);

        // Define a to recipient.
        if (is_array($toArr) && (!empty($toArr))) {
            foreach ($toArr as $eachTo) {
                $toDetail = $this->_getDetail($eachTo);
                $messageBldr->addToRecipient($toDetail['email'],
                        $toDetail['detail']);
            }
        } else {
            return false;
        }

        // Define a cc recipient.
        if (is_array($ccArr)) {
            foreach ($ccArr as $eachCc) {
                $ccDetail = $this->_getDetail($eachCc);
                $messageBldr->addCcRecipient($ccDetail['email'],
                        $ccDetail['detail']);
            }
        }

        // Define the subject.
        $messageBldr->setSubject($subject);

        // Define the body of the message.
        $messageBldr->setTextBody($textBody);

        // Define the body of the message.
        $messageBldr->setHtmlBody($htmlBody);

        // Define replyTo of the message
        if ('' != $replyTo) {
            $messageBldr->setReplyToAddress($replyTo);
        }

        // Other Optional Parameters.
//        $messageBldr->addCampaignId("My-Awesome-Campaign");
//        $messageBldr->addCustomHeader("Customer-Id", "12345");
//        $messageBldr->addAttachment("@/tron.jpg");
//        $messageBldr->setDeliveryTime("tomorrow 8:00AM", "PST");
//        $messageBldr->setClickTracking(true);
        // Finally, send the message.
        $status = $this->_mgClient->post("{$this->_domain}/messages",
                $messageBldr->getMessage(), $messageBldr->getFiles());

        return $status;
    }

}
