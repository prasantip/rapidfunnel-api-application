<?php

/**
 * Application bootstrap file
 *
 * RapidFunnel_Bootstrap
 * RapidFunnel/Bootstrap.php
 *
 * @category  Bootstrap
 * @package   RapidFunnel
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    const ENV_DEVELOPMENT = 'Development';
    const ENV_PRODUCTION = 'Production';
    const ENV_STAGING = 'Staging';
    const ENV_QA = 'QA';

    /**
     * @purpose load external library added by composer
     * @return void
     */
    protected function _initComposerLibraryLoader()
    {
        require_once APP_LIB . '/vendor/autoload.php';
    }

    /**
     * @purpose load module specific plugins
     * @params none
     * @return void
     */

    public function _initPluginLoader()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new RapidFunnel_Controller_Plugin_ModulePluginLoader(array(
            'api' => array(
                'RapidFunnel_Controller_Plugin_AuthApi',
            )
        )));
    }

    /**
     * function _initDatabase()
     * @purpose setup the database connections
     *
     * @param none
     * @return none
     */
    protected function _initDatabase()
    {
        $this->bootstrap('multidb');
        $resource = $this->getPluginResource('multidb');

        $masterDb = $resource->getDb('db1');
        $slaveDb = $resource->getDb('db2');

        Zend_Db_Table_Abstract::setDefaultAdapter ( $masterDb );

        Zend_Registry::set('masterDb', $masterDb);
        Zend_Registry::set('slaveDb', $slaveDb);
    }

    /**
     * Functionality to set the default metadata in cache
     * This metadata will be used by the Zend_Db_Table instead of querying again
     * to the database.
     * If we do not add this method then whenever meta data required
     * zend queries "describe table" to the database
     * Let this function be before session init so session table's metadata
     * also will be cached
     *
     * @return void
     */
    public function _initDbTableMetadataCache()
    {
        //Caching
        $frontendOptions = array(
            'lifetime' => 315360000, //for 10 years
            'automatic_serialization'   => true
        );
        $backendOptions = array('cache_dir' => APP_PATH . '/../cache/tables');
        $cache = Zend_Cache::factory(
            'Core',
            'File',
            $frontendOptions,
            $backendOptions
        );
        //Cache table metadata
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }

    /**
     * Functionality to set up session in database
     * Session is also initialized here
     *
     * @return void
     */
    protected function _initSession()
    {
        $requestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']
            : '';
        $requestUriPath = parse_url($requestUri, PHP_URL_PATH);
        $segments = explode('/', $requestUriPath);
        $moduleName = isset($segments[1]) ? $segments[1] : '';
        //To distinguish controller concat module name as well
        $controllerName = $moduleName . '-' . (isset($segments[2])
                ? $segments[2] : '');

        //get value from config file
        $config = RapidFunnel_Configs::_instance();
        $sesExcludeModules = $config->exclude->session->modules->toArray();
        $excludeControllers = $config->exclude->session->controllers->toArray();
        $includeControllers = $config->include->session->controllers->toArray();

        //Exclude module if it is in exclude list
        if (!empty($requestUri)
            && (!(in_array($moduleName, $sesExcludeModules)
                    || in_array($controllerName, $excludeControllers))
                || in_array($controllerName, $includeControllers))
        ) {
            $sessionConfig = ['name' => 'session', 'primary' => 'id',
                              'modifiedColumn' => 'modified',
                              'dataColumn' => 'data',
                              'lifetimeColumn' => 'lifetime'];

            //create Zend_Session_SaveHandler_DbTable and
            //set the save handler for Zend_Session
            Zend_Session::setSaveHandler(
                new Zend_Session_SaveHandler_DbTable($sessionConfig)
            );
            //start your session!
            Zend_Session::start();
        }
    }

//    /**
//     * function _initRoutes()
//     * Initializing routers for the application
//     */
//    protected function _initRoutes()
//    {
//        $requestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']
//            : '';
//        $segments = explode('/', parse_url($requestUri, PHP_URL_PATH));
//
//        $router = Zend_Controller_Front::getInstance()->getRouter();
//        $routes['api-upgrade'] = new Zend_Controller_Router_Route('/api/upgrade/:id/:key/:redirect',
//                array('module' => 'account', 'controller' => 'login', 'action' => 'login-via-api'));
//
//        foreach ($routes as $name => $route) {
//            $router->addRoute($name, $route);
//        }
//    }

    /**
     * Functionality that will handle session time out
     *
     * @return void
     */
    protected function _initSessionTimeOutHandler()
    {
        //only if user logged in
        if (Zend_Auth::getInstance()->hasIdentity()) {

            //get value from config file
            $expirationTime = RapidFunnel_Configs::_instance(
                'timeout'
            )->session;

            //Functionality to change the session life time in DB
            $sessionSaveHandler = Zend_Session::getSaveHandler();
            $sessionSaveHandler->setOverrideLifetime(true)->setLifetime(
                $expirationTime
            );
        }
    }

}
