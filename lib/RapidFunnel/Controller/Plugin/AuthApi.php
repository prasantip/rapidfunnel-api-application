<?php

/**
 * Authorization controller plugin for RapidFunnel
 *
 * RapidFunnel_Controller_Plugin_Auth
 * RapidFunnel/Controller/Plugin/Auth.php
 *
 * @category  Controller
 * @package   Plugin
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Controller_Plugin_AuthApi extends Zend_Controller_Plugin_Abstract
{

    /**
     * Run the Authentication for the user - user account good?
     * @param Zend_Controller_Request_Abstract $request
     */
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $response = array('response' => array('status' => 'false'));

        if ('login' != $request->getControllerName()) {
            //get the post data
            $userId = $this->_request->getPost('userId');
            $accessToken = $this->_request->getPost('accessToken');

            if (!is_numeric($userId)) {
                $response['response']['errorMessage'] = 'Invalid user id';
                //send the json data to the client
                echo json_encode($response);
                exit;
            }

            //checking for authontication
            $authHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('Authenticate');
            $isValidToken = $authHelper->authUserToken($userId, $accessToken);

            if (!$isValidToken) {
                $response['response']['errorMessage'] = 'Invalid access token';
                //send the json data to the client
                echo json_encode($response);
                exit;
            }
        }
    }

}
