<?php

/**
 * ModulePluginLoader controller plugin for RapidFunnel
 *
 * RapidFunnel_Controller_Plugin_ModulePluginLoader
 * RapidFunnel/Controller/Plugin/ModulePluginLoader.php
 *
 * @category  Controller
 * @package   Plugin
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Controller_Plugin_ModulePluginLoader extends Zend_Controller_Plugin_Abstract
{

    private $_pluginMap;

    public function __construct(array $pluginMap)
    {
        $this->_pluginMap = $pluginMap;
    }

    /**
     * @purpose on route shutdown loaded module specific plugins
     * @param Zend_Controller_Request_Abstract $request
     * @return type void
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $module = $request->getModuleName();

        if (isset($this->_pluginMap[$module])) {

            $front = Zend_Controller_Front::getInstance();

            foreach ($this->_pluginMap[$module] as $plugin) {
                $front->registerPlugin(new $plugin());
            }
        }
    }

}
