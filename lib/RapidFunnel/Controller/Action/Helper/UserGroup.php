<?php
/**
 * RapidFunnel_Controller_Action_Helper_UserGroup File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Controller_Action_Helper_UserGroup
 * RapidFunnel/Controller/Action/Helper/UserGroup.php
 *
 * @category  Action_Helper
 * @package   Digitalassets_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Controller_Action_Helper_UserGroup extends Zend_Controller_Action_Helper_Abstract
{
    /**
     * RapidFunnel_Controller_Action_Helper_UserGroup
     * ::getAllAccessibleGroupIdsByUserId()
     *
     * Function to get all accessible group ids by user id
     *
     * @param int $userId user id for which the function is going to return
     *                    the group ids
     * @param int $roleId user roleId
     *
     * @return array group ids
     */
    public function getAllAccessibleGroupIdsByUserId($userId, $roleId = null)
    {
        // $userId should not be empty
        if (empty($userId) || !is_numeric($userId)) {
            throw new InvalidArgumentException('Invalid userId argument supplied.');
        }

        if (!empty($roleId) && !is_numeric($roleId)) {
            throw new InvalidArgumentException('Invalid roleId argument supplied.');
        }

        $accountRoles = RapidFunnel_Configs::_instance('accountRoles');

        $accessibleIds = array();

        if (('' != $userId) && (0 < $userId)) {
            $accountUserGroup = new RapidFunnel_Model_AccountUserGroup();
            $groupDetails = $accountUserGroup->getGroupByUserId($userId);

            // $groupDetails always return array, so no need to check for null
            if (!empty($groupDetails)) {
                $accessibleIds = array();
                foreach ($groupDetails as $eachGroup) {
                    if ($accountRoles->user === $roleId) {
                        array_push(
                            $accessibleIds, $eachGroup['accountGroupId']
                        );
                    } else {
                        $childIds = explode(",", $eachGroup['childs']);
                        array_push($childIds, $eachGroup['accountGroupId']);

                        $accessibleIds = array_merge($accessibleIds, $childIds);
                    }
                }
            }

            // remove duplicate items
            $accessibleIds = array_filter(array_unique($accessibleIds));
        }

        return $accessibleIds;
    }

}
