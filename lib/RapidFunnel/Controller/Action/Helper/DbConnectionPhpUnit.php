<?php
/**
 * RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit
 * RapidFunnel/Controller/Action/Helper/DbConnectionPhpUnit.php
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit extends
    PHPUnit_Extensions_Database_TestCase
{

    /**
     * RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit::getConnection()
     *
     * Used by PHPUnit to get a database connection for testing the database
     *
     * @return PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection
     */
    protected function getConnection()
    {
        $configs = new RapidFunnel_Controller_Action_Helper_UnitTestConfig();
        $config = $configs->_instance();

        $db = new PDO(
            'mysql:host=' . $config->resources->multidb->db1->host .
            '; dbname=' . $config->resources->multidb->db1->dbname,
            $config->resources->multidb->db1->username,
            $config->resources->multidb->db1->password
        );
        return $this->createDefaultDBConnection($db, "rapidfunnel_test");
    }

    /**
     * RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit::getSetUpOperation()
     */
    public function getSetUpOperation()
    {
        // whether you want cascading truncates
        // set false if unsure
        $cascadeTruncates = false;

        return new PHPUnit_Extensions_Database_Operation_Composite(array(
            new PHPUnit_Extensions_Database_Operation_MySQL55Truncate(
                $cascadeTruncates
            ),
            PHPUnit_Extensions_Database_Operation_Factory::INSERT()
        ));
    }

    /**
     * RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit::getDataSet()
     *
     * This function is being overrided by test cases.
     */
    public function getDataSet(){}
}

/**
 * PHPUnit_Extensions_Database_Operation_MySQL55Truncate
 *
 * Contains functions to truncate the database after testing
 *
 * @Note    Custom class for truncate after insert operations
 */
class PHPUnit_Extensions_Database_Operation_MySQL55Truncate
    extends PHPUnit_Extensions_Database_Operation_Truncate
{
    public function execute(PHPUnit_Extensions_Database_DB_IDatabaseConnection $connection,
                            PHPUnit_Extensions_Database_DataSet_IDataSet $dataSet)
    {
        $connection->getConnection()->query("SET foreign_key_checks = 0");
        parent::execute($connection, $dataSet);
        $connection->getConnection()->query("SET foreign_key_checks = 1");
    }
}
