<?php

/**
 * RapidFunnel_Controller_Action_Helper_DtQuery
 * RapidFunnel/Controller/Action/Helper/DtQuery.php
 *
 * @category  Action Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Controller_Action_Helper_DtQuery extends Zend_Controller_Action_Helper_Abstract
{
    /**
     * @function setLimit()
     * @purpose functionality to set the limit
     * @param $select
     * @param $getParams
     * @return $select the db tabel odbject
     */
    public function setLimit($select, $getParams)
    {
        if (isset($getParams['iDisplayStart'])
            && isset($getParams['iDisplayLength'])
        ) {
            //For pagination adding limit
            $start = $getParams['iDisplayStart'];
            $limit = $getParams['iDisplayLength'];
            //adding limit
            if (isset($start) && ($limit != '-1')) {
                $select->limit($limit, $start);
            }
        }
        return $select;
    }

    /**
     * @function setOrder()
     * @purpose functionality to set the order
     * @param $select
     * @param $getParams
     * @param $allFields array to be listed
     * @param $defaultSortCol string
     * @return $select the db table object
     */
    public function setOrder($select, $getParams, $allFields,
            $defaultSortCol = '')
    {
        //Ordering
        if (isset($getParams['iSortCol_0'])) {
            $sOrder = "";

            for ($counter = 0; $counter < intval($getParams['iSortingCols']);
                        $counter++) {

                if ($getParams['bSortable_' . intval($getParams['iSortCol_' . $counter])]
                        == "true") {
                    $sOrder .= $allFields[intval($getParams['iSortCol_' . $counter])] . "
                        " . $getParams['sSortDir_' . $counter] . ", ";
                }
            }
            //Removing the last semicolon and the space
            if (strlen($sOrder) > 0) {
                $sOrder = substr_replace($sOrder, "", -2);
            }
        }
        //If order is set
        if ((isset($sOrder)) && (strlen($sOrder) > 0)) {
            $select->order($sOrder);
        } elseif ('' != $defaultSortCol) {
            $select->order($defaultSortCol);
        }

        return $select;
    }

    /**
     * @function setWhere()
     * @purpose  functionality to set the where condition
     *
     * @param array $getParams
     * @param array $allFields        array fields to be listed
     * @param mixed $db               db adapter
     * @param array $additionalSearch array of additional search data
     *
     * @return mixed  $select db object
     */
    public function getWhere($getParams, $allFields, $db,
        $additionalSearch = null
    )
    {
        if (empty($getParams) || !is_array($getParams)) {
            throw new InvalidArgumentException(
                'Invalid getParams argument supplied.'
            );
        }

        if (empty($allFields) || !is_array($allFields)) {
            throw new InvalidArgumentException(
                'Invalid allFields argument supplied.'
            );
        }

        if (empty($db) || !is_object(($db))) {
            throw new InvalidArgumentException(
                'Invalid db argument supplied.'
            );
        }

        if (!empty($additionalSearch) && !is_array($additionalSearch)) {
            throw new InvalidArgumentException(
                'Invalid additionalSearch argument supplied.'
            );
        }

        $totalFieldCount = count($allFields);
        $sWhere = '';

        //It will add where condition for those which are
        if (!empty($getParams['sSearch']) && $getParams['sSearch'] != "") {
            $searchConfig = RapidFunnel_Configs::_instance('search');
            //Escaping search strings
            if (intval($searchConfig->like)) {
                $searchData = $db->quote(trim($getParams['sSearch']) . '%');
            } else {
                $searchData = $db->quote(
                    '%' . trim($getParams['sSearch']) . '%'
                );
            }

            for ($counter = 0; $counter < $totalFieldCount; $counter++) {

                if ((isset($getParams['bSearchable_' . $counter])) && ($getParams['bSearchable_' . $counter]
                        == "true")) {
                    //for date field
                    $sWhere .= $allFields[$counter] . ' LIKE ' . $searchData . ' OR ';
                }
            }

            // add additional search
            if(!empty($additionalSearch) && 0 < count($additionalSearch)) {
                foreach($additionalSearch as $value) {
                    $sWhere .= $value . ' LIKE ' . $searchData . ' OR ';
                }
            }

            //remove the OR of the last
            $sWhere = substr_replace($sWhere, "", -3);
        }

        return $sWhere;
    }

    /**
     * @function setWhere()
     * @purpose  functionality to set the where condition
     *
     * @param array $getParams            dataTable generated data to fetch
     *                                    the required data
     * @param array $allFields            array fields to be listed
     * @param mixed $db                   db adapter
     * @param array $otherSearchingFields array of additional search data
     *
     * @return mixed  $select db object
     */
    public function getWhereForFullText($getParams, $allFields, $db, $otherSearchingFields = null)
    {
        if (empty($getParams) || !is_array($getParams)) {
            throw new InvalidArgumentException(
                'Invalid getParams argument supplied.'
            );
        }

        if (empty($allFields) || !is_array($allFields)) {
            throw new InvalidArgumentException(
                'Invalid allFields argument supplied.'
            );
        }

        if (empty($db) || !is_object(($db))) {
            throw new InvalidArgumentException(
                'Invalid db argument supplied.'
            );
        }

        $sWhere = '';

        //It will add where condition for those which are
        if (!empty($getParams['sSearch']) && $getParams['sSearch'] != "") {
            //Escaping search strings

            $fullTextString = trim($getParams['sSearch']);
            $fullTextString = trim(preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $fullTextString));
            
            if (!empty($fullTextString)) {
                $fullTextString = '+' . str_replace(
                        array(' ', '@'), ' +', $fullTextString
                    );
                $fullTextString = rtrim($fullTextString, " +");
                $searchData = $db->quote(
                    $fullTextString . '*'
                );

                foreach ($allFields as $allField) {
                    $matchFields = '';
                    $totalFieldCount = count($allField);
                    for ($counter = 0; $counter < $totalFieldCount; $counter++)
                    {
                        $matchFields .= $allField[$counter] . ',';
                    }
                    $matchFields = rtrim($matchFields, ',');
                    $sWhere .= ' MATCH (' . $matchFields . ')' . ' AGAINST ('
                        . $searchData . ' IN BOOLEAN MODE' . ') OR ';
                }
            }
                if (!empty($otherSearchingFields)) {
                    $searchData = $db->quote(
                        $getParams['sSearch'] . '%'
                    );
                    for (
                        $counter = 0; $counter < count($otherSearchingFields);
                        $counter++
                    ) {
                        $sWhere .= $otherSearchingFields[$counter] . ' LIKE '
                            . $searchData . ' OR ';
                    }
                }
                $sWhere = rtrim($sWhere, ' OR ');
            }
        return $sWhere;
    }

    /**
     * @function prepareDtResponse()
     * @purpose functionality to prepare dt response in normal
     * @param $select query
     * @param $selectNoLimit query without limit
     * @param $aadapter to run the query
     * @param $getParams
     *
     * @throws Exception
     *
     * @return array the db table response
     */
    public function prepareDtResponse($getParams, $select, $selectNoLimit,
            $adapter)
    {
        // get results
        try {
            $result = $adapter->fetchAll($select);
            //Getting all data count without limit
            $totalDisplayRecordsCount = $adapter->fetchOne($selectNoLimit);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        //Initializing the output
        $output = array(
            "sEcho" => isset($getParams['sEcho']) ?
                intval($getParams['sEcho']) : '',
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array(),
        );

        //If getting any data
        if (isset($result) && (!empty($result))) {
            $currentRowsCount = count($result);
            //Final Output array
            $output = array(
                "sEcho" => isset($getParams['sEcho']) ?
                    intval($getParams['sEcho']) : '',
                "iTotalRecords" => $currentRowsCount,
                "iTotalDisplayRecords" => intval($totalDisplayRecordsCount),
                "aaData" => array()
            );

            // If results then count and sort else return empty
            if ($currentRowsCount > 0) {
                //converting to array
                $currentData = $result;
                //initializing an array for output
                $aaData = array();

                //Put all the data in a array to print outside
                foreach ($currentData as $rowData) {
                    $row = array();

                    //Put each records of the row
                    foreach ($rowData as $key => $colData) {
                        //Formating the records
                        $row[$key] = htmlentities($colData);
                    }
                    //Put the data in the output array
                    $aaData[] = $row;
                }
                //add the output data in the array list
                $output['aaData'] = $aaData;
            }
        }
        return $output;
    }

}
