<?php
/**
 * RapidFunnel_Controller_Action_Helper_UnitTestConfig File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Controller_Action_Helper_UnitTestConfig
 * RapidFunnel/Controller/Action/Helper/UnitTestConfig.php
 *
 * @category  Action_Helper
 * @package   Digitalassets_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Controller_Action_Helper_UnitTestConfig
{

    protected static $_inst;

    /**
     * RapidFunnel_Controller_Action_Helper_UnitTestConfig::_instance()
     *
     * Function to return the instance with all config data
     *
     * @param string $keyParam index of the key
     *
     * @return array config data
     */
    public static function _instance($keyParam = NULL)
    {
        if (!isset(self::$_inst)) {
            self::$_inst = new RapidFunnel_Controller_Action_Helper_DbConnectionPhpUnit();
            //getting data from config file

            $config = new Zend_Config_Ini(
                APP_PATH . '/../lib/RapidFunnel/Config/app_unit_test.ini',
                APP_ENV
            );

            //Putting all data in an array
            foreach ($config as $key => $value) {
                self::$_inst->$key = $value;
            }
        }

        //If the key is set
        if ($keyParam) {
            $configData = self::$_inst->$keyParam;

            //checking if data exists
            if (isset($configData)) {
                return $configData;
            }
            return self::$_inst;
        }
        return self::$_inst;
    }

}