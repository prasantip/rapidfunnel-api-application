<?php
/**
 * RapidFunnel_Controller_Action_Helper_AccountUser File Doc Comment
 * PHP version 5.5.9
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Controller_Action_Helper_AccountUser
 * RapidFunnel/Controller/Action/Helper/AccountUser.php
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Controller_Action_Helper_AccountUser extends Zend_Controller_Action_Helper_Abstract
{

    public $message;

    /**
     * RapidFunnel_Controller_Action_Helper_AccountUser::isEnterpriseFreeUser()
     * Functionality to check if the user if enterprise free type or not
     *
     * @param array $userDetails user various details
     *
     * @return bool
     */
    public function isEnterpriseFreeUser($userDetails)
    {
        $returnVar = false;

        if ((!empty($userDetails['id'])) && (0 < $userDetails['id'])) {
            $accountModel = new RapidFunnel_Model_Account();

            //if ignore payment is ON the user will treated as paid user
            if(!$userDetails['ignoreUserPayment']) {
                $key = array('accountLevel', 'trial', 'passThrough');
                $accountModel->load($userDetails['accountId'], $key);

                // get constant
                $config = RapidFunnel_Configs::_instance();
                $accLevel = $config->accountLevel;
                $userConfig = $config->accountUserStatus;
                $enterpriseLevel = $accLevel->enterprisePro;

                // if enterprise level and passthrough is on and not in trial period
                if (($enterpriseLevel == $accountModel->accountLevel) // level enterprise
                        && ('0' == $accountModel->trial) // not in trial
                        && ($accountModel->passThrough) // pass through is on
                ) {
                    if ('1' == $userDetails['payThroughAuthNet']) {
                        // Check in card profile table
                        $acntUsrCrdProflModel = new RapidFunnel_Model_AccountUserCardProfile();
                        $acntUsrCrdProflDtl = $acntUsrCrdProflModel->getDetailByUserId($userDetails['id']);
                    } else {
                        // Check in billingInfo, payment through stripe
                        $acntUsrCrdProflModel = new RapidFunnel_Model_AccountUserBillingInfo();
                        $acntUsrCrdProflDtl = $acntUsrCrdProflModel->getBillingDetailsByUserId($userDetails['id']);
                    }

                    if (($config->accountLevel->userTrialOn !== $userDetails['isTrial'])
                        && ((!$acntUsrCrdProflDtl || ('' === $acntUsrCrdProflDtl['id']))
                            || ($userConfig->suspend === $userDetails['status']))
                    ) {
                        $legalshieldConfig = $config->legalshield->toArray();

                        if (in_array(
                            $userDetails['accountId'],
                            $legalshieldConfig['accountId']
                        )) {
                            if ($config->accountLevel->userTrialOn
                                !== $userDetails['isTrial']
                                && ($userConfig->suspend
                                    === $userDetails['status'])
                            ) {
                                $returnVar = true;
                            }
                        } else {
                            $returnVar = true;
                        }
                    }
                }
            }
        }

        return $returnVar;
    }

    /**
     * RapidFunnel_Controller_Action_Helper_AccountUser::isTrialUser()
     * Common function to check whether a user is a Trial User or not
     *
     * @param int $userId to update in user table
     * @param int $isTrial to check the User is in Trial period or not
     * @param string $trialExpiresOn string
     *
     * @return string showing
     * @throws Exception
     */
    public function isTrialUser($userId, $isTrial, $trialExpiresOn = null)
    {
        try {
            $config = RapidFunnel_Configs::_instance();

            // Check whether user is in Trial period or not
            if ($config->accountLevel->userTrialOn === $isTrial) {

                // check if trial period is over or not
                if (isset($trialExpiresOn)
                    && !empty($trialExpiresOn)
                ) {
                    $trialExpiresOn = Date(
                        "Y-m-d", strtotime($trialExpiresOn)
                    );
                    $todayDate = Date("Y-m-d");

                    // if trial period ends, update "isTrial" field to "0" for user
                    // While login if the user's trial expires it'll update the field accordingly
                    if ($todayDate > $trialExpiresOn) {
                        $isTrial = '0';
                        //update query
                        $userModel = new RapidFunnel_Model_AccountUser();
                        $userModel->db->query(
                            'UPDATE accountUser SET isTrial = 0 WHERE id = '
                            . $userId
                        );
                    }
                }
            }

            return $isTrial;
        } catch (Exception $exc) {
            error_log($exc->getMessage());
            $this->message = $exc->getMessage();
            return false;
        }
    }

}