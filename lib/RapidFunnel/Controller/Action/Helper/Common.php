<?php

/**
 * RapidFunnel_Controller_Action_Helper_Common
 * RapidFunnel/Controller/Action/Helper/Common.php
 *
 * @category  Action_Helper
 * @package   Digitalassets_Controller_Action_Helper
 * @copyright 2014 Digitalassets Companies
 * @license   Not licensed for external use
 */

class RapidFunnel_Controller_Action_Helper_Common extends
    Zend_Controller_Action_Helper_Abstract
{
    public  $message;
    /**
     * Concatenate array to string with comma separated
     *
     * @param <Array> $fieldVal array of values
     *
     * @return array|string
     */
    public function concatMultiField($fieldVal)
    {
        if (isset($fieldVal)) {
            $fieldVal = array_filter($fieldVal);
            if (!empty($fieldVal)) {
                if (count($fieldVal) > 1) {
                    $fieldVal = implode(",", $fieldVal);
                } else {
                    $fieldVal = $fieldVal[0];
                }
            } else {
                $fieldVal = '';
            }
        } else {
            $fieldVal = '';
        }

        return $fieldVal;
    }

    /**
     * Check if the user belongs to Legalshield check if advantage status is 1
     * or 0 and if 0 make user Free and if 1 make user Paid
     * @param int $accountId accountId
     * @param int $userId user Id
     *
     * @throws Exception
     *
     * @return array|bool
     */
    public function updateAdvantageStatus($accountId, $userId)
    {
        try {
            $legalshieldConfig = RapidFunnel_Configs::_instance('legalshield')
                ->toArray();

            if (in_array($accountId, $legalshieldConfig['accountId'])) {
                $accountUserModel = new RapidFunnel_Model_AccountUser();
                $legalshieldModel = new RapidFunnel_Model_AccountUserLegalshield();
                $legalshieldInfo = $legalshieldModel->getLegalshieldInfo(
                    $userId
                );

                if (!empty($legalshieldInfo)) {
                    $lsDetails = $legalshieldModel->getAdvantageStatus(
                        $legalshieldInfo['accessToken'], 1
                    );

                    $whereUser = "id = $userId";
                    $whereLS = "accountUserId = $userId";

                    $tableUser = 'accountUser';
                    $tableLS = 'accountUserLegalshield';

                    if (1 === $lsDetails['can_have_crm']) {
                        $dataUser = array(
                            'status' => '1', 'isTrial' => 0,
                            'repId' => $lsDetails['repId'],
                            'repId2' => $lsDetails['repId2']
                        );
                        $dataLS = array(
                            'advantageStatus' => 1

                        );

                        //update user status
                        $accountUserModel->updateUserStatus(
                            $dataUser, $whereUser, $tableUser
                        );

                        //Update advantage status
                        $accountUserModel->updateUserStatus(
                            $dataLS, $whereLS, $tableLS
                        );

                        //Delete Info from memcache
                        $memcache = new RapidFunnel_Model_Memcache();
                        $memcache->deleteLoginDashboardInfo($userId);

                    } elseif (0 === $lsDetails['can_have_crm']) {
                        $dataUser = array(
                            'status' => '3','repId' => $lsDetails['repId'],
                            'repId2' => $lsDetails['repId2']
                        );

                        //If trial period is not expired then
                        //no need to make user free
                        //else make user free
                        if ($accountUserModel->checkTrialExpiredOrNot($userId)) {
                            $dataUser['isTrial'] = 1;
                        } else {
                            $dataUser['isTrial'] = 0;
                        }

                        $dataLS = array(
                            'advantageStatus' => 0
                        );

                        //update user status
                        $accountUserModel->updateUserStatus(
                            $dataUser, $whereUser, $tableUser
                        );

                        //Update advantage status
                        $accountUserModel->updateUserStatus(
                            $dataLS, $whereLS, $tableLS
                        );
                    }
                }
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->message = $e->getMessage();
            return false;
        }
    }

    /**
     * Update access token for user after authentication
     *
     * @param object $accountUserModel account user model
     * @param int    $userId  user id
     * @param string $token   unique token
     * @param int    $webView web view for mobile
     *
     * @return int | string status Whether data updated into db or not
     *
     * @throws InvalidArgumentException
     */
    function updateAccessToken($accountUserModel, $userId, $token, $webView = 0)
    {
        try {

            //Ensure model is not empty and is object
            if (empty($accountUserModel) || !is_object($accountUserModel)) {
                throw new InvalidArgumentException('Invalid model provided.');
            }

            //Ensure userId is not empty and is numeric
            if (empty($userId) || !is_numeric($userId)) {
                throw new InvalidArgumentException('Invalid User Id provided.');
            }

            //Ensure token is not empty and is string
            if (empty($token) || !is_string($token)) {
                throw new InvalidArgumentException('Invalid token provided.');
            }

            $webViewToken = 0;
            //load the id
            $accountUserModel->load($userId);

            if (!empty($webView)) {
                $webViewToken = md5(
                    base_convert(
                        rand(8, 16) . intval(microtime(true) * 1000), 10, 36
                    )
                );
                $accountUserModel->webViewToken = $webViewToken;
            } else {
                // calculate access token expiry time
                $userAccessTokenExpireDays = RapidFunnel_Configs::_instance('user')
                    ->accessTokenExpires;

                $accountUserModel->accessTokenExpiresOn = date(
                    'Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '+' .
                    $userAccessTokenExpireDays . ' days'));

                $accountUserModel->accessToken = $token;
                $accountUserModel->lastLogin = date("Y-m-d H:i:s");
            }

            if ($accountUserModel->update()) {
                if (!empty($webView)) {
                    return $webViewToken;
                } else {
                    return 1;
                }
            } else {
                return 0;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->message = $e->getMessage();
            return 0;
        }

    }

     /** Functionality to add account details in mail content
     *
     * @param int $accountId account id
     * @param string $htmlContent mail html content
     * @param string $textContent mail text content
     *
     * @return array
     */
    function addAccountDetailsInMail($accountId, $htmlContent, $textContent)
    {
        $retArray = array();
        $retArray['status'] = 0;

        if(is_numeric($accountId) || !empty($accountId)) {
            // get account details
            $accountModel = new RapidFunnel_Model_Account();
            $accountDetails = $accountModel->getAccountDetail($accountId);

            $accountDetailsFontSize = RapidFunnel_Configs::_instance('accountDetails')
                ->fontSize;

            $accountMailAddContent = $accountDetails['address'] . ' ' .
                $accountDetails['suite'] . ', ' . $accountDetails['city'] .
                ', ' . $accountDetails['stateCode'] . ' ' . $accountDetails['zip'];

            $retArray['htmlContent'] = $htmlContent . '<br/><br/><p><font size="' .
                $accountDetailsFontSize . '">' . $accountMailAddContent . '</font></p>';
            $retArray['textContent'] = $textContent . ' ' .
                $accountMailAddContent;
            $retArray['status'] = 1;
        }

        return $retArray;
    }

    /**
     * Function to generate key
     *
     * @param string $keyType type of key
     * @param int $userId user's id
     * @param string $userEmail user email
     * @param string $groupCode group code
     *
     * @return string $key generated key
     */
    public function generateKey($keyType, $userId = 0, $userEmail = '',
    $groupCode = null)
    {
        $domainName = RapidFunnel_Configs::_instance('serverInfo')->domain;

        switch($keyType) {
            case 'apiLogin':
                $key = $domainName . '_' . $keyType . '_' . md5($userEmail);
                break;
            case 'apiContactList':
            case 'apiAuthToken':
            case 'apiDashboardInfo':
                $key = $domainName . '_' . $keyType . '_' . md5($userId);
                break;
        }
        return $key;
    }

    /**
     * Function to remove hyperlink from email body
     *
     * @param string $textToFind   string to find in hyperlink
     * @param string $mailHtmlBody html body to remove hyperlink
     *
     * @return string email body
     */
    public function removeHyperLinkInEmail($textToFind, $mailHtmlBody)
    {
        // $textToFind should not be empty
        if (empty($textToFind) || ! is_string($textToFind)) {
            throw new InvalidArgumentException('Invalid string supplied.');
        }
        // $mailHtmlBody should not be empty
        if (empty($mailHtmlBody) || ! is_string($mailHtmlBody)) {
            throw new InvalidArgumentException('Invalid mailHtmlBody supplied.');
        }

        $newPlaceHolder = str_replace(
            array('[', ']'), array('\[', '\]'), $textToFind
        );

        return preg_replace(
            '/href=\"' . $newPlaceHolder
            . '\"(.*?)>(.*?)<\/a>/', '', $mailHtmlBody
        );
    }

    /**
     * Function to encode an array to support utf8
     *
     * @param array $array data to be encoded to utf8
     *
     * @return array returns utf8 encoded data
     */
    public function utf8_converter($array)
    {
        if (isset($array) && !empty($array)) {
            array_walk_recursive($array, function (&$item) {
                if (!mb_detect_encoding($item, 'utf-8', true)) {
                    $item = utf8_encode($item);
                }
            });
        }
        return $array;
    }

}
