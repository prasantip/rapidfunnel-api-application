<?php

/**
 * RapidFunnel_Controller_Action_Helper_Authenticate
 * RapidFunnel/Controller/Action/Helper/Authenticate.php
 *
 * Used to authenticate a users based on password and email
 * Log the user in and set the necessary session info
 *
 * Also used to authenticate API requests using the provided
 * key. (future)
 *
 * @category  Controller
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    rapid funnel <kraft.adam@gmail.com>
 * @copyright 2014 RapidFunnel, Inc.
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com
 */
class RapidFunnel_Controller_Action_Helper_Authenticate extends
    Zend_Controller_Action_Helper_Abstract
{

    public $message;

    /**
     * @function getUserPaymentStatus($userId)
     * @purpose this method will decide whether user will be paid or free based and its status
     *
     * @param int $userId
     * @return array with payment Info
     */
    public function getUserAccountStatus($userId)
    {
        $response['status'] = false;
        try {

            //getting the models
            $user = new RapidFunnel_Model_AccountUser();
            $account = new RapidFunnel_Model_Account();
            //get user info
            $user->load($userId);
            //get account Info
            $account->load($user->accountId);

            // check for inactivated account
            if (2 == $account->status) {
                $this->message = 'This account is inactive. Please contact admin.';
            } else {

                if ('1' === $account->accountLevel) {
                    $response['accountLevel'] = 'free';
                } else {
                    $response['accountLevel'] = 'paid';
                    //check ignore payment
                    if ('1' == $user->ignoreUserPayment) {
                        $response['ignoreUserPayment'] = '1';
                    } else {
                        $response['ignoreUserPayment'] = '0';
                        // check for trial period
                        if (1 == $account->trial) {
                            // check if trial period is over or not
                            $todayDate = Date("Y-m-d");
                            $trialPeriodTill = Date('Y-m-d',
                                strtotime($account->dateCreated . " +" . $account->trialPeriod . " days"));

                            $response['trialPeriod'] = '1';
                            $response['trialPeriodTill'] = $trialPeriodTill;
                            // if trial period ends, update "trial" field
                            if ($todayDate > $trialPeriodTill) {
                                $account->trial = '0';
                                $account->update();
                                $response['trialPeriod'] = '0';
                            }
                        }

                        $response['passThrough'] = $account->passThrough;
                        if ('1' == $user->payThroughAuthNet) {
                            // store account user card profile id
                            $billingInfoModel = new RapidFunnel_Model_AccountUserCardProfile();
                            $cardProfileDetail = $billingInfoModel->getDetailByUserId($user->id);
                        } else {
                            // store account user card profile id
                            $billingInfoModel = new RapidFunnel_Model_AccountUserBillingInfo();
                            $cardProfileDetail = $billingInfoModel->getBillingDetailsByUserId($user->id);
                        }

                        $userCardProfileId = '';
                        if ($cardProfileDetail
                            && ('' != $cardProfileDetail['id'])
                        ) {
                            $userCardProfileId = $cardProfileDetail['id'];
                        }

                        $config = RapidFunnel_Configs::_instance();
                        $enterpriseProAccountLevel
                            = $config->accountLevel->enterprisePro;

                        //Free enterprisePro User
                        if (($enterpriseProAccountLevel
                                == $account->accountLevel) // level enterprise
                            && ($config->accountStatus->trial
                                === $account->trial) // not in trial
                            // pass through is on
                            && ($config->accountLevel->userTrialOn
                                !== $user->isTrial)
                            && (('' == $userCardProfileId)
                                || ($config->accountUserStatus->suspend
                                    === $user->status)) // card detail not present OR suspended user
                        ) {
                            if ($this->isLSFreeEnterpriseProUser(
                                $user->accountId, $config, $user->isTrial,
                                $user->status
                            )
                            ) {
                                $response['freeEnterpriseProUser'] = '1';
                            }
                        }
                    }
                }
            }

            $response['status'] = true;
        } catch (Exception $e) {
            error_log($e->getMessage());
            $this->message = $e->getMessage();
            $response['status'] = false;
        }

        return $response;
    }

    /**
     * Authenticating accountUsers by username and password or webViewToken
     *
     * @param string $email   user name
     * @param string $pass    password
     * @param int $webView    webView token send
     *                        to mobile app for legalshield user
     *
     * @return mixed authenticate details or false status if failed to authenticate
     * @throws Exception
     */
    public function authApiUser($email, $pass, $webView = 0)
    {
        //getting the user model
        $user = new RapidFunnel_Model_AccountUser();
        //get users information
        $userDetail = $user->loadByEmail($email);
        $config = RapidFunnel_Configs::_instance();
        $passwordLength = $config->accountUser->passwordLength;


        if ($userDetail) {
            $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
            $authAdapter->setTableName('accountUser')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password');

            if (1 === $webView) {
                $authAdapter->setCredentialTreatment(
                    '? AND status != "0" AND isDelete = 0'
                );

                // destroy previous session
                $this->logout();
            } elseif (strlen($userDetail['password']) == $passwordLength) {
                $authAdapter->setCredentialTreatment(
                    'md5(CONCAT(?, salt)) AND status != "0" AND isDelete = 0'
                );
            } else {
                $authAdapter->setCredentialTreatment(
                    'SHA2(CONCAT(?, salt), 512) AND status != "0" '
                    . 'AND isDelete = 0'
                );
            }

            $authAdapter->setIdentity($email);
            $authAdapter->setCredential($pass);
            $auth = Zend_Auth::getInstance();

            // check
            if ($auth->authenticate($authAdapter)->isValid()) {
                //Clearing the zend auth records after authentication
                Zend_Auth::getInstance()->clearIdentity();

                // Check whether user is in Trial period or not
                $accountUserHelper
                    = new RapidFunnel_Controller_Action_Helper_AccountUser();
                $userDetail['isTrial'] = $accountUserHelper->isTrialUser(
                    $userDetail['id'], $userDetail['isTrial'],
                    $userDetail['trialExpiresOn']
                );

                //get user account status
                $account = $this->getUserAccountStatus($userDetail['id']);

                //To check if correct account info fetched
                //Checking if user is active or is a free user
                if (($account['status']) && (('1' === $userDetail['status']) ||
                        (isset($account['freeEnterpriseProUser']) &&
                            ('1' === $account['freeEnterpriseProUser']))
                        || (isset($account['ignoreUserPayment']) &&
                            ('1' === $account['ignoreUserPayment']))
                        || (isset($userDetail['isTrial']) &&
                            ($config->accountLevel->userTrialOn === $userDetail['isTrial']))
                    )) {

                    $return['userId'] = $userDetail['id'];
                    $return['accountId'] = $userDetail['accountId'];
                    $return['firstName'] = $userDetail['firstName'];
                    $return['lastName'] = $userDetail['lastName'];
                    $return['role'] = 'account-user';
                    $return['status'] = $userDetail['status'];
                    $return['forceChange'] = $userDetail['forceChange'];
                    $return['isTrial'] = $userDetail['isTrial'];
                    $return['trialExpiresOn'] = $userDetail['trialExpiresOn'];
                    $return['accessToken'] = isset($userDetail['accessToken']) ?
                        $userDetail['accessToken'] : '';
                    $return['accessTokenExpiresOn'] = isset($userDetail['accessTokenExpiresOn']) ?
                        $userDetail['accessTokenExpiresOn'] : '';
                    $return['accountInfo'] = $account;
                    $return['freeUser'] = false;
                    $return['autoSuspendStatus'] = $userDetail['autoSuspendStatus'];
                } else {
                    if ($account['status']
                        && (isset($userDetail['status'])
                            && $config->accountUserStatus->suspend
                            === $userDetail['status'])
                    ) {
                        $return['freeUser'] = true;
                    } else {
                        $return = false;
                    }
                }
            } else {
                if (0 == $userDetail['status']) {
                    $return['inactiveStatus'] = false;
                }
                else {
                    $return = false;
                }
            }
        } else {
            $return = false;
        }

        return $return;
    }

    /**
     * To authenticate users by access token
     *
     * @param int    $userId      users id
     * @param string $accessToken token
     *
     * @return bool status
     */
    public function authUserToken($userId, $accessToken)
    {
        $ret = false;
        try {
            //To check access token in memcache
            $memcacheModel = new RapidFunnel_Model_Memcache();
            //Memcache checking
            $authenticated = $memcacheModel->validateAccessToken(
                $userId, $accessToken
            );

            if (true === $authenticated) {
                $ret = true;
            } else {
                //getting the user model
                $accountUserModel = new RapidFunnel_Model_AccountUser();
                //get account users information
                $keys = array('accessToken', 'accessTokenExpiresOn');

                $accountUserModel->load($userId, $keys);

                if ($accessToken === $accountUserModel->accessToken
                    && (date('Y-m-d H:i:s')
                        < $accountUserModel->accessTokenExpiresOn)
                ) {
                    $ret = true;
                }
            }

            return $ret;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * function logout()
     * @purpose to logout the user
     * @param none
     * @return boolean
     */
    public function logout()
    {
        //Clearing the zend auth records
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::namespaceUnset('legalshieldSession');
        return true;
    }

    /**
     * function getMessage()
     * @purpose to get error Messages
     * @param none
     * @return boolean
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Check freeEnterpriseProUser status
     *
     * @param int      $accountId  account id
     * @param object   $config     config related info
     * @param int      $isTrial    is trial or not
     * @param string   $userStatus active or not
     *
     * @return bool status of freeEnterpriseProUser
     */
    public function isLSFreeEnterpriseProUser(
        $accountId, $config, $isTrial, $userStatus
    )
    {
        $legalshieldConfig = $config->legalshield->toArray();

        if (in_array(
            $accountId, $legalshieldConfig['accountId']
        )) {
            if ($config->accountLevel->userTrialOn !== $isTrial
                && ($config->accountUserStatus->suspend === $userStatus)
            ) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Set response after authenticate
     *
     * @param $authenticate
     * @param int $rememberMe rememberMe value 1/0
     *
     * @return array
     */
    public function setAuthResponse($authenticate, $rememberMe = 0) {

        $updateStatus = true;
        $token = $authenticate['accessToken'];
        $response['response']['mobileLogo'] = "null";

        if(empty($token) || (date('Y-m-d H:i:s') >
                $authenticate['accessTokenExpiresOn'])) {
            //generate unique token for the user
            $token = md5(
                base_convert(
                    rand(8, 16) . intval(microtime(true) * 1000), 10, 36
                )
            );

            $cmnHelper = new RapidFunnel_Controller_Action_Helper_Common();
            $userModel = new RapidFunnel_Model_AccountUser();
            //Update access token
            $updateStatus = $cmnHelper->updateAccessToken(
                $userModel, $authenticate['userId'], $token
            );
        }

        //check if the access token updated to DB
        //else send the error message
        if (!empty($updateStatus)) {
            $accountId = $authenticate['accountId'];
            $config = RapidFunnel_Configs::_instance();
            $baseUrl = $config->serverInfo->domain;
            $imageLocation = $config->uploadPath->branding->account
                . '/' . $accountId;

            $accountBrandingModel
                = new RapidFunnel_Model_AccountBranding();
            $brandingDetail
                = $accountBrandingModel->getBrandingDetails(
                $accountId
            );

            $authenticate['accessToken'] = $token;
            $response['response']['status'] = 'true';
            $response['response']['rememberMe'] = $rememberMe;
            $response['response']['content'] = $authenticate;
            if (!empty($brandingDetail['mobileLogo'])) {
                $response['response']['mobileLogo'] = $baseUrl
                    . '/' . $imageLocation . '/'
                    . $brandingDetail['mobileLogo'];
            }

            // bind branding colors and their offset values
            $brandingColors['primaryColor']
                = $brandingDetail['primaryColor'];
            $brandingColors['primaryColorOffset']
                = $brandingDetail['primaryColorOffset'];
            $brandingColors['secondaryColor']
                = $brandingDetail['secondaryColor'];
            $brandingColors['secondaryColorOffset']
                = $brandingDetail['secondaryColorOffset'];
            $brandingColors['tertiaryColor']
                = $brandingDetail['tertiaryColor'];
            $brandingColors['tertiaryColorOffset']
                = $brandingDetail['tertiaryColorOffset'];

            $response['response']['brandingColors']
                = $brandingColors;
        } else {
            $response['response']['errorMessage']
                = 'Error during login, please retry after sometimes';
        }
        return $response;
    }

}
