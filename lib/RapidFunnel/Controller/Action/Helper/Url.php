<?php
/**
 * RapidFunnel_Controller_Action_Helper_Url File Doc Comment
 *
 * PHP version 5.5.9
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2013 DigitalAssets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */

/**
 * RapidFunnel_Controller_Action_Helper_Url
 * RapidFunnel/Controller/Action/Helper/Url.php
 *
 * @category  Action_Helper
 * @package   RapidFunnel_Controller_Action_Helper
 * @author    Rapid Funnel <support@rapidfunnel.com>
 * @copyright 2014 DigitalAssets Companies
 * @license   Not licensed for external use
 * @link      http://my.rapidfunnel.com/
 */
class RapidFunnel_Controller_Action_Helper_Url
    extends Zend_Controller_Action_Helper_Abstract
{

    public $resourceNotificationUrl = '';

    /**
     * RapidFunnel_Controller_Action_Helper_Url::changeResourceUrlsInContent()
     *
     * Functionality to modify the resource URLs in email content
     *
     * @param int        $userId
     * @param string     $emailContent
     * @param string     $receiverEmail
     * @param bool|false $campaign
     *
     * @return mixed modified url
     */
    public function changeResourceUrlsInContent($userId, $emailContent,
        $receiverEmail, $campaign = false
    ) {
        $config = RapidFunnel_Configs::_instance();
        $baseUrl = $config->serverInfo->domain;
        $this->resourceNotificationUrl = $baseUrl . '/link-notification/'
            . $userId . '/' . urlencode(base64_encode($receiverEmail)) . '/';

        // Functionality to replace all the links to resource notification links
        // Add link in anchor tag for content in campaign
        if ($campaign) {
            $emailContent = preg_replace_callback(
                '$\b(https?|ftp|file)://[-A-Z0-9+&()@#/%?=~_|!:,.;\$]*[-A-Z0-9+&()@#/%=~_|]$i',
                function ($matches) {
                    return '<a href="' . $this->resourceNotificationUrl
                    . urlencode(urlencode(base64_encode(($matches[0])))) . '">'
                    . $matches[0] . '</a>';
                }, $emailContent
            );
        } else {
            $emailContent = preg_replace_callback(
                '$\b(https?|ftp|file)://[-A-Z0-9+&()@#/%?=~_|!:,.;\$]*[-A-Z0-9+&()@#/%=~_|]$i',
                function ($matches) {
                    return $this->resourceNotificationUrl . urlencode(
                        urlencode(base64_encode(($matches[0])))
                    );
                }, $emailContent
            );
        }
        return $emailContent;
    }

}
