<?php

/*
 * This class is used to access the config variables of the ini setting
 * This class can be used in any file
 *
 * RapidFunnel_Configs
 * RapidFunnel/Configs.php
 *
 * @category  Config
 * @package   RapidFunnel
 * @copyright 2014 Digital assets Companies
 * @license   Not licensed for external use
 */

class RapidFunnel_Configs
{

    protected static $_inst;

    /*
     * function _instance()
     * @purpose function to return the instance with all cofig data
     * @param string $keyParam index of the key
     * @return array cofig data
     */
    public static function _instance($keyParam = NULL)
    {
        if (!isset(self::$_inst)) {
            self::$_inst = new RapidFunnel_Configs();
            //getting data from config file
            $commonConfig = new Zend_Config_Ini(COMMON_CONFIG_INI_PATH, APP_ENV);
            $config = new Zend_Config_Ini(APP_INI_PATH, APP_ENV, array('allowModifications'=>true));
            $config->merge($commonConfig);
            $config->setReadOnly();

            //Putting all data in an array
            foreach ($config as $key => $value) {
                self::$_inst->$key = $value;
            }
        }

        //If the key is set
        if ($keyParam) {
            $configData = self::$_inst->$keyParam;

            //checking if data exists
            if (isset($configData)) {
                return $configData;
            }
            return self::$_inst;
        }
        return self::$_inst;
    }

}
