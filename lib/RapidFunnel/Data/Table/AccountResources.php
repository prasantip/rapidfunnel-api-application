<?php

/**
 * RapidFunnel_Data_Table_AccountResources
 * RapidFunnel/Data/Table/AccountResources.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2015 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountResources extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountResources';
    protected $_primary = 'id';
}
