<?php

/**
 * RapidFunnel_Data_Table_AccountUserGroup
 * RapidFunnel/Data/Table/AccountUserGroup.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountUserGroup extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountUserGroup';
    protected $_primary = array('accountUserId', 'accountGroupId');
}
