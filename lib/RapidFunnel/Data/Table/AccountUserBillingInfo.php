<?php

/**
 * RapidFunnel_Data_Table_AccountUserBillingInfo
 * RapidFunnel/Data/Table/AccountUserBillingInfo.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountUserBillingInfo extends Zend_Db_Table_Abstract
{
    protected $_name = 'accountUserBillingInfo';
    protected $_primary = 'id';
}
