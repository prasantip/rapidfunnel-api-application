<?php

/**
 * RapidFunnel_Data_Table_AccountUserIncentive
 * RapidFunnel/Data/Table/AccountUserIncentive.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountUserIncentive extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountUserIncentive';
    protected $_primary = array('accountIncentiveProgramId', 'userId');
}
