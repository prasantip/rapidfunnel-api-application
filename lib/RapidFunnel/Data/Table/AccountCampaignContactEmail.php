<?php

/**
 * RapidFunnel_Data_Table_AccountCampaignContactEmail
 * RapidFunnel/Data/Table/AccountCampaignContactEmail.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountCampaignContactEmail extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountCampaignContactEmail';
    protected $_primary = 'id';
}