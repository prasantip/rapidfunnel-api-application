<?php

/**
 * RapidFunnel_Data_Table_AccountCampaignEmail
 * RapidFunnel/Data/Table/AccountCampaignEmail.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountCampaignEmail extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountCampaignEmail';
    protected $_primary = 'id';
}
