<?php

/**
 * RapidFunnel_Data_Table_AccountBranding
 * RapidFunnel/Data/Table/AccountBranding.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountBranding extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountBranding';
    protected $_primary = 'id';
}
