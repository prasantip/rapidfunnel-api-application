<?php

/**
 * RapidFunnel_Data_Table_Countries
 * RapidFunnel/Data/Table/Countries.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_Countries extends Zend_Db_Table_Abstract
{
    protected $_name = 'countries';
    protected $_primary = 'id';
}
