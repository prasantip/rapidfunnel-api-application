<?php

/**
 * RapidFunnel_Data_Table_AccountIncentiveProgram
 * RapidFunnel/Data/Table/AccountIncentiveProgram.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountIncentiveProgram extends Zend_Db_Table_Abstract {

    protected $_name = 'accountIncentiveProgram';
    protected $_primary = 'id';
}
