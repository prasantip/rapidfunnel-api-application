<?php

/**
 * RapidFunnel_Data_Table_Account
 * RapidFunnel/Data/Table/Account.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_Account extends Zend_Db_Table_Abstract
{

    protected $_name = 'account';
    protected $_primary = 'id';

}
