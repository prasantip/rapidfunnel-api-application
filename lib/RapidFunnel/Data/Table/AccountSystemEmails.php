<?php

/**
 * RapidFunnel_Data_Table_AccountSystemEmails
 * RapidFunnel/Data/Table/AccountSystemEmails.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountSystemEmails extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountSystemEmails';
    protected $_primary = 'id';
}
