<?php

/**
 * RapidFunnel_Data_Table_AccountUser
 * RapidFunnel/Data/Table/AccountUser.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountUser extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountUser';
    protected $_primary = 'id';
}
