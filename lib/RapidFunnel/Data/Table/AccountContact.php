<?php

/**
 * RapidFunnel_Data_Table_AccountContact
 * RapidFunnel/Data/Table/AccountContact.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountContact extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountContact';
    protected $_primary = 'id';
}
