<?php

/**
 * RapidFunnel_Data_Table_AccountUserCardProfile
 * RapidFunnel/Data/Table/AccountUserCardProfile.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountUserCardProfile extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountUserCardProfile';
    protected $_primary = 'id';
}
