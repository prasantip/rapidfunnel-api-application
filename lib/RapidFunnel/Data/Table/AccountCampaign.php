<?php

/**
 * RapidFunnel_Data_Table_AccountCampaign
 * RapidFunnel/Data/Table/AccountCampaign.php
 *
 * @category  Database
 * @package   RapidFunnel_Data_Table
 * @copyright 2013 Digitalassets Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Table_AccountCampaign extends Zend_Db_Table_Abstract
{

    protected $_name = 'accountCampaign';
    protected $_primary = 'id';
}
