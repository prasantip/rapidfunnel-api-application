<?php

/**
 * RapidFunnel_Data_Source
 * RapidFunnel/Data/Source.php
 *
 * Enum class for passing data
 * source information to helpers, etc.
 *
 * TODO: implement splEnum once released
 * Until then...think of this as an Enum class
 * It wont look much different once splEnum is avail
 *
 *
 * @category  Data
 * @package   RapidFunnel_Data
 * @copyright 2012 RapidFunnel Companies
 * @license   Not licensed for external use
 */
class RapidFunnel_Data_Source
{

    const COUNTRIES = 'RapidFunnel_Data_Table_Countries';

}
