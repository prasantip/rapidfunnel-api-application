# RF Release WorkFlow

####Branch - URL
**development - http://s1.rapidfunnel.com/**

**qa - http://qa1.rapidfunnel.com/**

**release-v1 / release-v2 /**

**hotfixes - http://r1.rapidfunnel.com**

**master - http://my.rapidfunnel.com/**


1. Create branch from master for each ticket in bit-bucket. (EX- TAN-KW813)
2. After resolve the ticket run unit tests in your local machine If any test case fails fix the issue.

Once test cases run successfully create pull request to merge the branch(TAN-KW813) with development and assign other developers to do peer review. After review, if the pull request is declined then developer has to make the changes as per comments added in peer review and follow step 1 and 2 unless it get approved. Once the pull request get approved then merge branch(Ex: TAN-KW813) to development and test.

3. Once It is tested by developer then merge branch(Ex: TAN-KW813) to qa and QA Team tests there.
4. If QA Team finds any issue in qa related to the ticket(Ex: TAN-KW813) then developer should fix the issue in that ticket and go through step 2 to 4 again. If QA Team finds any bug/issue in qa and that issue exists in production as well then new ticket should be created for the issue and go through step 2 to 4. The ticket marks as completed if there is no issue.
5. When we plan to do production push, we have to decide which tickets will be moved to production. At that time we will create a branch from master called as release-v1 and merge the decided tickets(completed) to release-v1 branch and then QA Team will do a quick round of test in r1.rapidfunnel.com and confirm.
6. After confirm from QA Team we will take backup of master called backup-v1 and take sql backup of production db and merge release-v1 to master and close the branch release-v1 after production looks fine.
7. master will be merged back to each branch(development, qa) so that every branch has latest changes. Dev need to update his current branch with master.
8. Step 5-7 will be followed before each release following different versions(release-v2, release-v3).
9. For hotfixes, we will follow step 1 and step 2. Create branch called hotfixes from master and merge the ticket created in step 1 into hotfixes. Once it is tested and confirmed by QA Team, merge hotfixes branch into master and follow step 7.  Once the fixes looks fine in production, delete the branch hotfixes.
10. once we are at step 5(release test going on release server - r1.rapidfunnel.com), no other ticket/hotfix ticket should go to that server until step 7 finishes.
11. Delete the temporary branch(TAN-KW813) after release once everything in production server looks fine.

**Note:-**

We can have replica of production server db which will be pointed by r1.rapifunnel.com so that before release we will have exact environment like Production.
