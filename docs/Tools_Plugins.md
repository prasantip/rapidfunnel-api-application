#Tools and Plugins for Development

**IDE**: PhpStorm 10.0.1

**OS**: Ubuntu 14.04 LTS

##Plugins:

###1.CodeSniffer:

 **Install CodeSniffer**:~$ sudo pear install PHP_CodeSniffer  
 
 Configuration with IDE:
---
 ![](img/ConfiguringCS1.png)  

 Inspection configuration in IDE:
---
 ![](img/ConfiguringCS2.png)

 **Ex**:  

 How it shows warning during development:
---
 ![](img/CSErrorDisplay.png)

###2.MessDetector:

  **Install MessDetector**:http://phpmd.org/download/index.html  
  
  Configure with IDE:
---
  ![](img/ConfiguringMD1.png)  
  
  Inspection configuration in IDE:
---
  ![](img/ConfiguringMD2.png) 
  
  **Ex**:  
  
How it shows warning during development:
---
![](img/MDErrorDisplay.png)
  
  **Installation and configuration**:https://www.jetbrains.com/phpstorm/help/configuring-xdebug.html
