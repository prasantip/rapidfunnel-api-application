#API Separation

Copied the code base from Rapidfunnel web app (let say servername : "rf.dev")

Created new project for RF API(let say servername :"rf-api.dev").

Place it in document root folder like /var/www/html/rf-api

Set up in vagrant which points to a new url for RF API.

##SET multiple website in Vagrant :

### Create New Virtual Host Files

sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/rf-api.conf

Open the new file in your editor with root privileges:

sudo nano /etc/apache2/sites-available/rf-api.conf

    NameVirtualHost *:8082
    <VirtualHost *:80>
    
            ServerAdmin webmaster@localhost
            DocumentRoot /var/www/html/rf-api/public
    
            ServerName rf-api.dev
            ServerAlias *.rf-api.dev
            # ServerName test.rapidfunnel.com
            Timeout 900
            DirectoryIndex api/index.php
    
            <Directory "/var/www/html/rf-api">
                    Options Indexes FollowSymLinks
                    AllowOverride All
                    Order allow,deny
                    Allow from all
            </Directory>
    
            SetEnv ENVIRONMENT Development
            php_value include_path ".:/var/www/html/ZendFramework-1.12.0/library"
    
            # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
            # error, crit, alert, emerg.
            # It is also possible to configure the loglevel for particular
            # modules, e.g.
            #LogLevel info ssl:warn
    
    
            ErrorLog ${APACHE_LOG_DIR}/error.log
            CustomLog ${APACHE_LOG_DIR}/access.log combined
    
    </VirtualHost>

We will customize the items here add some additional directives.

After this, we need to add two directives. 

The first, called ServerName, establishes the base domain that should match for this virtual host definition.
 
The second, called ServerAlias, defines further names that should match as if they were the base name.
 
The only other thing we need to change for a basic virtual host file is the location of the document root for this domain. 

Save and close the file.

## Add ports if any :

sudo nano /etc/apache2/ports.conf -
````
# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf

Listen 80
Listen 8082

<IfModule ssl_module>
        Listen 443
</IfModule>

<IfModule mod_gnutls.c>
        Listen 443
</IfModule>
````

##Enable the New Virtual Host Files
Now that we have created our virtual host files, we must enable them. Apache includes some tools that allow us to do this.

We can use the a2ensite tool to enable each of our sites like this:
````
sudo a2dissite rf-api.conf rf.conf
sudo a2ensite rf-api.conf rf.conf
````
When you are finished, you need to restart Apache to make these changes take effect:
````
sudo service apache2 restart
````
You will most likely receive a message saying something similar to:
````
 * Restarting web server apache2
 AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.0.1. Set the 'ServerName' directive globally to suppress this message
This is a harmless message that does not affect our site.
````

## Set Up Local Hosts File (Optional)

If you are on a Mac or Linux computer, edit your local file with administrative privileges by typing:
````
sudo nano /etc/hosts
````

The details that you need to add are the public IP address of your server followed by the domain you want to use to reach that server.

For the domains that I used in this guide, assuming that my IP address is 127.0.0.1, I could add the following lines to the bottom of my hosts file:
````
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1       localhost
127.0.0.1       rf.dev
127.0.0.1       rf-api.dev
255.255.255.255 broadcasthost
::1             localhost
````
This will direct any requests for rf-api.dev on our computer and send them to our server at 127.0.0.1. 

Save and close the file.


##Test your Results
Now that you have your virtual hosts configured, you can test your setup easily by going to the domains that you configured in your web browser:

http://rf-api.dev

If this sites work well, you've successfully configured virtual hosts on the same server.


## Set up code base
Updated **commonConfig.ini** to load default controller
````
resources.frontController.defaultControllerName = "login"
resources.frontController.defaultAction = "index"
resources.frontController.defaultModule = "api"
resources.frontController.params.prefixDefaultModule = "1"
````

Integrate all files and functionality relevant to api .

Update Test cases.




