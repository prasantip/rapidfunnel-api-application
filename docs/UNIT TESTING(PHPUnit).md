# UNIT TESTING(PHPUnit)

phpunit belongs to the family of xUnit test libraries. You use these libraries to create automatically executable tests which verify your application's behavior. This is important to ensure, among others, that your changes don't break existing functionality.



## Running Tests Cases:

#### Steps:
```
1. Get your testing database ready:
	a) Import "database-schema-oct-2015.sql"
    b) Import "database-data-oct-2015.sql"
    
2. Give your testing database credentilas in app_unit_test.ini file 
    
3. Open file "tests/app/Bootstrap.php" and set Zend path in line 5:
    ini_set('include_path', '.:/var/lib/ZendFramework/1.12.0/');
    
4. Go to tests directory(rapidfunnel/tests$) and run the following 
 	commands according to your requirements.
```

#### Command for running tests on test suite:

```
php phpunit.phar -c phpunit.xml
```

#### Command for running tests on a class:

```
php phpunit.phar -c phpunit.xml RapidFunnel_Model_AccountContactTest lib/RapidFunnel/Model/AccountContactTest.php
```

#### Command for running tests on a specific method:

```
php phpunit.phar -c phpunit.xml --filter testGetCustomContacts  RapidFunnel_Model_AccountContactTest lib/RapidFunnel/Model/AccountContactTest.php
```

## Generating Code Coverage Reports For Test Cases:

The code coverage report feature requires the Xdebug (2.2.1 or later) and tokenizer extensions. Generating XML reports requires the xmlwriter extension.


##### Open the terminal and install xdebug by using the following command.

```
~$ sudo apt-get install php5-xdebug
```

##### Next open the php ini file on Ubuntu so that we can ensure that xdebug is being seen by the PHP engine.

```
~$ sudo vim /etc/php5/apache2/php.ini
```

##### At the bottom of the file add the following make sure you test the path to the xdebug.so module as it does change. Always look through the php.ini and ensure that any reference to Zend Debugger is also commented out.

```
zend_extension="/usr/lib/php5/20121212/xdebug.so"

xdebug.remote_enable=1

xdebug.remote_autostart=1

xdebug.default_enable=1

xdebug.remote_handler=dbgp

xdebug.remote_mode=req

xdebug.remote_host=localhost

xdebug.remote_port=9000

xdebug.idekey="PHPSTORM"
```

##### Now restart Apache.

```
~$ sudo service apache2 restart
```

Note: We can prevent generating Code Coverage report everytime just by commenting 'logging' in the phpunit.xml file.