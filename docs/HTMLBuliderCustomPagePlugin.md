# HTML Builder 2.0
---
HTML Builder Lite takes a block oriented approach to building web pages and web sites. What this is means is that  HTML Builder Lite provides the user with a selection of pre-designed blocks, such as headers, content sections, forms, footers, etc. which are combined onto a canvas to create fully functional web page.

# Current Version
---
2.0

===============================
Relative changes during Set Up
===============================
- The code base is inside  <ProjectDir>/public/assets/builder_front

Few Config Settings needed that are specified in commonConfig.ini

;HTML BUILDER PLUGIN
builderFront.path = '/assets/builder_front/dist/';
builderFront.elementsPath = '/assets/builder_front/dist/elements/';
builderFront.draftPath = '/assets/builder_front/dist/draft/<account-id>/<resource-id>.site.json';
builderFront.previewPath = '/assets/builder_front/dist/preview/<resource-id>.html';
builderFront.uploadPath = 'assets/builder_front/dist/uploads/<account-id>/<resource-id>/';
builderFront.relativePath = 'uploads/<account-id>/<resource-id>/';
builderFront.allowedImageSize = '10MB';
builderFront.noOfAllowedFile = '1';
builderFront.allowedTypes[] = 'image/jpeg';
builderFront.allowedTypes[] = 'image/gif';
builderFront.allowedTypes[] = 'image/png';
builderFront.allowedTypes[] = 'image/svg';
builderFront.allowedTypes[] = 'application/pdf';

Minimum three queries needs to run on DB to change the schema
- To create the custom page resource type
- To add isPublished field in resource table
- To create accountCustomPageResource table

Few changes for following functionality

 - Preview
 ---------
  - There should be a directory as "preview" inside the pluginPath (public/assets/builder_front/dist/ as in config)
  - Follow the ReadMe files instructions to set it up correctly

 - Uploads
 ---------
 - It is to make file uploads through the plugin wherever needed.
 - There should be a symlink of upload directory inside preview and elements directory so
 run the command ln -s ../uploads uploads inside those directories.
 - Also go through the instructions inside the ReadMe file of upload.

 Upload directory is to store all files uploaded through layout blocks.
 This directory should have write permission.
 The files will be accessed through relative path while creating page from the
 layout files and from the preview page.
 So there should be symlink for this directory inside elements and preview directory.

 Please run below command inside elements and preview directory to create the symlinks

 ln -s ../uploads uploads

 There are more config settings related to file upload inside commonConfig file.

- Drafts
----------
 - There should be a draft directory inside the plugin path.

 All the directories should have write permissions.